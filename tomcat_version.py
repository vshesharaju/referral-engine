import requests
import datetime
url="https://archive.apache.org/dist/tomcat/tomcat-8/"
html_content = requests.get(url)

response = html_content
#response.encoding = 'utf-8'
#sorted_date = list()
key_value ={}

for j in response.iter_lines():
    j = j.decode(encoding="utf-8")


    if "v8." in j:
        j = j.split()
        tomcat_date = j[5] + " " + j[6]
        date_time_obj = datetime.datetime.strptime(tomcat_date, '%Y-%m-%d %H:%M')
        j = j[4].split("\"")
        version = j[1].strip("/")
        key_value[date_time_obj] = version



order_data = sorted(key_value.items())
print(order_data[-1][1].split("v")[1])
