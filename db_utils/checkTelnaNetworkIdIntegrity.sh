#!/bin/bash

echo
echo
echo
echo
if [ -z "$SQL_ARGS_WITH_DATABASE" ]; 
then
echo Using default SQL Connection params
SQL_ARGS_WITH_DATABASE=" -uroot -Dgigsky ";
fi
echo SQL_ARGS_WITH_DATABASE:$SQL_ARGS_WITH_DATABASE



echo
echo
echo
echo
echo =============================================================================
echo ONE-TO-ONE MAPPING CHECK : one telna network Id should be part of single gigsky network id
echo =============================================================================
#for each network id
#	get count of network groups covering carriers of this network

mysql $SQL_ARGS_WITH_DATABASE --execute="select telnaNetworkId from telnaMapTableForNetworkId group by telnaNetworkId" --silent --skip-column-names |
while read telnaNetworkId group
do
	#echo == telnaNetworkId:$telnaNetworkId networkIdCount:$networkIdCount;
	networkIdCount=$(mysql $SQL_ARGS_WITH_DATABASE --execute="select count(distinct networkGroupIDs_groupID) from networkGroups where carriers_carrierID in (select carrierID from carriers where concat(mcc,mnc) in (select mccmnc from telnaMapTableForNetworkId where telnaNetworkId=$telnaNetworkId))" --silent --skip-column-names);
	#echo telnaNetworkId:$telnaNetworkId networkIdCount:$networkIdCount;
	if [ ! -z "$networkIdCount" ] && [ $networkIdCount -gt 1 ]
	then
		echo ERROR: Multiple GigSky NetworkGroups count:$networkIdCount found for TelnaNetworkId:$telnaNetworkId

		mysql $SQL_ARGS_WITH_DATABASE --execute="select networkGroupIDs_groupID from networkGroups where carriers_carrierID in (select carrierID from carriers where concat(mcc,mnc) in (select mccmnc from telnaMapTableForNetworkId where telnaNetworkId=$telnaNetworkId)) group by networkGroupIDs_groupID" --silent --skip-column-names |
		while read networkGroupIDs_groupID group
		do
			echo "  "GigSky NetworkGroupId:$networkGroupIDs_groupID
		done;
	fi
done;



echo
echo
echo
echo
echo =============================================================================
echo COVERAGE 
echo NO-COVERAGE : None of MccMnc of Telna network Id map to associated carrier Ids
echo PARTIAL-COVERAGE : Part of MccMnc of Telna network Id do NOT map to associated carrier Ids
echo FULL-COVERAGE : All MccMnc of Telna network Id map to associated carrier Ids
echo =============================================================================
echo "|" telnaNetworkId "|" coverage "|";
while read telnaNetworkId group
do
	found=false;
	notfound=false;
	notfoundMccMnc="";
	foundMccMnc="";

	while read mccmnc group
	do
		#echo telnaNetworkId:$telnaNetworkId mccmnc:$mccmnc
		#carrierFound=$(mysql $SQL_ARGS_WITH_DATABASE --execute="select 1 from carriers where concat(mcc,mnc) in (select mccmnc from telnaMapTableForNetworkId where telnaNetworkId=$telnaNetworkId)" --silent --skip-column-names);
		carrierFound=$(mysql $SQL_ARGS_WITH_DATABASE --execute="select 1 from carriers where concat(mcc,mnc)=$mccmnc limit 1" --silent --skip-column-names);

		#echo carrierFound:$carrierFound
		if [ -z "$carrierFound" ] || [ "$carrierFound" == "0" ]
		then
			notfoundMccMnc+=$mccmnc" "
			notfound=true;
		else
			foundMccMnc+=$mccmnc" "
			found=true;
		fi
	done  < <(mysql $SQL_ARGS_WITH_DATABASE --execute="select mccmnc from telnaMapTableForNetworkId where telnaNetworkId=$telnaNetworkId" --silent --skip-column-names)


	if [ "$found" == "false" ] 
	then
		echo "|" $telnaNetworkId "|" NO-COVERAGE "|";
	elif [ "$notfound" == "true" ] 
	then
		echo "|" $telnaNetworkId "|" PARTIAL-COVERAGE "|" "Covered:"$foundMccMnc "|" "NOT covered:"$notfoundMccMnc "|";
	else
		echo "|" $telnaNetworkId "|" FULL-COVERAGE "|";
	fi;

done  < <(mysql $SQL_ARGS_WITH_DATABASE --execute="select telnaNetworkId from telnaMapTableForNetworkId group by telnaNetworkId" --silent --skip-column-names)


