#!/bin/bash

echo "usage: $1 [-u mySqlRootUserName] [-p mySqlRootPassword] [-h mySqlServerHostName] [-P mySqlServerPortNum] [-d gigsky/gigStore] [-e dev/live/stage] [-x dev/live/stage password] [-v versionNumber] [-s snapShorDirectory]"

echo "Optional params: [-p mySqlRootPassword] [-h mySqlServerHostName] [-P mySqlServerPortNum]"
echo "Default params: [-e dev] [-x dev1234] [-s ./snapshot]"
