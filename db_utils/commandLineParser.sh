#!/bin/bash

#echo "CommandLinerParser.sh args:"$@

parsed_MySql_HostName=
parsed_MySql_HostName_Present=NO
parsed_MySql_PortNum=
parsed_MySql_PortNum_Present=NO;
parsed_DBUserName=
parsed_DBUserName_Present=NO;
parsed_DBPassword=
parsed_DBPassword_Present=NO;
parsed_DatabaseName=
parsed_DatabaseName_Present=NO;

parsed_DatabaseType=dev
parsed_DatabaseType_Password=dev1234

parsed_Database_Version=
parsed_Database_Version_Present=NO
parsed_Snapshot_Directory=./snapshot
if [[ -z "$parsed_Database_Path" ]]
then
	parsed_Database_Path=./
fi


set -- `getopt u:p:h:P:d:e:x:v:c:s: "$@"`
#[ $# -le 1 ] && exit 1	# getopt failed
while [ $# -gt 0 ]
    do
        case "$1" in
            -u)	parsed_DBUserName="$2"; parsed_DBUserName_Present="YES";shift;;
            -p)	parsed_DBPassword="$2"; parsed_DBPassword_Present="YES";shift;;
            -h)	parsed_MySql_HostName="$2"; parsed_MySql_HostName_Present="YES";shift;;
            -P)	parsed_MySql_PortNum="$2"; parsed_MySql_PortNum_Present="YES";shift;;
            -d)	parsed_DatabaseName="$2"; parsed_DatabaseName_Present="YES";shift;;
            -e)	parsed_DatabaseType="$2"; shift;;
            -x)parsed_DatabaseType_Password="$2"; shift;;
            -v)parsed_Database_Version="$2"; parsed_Database_Version_Present="YES";shift;;
            -c)parsed_Database_Path="$2";shift;;
            -s)parsed_Snapshot_Directory="$2"; shift;;
            --)	shift; break;;
            -*) 
#echo >&2 \
#            "usage: $0 [-u parsed_DBUserName] [-p parsed_DBPassword] [-h mySqlServerHostName] [-P mySqlServerPortNumber] [-d parsed_DatabaseName]"
            exit 1;;
            *)	break;;		# terminate while loop
        esac
        shift
done

export parsed_MySql_HostName
export parsed_MySql_PortNum
export parsed_DBUserName
export parsed_DBPassword
export parsed_DatabaseName
export parsed_DatabaseType
export parsed_DatabaseType_Password
export parsed_Database_Version
export parsed_Snapshot_Directory
export parsed_Database_Path

export parsed_MySql_HostName_Present
export parsed_MySql_PortNum_Present
export parsed_DBUserName_Present
export parsed_DBPassword_Present
export parsed_DatabaseName_Present
export parsed_Database_Version_Present

printParsedContent=0
if [ $printParsedContent == 1 ]
then
echo "MySqlHost Name="$parsed_MySql_HostName 
echo "MySqlHost Name Present="$parsed_MySql_HostName_Present
echo "MySqlHost PortNum="$parsed_MySql_PortNum 
echo "MySqlHost PortNum Present="$parsed_MySql_PortNum_Present
echo "Mysql User Name="$parsed_DBUserName 
echo "Mysql User Name Present="$parsed_DBUserName_Present
echo "Mysql Password="$parsed_DBPassword 
echo "Mysql Password: Present="$parsed_DBPassword_Present
echo "Database Name="$parsed_DatabaseName 
echo "Database Present="$parsed_DatabaseName_Present
echo "Database type="$parsed_DatabaseType
echo "parsed_DatabaseType_Password="$parsed_DatabaseType_Password
echo "parsed_Database_Version="$parsed_Database_Version
echo "parsed_Database_Version_Present="$parsed_Database_Version_Present
echo "parsed_Database_Path="$parsed_Database_Path
echo "Snapshot directory="$parsed_Snapshot_Directory
fi

