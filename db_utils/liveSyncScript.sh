#!/bin/bash

args=$@
DB_UTILS_PATH=$(cd "$(dirname "$0")"; pwd)

. $DB_UTILS_PATH/commandLineParser.sh $args

if [ "$parsed_Database_Version_Present" == "NO" ]
then
    echo "Incorrect Arguments: Provide version number to be upgraded with -v option"
    exit 1;
fi

if [[ -z "$parsed_DatabaseName" ]]
then
    echo "Incorrect Arguments: Provide db name with -d option"
    exit 1;
fi


if [[ -z "$parsed_Database_Path" ]]
then
    echo "Path to DB folder not specified through -c; defaults to current directory"
	parsed_Database_Path=./
fi



if [ "$parsed_DatabaseType" == "live" ]
then
    echo "****For live sync is not supported"
    exit 0
fi

if [[ -z "$DB_TARGET_PATH" ]]
then
	DB_TARGET_PATH="$parsed_Database_Path/$parsed_DatabaseName/"
fi


BASE_ARGUMENTS_TO_MYSQL_COMMAND=$($DB_UTILS_PATH/getMysqlArguments.sh)
BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME=$($DB_UTILS_PATH/getMysqlArguments.sh inc-database)


#Directory from where to read insert data
BASE_PATH=$DB_TARGET_PATH/"db_versions/"$parsed_DatabaseType/$parsed_Database_Version
LIVE_SYNC_DIR=$BASE_PATH/liveSyncFolder/

#check if directories exist
if [ ! -d "$BASE_PATH" ]
then
    echo "$BASE_PATH doesn't exist. Please pass -e[dev/live/stage] -v [version] correctly"
    exit 1
fi

if [ ! -d "$LIVE_SYNC_DIR" ]
    then
    echo "$LIVE_SYNC_DIR doesn't exist. No syncing with live required"
    #exit with zero as it is fine to not have live sync
    exit 0
fi


echo "Live sync called with dir: $LIVE_SYNC_DIR"

# write the insert files
TEMPARORY_SQL_FILE_NAME=temperary_sql_file_name_1112.sql
rm -f $TEMPARORY_SQL_FILE_NAME
echo "set autocommit=0;" >> $TEMPARORY_SQL_FILE_NAME
echo "begin;" >> $TEMPARORY_SQL_FILE_NAME
echo "SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;" >> $TEMPARORY_SQL_FILE_NAME


#Load all data from csv files present in insert_data folder
CSV_FILE_LIST=$(ls $LIVE_SYNC_DIR/*.csv | xargs -n1 basename)
echo "List of csv files from:" $LIVE_SYNC_DIR
echo $CSV_FILE_LIST
echo "" ""
for CSV_FILE in $CSV_FILE_LIST
do
    TABLE_NAME=${CSV_FILE//.csv/}  # remove .csv from csv file
    echo "delete entries from table $TABLE_NAME and inserting new entries from live csv file $CSV_FILE"

    #delete the table
    echo "DELETE FROM $TABLE_NAME;" >> $TEMPARORY_SQL_FILE_NAME

    echo "LOAD DATA LOCAL INFILE '$LIVE_SYNC_DIR/$CSV_FILE' REPLACE INTO TABLE $TABLE_NAME  FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n' IGNORE 1 LINES;" >> $TEMPARORY_SQL_FILE_NAME
done

echo "SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;" >> $TEMPARORY_SQL_FILE_NAME
echo "commit;" >> $TEMPARORY_SQL_FILE_NAME


mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND_WITH_DB_NAME < $TEMPARORY_SQL_FILE_NAME

if [ "$?" != "0" ]; then
    echo "" " "
    echo "****Insert from live failed "
    mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND --execute="drop schema $parsed_DatabaseName"
    rm -f $TEMPARORY_SQL_FILE_NAME
    exit 1
fi

rm -f $TEMPARORY_SQL_FILE_NAME

orphanChildRows=$($DB_UTILS_PATH/checkReferentialIntegrity.sh $args)
if [ "$?" != "0" ] || [ "$orphanChildRows" != "" ]; then
    echo "Referential Integrity Failed, $orphanChildRows, Exiting.."
    mysql $BASE_ARGUMENTS_TO_MYSQL_COMMAND --execute="drop schema $parsed_DatabaseName"
    exit 1
fi

echo "-Successfully synced with live db"
