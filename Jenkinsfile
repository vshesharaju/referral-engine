def buildComponent = "referralengine"
def buildWarName = "gsre.war"
def buildDirectory = "gsreferralengine"
def ecrName = "076544065911.dkr.ecr.us-east-1.amazonaws.com"
def ecrProject = "referralengine-stage"
//def oldImage = "076544065911.dkr.ecr.us-east-1.amazonaws.com/notificationgw-stage:v1.34"
def newImage = ""
def buildMajorVersion = "1.0.0"
//def testAutomationPath = "notificationgatewaytestautomation"

pipeline {

    agent any

    options {
	    //Only keep the 30 most recent builds
	    buildDiscarder(logRotator(numToKeepStr:'30'))
	    disableConcurrentBuilds()
	 }

     stages {

         stage ('Choose your pipeline stages'){

             agent{ 
                label 'slave_v1_1'
                }

			steps {
				script {

					env.DEPLOY_STAGE_CHOICE = input message: 'User input required', ok: 'Continue',
							parameters: [choice(name: 'DEPLOY_STAGE_CHOICE', choices: 'execute\ndo not execute', description: 'Do you want to execute the Deploy stage?')]
					env.TESTING_STAGE_CHOICE = input message: 'User input required', ok: 'Continue',
							parameters: [choice(name: 'TESTING_STAGE_CHOICE', choices: 'execute\ndo not execute', description: 'Do you want to execute the Testing stage?')]
				}
				echo "Deploy stage: ${env.DEPLOY_STAGE_CHOICE}"
				echo "Testing stage: ${env.TESTING_STAGE_CHOICE}"
				
			}
		}

         stage ('Initialization & Preparation') {
             agent{ 
                label 'slave_v1_1'
            }
            
            steps {          	 
					
                    
                    script {

					env.DB_VERSION = input(
                        id: 'userInput', message: 'Enter latest DB version', parameters: [
                        [$class: 'TextParameterDefinition', defaultValue: '', description: 'requesting latest db version', name: 'db_version']])
					
				   } 
                   sh """
                        echo "Starting CI for referral engine"
                        
                    """      
                 }
        }

        stage ('Build process for referral gateway') {
            agent{ 
                label 'slave_v1_1'
            }

            steps{
                
                configFileProvider([configFile(fileId: 'nexus-maven-settings', variable: 'NEXUS_SETTINGS')]) {
                        sh "mvn -f ${buildDirectory}/pom.xml -s $NEXUS_SETTINGS -DskipTests -Drevision=${buildMajorVersion}-${env.BUILD_ID} clean deploy"
                }
                

                sh """
                tar -czvf db_schemas.tar.gz db_schemas 
                tar -czvf db_utils.tar.gz db_utils 
                tar -czvf Tomcat.tar.gz ${WORKSPACE}/configurations/Tomcat 
                tar -czvf db_configurations.tar.gz ${WORKSPACE}/configurations/db_configurations   
                """
            }
            post {

                success {
			      //archive "target/**/*"			      
			      archiveArtifacts artifacts:"${buildDirectory}/target/${buildWarName},${WORKSPACE}/Tomcat.tar.gz,${WORKSPACE}/db_configurations.tar.gz"
                  stash includes: "${buildDirectory}/target/${buildWarName}", name: "${buildComponent}"
                  echo "Maven build successfully completed. Artifacts have been archived"			      
			    }

			    failure {
			    	echo "Maven build failed. Please check logs"
			    }
            }
        }

        /*stage ('Unit Tests') {
                //Placeholder for unit tests stage
        }*/

         /*stage (SonarQube Analysis) {
                //Placeholder for SonarQube stage
        }*/

        stage ('Dockerize referral engine and push to ECR') {

            when {
				expression {
						return env.DEPLOY_STAGE_CHOICE=='execute';
					}
            }
            agent{ 
                label 'slave_v1_1'
             }

            steps{
            unstash "${buildComponent}"
            script{
                newImage = "${ecrName}/${ecrProject}:v1.${env.BUILD_ID}"
            }
            sh """
            python3 tomcat_version.py >> version.txt
            apache_version=\$(cat version.txt)
            pwd
            aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin ${ecrName}/${ecrProject}
            docker build --build-arg VERSION=\${apache_version} -t ${newImage} .
            docker push ${newImage}
            rm -r version.txt
            """
            }

        }

        
        stage ('Deploy using Docker Compose') {

            when {
				expression {
						return env.DEPLOY_STAGE_CHOICE=='execute';
					}
            }
            agent{ 
                label 'testnode_2a'
             }

            steps{
            unstash "${buildComponent}"    
            sh """
            aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin ${ecrName}/${ecrProject}
            IMAGE_ID=${newImage} docker-compose up -d
            sleep 60
            docker exec -i rfeng-db /var/db-install.sh ${env.DB_VERSION}
            echo "done" > db.txt
            docker cp ./db.txt rfeng-service:/db.txt
            sleep 600
            """
            }
        }

        /*
        stage ('QA Automation') {
                when {
				expression {
						return env.TESTING_STAGE_CHOICE=='execute';
					}
            }

            agent{ 
                label 'testnode_2a'
             }
                //Placeholder for QA Automation stage


            steps{
                		        
		    	echo "..............................\nExecuting Test NG based testing\n.............................."
		    	unstash "${buildComponent}" 
	   			configFileProvider([configFile(fileId: 'nexus-maven-settings', variable: 'NexusSettings')]) {
		   		
		    		catchError {
		    			sh "mvn -f ${testAutomationPath}/pom.xml -DSUITE_XML_FILES='suites/AllSuites.xml' -DCONFIG_FILE='local_mac/config_keyvalue.txt' clean test"
		    		}
		
				}
					   
										
		
		    	}		    	 
 
		    
			post {
				always {
                    
                     publishHTML([allowMissing: true, alwaysLinkToLastBuild: true, keepAll: true, reportDir: 'notificationgatewaytestautomation/gsnotificationgateway/target/surefire-reports/html/', reportFiles: 'index.html', reportName: 'HTML Report', reportTitles: ''])
                     step([$class: 'Publisher', reportFilenamePattern: 'notificationgatewaytestautomation/gsnotificationgateway/target/surefire-reports/*.xml'])
                     }

                success {		
                	echo "TestNG Execution Successful"
			    }

			    failure {
			    	echo "Ops! Maven build failed for Test automation"
			        }
			    }	
            } */
        }
        


     post {


		cleanup {
  
            echo "cleaning the workspace for this branch to avoid leaving unnecessary directories. It is understood that it will impact the build performance negatively in the next run on this branch"
            cleanWs()
            
    }

	} 
}
