package com.gigsky.healthcheck;

import com.gigsky.common.CommonUtils;
import com.gigsky.database.bean.HealthCheckEventDBBean;
import com.gigsky.database.dao.HealthCheckEventDao;
import com.gigsky.scheduler.ReferralQueueMonitorScheduler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by prsubbareddy on 08/01/18.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
public class ReferralQueueMonitorTest {

    @Autowired
    ReferralQueueMonitorScheduler referralQueueMonitorScheduler;

    @Autowired
    HealthCheckEventDao healthCheckEventDao;

    @Test
    // No entry in database
    public void referralQueueMonitorTest1() {

        HealthCheckEventDBBean healthCheckEventDBBean = healthCheckEventDao.getHealthCheckEvent();
        Assert.assertTrue(healthCheckEventDBBean == null);
    }

    @Test
    // Create Entry in database with received event time with in 10 MINUTES range
    // Expected result: queue should be up
    public void referralQueueMonitorTest2() {

        HealthCheckEventDBBean healthCheckEventDBBean = new HealthCheckEventDBBean();
        healthCheckEventDBBean.setEventType("HealthCheckEvent");
        healthCheckEventDBBean.setEventMetaData("Data");
        healthCheckEventDBBean.setEventReceivedTime(CommonUtils.getCurrentTimestamp());

        healthCheckEventDao.addHealthCheckEventInfo(healthCheckEventDBBean);

        referralQueueMonitorScheduler.processReferralQueueMonitor();

        healthCheckEventDBBean = healthCheckEventDao.getHealthCheckEvent();
        boolean isQueueUp = (healthCheckEventDBBean.getIsQueueUp() == 1);
        Assert.assertTrue(isQueueUp);

        deleteHealthCheckEntry(healthCheckEventDBBean);
        healthCheckEventDBBean = healthCheckEventDao.getHealthCheckEvent();
        Assert.assertTrue(healthCheckEventDBBean == null);
    }

    @Test
    // Entry in database with received event time greater than 10 MINUTES range
    // Expected result: queue should be down
    public void referralQueueMonitorTest3() {

        HealthCheckEventDBBean healthCheckEventDBBean = new HealthCheckEventDBBean();
        healthCheckEventDBBean.setEventType("HealthCheckEvent");
        healthCheckEventDBBean.setEventMetaData("Data");

        // Setting current time stamp with 11 minutes old
        long delayTimeInMillis = CommonUtils.getCurrentTimestamp().getTime() - 11 * 60 *1000;

        healthCheckEventDBBean.setEventReceivedTime(CommonUtils.convertMilliSecondsToTimeStamp(delayTimeInMillis));

        healthCheckEventDao.addHealthCheckEventInfo(healthCheckEventDBBean);

        referralQueueMonitorScheduler.processReferralQueueMonitor();

        healthCheckEventDBBean = healthCheckEventDao.getHealthCheckEvent();
        boolean isQueueUp = (healthCheckEventDBBean.getIsQueueUp() == 0);
        Assert.assertTrue(isQueueUp);
    }

    @Test
    // update Entry with received event time with 10 MINUTES range and
    // Queue should be up
    public void referralQueueMonitorTest4() {

        HealthCheckEventDBBean healthCheckEventDBBean = healthCheckEventDao.getHealthCheckEvent();
        healthCheckEventDBBean.setEventType("HealthCheckEvent");
        healthCheckEventDBBean.setEventMetaData("Data");
        healthCheckEventDBBean.setEventReceivedTime(CommonUtils.getCurrentTimestamp());

        healthCheckEventDao.updateHealthCheckEventInfo(healthCheckEventDBBean);

        referralQueueMonitorScheduler.processReferralQueueMonitor();

        healthCheckEventDBBean = healthCheckEventDao.getHealthCheckEvent();
        boolean isQueueUp = (healthCheckEventDBBean.getIsQueueUp() == 1);
        Assert.assertTrue(isQueueUp);

        deleteHealthCheckEntry(healthCheckEventDBBean);
        healthCheckEventDBBean = healthCheckEventDao.getHealthCheckEvent();
        Assert.assertTrue(healthCheckEventDBBean == null);
    }


    private void deleteHealthCheckEntry(HealthCheckEventDBBean healthCheckEventDBBean)
    {
        healthCheckEventDao.deleteHealthCheckEvent(healthCheckEventDBBean);
    }
}
