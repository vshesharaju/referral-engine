package com.gigsky.loadtest;

import com.gigsky.messaging.MessageSender;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * Created by prsubbareddy on 14/12/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
@Ignore
public class IccidPromotion {

    @Autowired
    MessageSender messageSender;

    String inputFilePath = "/Users/prsubbareddy/Desktop/events.txt";

    @Test
    public void loadPromotionEvents() {

        try {
            BufferedReader br = new BufferedReader(new FileReader(inputFilePath));
            String line;
            while ((line = br.readLine()) != null) {
                // process the line.

                messageSender.sendMessages(line);

                System.out.printf("Line:" +line);
            }
        } catch (Exception e){
            System.out.printf("Error:" + e);
        }

    }

}
