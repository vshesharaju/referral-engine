package com.gigsky.backend;

import com.gigsky.backend.credit.SupportedCurrenciesInterface;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by pradeepragav on 29/03/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
@Ignore
public class SupportedCurrenciesImplTest {

    @Autowired
    SupportedCurrenciesInterface supportedCurrenciesInterface;

    @Test
    public void updateSupportedCurrenciesTest() throws Exception{

        supportedCurrenciesInterface.updateSupportedCurrencies(1);
    }
}
