package com.gigsky.backend;

import com.gigsky.backend.credit.CreditManagementInterface;
import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.TenantDBBean;
import com.gigsky.database.dao.TenantMockDao;
import com.gigsky.rest.bean.Credit;
import com.gigsky.util.GSAddCreditUtil;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by pradeepragav on 15/02/17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
@Ignore
public class CreditManagementServiceTest {

    @Autowired
    private
    CreditManagementInterface creditManagementInterface;

    @Autowired
    private
    GSAddCreditUtil addCreditUtil;

    @Autowired
    private TenantMockDao tenantMockDao;

    private ArrayList<TenantDBBean> tenants;
    private int tenantLength;
    private int testTenantId;

    @Before
    public void init() throws Exception{
        //adding tenants
        tenants = tenantMockDao.addTenants();
        tenantLength = tenants.size();
        testTenantId = tenants.get(0).getId();

    }

    @After
    public void tearDown() throws Exception{
        //delete tenants
        tenantMockDao.deleteTenants(tenants);
    }

    @Test
    public void addGigSkyCreditTest () throws Exception{

        Credit credit = new Credit();
        credit.setDescription("Referral credit add");
        credit.setCurrency("USD");
        credit.setCreditBalance(10);
        credit.setCreditType(Configurations.CreditType.REF_ADV);
        Timestamp creditExpiry = addCreditUtil.getReferralCreditExpiry();
        credit.setExpiry(creditExpiry);

        Credit creditResponse = creditManagementInterface.addCredit(1245, credit, testTenantId);

        Assert.assertNotNull(creditResponse);
    }

    @Test
    public void getCreditDetailsTest () throws Exception{

        Credit creditResponse = creditManagementInterface.getCreditDetails(1247, 10, testTenantId);

        Assert.assertNotNull(creditResponse);
    }

    @Test
    public void deductGigskyCreditTest() throws Exception{

        Credit creditResponse = creditManagementInterface.deductCredit(1246 ,6, testTenantId);
        Assert.assertNotNull(creditResponse);

    }

    @Test
    public void getCreditDetailsOfCustomer() throws Exception{

        int gsCustomerId = 1246;

        Credit creditResponse = creditManagementInterface.getCreditDetailsOfCustomer(gsCustomerId, testTenantId);
        Assert.assertNotNull(creditResponse);
    }

    @Test
    public void addCreditToMaxCredit() throws Exception{

        Credit credit = new Credit();
        credit.setDescription("Referral credit add");
        credit.setCurrency("USD");
        credit.setCreditBalance(1000);
        credit.setCreditType(Configurations.CreditType.REF_ADV);
        Timestamp creditExpiry = getCreditExpiry(5);
        credit.setExpiry(creditExpiry);

        Credit creditResponseOfAddCredit = new Credit();
        while (creditResponseOfAddCredit.getErrorInt()==0){

            creditResponseOfAddCredit = creditManagementInterface.addCredit(1245, credit, testTenantId);
        }

        Credit creditResponseOfCreditBalance = creditManagementInterface.getCreditDetailsOfCustomer(1245, testTenantId);
        System.out.println(creditResponseOfCreditBalance.getCreditBalance());


    }

    private Timestamp getCreditExpiry(int expiryInDays) throws ParseException {

        //Get current date
        Date currentDate = CommonUtils.getCurrentDate();

        //Get october 1st date
        Calendar date = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
        date.set(Calendar.MONTH, 9);
        date.set(Calendar.DAY_OF_MONTH, 1);
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        //Get java.sql.Date
        Date octoberDate = new java.sql.Date(date.getTimeInMillis());
        currentDate = octoberDate;

        //Set month as December
        Calendar expiryDate = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
        expiryDate.set(Calendar.MONTH, 11);
        expiryDate.set(Calendar.DAY_OF_MONTH, 31);
        expiryDate.set(Calendar.HOUR_OF_DAY, 24);
        expiryDate.set(Calendar.MINUTE, 0);
        expiryDate.set(Calendar.SECOND, 0);
        expiryDate.set(Calendar.MILLISECOND, 0);


        //Check whether it is before october 1st
        if(octoberDate.compareTo(currentDate) <= 0)
        {
            //Set year
            expiryDate.set(Calendar.YEAR,expiryDate.get(Calendar.YEAR) + 1);
        }
        return CommonUtils.convertMilliSecondsToTimeStampInUTC(expiryDate.getTimeInMillis());
    }
}
