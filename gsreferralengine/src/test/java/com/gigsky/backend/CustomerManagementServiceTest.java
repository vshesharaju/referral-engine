package com.gigsky.backend;

import com.gigsky.backend.customer.CustomerManagementInterface;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.ReferralEventDBBean;
import com.gigsky.database.dao.ReferralEventDao;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.rest.bean.Customer;
import com.gigsky.util.GSTaskUtil;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by pradeepragav on 14/02/17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
@Ignore
public class CustomerManagementServiceTest {

    @Autowired
    CustomerManagementInterface customerManagementInterface;

    @Autowired
    ReferralEventDao referralEventDao;

    @Autowired
    GSTaskUtil gsTaskUtil;

    //CustomerID : 1247
    //token : 5ec38a6281264d26abaae6874e34f9f4
    @Test
    public void getCustomerDetailsTest() throws Exception{

        long customerId = 1251;
        String token = "80f424e3c11d461799c8f28a8f353aa1";
        Customer customer = customerManagementInterface.getCustomerDetails(1251,token, 1);
        Assert.assertNotNull(customer);
    }

    @Test
    public void checkDeSerialize() throws Exception{

        List referralEventDBBeanList = referralEventDao.getReferralEventsByStatus(Configurations.StatusType.STATUS_COMPLETE, 1);

        ReferralEventDBBean referralEventDBBean = (ReferralEventDBBean) referralEventDBBeanList.get(0);
        EventMessageData eventMessageData = GSTaskUtil.getMessageData(referralEventDBBean.getEventData(), referralEventDBBean.getEventType());

    }

}
