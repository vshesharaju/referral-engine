package com.gigsky.backend;

import com.gigsky.backend.auth.AuthManagementInterface;
import com.gigsky.common.Configurations;
import com.gigsky.gscommon.util.encryption.EncryptionFactory;
import com.gigsky.gscommon.util.encryption.GigSkyEncryptor;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by pradeepragav on 14/02/17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
public class AuthManagementServiceTest {

    @Autowired
    AuthManagementInterface authManagementInterface;

    @Autowired
    private EncryptionFactory encryptionFactory;

    @Test
    public void validateCusHavingPermissionTest() throws Exception{

       boolean isValidToken1 = authManagementInterface.validateAdminPermission("bbe54d3e117049f0828f72c24d84c7e9", 1);
       boolean isValidToken2 = authManagementInterface.validateAdminPermission("bbe54d3e117049f0828f72c24d84c7e9", 12);

        Assert.assertTrue(isValidToken1);
        Assert.assertFalse(isValidToken2);
    }


    @Test
    public void validateCusNotHavingPermissionTest() throws Exception{

        boolean isValidToken1 = authManagementInterface.validateAdminPermission("d15af80d7d3f401cbf8aa007b28ac7e5", 1);
        boolean isValidToken2 = authManagementInterface.validateAdminPermission("d15af80d7d3f401cbf8aa007b28ac7e5", 12);

        Assert.assertFalse(isValidToken1);
        Assert.assertFalse(isValidToken2);
    }

    @Test
    public void encryptStringsTest() throws Exception{

        GigSkyEncryptor encryptor = encryptionFactory.getEncryptor(Configurations.LATEST_ENCRYPTION_VERSION);

        String emailId = "smaragal@gigsky.com";
        String password = "Sowjanya1234";
        String example1 = "guest";
//        String example = "oUjgOXQiltedhpa/7fbz3fIXOoq7wMZKumeuOg==";

        String encryptedEId = encryptor.encryptString(emailId);
        String encryptedPwd = encryptor.encryptString(password);
        String excryptedExample1 = encryptor.encryptString(example1);
//        String decrypterExample = encryptor.decryptString(example);

        System.out.print(encryptedEId + " " + encryptedPwd);
    }

    @Test
    public void validateSimId() throws Exception
    {
        boolean isValidSim = authManagementInterface.validateCustomerSimId(null, 1, 1, "12345678910");
        Assert.assertTrue(isValidSim);
    }

}
