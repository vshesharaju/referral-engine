package com.gigsky.database.dao;


import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.TenantDBBean;
import com.gigsky.util.HttpClientUtil;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;

/**
 * Created by pradeepragav on 04/01/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
public class CustomerDaoTest {

    @Autowired
    private CustomerDao customerDao;

    private int testCustomerId;
    private long testGsCustomerId;
    private ArrayList<TenantDBBean> tenants;
    private int tenantLength;
    private int testTenantId;

    @Autowired
    private TenantMockDao tenantMockDao;


//    @Before
//    public void init() throws Exception{
//        //adding tenants
//        tenants = tenantMockDao.addTenants();
//        tenantLength = tenants.size();
//        testTenantId = tenants.get(0).getId();
//
//        //Create customerbean for testing
//        CustomerDBBean customerDBBean = new CustomerDBBean();
//
//        long gsCustomerId = 112;
//        //Assign values to the properties of customerbean
//        customerDBBean.setGsCustomerId(gsCustomerId);
//        customerDBBean.setEmailId("praghav@gigsky.com");
//        customerDBBean.setLanguage("en");
//        customerDBBean.setTenantId(testTenantId);
//
//        //Create customer through customerDao
//        customerDao.createCustomerInfo(customerDBBean);
//        testCustomerId = customerDBBean.getId();
//        testGsCustomerId = customerDBBean.getGsCustomerId();
//        Assert.assertNotNull(testCustomerId);
//    }

//    @After
//    public void tearDown() throws Exception{
//        //Delete the testCustomer which was created in @Before
//        customerDao.deleteCustomerInfoByCustomerId(testCustomerId, testTenantId);
//
//        //delete tenants
//        tenantMockDao.deleteTenants(tenants);
//    }

    @Test
    public void testCreateCustomerInfo() throws Exception{
        CustomerDBBean customerDBBean = createCustomerData(114, tenants.get((tenantLength -1)).getId());
        customerDao.createCustomerInfo(customerDBBean);
        Assert.assertNotNull(customerDBBean);
        Assert.assertNotNull(customerDBBean.getId());
        Assert.assertEquals(customerDBBean.getTenantId(), tenants.get((tenantLength -1)).getId());
        customerDao.deleteCustomerInfo(customerDBBean);
    }

    @Test
    public void testGetCustomerId(){
        int customerId = customerDao.getCustomerIdForBackendCustomerId(testGsCustomerId, testTenantId);
        Assert.assertEquals(customerId,testCustomerId);
    }

    @Test
    public void testGetCustomerInfo() throws Exception{
        CustomerDBBean customerDBBean = customerDao.getCustomerInfo(testCustomerId, 1);
        Assert.assertNull(customerDBBean);
    }

    @Test
    public void testGetCustomerInfoByGsCustomerId() throws Exception{
        CustomerDBBean customerDBBean1 = customerDao.getCustomerInfoByGsCustomerId(testGsCustomerId, testTenantId);
        Assert.assertNotNull(customerDBBean1);

        CustomerDBBean customerDBBean2 = customerDao.getCustomerInfoByGsCustomerId(testGsCustomerId, tenants.get(1).getId());
        Assert.assertNull(customerDBBean2);
    }

    @Test
    public void updateCustomerInfo() throws Exception{
        //Get customer by testCustomerId
        CustomerDBBean customerDBBean = customerDao.getCustomerInfo(testCustomerId, testTenantId);

        //Change a property of customer
        String emailBeforeUpdating = customerDBBean.getEmailId();
        String emailAfterUpdating = "preciousandsacred@gmail.com";
        customerDBBean.setEmailId(emailAfterUpdating);

        //Update the customer using customerDao
        customerDao.updateCustomerInfo(customerDBBean);
        Assert.assertNotNull(customerDBBean.getId());

        //Check whether the property is updated
        customerDBBean = customerDao.getCustomerInfo(testCustomerId, testTenantId);
        emailAfterUpdating = customerDBBean.getEmailId();
        Assert.assertNotEquals(emailBeforeUpdating,emailAfterUpdating);
    }

    @Test
    public void testRestClient() throws Exception{

        HttpClientUtil httpClientUtil =  new HttpClientUtil();

        HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                httpClientUtil.sendHttpRequest("GET", "http://localhost:8080/api/v4/countries", null, 1000);

        System.out.println(gsRestHttpResponse.toString());

        Assert.assertNotNull(gsRestHttpResponse);
    }

    @Test
    public void testDuplicateEntries() throws Exception {

        CustomerDBBean customerDBBean = new CustomerDBBean();
        customerDBBean.setEmailId("pragav94@gmail.com");
        customerDBBean.setFirstName("Pradeep3");
        customerDBBean.setGsCustomerId(1246L);
        customerDBBean.setTenantId(1);

        try
        {

//            customerDBBean.setId(5);

            int customerInfo = customerDao.createCustomerInfo(customerDBBean);

//            customerDao.updateCustomerInfo(customerDBBean);
        }
        catch (Exception e)
        {
//            if(e instanceof ConstraintViolationException)
            {
                System.out.println(e.getMessage());
//                CustomerDBBean customerInfoByGsCustomerId = customerDao.getCustomerInfoByGsCustomerId(customerDBBean.getGsCustomerId(), customerDBBean.getTenantId());
//                customerDBBean.setId(customerInfoByGsCustomerId.getId());
//                customerDao.updateCustomerInfo(customerDBBean);
            }
        }

    }

    public static CustomerDBBean createCustomerData(int gsCustomerId, int tenantId)
    {
        CustomerDBBean customerDBBean = new CustomerDBBean();
        //Assign values to the properties of customerbean
        customerDBBean.setGsCustomerId(new Long(gsCustomerId));
        customerDBBean.setEmailId("pragav94@gigsky.com");
        customerDBBean.setLanguage("en");
        customerDBBean.setTenantId(tenantId);

        return customerDBBean;
    }

}
