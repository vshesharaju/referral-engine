package com.gigsky.database.dao;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.PromoCountryDBBean;
import com.gigsky.database.bean.PromoCreditDBBean;
import com.gigsky.database.bean.PromotionDBBean;
import com.gigsky.database.bean.TenantDBBean;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by shwethars on 09/01/19.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
public class PromotionDaoTest {

    @Autowired
    private PromotionDao promotionDao;

    @Autowired
    private TenantMockDao tenantMockDao;

    private ArrayList<TenantDBBean> tenants;
    private int testTenantId;

    @Before
    public void init() throws Exception{
        //adding tenants
        tenants = tenantMockDao.addTenants();
        testTenantId = tenants.get(0).getId();

    }

    @After
    public void tearDown() throws Exception{

        //delete tenants
        tenantMockDao.deleteTenants(tenants);
    }

    /**
     * verify add promotion with country as ALL, single promo credit and promo enabled
     */
    @Test
    public void testAddPromotion1() throws Exception{
        PromotionDBBean promotionDBBean = CreatePromotion("ABC12", testTenantId, 0);

        List<PromoCountryDBBean> promoCountryDBBeans = new ArrayList<PromoCountryDBBean>();
        promoCountryDBBeans.add(getPromoCountries("ALL"));

        List<PromoCreditDBBean> promoCreditDBBeans = new ArrayList<PromoCreditDBBean>();
        promoCreditDBBeans.add(getPromoCreditDBBean("USD", new Float(100)));

        int promoId = promotionDao.addPromotion(promotionDBBean, promoCountryDBBeans, promoCreditDBBeans);

        Assert.assertEquals(promoId, promotionDBBean.getId());
        Assert.assertNotNull(promotionDBBean.getId());
        Assert.assertEquals(promotionDBBean.getPromoCode(), "ABC12");
        //Delete the entry
        promotionDao.deletePromotion(promotionDBBean);
    }

    /**
     * verify add promotion with country - US, CA, multiple promo credit and promo disabled
     */
    @Test
    public void testAddPromotion2() throws Exception{
        PromotionDBBean promotionDBBean = CreatePromotion("ABC123", testTenantId, 1);

        List<PromoCountryDBBean> promoCountryDBBeans = new ArrayList<PromoCountryDBBean>();
        promoCountryDBBeans.add(getPromoCountries("US"));
        promoCountryDBBeans.add(getPromoCountries("AL"));

        List<PromoCreditDBBean> promoCreditDBBeans = new ArrayList<PromoCreditDBBean>();
        promoCreditDBBeans.add(getPromoCreditDBBean("USD", new Float(300)));
        promoCreditDBBeans.add(getPromoCreditDBBean("JPY", new Float(1200)));
        promoCreditDBBeans.add(getPromoCreditDBBean("EUR", new Float(200)));
        promoCreditDBBeans.add(getPromoCreditDBBean("GBP", new Float(250)));

        int promoId = promotionDao.addPromotion(promotionDBBean, promoCountryDBBeans, promoCreditDBBeans);


        Assert.assertEquals(promoId, promotionDBBean.getId());
        Assert.assertNotNull(promotionDBBean.getId());
        Assert.assertEquals(promotionDBBean.getPromoCode(), "ABC123");
        //Delete the entry
        promotionDao.deletePromotion(promotionDBBean);
    }

    /**
     * verify get promotion by id
     */
    @Test
    public void testGetPromotion() throws Exception{
        PromotionDBBean promotionDBBean = CreatePromotion("ABC12", testTenantId, 1);

        List<PromoCountryDBBean> promoCountryDBBeans = new ArrayList<PromoCountryDBBean>();
        promoCountryDBBeans.add(getPromoCountries("ALL"));

        List<PromoCreditDBBean> promoCreditDBBeans = new ArrayList<PromoCreditDBBean>();
        promoCreditDBBeans.add(getPromoCreditDBBean("USD", new Float(100)));

        int promoId = promotionDao.addPromotion(promotionDBBean, promoCountryDBBeans, promoCreditDBBeans);

        PromotionDBBean promotionDBBean1 = promotionDao.getPromotion(promoId, testTenantId);

        Assert.assertEquals(promotionDBBean1.getPromoCode(), promotionDBBean.getPromoCode());
        Assert.assertEquals(promotionDBBean1.getPromoEnabled(), promotionDBBean.getPromoEnabled());

        //Delete the entry
        promotionDao.deletePromotion(promotionDBBean);
    }


    /**
     * verify update promotion
     */
    @Test
    public void testUpdatePromotion1() throws Exception{
        PromotionDBBean promotionBeforeUpdate = CreatePromotion("ABC12", testTenantId, 1);

        List<PromoCountryDBBean> promoCountryDBBeans = new ArrayList<PromoCountryDBBean>();
        promoCountryDBBeans.add(getPromoCountries("ALL"));

        List<PromoCreditDBBean> promoCreditDBBeans = new ArrayList<PromoCreditDBBean>();
        promoCreditDBBeans.add(getPromoCreditDBBean("USD", new Float(100)));

        int promoId = promotionDao.addPromotion(promotionBeforeUpdate, promoCountryDBBeans, promoCreditDBBeans);

        PromotionDBBean promotionForUpdate = promotionDao.getPromotion(promoId, testTenantId);

        promotionForUpdate.setName("Promotion 2");
        promotionForUpdate.setPromoCode("ABC123");

        promotionDao.updatePromotion(promotionForUpdate);

        PromotionDBBean promotionAfterUpdate = promotionDao.getPromotion(promoId, testTenantId);


        Assert.assertNotEquals(promotionBeforeUpdate.getPromoCode(), promotionAfterUpdate.getPromoCode());
        Assert.assertNotEquals(promotionBeforeUpdate.getName(), promotionAfterUpdate.getName());
        Assert.assertEquals(promotionBeforeUpdate.getPromoEnabled(), promotionAfterUpdate.getPromoEnabled());

        //Delete the entry
        promotionDao.deletePromotion(promotionAfterUpdate);
    }

    /**
     * verify update promotion - promo countries and promo credits
     */
    @Test
    public void testUpdatePromotion2() throws Exception{
        PromotionDBBean promotionBeforeUpdate = CreatePromotion("ABC12", testTenantId, 1);

        List<PromoCountryDBBean> promoCountryBeforeUpdate = new ArrayList<PromoCountryDBBean>();
        promoCountryBeforeUpdate.add(getPromoCountries("ALL"));

        List<PromoCreditDBBean> promoCreditBeforeUpdate = new ArrayList<PromoCreditDBBean>();
        promoCreditBeforeUpdate.add(getPromoCreditDBBean("USD", new Float(100)));

        int promoId = promotionDao.addPromotion(promotionBeforeUpdate, promoCountryBeforeUpdate, promoCreditBeforeUpdate);

        PromotionDBBean promotionForUpdate = promotionDao.getPromotion(promoId, testTenantId);

        promotionForUpdate.setName("Promotion 2");
        promotionForUpdate.setPromoCode("ABC123");

        List<PromoCountryDBBean> promoCountryForUpdate = new ArrayList<PromoCountryDBBean>();
        promoCountryForUpdate.add(getPromoCountries("US"));
        promoCountryForUpdate.add(getPromoCountries("AL"));

        List<PromoCreditDBBean> promoCreditForUpdate = new ArrayList<PromoCreditDBBean>();
        promoCreditForUpdate.add(getPromoCreditDBBean("EUR", new Float(100)));
        promoCreditForUpdate.add(getPromoCreditDBBean("GBP", new Float(100)));

        promotionDao.updatePromotion(promotionForUpdate, promoCountryForUpdate, promoCreditForUpdate);

        Map<String, Object> promotionAfterUpdate = promotionDao.getPromotionDetails(promoId, testTenantId);

        PromotionDBBean activePromotionDBBean = (PromotionDBBean) promotionAfterUpdate.get(Configurations.PROMOTION_BEAN);

        List<PromoCountryDBBean> promoCountryAfterUpdate = (List<PromoCountryDBBean>) promotionAfterUpdate.get(Configurations.PROMO_COUNTRY_BEAN_LIST);

        List<PromoCreditDBBean> promoCreditAfterUpdate = (List<PromoCreditDBBean>) promotionAfterUpdate.get(Configurations.PROMO_CREDIT_BEAN);


        Assert.assertNotEquals(promoCountryBeforeUpdate.size(), promoCountryForUpdate.size());
        Assert.assertEquals(promoCountryForUpdate.size(), promoCountryAfterUpdate.size());

        Assert.assertNotEquals(promoCreditBeforeUpdate.size(), promoCreditForUpdate.size());
      //  Assert.assertEquals(promoCreditForUpdate.size(), promoCreditAfterUpdate.size());


        //Delete the entry
        promotionDao.deletePromotion(promotionForUpdate);
    }


    /**
     * verify delete promotion
     */
    @Test
    public void testDeletePromotion() throws Exception{
        PromotionDBBean promotionDBBean = CreatePromotion("ABC123", testTenantId, 1);

        List<PromoCountryDBBean> promoCountryDBBeans = new ArrayList<PromoCountryDBBean>();
        promoCountryDBBeans.add(getPromoCountries("ALL"));

        List<PromoCreditDBBean> promoCreditDBBeans = new ArrayList<PromoCreditDBBean>();
        promoCreditDBBeans.add(getPromoCreditDBBean("USD", new Float(100)));

        int promoId = promotionDao.addPromotion(promotionDBBean, promoCountryDBBeans, promoCreditDBBeans);

        PromotionDBBean promotionBeforeDelete = promotionDao.getPromotion(promoId, testTenantId);

        Assert.assertEquals(promotionBeforeDelete.getPromoCode(), promotionDBBean.getPromoCode());

        //Delete the entry
        promotionDao.deletePromotion(promotionDBBean);

        PromotionDBBean promotionAfterDelete = promotionDao.getPromotion(promoId, testTenantId);

        Assert.assertNull(promotionAfterDelete);
    }

    private PromoCountryDBBean getPromoCountries(String country){
        PromoCountryDBBean promoCountryDBBean = new PromoCountryDBBean();
        promoCountryDBBean.setCountry(country);
        return promoCountryDBBean;
    }

    private PromoCreditDBBean getPromoCreditDBBean(String currency, Float amt){

        PromoCreditDBBean promoCreditDBBean = new PromoCreditDBBean();
        promoCreditDBBean.setCurrency(currency);
        promoCreditDBBean.setCreditAmount(amt);

        return promoCreditDBBean;

    } //getPromoCreditDBBean("USD", new Float(100)

    //Create promotion
    public static PromotionDBBean CreatePromotion(String promoCode,int tenantId, int promoEnabled){
        PromotionDBBean promotionDBBean = new PromotionDBBean();
        promotionDBBean.setName("Promotion 1");
        promotionDBBean.setId(1);
        promotionDBBean.setTenantId(tenantId);
        promotionDBBean.setPromoEnabled((byte) promoEnabled);
        promotionDBBean.setPromoCode(promoCode);
        return promotionDBBean;
    }
}
