package com.gigsky.database.dao;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by pradeepragav on 05/04/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
public class CountryToCurrencyMappingDaoTest {

    @Autowired
    CountryToCurrencyMapDao countryToCurrencyMapDao;

    @Test
    public void getCurrencyTest() throws Exception{

        Assert.assertNotNull(countryToCurrencyMapDao.getCurrency("US"));
    }
}
