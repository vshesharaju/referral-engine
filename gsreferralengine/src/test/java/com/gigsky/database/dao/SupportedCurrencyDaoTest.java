package com.gigsky.database.dao;

import com.gigsky.database.bean.TenantDBBean;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;

/**
 * Created by pradeepragav on 05/04/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
@Ignore
public class SupportedCurrencyDaoTest {

    @Autowired
    SupportedCurrenciesDao supportedCurrenciesDao;

    @Autowired
    private TenantMockDao tenantMockDao;

    private ArrayList<TenantDBBean> tenants;
    private int tenantLength;
    private int testTenantId;

    @Before
    public void init() throws Exception{
        //adding tenants
        tenants = tenantMockDao.addTenants();
        tenantLength = tenants.size();
        testTenantId = tenants.get(0).getId();

    }

    @After
    public void tearDown() throws Exception{
        //delete tenants
        tenantMockDao.deleteTenants(tenants);
    }

    @Test
    public void getSupportedCurrencyDetailsTest() throws Exception{
        Assert.assertNotNull(supportedCurrenciesDao.getSupportedCurrencyDetails("USD", testTenantId));
    }

    @Test
    public void getSupportedCurrenciesTest() throws Exception{
       Assert.assertNotNull(supportedCurrenciesDao.getSupportedCurrencies(testTenantId));
    }
}
