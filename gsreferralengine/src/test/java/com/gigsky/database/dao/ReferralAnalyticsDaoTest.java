package com.gigsky.database.dao;

import com.gigsky.database.bean.ReferralAnalyticsDBBean;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by pradeepragav on 08/01/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
public class ReferralAnalyticsDaoTest {

    @Autowired
    ReferralAnalyticsDao testReferralAnalyticsDao;

    private int testReferralAnalyticsId;

    @Before
    public void init() throws Exception{
        //Create referralAnalytics bean and insert into table
        ReferralAnalyticsDBBean referralAnalyticsDBBean = createReferralAnalytics();
        testReferralAnalyticsId = testReferralAnalyticsDao.addReferralAnalytics(referralAnalyticsDBBean);
        //Validate insertion
        Assert.assertNotNull(testReferralAnalyticsId);
    }

    @After
    public void tearDown() throws Exception{
        ReferralAnalyticsDBBean referralAnalyticsDBBean = testReferralAnalyticsDao.getReferralAnalytics(testReferralAnalyticsId);
        testReferralAnalyticsDao.deleteReferralAnalytics(referralAnalyticsDBBean);
    }

    @Test
    public void testAddReferralAnalytics() throws Exception{
        //Tested in @before and @after
    }

    @Test
    public void testUpdateReferralAnalytics() throws Exception{
        ReferralAnalyticsDBBean referralAnalyticsDBBean = testReferralAnalyticsDao.getReferralAnalytics(testReferralAnalyticsId);
        referralAnalyticsDBBean.setPlatform("WEB-APP");
        testReferralAnalyticsDao.updateReferralAnalytics(referralAnalyticsDBBean);
        referralAnalyticsDBBean = testReferralAnalyticsDao.getReferralAnalytics(testReferralAnalyticsId);
        Assert.assertEquals("WEB-APP",referralAnalyticsDBBean.getPlatform());
    }

    @Test
    public void testGetReferralAnalytics() throws Exception{
        //Tested in @before and @after
    }

    @Test
    public void testDeleteReferralAnalytics() throws Exception{
        //Tested in @before and @after
    }

    public ReferralAnalyticsDBBean createReferralAnalytics(){
        ReferralAnalyticsDBBean referralAnalyticsDBBean = new ReferralAnalyticsDBBean();
        referralAnalyticsDBBean.setPlatform("MOBILE-APP");
        referralAnalyticsDBBean.setLanguage("EN");
        referralAnalyticsDBBean.setUserAgent("MOZILLA");
        return referralAnalyticsDBBean;
    }
}
