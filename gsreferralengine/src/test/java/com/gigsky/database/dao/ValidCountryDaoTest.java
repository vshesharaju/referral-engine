package com.gigsky.database.dao;

import com.gigsky.database.bean.IncentiveSchemeDBBean;
import com.gigsky.database.bean.TenantDBBean;
import com.gigsky.database.bean.ValidCountryDBBean;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pradeepragav on 05/01/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
@Ignore
public class ValidCountryDaoTest {
    @Autowired
    ValidCountryDao testValidcountryDao;

    @Autowired
    IncentiveSchemeDao testIncentiveSchemeDao;

    @Autowired
    private TenantMockDao tenantMockDao;

    private int incentiveSchemeId;
    private int validCountryId;
    private ArrayList<TenantDBBean> tenants;
    private int tenantLength;
    private int testTenantId;

    @Before
    public void init() throws Exception{
        //adding tenants
        tenants = tenantMockDao.addTenants();
        tenantLength = tenants.size();
        testTenantId = tenants.get(0).getId();
        
        IncentiveSchemeDaoTest incentiveSchemeDaoTest = new IncentiveSchemeDaoTest();

        //Create incentiveScheme
        IncentiveSchemeDBBean incentiveSchemeDBBean = incentiveSchemeDaoTest.createIncentiveScheme(1, testTenantId);
        testIncentiveSchemeDao.addIncentiveScheme(incentiveSchemeDBBean);
        incentiveSchemeId = incentiveSchemeDBBean.getId();
        Assert.assertNotNull(incentiveSchemeId);

        //Create validcountry
        ValidCountryDBBean validCountryDBBean = new ValidCountryDBBean();
        validCountryDBBean.setCountry("US");
        validCountryDBBean.setIncentiveSchemeId(incentiveSchemeId);
        testValidcountryDao.addValidCountry(validCountryDBBean);
        validCountryId = validCountryDBBean.getId();
        Assert.assertNotNull(validCountryId);
    }

    @After
    public void tearDown() throws Exception{
        //Delete incentiveScheme
        IncentiveSchemeDBBean incentiveSchemeDBBean = testIncentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, testTenantId);
        testIncentiveSchemeDao.deleteIncentiveScheme(incentiveSchemeDBBean);

        //delete tenants
        tenantMockDao.deleteTenants(tenants);
    }

    //Test addValidCountry
    @Test
    public void  testAddValidCountry() throws Exception{
        ValidCountryDBBean validCountryDBBean = new ValidCountryDBBean();
        validCountryDBBean.setCountry("US");
        validCountryDBBean.setIncentiveSchemeId(incentiveSchemeId);

        testValidcountryDao.addValidCountry(validCountryDBBean);
        Assert.assertNotNull(validCountryDBBean.getId());
    }

    //Test getValidCountryById
    @Test
    public void testGetValidCountryById() throws Exception{
        ValidCountryDBBean validCountryDBBean = testValidcountryDao.getValidCountry(validCountryId);
        Assert.assertNotNull(validCountryDBBean.getId());
    }

    //Test getValidCountriesForIncentiveSchemeId
    @Test
    public void testGetValidCountriesForIncentiveScheme() throws Exception{
        List<ValidCountryDBBean> validCountryDBBeans = testValidcountryDao.getValidCountriesForIncentiveSchemeId(incentiveSchemeId);
        Assert.assertNotNull(validCountryDBBeans);
    }

    //Test deleteValidCountry
    @Test
    public void testDeleteValidCountry() throws  Exception{
        IncentiveSchemeDBBean incentiveSchemeDBBean = testIncentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, testTenantId);
        ValidCountryDBBean validCountryDBBean = createValidCountryFor(incentiveSchemeDBBean,"IN");
        testValidcountryDao.addValidCountry(validCountryDBBean);
        Assert.assertNotNull(validCountryDBBean.getId());

        testValidcountryDao.deleteValidCountry(validCountryDBBean);
        validCountryDBBean = testValidcountryDao.getValidCountry(validCountryDBBean.getId());
        Assert.assertNull(validCountryDBBean);
    }

    //Test updateValidCountry
    @Test
    public void updateValidCountry() throws  Exception{
        ValidCountryDBBean validCountryDBBean = testValidcountryDao.getValidCountry(validCountryId);
        validCountryDBBean.setCountry("UA");
        testValidcountryDao.updateValidCountry(validCountryDBBean);
        validCountryDBBean = testValidcountryDao.getValidCountry(validCountryId);
        Assert.assertEquals(validCountryDBBean.getCountry(),"UA");
    }

    //Test getActiveIncentiveSchemeForCountry
    @Test
    public void getActiveIncentiveSchemeForCountryTest() throws Exception{
        List<String> countries = new ArrayList<String>();
        countries.add("IN");
        countries.add("PK");
        countries.add("JP");

        for(String country : countries){

            //Create validcountry
            ValidCountryDBBean validCountryDBBean = new ValidCountryDBBean();
            validCountryDBBean.setCountry(country);
            validCountryDBBean.setIncentiveSchemeId(incentiveSchemeId);
            testValidcountryDao.addValidCountry(validCountryDBBean);
        }

        IncentiveSchemeDBBean incentiveSchemeDBBean = testIncentiveSchemeDao.getActiveSchemeForCountry("IN", testTenantId);

        Assert.assertNotNull(incentiveSchemeDBBean);
    }

    //Test getActiveIncentiveSchemeForCountries
    @Test
    public void getActiveIncentiveSchemeForCountriesTest() throws Exception{
        List<String> countries = new ArrayList<String>();
        countries.add("IN");
        countries.add("PK");
        countries.add("JP");

        for(String country : countries){

            //Create validcountry
            ValidCountryDBBean validCountryDBBean = new ValidCountryDBBean();
            validCountryDBBean.setCountry(country);
            validCountryDBBean.setIncentiveSchemeId(incentiveSchemeId);
            testValidcountryDao.addValidCountry(validCountryDBBean);
        }

        List<IncentiveSchemeDBBean> incentiveSchemeDBBeanList = testIncentiveSchemeDao.getEnabledSchemeForCountries(countries, testTenantId);

        Assert.assertNotNull(incentiveSchemeDBBeanList);

    }

    public ValidCountryDBBean createValidCountryFor(IncentiveSchemeDBBean incentiveSchemeDBBean,String country){
        ValidCountryDBBean validCountryDBBean = new ValidCountryDBBean();
        validCountryDBBean.setCountry(country);
        validCountryDBBean.setIncentiveSchemeId(incentiveSchemeDBBean.getId());
        return validCountryDBBean;
    }
}
