package com.gigsky.database.dao;

import com.gigsky.database.bean.TenantDBBean;
import com.gigsky.rest.bean.PageInfo;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by shwethars on 25/01/19.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
public class TenantDaoTest {

    @Autowired
    TenantDao tenantDao;

    @Test
    public void testAddPromotion() throws Exception{
        List<TenantDBBean> tenantDBBean = tenantDao.getAllTenants();

        Assert.assertNotNull(tenantDBBean);

        TenantDBBean tenantDBBean1 = tenantDao.getTenantInfo(2);

      //  Assert.assertNotNull(tenantDBBean1);

    }
}
