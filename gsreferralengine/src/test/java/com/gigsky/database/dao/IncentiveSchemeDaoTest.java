package com.gigsky.database.dao;

import com.gigsky.database.bean.IncentiveSchemeDBBean;
import com.gigsky.database.bean.TenantDBBean;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;

/**
 * Created by pradeepragav on 05/01/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
public class IncentiveSchemeDaoTest {

    @Autowired
    IncentiveSchemeDao incentiveSchemeDao;

    @Autowired
    private TenantMockDao tenantMockDao;

    private int testIncentiveSchemeId;
    private ArrayList<TenantDBBean> tenants;
    private int tenantLength;
    private int testTenantId;

    @Before
    public void init() throws Exception{
        //adding tenants
        tenants = tenantMockDao.addTenants();
        testTenantId = tenants.get(0).getId();

        //Create incentive bean
        IncentiveSchemeDBBean incentiveSchemeDBBean = new IncentiveSchemeDBBean();
        incentiveSchemeDBBean.setTenantId(testTenantId);
        //Add properties
        incentiveSchemeDBBean.setSchemeEnabled((byte) 1);
        //Add incentive using dao
        incentiveSchemeDao.addIncentiveScheme(incentiveSchemeDBBean);

        testIncentiveSchemeId = incentiveSchemeDBBean.getId();
        //Assert adding
        Assert.assertNotNull(testIncentiveSchemeId);
    }

    @After
    public void tearDown() throws Exception{
        //Delete incentiveScheme created for testing
        IncentiveSchemeDBBean incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(testIncentiveSchemeId, testTenantId);
        incentiveSchemeDao.deleteIncentiveScheme(incentiveSchemeDBBean);

        //delete tenants
        tenantMockDao.deleteTenants(tenants);
    }

    //Test addIncentiveScheme
    @Test
    public void testAddIncentiveScheme() throws Exception{
        IncentiveSchemeDBBean incentiveSchemeDBBean = createIncentiveScheme(1, testTenantId);
        incentiveSchemeDBBean.setTenantId(testTenantId);
        incentiveSchemeDao.addIncentiveScheme(incentiveSchemeDBBean);
        Assert.assertNotNull(incentiveSchemeDBBean.getId());
        //Delete the entry
        incentiveSchemeDao.deleteIncentiveScheme(incentiveSchemeDBBean);
    }

    //Test updateIncentiveScheme
    @Test
    public void testUpdateIncentiveScheme() throws Exception{
        IncentiveSchemeDBBean incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(testIncentiveSchemeId, testTenantId);
        incentiveSchemeDBBean.setSchemeEnabled((byte) 0);
        incentiveSchemeDao.updateIncentiveScheme(incentiveSchemeDBBean);
        Byte firstScheme = incentiveSchemeDBBean.getSchemeEnabled();
        Byte secondScheme = (byte) 1;
        Assert.assertNotEquals(firstScheme,secondScheme);
    }

    //Test getIncentiveScheme
    @Test
    public void testGetIncentiveScheme() throws Exception{
        IncentiveSchemeDBBean incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(testIncentiveSchemeId, testTenantId);
        Assert.assertNotNull(incentiveSchemeDBBean.getId());
    }

    //Test deleteIncentiveScheme
    @Test
    public void testDeleteIncentiveScheme() throws Exception{
        IncentiveSchemeDBBean incentiveSchemeDBBean = createIncentiveScheme(1, testTenantId);
        incentiveSchemeDBBean.setTenantId(testTenantId);
        incentiveSchemeDao.addIncentiveScheme(incentiveSchemeDBBean);
        int incentiveId = incentiveSchemeDBBean.getId();
        incentiveSchemeDao.deleteIncentiveScheme(incentiveSchemeDBBean);

        incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveId, testTenantId);
        Assert.assertNull(incentiveSchemeDBBean);
    }

    @Test
    public void testGetIncentiveSchemesWhichAreIsEnabled() throws Exception{

//        List<Object[]> objects = incentiveSchemeDao.getIncentiveSchemesWhichAreIsEnabled();
//
//        for ( Object[] results : objects){
//
//            for (Object object : results){
//
//                if(object instanceof IncentiveSchemeDBBean){
//
//                    IncentiveSchemeDBBean incentiveSchemeDBBean = (IncentiveSchemeDBBean) object;
//                    System.out.print(incentiveSchemeDBBean.getId());
//                }
//                else if(object instanceof ValidCountryDBBean){
//
//                    ValidCountryDBBean validCountryDBBean = (ValidCountryDBBean) object;
//                    System.out.print(validCountryDBBean.getId());
//                }
//            }
//        }
    }

    //Create incentive scheme
    public static IncentiveSchemeDBBean createIncentiveScheme(int schemeEnabled, int tenantId){
        IncentiveSchemeDBBean incentiveSchemeDBBean = new IncentiveSchemeDBBean();
        incentiveSchemeDBBean.setSchemeEnabled((byte) schemeEnabled);
        incentiveSchemeDBBean.setActiveScheme((byte) schemeEnabled);
        incentiveSchemeDBBean.setTenantId(tenantId);
        return incentiveSchemeDBBean;
    }
}
