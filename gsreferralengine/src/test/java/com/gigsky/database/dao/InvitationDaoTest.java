package com.gigsky.database.dao;

import com.gigsky.database.bean.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pradeepragav on 08/01/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
public class InvitationDaoTest {
    @Autowired
    CustomerDao testCustomerDao;

    @Autowired
    IncentiveSchemeDao testIncentiveSchemeDao;

    @Autowired
    ReferralCodeDao testReferralCodeDao;

    @Autowired
    InvitationDao testInvitationDao;

    @Autowired
    private TenantMockDao tenantMockDao;

    private int testCustomerId;
    private int testIncentiveSchmeId;
    private int testReferralCodeId;
    private int testInvitationId;
    private ArrayList<TenantDBBean> tenants;
    private int tenantLength;
    private int testTenantId;


    @Before
    public void init() throws Exception{
        //adding tenants
        tenants = tenantMockDao.addTenants();
        tenantLength = tenants.size();
        testTenantId = tenants.get(0).getId();

        //Create customer bean and insert to table
        CustomerDBBean customerDBBean = CustomerDaoTest.createCustomerData(114, testTenantId);
        testCustomerId = testCustomerDao.createCustomerInfo(customerDBBean);

        //Validate insertion
        Assert.assertNotNull(testCustomerId);

        //Create incentiveScheme bean adn insert to table
        IncentiveSchemeDBBean incentiveSchemeDBBean = IncentiveSchemeDaoTest.createIncentiveScheme(1, testTenantId);
        testIncentiveSchmeId = testIncentiveSchemeDao.addIncentiveScheme(incentiveSchemeDBBean);

        //validate insertiona
        Assert.assertNotNull(testIncentiveSchmeId);

        //Create referralCode bean and insert to table
        ReferralCodeDBBean referralCodeDBBean = ReferralCodeDaoTest.createReferralCode(testCustomerId,testIncentiveSchmeId, testTenantId);
        testReferralCodeId = testReferralCodeDao.addReferralCode(referralCodeDBBean);

        //Validate insertion
        Assert.assertNotNull(testReferralCodeId);

        //Create invitation bean and insert to table
        InvitationDBBean invitationDBBean = createInvitation(testReferralCodeId, "test@gmail.com");
        testInvitationId = testInvitationDao.addInvitation(invitationDBBean);

        //Validate insertion
        Assert.assertNotNull(testInvitationId);
    }

    @After
    public void tearDown() throws Exception{
        testCustomerDao.deleteCustomerInfo(testCustomerDao.getCustomerInfo(testCustomerId, testTenantId));
        testIncentiveSchemeDao.deleteIncentiveScheme(testIncentiveSchemeDao.getIncentiveScheme(testIncentiveSchmeId, testTenantId));

        //delete tenants
        tenantMockDao.deleteTenants(tenants);
    }

    @Test
    public void testAddInvitation() throws Exception{
        //Tested in @before and @after methods
    }

    @Test
    public void testUpdateInvitation() throws Exception{
        InvitationDBBean invitationDBBean = testInvitationDao.getInvitationById(testInvitationId);
//        invitationDBBean.setPlatform("WEB");
        testInvitationDao.updateInvitation(invitationDBBean);
        invitationDBBean = testInvitationDao.getInvitationById(testInvitationId);
//        Assert.assertEquals("WEB",invitationDBBean.getPlatform());
    }

    @Test
    public void testGetInvitation() throws Exception{
        //Tested in @befote and @after methods
    }

    @Test
    public void testDeleteInvitaiton() throws Exception{
        //Tested in @before and @after methods
    }

    @Test
    public void testGetInvitationsForReferralCode() throws Exception{
        List<InvitationDBBean> invitationDBBeans = testInvitationDao.getInvitationsForReferralCode(testReferralCodeId);
        Assert.assertNotNull(invitationDBBeans);
    }

    @Test
    public void testGetInvitationsForCustomer() throws Exception{
        List<InvitationDBBean> invitationDBBeans = testInvitationDao.getInvitationsForCustomer(testCustomerId,0,10, testTenantId);
        Assert.assertNotNull(invitationDBBeans);
    }

    @Test
    public void testIncrementResendCount() throws Exception{
        testInvitationDao.incrementInvitationResendCount(testInvitationId);
        InvitationDBBean invitationDBBean = testInvitationDao.getInvitationById(testInvitationId);
        Assert.assertEquals((Integer)1,(Integer) invitationDBBean.getResendCount());
    }

    public InvitationDBBean createInvitation(int referralCodeId, String emailId){
        InvitationDBBean invitationDBBean = new InvitationDBBean();
        invitationDBBean.setReferralCodeId(referralCodeId);
        invitationDBBean.setEmailId(emailId);
        invitationDBBean.setStatus("In progress");
        invitationDBBean.setResendCount(0);
        return invitationDBBean;
    }
}
