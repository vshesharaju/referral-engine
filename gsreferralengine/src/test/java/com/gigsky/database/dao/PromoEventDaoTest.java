package com.gigsky.database.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.PromoEventDBBean;
import com.gigsky.database.bean.TenantDBBean;
import com.gigsky.messaging.beans.UserSignup;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by pradeepragav on 04/02/19.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
public class PromoEventDaoTest {

    @Autowired
    PromoEventDao promoEventDao;

    @Autowired
    TenantDao tenantDao;

    @Autowired
    CustomerDao customerDao;

    private static int customerId;
    private static int tenantId;
    private static int gsCustomerId = 12;
    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void prepareDataBase()
    {
        //Create Tenant
        TenantDBBean tenantDBBean = new TenantDBBean("GigSky","GigSky description");
        tenantId = tenantDao.createTenant(tenantDBBean);

        //Create Customer
        CustomerDBBean customerDBBean = new CustomerDBBean(new Long(gsCustomerId),
                tenantId,
                "testgig123@gigsky.com",
                "Test",
                "Gig");
        customerId = customerDao.createCustomerInfo(customerDBBean);

        //Add two events for the same customer
        promoEventDao.addPromoEvent(getPromoEventBean(Configurations.PromoEventType.SIGNUP_EVENT));
        promoEventDao.addPromoEvent(getPromoEventBean(Configurations.PromoEventType.SIM_ACTIVATION_EVENT));

    }

    @Test
    public void getSignUpEventsInPromoEventTest()
    {
        //Check if the promoEvent is of type signUp event
        List<PromoEventDBBean> promoEvent = promoEventDao.getPromoEvent(customerId, tenantId,
                Configurations.PromoEventType.SIGNUP_EVENT);

        Assert.assertTrue(promoEvent.size() == 1 &&
                promoEvent.get(0).getEventType().equals(Configurations.PromoEventType.SIGNUP_EVENT));
    }

    @After
    public void deleteEntriesInDataBase()
    {
        customerDao.deleteCustomerInfoByCustomerId(customerId, tenantId);
        tenantDao.deleteTenant(tenantId);
        promoEventDao.deleteAllPromoEvents();
    }

    private String getSignupEventData()
    {
        UserSignup userSignup = new UserSignup();
        userSignup.setEmailId("testgig123@gigsky.com");
        userSignup.setAccountType("GigSky");
        userSignup.setCustomerId(new Long(gsCustomerId));
        userSignup.setReferralCode("GS123");


        try {
            return objectMapper.writeValueAsString(userSignup);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

    private PromoEventDBBean getPromoEventBean(String promoEventType)
    {
        //Create PromoEvent
        PromoEventDBBean promoEventDBBean = new PromoEventDBBean();
        promoEventDBBean.setTenantId(tenantId);
        promoEventDBBean.setCustomerId(customerId);
        promoEventDBBean.setEventData(getSignupEventData());
        promoEventDBBean.setEventMetaData("");
        promoEventDBBean.setEventType(promoEventType);
        promoEventDBBean.setStatus(Configurations.StatusType.STATUS_NEW);
        promoEventDBBean.setStatusMessage("");
        return promoEventDBBean;
    }


}
