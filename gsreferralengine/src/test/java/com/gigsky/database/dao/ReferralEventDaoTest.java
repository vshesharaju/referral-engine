package com.gigsky.database.dao;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.ReferralEventDBBean;
import com.gigsky.database.bean.TenantDBBean;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pradeepragav on 08/01/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
public class ReferralEventDaoTest {
    @Autowired
    CustomerDao testCustomerDao;

    @Autowired
    ReferralEventDao testReferralEventDao;

    @Autowired
    private TenantMockDao tenantMockDao;

    private int testCustomerId;
    private int testReferralEventId;
    private ArrayList<TenantDBBean> tenants;
    private int tenantLength;
    private int testTenantId;

    @Before
    public void init() throws Exception{
        //adding tenants
        tenants = tenantMockDao.addTenants();
        tenantLength = tenants.size();
        testTenantId = tenants.get(0).getId();

        //Create customer bean and enter to table
        CustomerDBBean customerDBBean = CustomerDaoTest.createCustomerData(114, testTenantId);
        testCustomerId=testCustomerDao.createCustomerInfo(customerDBBean);
        //Validate insertion
        Assert.assertNotNull(testCustomerId);
        //Create referal event bean and enter to table
        ReferralEventDBBean referralEventDBBean = createReferralEvent(testCustomerId, testTenantId);
        testReferralEventId=testReferralEventDao.addReferralEvent(referralEventDBBean);
        //Validate insertion
        Assert.assertNotNull(testReferralEventId);
    }

    @After
    public void tearDown() throws Exception{
        //Delete customer
        testCustomerDao.deleteCustomerInfo(testCustomerDao.getCustomerInfo(testCustomerId, testTenantId));

        //delete tenants
        tenantMockDao.deleteTenants(tenants);
    }

    @Test
    public void testAddReferralEvent() throws Exception{
        //Tested in @before and @after methods
    }

    @Test
    public void testDeleteReferralEvent() throws Exception{
        //Tested in @before and @after methods
    }

    @Test
    public void testUpdateReferralEvent() throws Exception{
        ReferralEventDBBean referralEventDBBean = testReferralEventDao.getReferralEvent(testReferralEventId);
        referralEventDBBean.setEventData("Facebook invite");
        testReferralEventDao.updateReferralEvent(referralEventDBBean);
        referralEventDBBean = testReferralEventDao.getReferralEvent(testReferralEventId);
        Assert.assertEquals("Facebook invite",referralEventDBBean.getEventData());
    }

    @Test
    public void testGetReferralEvent() throws Exception{
        //Tested in @before and @after methods
    }

    @Test
    public void testGetReferralEventsForCustomerId() throws Exception{
        List<ReferralEventDBBean> referralEventDBBeans = testReferralEventDao.getReferralEventsForCustomer(testCustomerId);
        Assert.assertNotNull(referralEventDBBeans);
    }

    @Test
    public void testGetReferralEventForCustomerByEventType() throws Exception{
        List<ReferralEventDBBean> referralEventDBBeans = testReferralEventDao.getReferralEventsForCustomerByEventType(testCustomerId,"Email invite", testTenantId);
        Assert.assertNotNull(referralEventDBBeans);
    }


//    {
//        "messageId":"1234"
//            ,"source":"imsi_server"
//            ,"objectType":"SimActivation"
//            ,"eventType":"SIMActivationEvent"
//            ,"data":
//        "{\"type\":\"SIMActivationEvent\",\"simType\":\"GIGSKY_SIM\",\"iccID\":\"1234-1234\",\"customerId\":2,\"objectType\":\"SimActivation\"}","eventOccurredTime":"2017-02-08 11:16:49"}
    @Test
    public void getDuplicateEvent() throws Exception{

        String keyValuePairString = getUniqueEventDataAttribute("iccID","1234-1234");
        CustomerDBBean customerDBBean = new CustomerDBBean();
        customerDBBean.setId(3);
        String eventType = Configurations.ReferralEventType.SIM_ACTIVATION_EVENT;

        ReferralEventDBBean referralEventDBBean = testReferralEventDao.getDuplicateEvent(eventType, customerDBBean, keyValuePairString);
    }

    public ReferralEventDBBean createReferralEvent(int customerId, int tenantId){
        ReferralEventDBBean referralEventDBBean = new ReferralEventDBBean();
        referralEventDBBean.setCustomerId(customerId);
        referralEventDBBean.setEventData("Created by tego");
        referralEventDBBean.setEventType("Email invite");
        referralEventDBBean.setTenantId(tenantId);
        return referralEventDBBean;
    }

    private String getUniqueEventDataAttribute(String key, String value){

        StringBuffer keyValuePairString = new StringBuffer();
        keyValuePairString.append("\"");
        keyValuePairString.append(key);
        keyValuePairString.append("\"");
        keyValuePairString.append(":");
        keyValuePairString.append("\"");
        keyValuePairString.append(value);
        keyValuePairString.append("\"");

        return keyValuePairString.toString();
    }
}
