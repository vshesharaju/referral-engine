package com.gigsky.database.dao;

import com.gigsky.backend.messages.dbbeans.MessagingQueueDetails;
import com.gigsky.common.Configurations;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by pradeepragav on 11/04/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
public class MessageServerQueueTest {

    @Autowired
    private MessageServerQueueDao messageServerQueueDao;

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    @Test
    public void getMessagingQueueDetailsTest() throws Exception{

        String queueName = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.MQ_QUEUE_NAME);

        MessagingQueueDetails messagingQueueDetails = messageServerQueueDao.getMessagingQueueDetails(queueName);

        Assert.assertNotNull(messagingQueueDetails);
    }
}
