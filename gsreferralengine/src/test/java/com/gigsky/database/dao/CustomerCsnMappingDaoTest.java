package com.gigsky.database.dao;

import com.gigsky.database.bean.CustomerCsnMappingDBBean;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by pradeepragav on 08/01/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
@Ignore
public class CustomerCsnMappingDaoTest {

    @Autowired
    CustomerCSNMappingDao testCustomerCsnMappingDao;

    private int testCustomerCsnMappingId;

    @Before
    public void init() throws Exception{
        //Create customercsnmapping bean and insert to table
        CustomerCsnMappingDBBean customerCsnMappingDBBean = createCustomerCsnMapping();
        testCustomerCsnMappingId = testCustomerCsnMappingDao.addCustomerCSNMapping(customerCsnMappingDBBean);
        //Validate insertion
        Assert.assertNotNull(customerCsnMappingDBBean.getId());
    }

    @After
    public void tearDown() throws Exception{
        CustomerCsnMappingDBBean csnMappingDBBean = testCustomerCsnMappingDao.getCustomerCsnMappingbyId(testCustomerCsnMappingId);
        testCustomerCsnMappingDao.deleteCustomerCSNMapping(csnMappingDBBean);
    }

    @Test
    public void testAddCustomerCsnMapping() throws Exception{
        //Tested in @before and @after methods
    }

    @Test
    public void testUpdateCustomerCsnMapping() throws Exception{
        CustomerCsnMappingDBBean customerCsnMappingDBBean = testCustomerCsnMappingDao.getCustomerCsnMappingbyId(testCustomerCsnMappingId);
        customerCsnMappingDBBean.setCsn("1234ASDFGH");
        testCustomerCsnMappingDao.updateCustomerCSNMapping(customerCsnMappingDBBean);
        customerCsnMappingDBBean = testCustomerCsnMappingDao.getCustomerCsnMappingbyId(testCustomerCsnMappingId);
        Assert.assertEquals("1234ASDFGH",customerCsnMappingDBBean.getCsn());
    }

    @Test
    public void testGetCustomerCsnMapping() throws Exception{
        //Tested in @before and @after methods
    }

    @Test
    public void testDeleteCustomerCsnMapping() throws Exception{
        //Tested in @before and @after methods
    }

    public CustomerCsnMappingDBBean createCustomerCsnMapping(){
        CustomerCsnMappingDBBean customerCsnMappingDBBean = new CustomerCsnMappingDBBean();
        customerCsnMappingDBBean.setCsn("XYZ123ASDF");
        customerCsnMappingDBBean.setGsCustomerId(123);
        return customerCsnMappingDBBean;
    }
}
