package com.gigsky.database.dao;

import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.IncentiveSchemeDBBean;
import com.gigsky.database.bean.ReferralCodeDBBean;
import com.gigsky.database.bean.TenantDBBean;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pradeepragav on 08/01/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
public class ReferralCodeDaoTest {

    @Autowired
    CustomerDao testCustomerDao;

    @Autowired
    IncentiveSchemeDao testIncentiveSchemeDao;

    @Autowired
    ReferralCodeDao testReferralCodeDao;

    @Autowired
    private TenantMockDao tenantMockDao;

    private int testCustomerId;
    private int testIncentiveSchemeId;
    private int testReferralCodeId;
    private ArrayList<TenantDBBean> tenants;
    private int tenantLength;
    private int testTenantId;

    @Before
    public void init() throws Exception{
        //adding tenants
        tenants = tenantMockDao.addTenants();
        testTenantId = tenants.get(0).getId();

        //Create incentive and customer bean
        IncentiveSchemeDBBean incentiveSchemeDBBean = IncentiveSchemeDaoTest.createIncentiveScheme(1, testTenantId);
        incentiveSchemeDBBean.setSchemeEnabled((byte)1);
        CustomerDBBean customerDBBean = CustomerDaoTest.createCustomerData(114, testTenantId);
        //Insert them to table
        testCustomerId = testCustomerDao.createCustomerInfo(customerDBBean);
        testIncentiveSchemeId = testIncentiveSchemeDao.addIncentiveScheme(incentiveSchemeDBBean);
        //Validate insertion
        Assert.assertNotNull(testIncentiveSchemeId);
        Assert.assertNotNull(testCustomerId);
        //Create referralCode bean
        ReferralCodeDBBean referralCodeDBBeanOne = createReferralCode(testCustomerId,testIncentiveSchemeId,testTenantId);
        //Insert to table
        testReferralCodeId=testReferralCodeDao.addReferralCode(referralCodeDBBeanOne);
        //Validate insertion
        Assert.assertNotNull(testReferralCodeId);
    }

    @After
    public void tearDown() throws Exception{
        //Delete customer and incentiveScheme entries
        testCustomerDao.deleteCustomerInfo(testCustomerDao.getCustomerInfo(testCustomerId, testTenantId));
        testIncentiveSchemeDao.deleteIncentiveScheme(testIncentiveSchemeDao.getIncentiveScheme(testIncentiveSchemeId, testTenantId));

        //delete tenants
        tenantMockDao.deleteTenants(tenants);
    }

    @Test
    public void testAddReferralCode() throws Exception{
        //Tested in @before and @after
    }

    @Test
    public void testUpdateReferralCode() throws Exception{
        ReferralCodeDBBean referralCodeDBBean = testReferralCodeDao.getReferralCode(testReferralCodeId);
        referralCodeDBBean.setChannel("Facebook");
        testReferralCodeDao.updateReferralCode(referralCodeDBBean);
        referralCodeDBBean = testReferralCodeDao.getReferralCode(testReferralCodeId);
        Assert.assertEquals("Facebook",referralCodeDBBean.getChannel());
    }

    @Test
    public void testDeleteReferralCode() throws Exception{
        //Tested in @before and @after
    }

    @Test
    public void testGetReferralCode() throws Exception{
        //Tested in @before and @after
    }

    @Test
    public void testGetReferralCodesByCustomerId() throws Exception{
        List<ReferralCodeDBBean> referralCodeDBBeans = testReferralCodeDao.getReferralCodesByCustomerId(testCustomerId, testTenantId);
        Assert.assertNotNull(referralCodeDBBeans);
    }

    @Test
    public void testGetReferralCodesByIncentiveSchemId() throws Exception{
        List<ReferralCodeDBBean> referralCodeDBBeans = testReferralCodeDao.getReferralCodesByIncentiveSchemeId(testIncentiveSchemeId);
        Assert.assertNotNull(referralCodeDBBeans);
    }

    @Test
    public void testGetReferralCodesForChannel() throws Exception{
        ReferralCodeDBBean referralCodeDBBean = testReferralCodeDao.getActiveReferralCodeForChannel(testCustomerId,"Email");
        Assert.assertNotNull(referralCodeDBBean);
    }

    public static ReferralCodeDBBean createReferralCode(int customerId,int incentiveSchemeId, int tenantId){
        ReferralCodeDBBean referralCodeDBBean = new ReferralCodeDBBean();
        referralCodeDBBean.setIncentiveSchemeId(incentiveSchemeId);
        referralCodeDBBean.setCustomerId(customerId);
        referralCodeDBBean.setChannel("Email");
        referralCodeDBBean.setReferralCode("XYZ123");
        referralCodeDBBean.setIsActive((byte) 1);
        referralCodeDBBean.setTenantId(tenantId);
        return referralCodeDBBean;
    }
}
