package com.gigsky.database.dao;

import com.gigsky.database.bean.CreditSchemeDBBean;
import com.gigsky.database.bean.IncentiveSchemeDBBean;
import com.gigsky.database.bean.TenantDBBean;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pradeepragav on 08/01/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
public class CreditSchemeDaoTest {
    @Autowired
    CreditSchemeDao creditSchemeDao;

    @Autowired
    IncentiveSchemeDao incentiveSchemeDao;

    @Autowired
    private TenantMockDao tenantMockDao;

    private int testIncentiveSchemeId;
    private int testCreditSchemeId;
    private ArrayList<TenantDBBean> tenants;
    private int testTenantId;

    @Before
    public void init() throws Exception{
        //adding tenants
        tenants = tenantMockDao.addTenants();
        testTenantId = tenants.get(0).getId();
        
        //Create incentiveScheme bean and add to db
        IncentiveSchemeDBBean incentiveSchemeDBBean = IncentiveSchemeDaoTest.createIncentiveScheme(1, testTenantId);
        incentiveSchemeDBBean.setTenantId(testTenantId);
        incentiveSchemeDao.addIncentiveScheme(incentiveSchemeDBBean);
        testIncentiveSchemeId = incentiveSchemeDBBean.getId();
        //Validate insertion
        Assert.assertNotNull(testIncentiveSchemeId);
        //Create creditScheme bean and add to db
        CreditSchemeDBBean creditSchemeDBBean = createCreditScheme(testIncentiveSchemeId);
        testCreditSchemeId = creditSchemeDao.addCreditScheme(creditSchemeDBBean);
        //Validate insertion
        Assert.assertNotNull(testCreditSchemeId);
    }

    @After
    public void tearDown() throws Exception{
        //Delete incentiveScheme entry in db
        incentiveSchemeDao.deleteIncentiveScheme(incentiveSchemeDao.getIncentiveScheme(testIncentiveSchemeId, testTenantId));

        //delete tenants
        tenantMockDao.deleteTenants(tenants);
    }

    @Test
    public void testAddCreditScheme() throws Exception{
        Assert.assertNotNull(creditSchemeDao.addCreditScheme(createCreditScheme(testIncentiveSchemeId)));
    }

    @Test
    public void testUpdateCreditScheme() throws Exception{
        CreditSchemeDBBean creditSchemeDBBean = creditSchemeDao.getCreditScheme(testCreditSchemeId);
        creditSchemeDBBean.setCurrency("IN");
        creditSchemeDao.updateCreditScheme(creditSchemeDBBean);
        creditSchemeDBBean = creditSchemeDao.getCreditScheme(testCreditSchemeId);
        Assert.assertNotEquals("USD",creditSchemeDBBean.getCurrency());
    }

    @Test
    public void testGetCreditScheme() throws Exception{
        CreditSchemeDBBean creditSchemeDBBean = creditSchemeDao.getCreditScheme(testCreditSchemeId);
        Assert.assertNotNull(creditSchemeDBBean);
    }

    @Test
    public void testDeleteCreditScheme() throws Exception{
        int addCreditSchemeId = creditSchemeDao.addCreditScheme(createCreditScheme(testIncentiveSchemeId));
        CreditSchemeDBBean creditSchemeDBBean = creditSchemeDao.getCreditScheme(addCreditSchemeId);
        creditSchemeDao.deleteCreditScheme(creditSchemeDBBean);
        Assert.assertNull(creditSchemeDao.getCreditScheme(addCreditSchemeId));
    }

    @Test
    public void testGetAllCreditSchemeForIncentiveScheme() throws Exception{
        int addCreditSchemeId = creditSchemeDao.addCreditScheme(createCreditScheme(testIncentiveSchemeId));
        List<CreditSchemeDBBean> creditSchemeDBBeans = creditSchemeDao.getCreditSchemesForIncentiveScheme(testIncentiveSchemeId);
        Assert.assertNotNull(creditSchemeDBBeans);
    }


    public CreditSchemeDBBean createCreditScheme(int incentiveSchemeId){
        CreditSchemeDBBean creditSchemeDBBean = new CreditSchemeDBBean();
        creditSchemeDBBean.setAdvocateAmount((float)100);
        creditSchemeDBBean.setCurrency("USD");
        creditSchemeDBBean.setNewCustomerAmount((float)100);
        creditSchemeDBBean.setIncentiveSchemeId(incentiveSchemeId);
        return creditSchemeDBBean;
    }
}
