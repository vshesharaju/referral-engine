package com.gigsky.database.dao;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.PreferenceDBBean;
import com.gigsky.database.bean.TenantDBBean;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pradeepragav on 08/01/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml", "classpath*:testSpringBeansConfig.xml"})
public class PreferenceDaoTest {

    @Autowired
    CustomerDao testCustomerDao;

    @Autowired
    PreferenceDao testPreferenceDao;

    @Autowired
    private TenantMockDao tenantMockDao;

    private int testCustomerId;
    private int testPreferenceId;
    private ArrayList<TenantDBBean> tenants;
    private int tenantLength;
    private int testTenantId;

    @Before
    public void init() throws Exception{
        //adding tenants
        tenants = tenantMockDao.addTenants();
        tenantLength = tenants.size();
        testTenantId = tenants.get(0).getId();

        CustomerDBBean customerDBBean = CustomerDaoTest.createCustomerData(114, testTenantId);
        testCustomerId=testCustomerDao.createCustomerInfo(customerDBBean);
        Assert.assertNotNull(customerDBBean);

        PreferenceDBBean preferenceDBBean = createPreference(testCustomerId);
        testPreferenceId=testPreferenceDao.addPreference(preferenceDBBean);
        Assert.assertNotNull(testPreferenceId);
    }

    @After
    public void tearDown() throws Exception{
        testCustomerDao.deleteCustomerInfo(testCustomerDao.getCustomerInfo(testCustomerId, testTenantId));

        //delete tenants
        tenantMockDao.deleteTenants(tenants);
    }

    @Test
    public void testAddPreference() throws Exception{
        //Tested in @before and @after methods
    }

    @Test
    public void testUpdatePreference() throws Exception{
        PreferenceDBBean preferenceDBBean = testPreferenceDao.getPreference(testPreferenceId);
        preferenceDBBean.setValue("Credited");
        testPreferenceDao.updatePreference(preferenceDBBean);
        preferenceDBBean = testPreferenceDao.getPreference(testPreferenceId);
        Assert.assertEquals("Credited",preferenceDBBean.getValue());
    }

    @Test
    public void testGetPreference() throws Exception{
        //Tested in @before and @after methods
    }

    @Test
    public void testDeletePreference() throws Exception{
        //Tested in @before and @after methods
    }

    @Test
    public void testGetAllPreferenceForCustomerId() throws Exception{
        List<PreferenceDBBean> preferenceDBBeans = testPreferenceDao.getPreferencesForCustomer(testCustomerId);
        Assert.assertNotNull(preferenceDBBeans);
    }

    @Test
    public void testGetPreferenceByKey() throws Exception{
        PreferenceDBBean preferenceDBBean = testPreferenceDao.getPreferenceByKey(testCustomerId,Configurations.PreferrenceKeyType.OPT_OUT);
        Assert.assertNotNull(preferenceDBBean.getId());
    }

    public PreferenceDBBean createPreference(int customerId){
        PreferenceDBBean preferenceDBBean = new PreferenceDBBean();
        preferenceDBBean.setPkey("QWERTY");
        preferenceDBBean.setValue("true");
        preferenceDBBean.setCustomerId(customerId);
        preferenceDBBean.setPkey(Configurations.PreferrenceKeyType.OPT_OUT);
        return preferenceDBBean;
    }
}
