package com.gigsky.messaging;

import com.gigsky.backend.messages.MessagingFactory;
import com.gigsky.backend.messages.dbbeans.MessageServerDBBean;
import com.gigsky.backend.messages.dbbeans.QueueDetailsDBBean;
import com.gigsky.gscommon.util.encryption.GigSkyEncryptorV2;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by pradeepragav on 13/12/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
@Ignore
public class MessageSender
{
    private final static String QUEUE_NAME = "referral.subbu";
    public static final boolean DURABLE = true;

    public void sendMessages(String eventMessage) throws Exception
    {
        QueueDetailsDBBean queueDetails = new QueueDetailsDBBean();
        //Set queue properties
        queueDetails.setId(1);
        queueDetails.setExchangeName("exchange.direct.referral");
        queueDetails.setQueueName(QUEUE_NAME);
        queueDetails.setRoutingKey("referral.subbu.events");

        //Set server details
        MessageServerDBBean serverDetails = new MessageServerDBBean();
        serverDetails.setId(1);
        serverDetails.setHost("localhost");
        serverDetails.setPort("15672");
        serverDetails.setUsername("guest");
        serverDetails.setPassword("guest");
        queueDetails.setServerDBBean(serverDetails);

        Connection connection = MessagingFactory.getRemoteConnection(queueDetails);

        Channel channel = connection.createChannel();
        try {

            channel.exchangeDeclare(queueDetails.getExchangeName(), "direct", DURABLE);
            String encMsg = new GigSkyEncryptorV2().encryptString(eventMessage);
            channel.basicPublish(queueDetails.getExchangeName(), queueDetails.getRoutingKey(), null, encMsg.getBytes());
            System.out.println(" [x] Sent '" + eventMessage + "'");
        }
        finally {
            if (channel != null && channel.isOpen()) {
                channel.close();
            }
            if (connection.isOpen()) {
                connection.close();
            }
        }
    }

}
