package com.gigsky.utils;

import com.gigsky.backend.beans.RolesResponse;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.util.GSTokenUtil;
import com.gigsky.util.HttpClientUtil;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by pradeepragav on 08/03/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
@Ignore
public class HttpClientUtilTest {

    @Autowired
    GSTokenUtil gsTokenUtil;

    @Autowired
    Configurations configurations;

    @Autowired
    HttpClientUtil httpClientUtil;

    @Test
    public void apiRequestTest() throws Exception
    {
        //Make an API call
        String token = "9ba5ecf8e6c54931b2eb34c27d89abbc";

        getResponse(token);
        //Check whether it retries for adminToken
        //Check whether it does not retry for normal token
    }

    //Get response for the api get user roles
    private RolesResponse getResponse(String token) throws BackendRequestException {

        ////Get url required for get user roles api
        String url = configurations.getBackendBaseUrl() + "account/roles";

        //Create headers map
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Basic " +token);

        //Get the timeout in ms from ConfigurationsKeyValue
        long timeOutInMs = configurations.getRequestTimeOut();

            //Call the api
            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                    httpClientUtil.sendHttpRequest("GET", url, null, null, headers, timeOutInMs);

            if(gsRestHttpResponse != null){

                //Deserialize the response
                RolesResponse rolesResponse =
                        (RolesResponse)HttpClientUtil.convertToBean(gsRestHttpResponse.getResponseBody(), RolesResponse.class);

                return rolesResponse;
            }

        return null;
    }
}
