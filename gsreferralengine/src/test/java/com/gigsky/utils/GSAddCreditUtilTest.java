package com.gigsky.utils;

import com.gigsky.util.GSAddCreditUtil;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by pradeepragav on 28/03/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
@Ignore
public class GSAddCreditUtilTest {

    @Autowired
    GSAddCreditUtil gsAddCreditUtil;

    @Test
    public void shouldAdminBeAlertedForAddingCreditTest() throws Exception{

        gsAddCreditUtil.shouldAdminBeAlertedForAddingCredit(1236, 1);
    }
}
