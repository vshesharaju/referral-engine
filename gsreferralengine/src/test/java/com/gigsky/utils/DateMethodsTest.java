package com.gigsky.utils;

import com.gigsky.common.CommonUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by pradeepragav on 17/03/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
@Ignore
public class DateMethodsTest {


    @Test
    public void utilMethodDoesTimelineMatchesCurrentTimelineTest() throws Exception
    {
        String dateOne = "2017-03-18";
        String dateTwo = "2017-03-19";
       Assert.assertTrue(utilMethodDoesTimelineMatchesCurrentTimeline(CommonUtils.convertStringToDate(dateOne), CommonUtils.convertStringToDate(dateTwo)));
    }

    //Returns true if current time is within the parameter start time and end time
    private boolean utilMethodDoesTimelineMatchesCurrentTimeline(java.sql.Date startDate, java.sql.Date endDate){

        //Get currentDate
        java.sql.Date currentDate = CommonUtils.getCurrentDate();

        //Compare current date with startDate and endDate
        if(currentDate.compareTo(startDate) >= 0 && currentDate.compareTo(endDate) <= 0){

            return true;
        }

        return false;
    }

    @Test
    public void dateIsInRangeTest() throws Exception{

        Assert.assertTrue(dateIsInRange("2017-04-11","2017-04-11","2017-04-11"));
    }

    boolean dateIsInRange(String startDate, String endDate, String date) throws ParseException {

        Date requiredDate = CommonUtils.convertStringToDate(date);

        //Get validity start and end dates of the existing scheme
        Date validityStartDate = CommonUtils.convertStringToDate(date);
        Date validityEndDate = CommonUtils.convertStringToDate(date);

        if((requiredDate.compareTo(validityStartDate)) >=0
                && (requiredDate.compareTo(validityEndDate) <=0)){

            return true;
        }
        return false;
    }
}
