package com.gigsky.utils;


import com.gigsky.rest.api.v1.PromotionResource;
import com.gigsky.rest.bean.PromoCredit;
import com.gigsky.service.PromotionService;
import com.gigsky.util.GSSchemeUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by karthik on 4/09/19.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
public class TestPromoCreditTest {

    @Autowired
    PromotionService promotionService;

    @Autowired
    GSSchemeUtil gsSchemeUtil;

    @Autowired
    PromotionResource promotionResource;

    @Test
    public void promotionCreditValidation() throws Exception {
        List<String> currencies = new ArrayList<String>();
        currencies.add("USD");
        currencies.add("EUR");
        currencies.add("GBP");
        currencies.add("JPY");


        List<PromoCredit> list = new ArrayList<PromoCredit>();
        for(int i=0;i<4;i++) {
            PromoCredit promoCredit =new PromoCredit();
            promoCredit.setCreditAmount(40f);
            promoCredit.setMinimumCartAmount(20f);
            promoCredit.setCurrency(currencies.get(i));
            list.add(promoCredit);
        }


//         = new PromotionResource();

        boolean test = (promotionResource.isValidPromoCredit(list,true,1));
        Assert.assertTrue(test);

    }
}
