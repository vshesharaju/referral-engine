package com.gigsky.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigsky.backend.credit.CreditManagementInterface;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.PromoCreditDBBean;
import com.gigsky.rest.bean.*;
import com.gigsky.service.PromotionService;
import com.gigsky.util.GSAddCreditUtil;
import com.gigsky.util.GSSchemeUtil;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by karthik on 4/09/19.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
public class CreditTypeChangeTest {

    @Autowired
    private
    GSAddCreditUtil addCreditUtil;

    @Autowired
    GSSchemeUtil gsSchemeUtil;

    @Autowired
    Configurations configurations;

    @Autowired
    PromotionService promotionService;

    PromoCreditDBBean promoCreditDBBean;

    @Autowired
    private
    CreditManagementInterface creditManagementInterface;


    @Test
    public void creditTypeChange() throws Exception {
        promoCreditDBBean = new PromoCreditDBBean();
        Float creditAmount = 100.0f;
        promoCreditDBBean.setCreditAmount(creditAmount);

        Credit credit = gsSchemeUtil.prepareCreditDetails("USD", promoCreditDBBean, 7);
        Assert.assertTrue(credit.getCreditType().toString().equals("PROMOTION_CREDIT"));
    }

    @Test
    public void addGigSkyCreditTest () throws Exception{

        int testTenantId = 2;
        Credit credit = new Credit();
        credit.setDescription("Referral credit add");
        credit.setCurrency("USD");
        credit.setCreditBalance(40);
        credit.setCreditType(Configurations.CreditType.PROMOTION);
        Timestamp creditExpiry = addCreditUtil.getReferralCreditExpiry();
        credit.setExpiry(creditExpiry);

        Credit creditResponse = creditManagementInterface.addCredit(6, credit, testTenantId);

        Assert.assertNotNull(creditResponse);
    }


}
