package com.gigsky.service;

import com.gigsky.common.CommonUtils;
import com.gigsky.database.bean.IncentiveSchemeDBBean;
import com.gigsky.database.dao.IncentiveSchemeDao;
import com.gigsky.util.GSAddCreditUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by pradeepragav on 06/03/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
public class GSAddCreditUtilTest {

    @Autowired
    GSAddCreditUtil gsAddCreditUtil;

    @Autowired
    IncentiveSchemeDao incentiveSchemeDao;

    private IncentiveSchemeDBBean incentiveSchemeDBBean;

    @Before
    public void createInvitationBean() throws Exception{

        incentiveSchemeDBBean = new IncentiveSchemeDBBean();
        Date todayDate = CommonUtils.getCurrentDate();
        incentiveSchemeDBBean.setValidityStartDate(todayDate);
        incentiveSchemeDBBean.setValidityEndDate(todayDate);
        incentiveSchemeDBBean.setSchemeEnabled((byte) 1);
        incentiveSchemeDBBean.setActiveScheme((byte) 1);
        incentiveSchemeDBBean.setAdvocateCreditExpiryPeriod(20);
        incentiveSchemeDBBean.setReferralCreditExpiryPeriod(20);
        incentiveSchemeDBBean.setTenantId(1);

        incentiveSchemeDao.addIncentiveScheme(incentiveSchemeDBBean);
    }

    @Test
    public void getReferralCreditExpiryTest() throws Exception{

        Timestamp timestamp = gsAddCreditUtil.getReferralCreditExpiry(incentiveSchemeDBBean.getReferralCreditExpiryPeriod());
    }

    @Test
    public void getReferralExpiryTest() throws Exception{

        Timestamp timestamp = gsAddCreditUtil.getReferralCreditExpiry(2);
        System.out.println(timestamp);
    }


    @After
    public void delete() throws Exception{

        incentiveSchemeDao.deleteIncentiveScheme(incentiveSchemeDBBean);
    }
}
