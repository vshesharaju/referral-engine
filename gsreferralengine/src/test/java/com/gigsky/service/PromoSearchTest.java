package com.gigsky.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.InvitationDao;
import com.gigsky.gsenterprisecommon.util.csvutils.CSVExporter;
import com.gigsky.rest.api.v1.PromotionResource;
import com.gigsky.rest.bean.*;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.task.ReferralEventTaskFactory;
import com.gigsky.task.ReferralSendEmailTask;
import com.gigsky.util.GSSchemeUtil;
import com.gigsky.util.GSTaskUtil;
import com.gigsky.util.GSUtil;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
public class PromoSearchTest {

    @Autowired
    private PromotionService promotionService;

    @Autowired
    private PromotionResource promotionResource;

    @Autowired
    private GSUtil gsUtil;

    @Autowired
    ReferralEventTaskFactory referralEventFactory;

    @Autowired
    GSTaskUtil gsTaskUtil;

    @Autowired
    InvitationDao invitationDao;

    @Autowired
    CustomerDao customerDao;


    @Autowired
    ConfigurationKeyValueDao configurationKeyValueDao;


    @Test
    public void TestPromoSearch() throws Exception{
        String promoCode = "CODE1";
        String isEnabled = "true";
        String countryCode = "US";
        int tenantId =1;
        String date = "2019-02-18";
        PageInfo inputPageInfo = new PageInfo(0, 15);
        PageInfo outputPageInfo = new PageInfo();



        if(countryCode != null)
            promotionResource.validateCountryCode(countryCode);

        if(isEnabled != null && !GSSchemeUtil.validateIsEnabled(isEnabled)){
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_PROMO_ENABLED);
        }

        if(date != null)
            CommonUtils.isValidDateFormat(date);

        List<Promotion> list = promotionService.getAllPromotions(countryCode, isEnabled, promoCode, inputPageInfo, outputPageInfo, date, tenantId);

        PromotionList promoList = new PromotionList();
        promoList.setType("PromoDetails");
        promoList.setStartIndex(outputPageInfo.getStart());
        promoList.setCount(list.size());
        promoList.setTotalCount(outputPageInfo.getCount());

        promoList.setList(list);
        convert(promoList);
    }

    @Test
    public void TestPartialPromo() throws Exception{
        String promoCode = "DEFAULT";
        String isEnabled = "true";
        String countryCode = null;
        int tenantId =1;
        String date = null;
        PageInfo inputPageInfo = new PageInfo(0, 10);
        PageInfo outputPageInfo = new PageInfo();
        if(countryCode != null)
            promotionResource.validateCountryCode(countryCode);

        if(isEnabled != null && !GSSchemeUtil.validateIsEnabled(isEnabled)){
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_PROMO_ENABLED);
        }

        if(date != null)
            CommonUtils.isValidDateFormat(date);


        List<Promotion> list = promotionService.getAllPromotions(countryCode, isEnabled, promoCode, inputPageInfo, outputPageInfo, date, tenantId);

        PromotionList promoList = new PromotionList();
        promoList.setType("PromoDetails");
        promoList.setStartIndex(outputPageInfo.getStart());
        promoList.setCount(list.size());
        promoList.setTotalCount(outputPageInfo.getCount());

        promoList.setList(list);
        convert(promoList);
    }

    @Test
    public void TestForInvalidCountry() throws Exception{
       try {

           String promoCode = "PROMO";
           String isEnabled = "true";
           String countryCode = "XY";
           int tenantId = 1;
           String date = null;
           PageInfo inputPageInfo = new PageInfo(0, 10);
           PageInfo outputPageInfo = new PageInfo();

           if (countryCode != null)
               promotionResource.validateCountryCode(countryCode);

           if (isEnabled != null && !GSSchemeUtil.validateIsEnabled(isEnabled)) {
               throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_PROMO_ENABLED);
           }

           if (date != null)
               CommonUtils.isValidDateFormat(date);

           List<Promotion> list = promotionService.getAllPromotions(countryCode, isEnabled, promoCode, inputPageInfo, outputPageInfo, date, tenantId);

           PromotionList promoList = new PromotionList();
           promoList.setType("PromoDetails");
           promoList.setStartIndex(outputPageInfo.getStart());
           promoList.setCount(list.size());
           promoList.setTotalCount(outputPageInfo.getCount());

           promoList.setList(list);
           convert(promoList);
       }
       catch (Exception e){

           System.out.println(e);
       }
    }

    @Test
    public void TestForNullInput() throws Exception{
        String promoCode = null;
        String isEnabled = null;
        String countryCode = null;
        int tenantId =1;
        String date = null;
        PageInfo inputPageInfo = new PageInfo(0, 10);
        PageInfo outputPageInfo = new PageInfo();

        if(countryCode != null)
            promotionResource.validateCountryCode(countryCode);

        if(isEnabled != null && !GSSchemeUtil.validateIsEnabled(isEnabled)){
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_PROMO_ENABLED);
        }

        if(date != null)
            CommonUtils.isValidDateFormat(date);


//        List<Promotion> list = promotionService.getAllPromotions(countryCode,isEnabled,inputPageInfo,outputPageInfo,date,tenantId);

        List<Promotion> list = promotionService.getAllPromotions(countryCode, isEnabled, promoCode, inputPageInfo, outputPageInfo, date, tenantId);

        PromotionList promoList = new PromotionList();
        promoList.setType("PromoDetails");
        promoList.setStartIndex(outputPageInfo.getStart());
        promoList.setCount(list.size());
        promoList.setTotalCount(outputPageInfo.getCount());

        promoList.setList(list);
        convert(promoList);
    }



    @Test
    public void TestForALLInvalid() throws Exception{
        try {
            String promoCode = "ASD";
            String isEnabled = "Yes";
            String countryCode = "XZ";
            int tenantId = 1;
            String date = "12-12-1997";
            PageInfo inputPageInfo = new PageInfo(0, 10);
            PageInfo outputPageInfo = new PageInfo();

            if (countryCode != null)
                promotionResource.validateCountryCode(countryCode);

            if (isEnabled != null && !GSSchemeUtil.validateIsEnabled(isEnabled)) {
                throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_PROMO_ENABLED);
            }

            if (date != null)
                CommonUtils.isValidDateFormat(date);

            List<Promotion> list = promotionService.getAllPromotions(countryCode, isEnabled, promoCode, inputPageInfo, outputPageInfo, date, tenantId);

            PromotionList promoList = new PromotionList();
            promoList.setType("PromoDetails");
            promoList.setStartIndex(outputPageInfo.getStart());
            promoList.setCount(list.size());
            promoList.setTotalCount(outputPageInfo.getCount());

            promoList.setList(list);
            convert(promoList);
        }
        catch (Exception e){
            System.out.println(e);
        }
    }

    @Test
    public void TestForInvalidDate() throws Exception{
       try {

           String promoCode = "ASD";
           String isEnabled = "true";
           String countryCode = "IN";
           int tenantId = 1;
           String date = "12-12-1997";
           PageInfo inputPageInfo = new PageInfo(0, 10);
           PageInfo outputPageInfo = new PageInfo();

           if (countryCode != null)
               promotionResource.validateCountryCode(countryCode);

           if (isEnabled != null && !GSSchemeUtil.validateIsEnabled(isEnabled)) {
               throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_PROMO_ENABLED);
           }

           if (date != null)
               CommonUtils.isValidDateFormat(date);

           List<Promotion> list = promotionService.getAllPromotions(countryCode, isEnabled, promoCode, inputPageInfo, outputPageInfo, date, tenantId);

           PromotionList promoList = new PromotionList();
           promoList.setType("PromoDetails");
           promoList.setStartIndex(outputPageInfo.getStart());
           promoList.setCount(list.size());
           promoList.setTotalCount(outputPageInfo.getCount());

           promoList.setList(list);
           convert(promoList);
       }
       catch (Exception e){
           System.out.println(e);
       }
    }

    @Test
    public void TestForInvalidIsEnabled() throws Exception{
       try {

           String promoCode = "CODE1";
           String isEnabled = "Yes";
           String countryCode = "US";
           int tenantId = 1;
           String date = "2019-02-18";
           PageInfo inputPageInfo = new PageInfo(0, 15);
           PageInfo outputPageInfo = new PageInfo();

           if (countryCode != null)
               promotionResource.validateCountryCode(countryCode);

           if (isEnabled != null && !GSSchemeUtil.validateIsEnabled(isEnabled)) {
               throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_PROMO_ENABLED);
           }

           if (date != null)
               CommonUtils.isValidDateFormat(date);

           List<Promotion> list = promotionService.getAllPromotions(countryCode, isEnabled, promoCode, inputPageInfo, outputPageInfo, date, tenantId);

           PromotionList promoList = new PromotionList();
           promoList.setType("PromoDetails");
           promoList.setStartIndex(outputPageInfo.getStart());
           promoList.setCount(list.size());
           promoList.setTotalCount(outputPageInfo.getCount());

           promoList.setList(list);
           convert(promoList);
       }
       catch (Exception e){
           System.out.println(e);
       }
    }

    @Test
    public void TestSearchPromoName() throws Exception{
        String promoCode = "PROMO1";
        String isEnabled = "true";
        String countryCode = "US";
        int tenantId =1;
        String date = "2019-03-12";
        PageInfo inputPageInfo = new PageInfo(0, 10);
        PageInfo outputPageInfo = new PageInfo();

        if(countryCode != null)
            promotionResource.validateCountryCode(countryCode);

        if(isEnabled != null && !GSSchemeUtil.validateIsEnabled(isEnabled)){
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_PROMO_ENABLED);
        }

        if(date != null)
            CommonUtils.isValidDateFormat(date);

        List<Promotion> list = promotionService.getAllPromotions(countryCode, isEnabled, promoCode, inputPageInfo, outputPageInfo, date, tenantId);

        PromotionList promoList = new PromotionList();
        promoList.setType("PromoDetails");
        promoList.setStartIndex(outputPageInfo.getStart());
        promoList.setCount(list.size());
        promoList.setTotalCount(outputPageInfo.getCount());

        promoList.setList(list);
        convert(promoList);
    }

    //API for Redeemed customers

    @Test
    public void TestRedeemedPromoInfo() throws Exception{
        int promoId = 1;
        String sortBy = "ASC";
        PageInfo inputPageInfo = new PageInfo(0, 10);
        PageInfo outputPageInfo = new PageInfo();

        //validate sortBy entry
        if(sortBy!= null){
            promotionResource.validateSortByInput(sortBy);
        }


        PromoHistoryList promoHistoryList = promotionService.getPromoHistoryByPromoId(promoId, sortBy, inputPageInfo);
        convert(promoHistoryList);

    }

    @Test
    public void TestInvalidpromoId() throws Exception{
        try {
            int promoId = 123;
            String sortBy = null;
            PageInfo inputPageInfo = new PageInfo(0, 10);
            PageInfo outputPageInfo = new PageInfo();

            //validate sortBy entry
            if (sortBy != null) {
                promotionResource.validateSortByInput(sortBy);
            }


            PromoHistoryList promoHistoryList = promotionService.getPromoHistoryByPromoId(promoId, sortBy, inputPageInfo);
            convert(promoHistoryList);
        }
        catch (Exception e){
            System.out.println(e);
        }
    }

    @Test
    public void TestSortByDESC() throws Exception{
        int promoId = 2;
        String sortBy = "DESC";
        PageInfo inputPageInfo = new PageInfo(0, 10);
        PageInfo outputPageInfo = new PageInfo();

        //validate sortBy entry
        if(sortBy!= null){
            promotionResource.validateSortByInput(sortBy);
        }

        PromoHistoryList promoHistoryList = promotionService.getPromoHistoryByPromoId(promoId, sortBy, inputPageInfo);
        convert(promoHistoryList);
    }

    @Test
    public void TestforInvalidSort() throws Exception{
       try {

           int promoId = 1;
           String sortBy = "DESC";
           PageInfo inputPageInfo = new PageInfo(0, 10);
           PageInfo outputPageInfo = new PageInfo();

           //validate sortBy entry
           if (sortBy != null) {
               promotionResource.validateSortByInput(sortBy);
           }

           PromoHistoryList promoHistoryList = promotionService.getPromoHistoryByPromoId(promoId, sortBy, inputPageInfo);
           convert(promoHistoryList);
       }
       catch (Exception e){
           System.out.println(e);
       }
    }

    @Test
    public void TestForRedeemValue() throws Exception{
        try {

            int promoId = 5;
            String sortBy = "ASC";
            PageInfo inputPageInfo = new PageInfo(0, 10);
            PageInfo outputPageInfo = new PageInfo();

            //validate sortBy entry
            if (sortBy != null) {
                promotionResource.validateSortByInput(sortBy);
            }

            PromoHistoryList promoHistoryList = promotionService.getPromoHistoryByPromoId(promoId, sortBy, inputPageInfo);
            convert(promoHistoryList);
        }
        catch (Exception e){
            System.out.println(e);
        }
    }

    @Test
    public void TestForNoParams() throws Exception{
        try {

            Integer promoId = null ;
            String sortBy = null;
            PageInfo inputPageInfo = new PageInfo(0, 10);
            PageInfo outputPageInfo = new PageInfo();

            //validate sortBy entry
            if (sortBy != null) {
                promotionResource.validateSortByInput(sortBy);
            }

            PromoHistoryList promoHistoryList = promotionService.getPromoHistoryByPromoId(promoId, sortBy, inputPageInfo);
            convert(promoHistoryList);
        }
        catch (Exception e){
            System.out.println(e);
        }
    }

    //test for csv export
    @Test
    public void TestExportPromoId() throws Exception{
        try {
            int promoId = 1;
            String sortBy = "ASC";
            String format = "csv";
            PageInfo inputPageInfo = new PageInfo(0, 10);

            //validate sortBy entry
            if (sortBy != null) {
                promotionResource.validateSortByInput(sortBy);
            }

            boolean isCSV = (StringUtils.isNotEmpty(format) && format.toUpperCase().equals(gsUtil.CSV));;

            PromoHistoryList promoHistoryList = promotionService.getPromoHistoryByPromoId(promoId, sortBy, inputPageInfo);
            if(isCSV){
                System.out.println(CSVExporter.exportToCsv(promoHistoryList.getList(),PromoUsageCsv.class,gsUtil.PROMO_USAGE_FILE_NAME+promoHistoryList.getPromotionId()+".csv").getMetadata());
            }
            else
                convert(promoHistoryList);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    @Test
    public void TestForJsonOut() throws Exception{
        try {
            int promoId = 1;
            String sortBy= null;
            String format = "json";
            PageInfo inputPageInfo = new PageInfo(0, 10);

            //validate sortBy entry
            if (sortBy != null) {
                promotionResource.validateSortByInput(sortBy);
            }

            boolean isCSV = (StringUtils.isNotEmpty(format) && format.toUpperCase().equals(gsUtil.CSV));;

            PromoHistoryList promoHistoryList = promotionService.getPromoHistoryByPromoId(promoId, sortBy, inputPageInfo);
            if(isCSV){
                System.out.println(CSVExporter.exportToCsv(promoHistoryList.getList(),PromoUsageCsv.class,gsUtil.PROMO_USAGE_FILE_NAME+promoHistoryList.getPromotionId()+".csv").getMetadata());
            }
            else
                convert(promoHistoryList);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    @Test
    public void TestOptionalQueryParams() throws Exception{
        try {
            int promoId = 1;
            String sortBy= null;
            String format = null;
            PageInfo inputPageInfo = new PageInfo(0, 10);

            //validate sortBy entry
            if (sortBy != null) {
                promotionResource.validateSortByInput(sortBy);
            }

            boolean isCSV = (StringUtils.isNotEmpty(format) && format.toUpperCase().equals(gsUtil.CSV));;

            PromoHistoryList promoHistoryList = promotionService.getPromoHistoryByPromoId(promoId, sortBy, inputPageInfo);
            if(isCSV){
                System.out.println(CSVExporter.exportToCsv(promoHistoryList.getList(),PromoUsageCsv.class,gsUtil.PROMO_USAGE_FILE_NAME+promoHistoryList.getPromotionId()+".csv").getMetadata());
            }
            else
                convert(promoHistoryList);
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }


    @Test
    public void checkEmailTrigger(){

        int count =1;
        long initialDelay =0l;
        int resendDelay = 5;

        List<CustomerDBBean> customers = customerDao.getValidCustomers(count, initialDelay, resendDelay);


        //Check customer specific tenant referral enable and processs
        for(CustomerDBBean customerDBBean : customers) {

            if(gsTaskUtil.checkReferralEnable(customerDBBean.getTenantId()) && gsTaskUtil.checkReferralEmailEnable(customerDBBean.getTenantId())){
                // referral send email task will send emails to customers
                ReferralSendEmailTask emailTask = (ReferralSendEmailTask) referralEventFactory.createReferralEventTask("REFERRAL_EMAIL_TASK");
                emailTask.setCustomerInfo(customerDBBean);
                //send email to customers
//                threadPoolExecutor.submit(emailTask);
            }

        }
    }

    @Test
    public void checkint(){
        int k= Integer.parseInt(configurationKeyValueDao.getConfigurationValue(Configurations.CREATED_ON_MAX_DURATION));
        System.out.println(k);
    }


    public void convert(Object objt) {

        ObjectMapper Obj = new ObjectMapper();
//
        try {
            // get Oraganisation object as a json string
            String objString = Obj.writeValueAsString(objt);
            System.out.println("Formatted\n" + objString);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
