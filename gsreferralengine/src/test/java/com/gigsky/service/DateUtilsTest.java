package com.gigsky.service;

import com.gigsky.common.CommonUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Date;

/**
 * Created by pradeepragav on 06/04/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
@Ignore
public class DateUtilsTest {

    @Test
    public void getCurrentDateTest() throws Exception{

        Date date = CommonUtils.getCurrentDate();
        Assert.assertNotNull(date);
    }

    @Test
    public void getDateInStringTest() throws Exception{
        String date = CommonUtils.getDateInString(CommonUtils.getCurrentDate());
        Assert.assertNotNull(date);
    }

    @Test
    public void convertStringToDateTest() throws Exception{
        Date date = CommonUtils.convertStringToDate(CommonUtils.getDateInString(CommonUtils.getCurrentDate()));
        Assert.assertNotNull(date);
    }

}
