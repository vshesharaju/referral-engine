package com.gigsky.service;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import com.gigsky.rest.bean.Account;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;

/**
 * Created by pradeepragav on 11/01/17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
@Ignore
public class AccountServiceTest {
    @Autowired
    AccountService accountService;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    PreferenceDao preferenceDao;

    @Autowired
    private IncentiveSchemeDao incentiveSchemeDao;

    @Autowired
    private ReferralSchemeService referralSchemeService;

    @Autowired
    private MockCustomerDao mockCustomerDao;

    @Autowired
    private TenantMockDao tenantMockDao;

    @Autowired
    private ValidCountryDao validCountryDao;

    private int customerId;
    private int incentiveSchemeId;
    private ArrayList<TenantDBBean> tenants;
    private int tenantLength;
    private int testTenantId;
    private int gsCustomerId;
    private int testIncentiveSchemeId;
    private int validCountryDBBeanId;

    @Before
    public void init() throws Exception{
        //adding tenants
        tenants = tenantMockDao.addTenants();
        tenantLength = tenants.size();
        testTenantId = tenants.get(0).getId();

        MockCustomerDBBean mockCustomerDBBean = new MockCustomerDBBean();
        mockCustomerDBBean.setTenantId(testTenantId);
        mockCustomerDBBean.setFirstName("test 1");
        mockCustomerDBBean.setToken("d15af80d7d3f401cbf8aa007b28ac7e5");
        mockCustomerDBBean.setCountry("US");
        gsCustomerId = mockCustomerDao.addCustomer(mockCustomerDBBean);

        //Create customer
        CustomerDBBean customerDBBean = CustomerDaoTest.createCustomerData(gsCustomerId, testTenantId);
        customerDBBean.setCountry("US");
        customerId = customerDao.createCustomerInfo(customerDBBean);

        createIncentiveScheme();
    }

    @After
    public void tearDown() throws Exception{

        customerDao.deleteCustomerInfo(customerDao.getCustomerInfo(customerId, testTenantId));
        mockCustomerDao.deleteCustomer(mockCustomerDao.getCustomer(gsCustomerId, testTenantId));

        validCountryDao.deleteValidCountry(validCountryDao.getValidCountry(validCountryDBBeanId));
        incentiveSchemeDao.deleteIncentiveScheme(incentiveSchemeDao.getIncentiveScheme(testIncentiveSchemeId, testTenantId));

        //delete tenants
        tenantMockDao.deleteTenants(tenants);
    }

    @Test
    public void testCustomerOptOut() throws Exception{
        //Call referralOptout
        accountService.setOptOut(customerId, "true");
        //Get customer preferrence using pkey
        PreferenceDBBean preferenceDBBean = preferenceDao.getPreferenceByKey(customerId, Configurations.PreferrenceKeyType.OPT_OUT);
        Assert.assertNotNull(preferenceDBBean);
        //Validate the value
        Assert.assertEquals("true",preferenceDBBean.getValue());
        //Validate key
        Assert.assertEquals(Configurations.PreferrenceKeyType.OPT_OUT,preferenceDBBean.getPkey());
    }


    @Test
    public void testGetAccountDetails() throws Exception
    {
        String token = "d15af80d7d3f401cbf8aa007b28ac7e5";
        String language = "en";

        Account account = accountService.getAccountDetails(gsCustomerId, token, testTenantId);
        Assert.assertNotNull(account);

        //Validate a few parameters
        Assert.assertNotNull(account.getReferralCode());
        Assert.assertNotNull(account.getEmailId());
        Assert.assertNotNull(account.getFirstName());
    }


    private void createIncentiveScheme(){
        //Create incentive bean
        IncentiveSchemeDBBean incentiveSchemeDBBean = new IncentiveSchemeDBBean();
        incentiveSchemeDBBean.setTenantId(testTenantId);
        //Add properties
        incentiveSchemeDBBean.setSchemeEnabled((byte) 1);
        incentiveSchemeDBBean.setActiveScheme((byte) 1);
        //Add incentive using dao
        incentiveSchemeDao.addIncentiveScheme(incentiveSchemeDBBean);

        testIncentiveSchemeId = incentiveSchemeDBBean.getId();

        ValidCountryDBBean validCountryDBBean = new ValidCountryDBBean();
        validCountryDBBean.setCountry("US");
        validCountryDBBean.setIncentiveSchemeId(testIncentiveSchemeId);

        validCountryDBBeanId = validCountryDao.addValidCountry(validCountryDBBean);

    }
}
