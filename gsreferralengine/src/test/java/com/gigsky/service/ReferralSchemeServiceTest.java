package com.gigsky.service;

import com.gigsky.common.CommonUtils;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import com.gigsky.rest.bean.CreditScheme;
import com.gigsky.rest.bean.PageInfo;
import com.gigsky.rest.bean.Scheme;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by pradeepragav on 13/01/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
@Ignore
public class ReferralSchemeServiceTest {

    @Autowired
    private ReferralSchemeService referralSchemeService;

    @Autowired
    private IncentiveSchemeDao incentiveSchemeDao;

    @Autowired
    private ValidCountryDao validCountryDao;

    @Autowired
    private CreditSchemeDao creditSchemeDao;

    @Autowired
    private ReferralCodeDao referralCodeDao;

    @Autowired
    private CustomerDao customerDao;

    private int incentiveSchemeId;

    @Before
    public void init() throws Exception{
        //Create IncentiveScheme
        IncentiveSchemeDBBean incentiveSchemeDBBean = new IncentiveSchemeDBBean();
        incentiveSchemeDBBean.setSchemeEnabled((byte) 1);

        //Get current time (Testing purpose)
        java.sql.Date date = CommonUtils.getCurrentDate();

        incentiveSchemeDBBean.setValidityStartDate(date);
        incentiveSchemeDBBean.setValidityEndDate(date);
        incentiveSchemeDBBean.setAdvocateCreditExpiryPeriod(1);
        incentiveSchemeDBBean.setReferralCreditExpiryPeriod(1);

        //Add IncentiveScheme
        incentiveSchemeId = incentiveSchemeDao.addIncentiveScheme(incentiveSchemeDBBean);

        //Validate insertion
        Assert.assertNotNull(incentiveSchemeId);

        //Create Valid Country
        ValidCountryDBBean validCountryDBBean = new ValidCountryDBBean();
        validCountryDBBean.setIncentiveSchemeId(incentiveSchemeId);
        validCountryDBBean.setCountry("ALL");

        //Add Valid Country
        validCountryDao.addValidCountry(validCountryDBBean);

        //Validate insertion
        Assert.assertNotNull(validCountryDBBean.getId());

        //Create CreditScheme
        CreditSchemeDBBean creditSchemeDBBean = new CreditSchemeDBBean();
        creditSchemeDBBean.setCurrency("USD");
        creditSchemeDBBean.setAdvocateAmount((float)100);
        creditSchemeDBBean.setNewCustomerAmount((float)100);
        creditSchemeDBBean.setIncentiveSchemeId(incentiveSchemeId);

        //Add CreditScheme
        creditSchemeDao.addCreditScheme(creditSchemeDBBean);

        //Validate insertion
        Assert.assertNotNull(creditSchemeDBBean.getId());
    }

    @After
    public void tearDown(){

        //Delete incentive scheme
        incentiveSchemeDao.deleteIncentiveScheme(incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, 1));
    }

    @Test
    public void getIncentiveSchemeTest() throws Exception{
        //Get scheme using incentiveSchemeID
        Scheme scheme = referralSchemeService.getIncentiveScheme(incentiveSchemeId, 1);
        Assert.assertNotNull(scheme);
    }

    @Test
    public void addIncentiveSchemeTest() throws Exception{

        //Create scheme
        List<String> country = new ArrayList<String>();
        country.add("US");
        country.add("UK");
        country.add("IN");

        List<String> currency = new ArrayList<String>();
        currency.add("USD");

        Scheme scheme = createScheme(country,currency);

        Scheme outPutScheme = referralSchemeService.addIncentiveScheme(scheme, 1);
        Assert.assertNotNull(outPutScheme);
    }

    @Test
    public void updateIncentiveSchemeTest() throws Exception{

        //Create scheme
        //Create scheme
        List<String> country = new ArrayList<String>();
        country.add("US");
        country.add("UK");
        country.add("IN");

        List<String> currency = new ArrayList<String>();
        currency.add("USD");

        Scheme scheme = createScheme(country,currency);

        Scheme outPutScheme = referralSchemeService.updateIncentiveScheme(incentiveSchemeId,scheme, 1);
        Assert.assertNotNull(outPutScheme);

    }

    @Test
    public void getAllIncentiveSchemeForCountryTest() throws Exception{

        //Add scheme with new countries
        List<String> country = new ArrayList<String>();
        country.add("US");
        country.add("UK");
        country.add("IN");

        List<String> currency = new ArrayList<String>();
        currency.add("USD");

        Scheme scheme = createScheme(country,currency);

        Scheme outPutScheme = referralSchemeService.addIncentiveScheme(scheme, 1);

        int incentiveSchemeId = outPutScheme.getIncentiveSchemeId();
        Assert.assertNotNull(outPutScheme);

        PageInfo pageInfoInput = new PageInfo();
        pageInfoInput.setCount(10);
        pageInfoInput.setStart(0);

        List<Scheme> anotherScheme = referralSchemeService.getAllIncentiveScheme("US", "YES", pageInfoInput, null, null, 1);
        Assert.assertNotNull(anotherScheme);

        incentiveSchemeDao.deleteIncentiveScheme(incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, 1));

    }

    @Test
    public void getActiveIncentiveSchemeTest() throws Exception{
        Scheme activeScheme = referralSchemeService.getActiveScheme("US","USD", 1);
        Assert.assertNotNull(activeScheme);
    }

    @Test
    public void getSchemeForReferralSchemeDetailsTest() throws Exception{

        CustomerDBBean customerDBBean = CustomerDaoTest.createCustomerData(114,1);
        customerDao.createCustomerInfo(customerDBBean);

        String referralCode = "pradeep123";

        ReferralCodeDBBean referralCodeDBBean = new ReferralCodeDBBean();
        referralCodeDBBean.setIncentiveSchemeId(incentiveSchemeId);
        referralCodeDBBean.setCustomerId(customerDBBean.getId());
        referralCodeDBBean.setIsActive((byte) 1);
        referralCodeDBBean.setReferralCode(referralCode);
        referralCodeDBBean.setChannel("MOBILE");


        referralCodeDao.addReferralCode(referralCodeDBBean);


        Scheme scheme = referralSchemeService.getSchemeForReferralSchemeDetails(referralCode,"USD", 1);
        Assert.assertNotNull(scheme);
    }

    @Test
    public void deleteIncentiveSchemeTest(){

    }

    public static Scheme createScheme(List<String> countryList, List<String> currencyList){

        Date date = new Date();
        Timestamp currentTimeStamp = new Timestamp(date.getTime());
        String currentTimeStampStr = CommonUtils.convertTimestampToString(currentTimeStamp);

        List<CreditScheme> creditSchemeList = new ArrayList<CreditScheme>();

        for(String currency : currencyList){

            CreditScheme creditScheme = new CreditScheme();
            creditScheme.setNewCustomerAmount(100);
            creditScheme.setAdvocateAmount(100);
            creditScheme.setCurrency(currency);
            creditSchemeList.add(creditScheme);
        }

        Scheme scheme = new Scheme("SchemeDetail",
                0,
                currentTimeStampStr,
                currentTimeStampStr,
                countryList,
                180,
                180,
                true,
                creditSchemeList);

        return scheme;
    }

}
