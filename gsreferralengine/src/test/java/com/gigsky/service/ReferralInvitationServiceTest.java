package com.gigsky.service;

import com.gigsky.common.CommonUtils;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.IncentiveSchemeDBBean;
import com.gigsky.database.bean.ReferralCodeDBBean;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.IncentiveSchemeDao;
import com.gigsky.database.dao.InvitationDao;
import com.gigsky.database.dao.ReferralCodeDao;
import com.gigsky.rest.bean.Invite;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prsubbareddy on 06/01/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
@Ignore
public class ReferralInvitationServiceTest {

    @Autowired
    private ReferralInviteService referralInviteService;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    ReferralCodeDao referralCodeDao;

    @Autowired
    IncentiveSchemeDao incentiveSchemeDao;

    @Autowired
    InvitationDao invitationDao;

    private CustomerDBBean customerDBBean;
    private IncentiveSchemeDBBean incentiveSchemeDBBean;
    private ReferralCodeDBBean defaultReferralCodeBean;
    private ReferralCodeDBBean emailReferralCodeBean;

    @Before
    public void init() throws Exception{

        long gsCustomerId = 1234;
        customerDBBean = new CustomerDBBean();
        customerDBBean.setEmailId("test123@gigsky.com");
        customerDBBean.setGsCustomerId(gsCustomerId);
        customerDBBean.setLanguage("en");
        int customerId = customerDao.createCustomerInfo(customerDBBean);

        incentiveSchemeDBBean = new IncentiveSchemeDBBean();
        incentiveSchemeDBBean.setAdvocateCreditExpiryPeriod(180);
        incentiveSchemeDBBean.setReferralCreditExpiryPeriod(180);
        incentiveSchemeDBBean.setValidityStartDate(CommonUtils.getCurrentDate());
        incentiveSchemeDBBean.setValidityStartDate(CommonUtils.getCurrentDate());
        incentiveSchemeDBBean.setSchemeEnabled((byte) 1);

        int incentiveSchemeId = incentiveSchemeDao.addIncentiveScheme(incentiveSchemeDBBean);

        defaultReferralCodeBean = new ReferralCodeDBBean();
        defaultReferralCodeBean.setChannel("DEFAULT");
        defaultReferralCodeBean.setReferralCode("testGSD");
        defaultReferralCodeBean.setCustomerId(customerId);
        defaultReferralCodeBean.setIncentiveSchemeId(incentiveSchemeId);
        defaultReferralCodeBean.setIsActive((byte) 1);

        referralCodeDao.addReferralCode(defaultReferralCodeBean);

        emailReferralCodeBean = new ReferralCodeDBBean();
        emailReferralCodeBean.setChannel("EMAIL");
        emailReferralCodeBean.setReferralCode("refGSE");
        emailReferralCodeBean.setCustomerId(customerId);
        emailReferralCodeBean.setIncentiveSchemeId(incentiveSchemeId);
        emailReferralCodeBean.setIsActive((byte) 1);

        referralCodeDao.addReferralCode(emailReferralCodeBean);
    }

    @After
    public void tearDown() throws Exception{
        referralCodeDao.deleteReferralCode(defaultReferralCodeBean);
        referralCodeDao.deleteReferralCode(emailReferralCodeBean);
        customerDao.deleteCustomerInfo(customerDBBean);
        incentiveSchemeDao.deleteIncentiveScheme(incentiveSchemeDBBean);
    }

    private Invite createInvite()
    {
        Invite newInvite = new Invite();
        newInvite.setType("InviteDetail");
        List<String> emailIds = new ArrayList<String>();
        emailIds.add(0, "subba@gigsky.com");
        emailIds.add(1, "vinay@gigsky.com");
        newInvite.setEmailIds(emailIds);
        newInvite.setUserAgent("Mozilla");
        return newInvite;
    }
}
