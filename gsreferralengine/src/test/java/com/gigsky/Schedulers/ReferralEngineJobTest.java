package com.gigsky.Schedulers;

import com.gigsky.common.CommonUtils;
import com.gigsky.database.bean.IncentiveSchemeDBBean;
import com.gigsky.database.bean.ValidCountryDBBean;
import com.gigsky.database.dao.IncentiveSchemeDao;
import com.gigsky.database.dao.ValidCountryDao;
import com.gigsky.scheduler.ReferralEngineJob;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Date;

/**
 * Created by pradeepragav on 20/02/17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
@Ignore
public class ReferralEngineJobTest {

    @Autowired
    ReferralEngineJob referralEngineJob;

    @Autowired
    IncentiveSchemeDao incentiveSchemeDao;

    @Autowired
    ValidCountryDao validCountryDao;

    private int incentiveSchemeId;
    private int validCountryId;
    private int tenantId;

    //Incentivescheme validation test cases
    //1. DeActivate expired schemes
    //2. Activate schemes which are starting today
    //3. There should be only one incentiveScheme for a country

    @Before
    public void addIncentiveSchemes() throws Exception{
        tenantId =1;
        incentiveSchemeId = incentiveSchemeDao.addIncentiveScheme(
                getIncentiveScheme(CommonUtils.getCurrentDate(), CommonUtils.getCurrentDate(),(byte) 1,(byte) 0));

        ValidCountryDBBean validCountryDBBean = new ValidCountryDBBean();
        validCountryDBBean.setCountry("US");
        validCountryDBBean.setIncentiveSchemeId(incentiveSchemeId);

        validCountryId = validCountryDao.addValidCountry(validCountryDBBean);
    }

    //Test startDate and date as current date
    @Test
    public void validateIncentiveSchemesTestOne() throws Exception{

        //Today
        Date startDate = CommonUtils.getCurrentDate();

        //Today
        Date endDate = CommonUtils.getCurrentDate();

        //Add incentiveScheme
        IncentiveSchemeDBBean incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);
        incentiveSchemeDBBean.setActiveScheme((byte) 0);
        incentiveSchemeDBBean.setSchemeEnabled((byte) 1);
        incentiveSchemeDBBean.setValidityStartDate(startDate);
        incentiveSchemeDBBean.setValidityEndDate(endDate);

        //Update incentiveScheme
        incentiveSchemeDao.updateIncentiveScheme(incentiveSchemeDBBean);

        //Check the status of incentiveScheme
        incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);

        //Run validator
        referralEngineJob.validateIncentiveSchemes();

        //Check the status of incentiveScheme
        incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);

        boolean isActive = (incentiveSchemeDBBean.getActiveScheme() == 1);
        Assert.assertTrue(isActive);
    }

    //Today + today+X
    @Test
    public void validateIncentiveSchemesTestTwo()throws Exception{

        //Today
        Date startDate = CommonUtils.getCurrentDate();

        //Today + X
        Date endDate =  new Date(startDate.getTime() + 86400000);

        //Add incentiveScheme
        IncentiveSchemeDBBean incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);
        incentiveSchemeDBBean.setActiveScheme((byte) 0);
        incentiveSchemeDBBean.setSchemeEnabled((byte) 1);
        incentiveSchemeDBBean.setValidityStartDate(startDate);
        incentiveSchemeDBBean.setValidityEndDate(endDate);

        //Update incentiveScheme
        incentiveSchemeDao.updateIncentiveScheme(incentiveSchemeDBBean);

        //Run validator
        referralEngineJob.validateIncentiveSchemes();

        //Check the status of incentiveScheme
        incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);

        boolean isActive = (incentiveSchemeDBBean.getActiveScheme() == 1);
        Assert.assertTrue(isActive);
    }

    @Test
    public void validateIncentiveSchemesTestThree()throws Exception{

        Date currentDate = CommonUtils.getCurrentDate();

        //Today - X
        Date startDate = new Date((currentDate.getTime() - 86400000));

        //Today + X
        Date endDate =  new Date((currentDate.getTime() + 86400000));

        //Add incentiveScheme
        IncentiveSchemeDBBean incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);
        incentiveSchemeDBBean.setActiveScheme((byte) 0);
        incentiveSchemeDBBean.setSchemeEnabled((byte) 1);
        incentiveSchemeDBBean.setValidityStartDate(startDate);
        incentiveSchemeDBBean.setValidityEndDate(endDate);

        //Update incentiveScheme
        incentiveSchemeDao.updateIncentiveScheme(incentiveSchemeDBBean);

        //Run validator
        referralEngineJob.validateIncentiveSchemes();

        //Check the status of incentiveScheme
        incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);

        boolean isActive = (incentiveSchemeDBBean.getActiveScheme() == 1);
        Assert.assertTrue(isActive);
    }

    @Test
    public void validateIncentiveSchemesTestFour()throws Exception{

        Date currentDate = CommonUtils.getCurrentDate();

        //Today - X
        Date startDate = new Date((currentDate.getTime() - 86400000));

        //Today
        Date endDate =  currentDate;

        //Add incentiveScheme
        IncentiveSchemeDBBean incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);
        incentiveSchemeDBBean.setActiveScheme((byte) 0);
        incentiveSchemeDBBean.setSchemeEnabled((byte) 1);
        incentiveSchemeDBBean.setValidityStartDate(startDate);
        incentiveSchemeDBBean.setValidityEndDate(endDate);

        //Update incentiveScheme
        incentiveSchemeDao.updateIncentiveScheme(incentiveSchemeDBBean);

        //Run validator
        referralEngineJob.validateIncentiveSchemes();

        //Check the status of incentiveScheme
        incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);

        boolean isActive = (incentiveSchemeDBBean.getActiveScheme() == 1);
        Assert.assertTrue(isActive);
    }

    @Test
    public void validateIncentiveSchemesTestFive()throws Exception{

        Date currentDate = CommonUtils.getCurrentDate();

        //Today - X
        Date startDate = new Date((currentDate.getTime() - 86400000*2));

        //Today - Y
        Date endDate =  new Date((currentDate.getTime() - 86400000));

        //Add incentiveScheme
        IncentiveSchemeDBBean incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);
        incentiveSchemeDBBean.setActiveScheme((byte) 0);
        incentiveSchemeDBBean.setSchemeEnabled((byte) 1);
        incentiveSchemeDBBean.setValidityStartDate(startDate);
        incentiveSchemeDBBean.setValidityEndDate(endDate);

        //Update incentiveScheme
        incentiveSchemeDao.updateIncentiveScheme(incentiveSchemeDBBean);

        //Run validator
        referralEngineJob.validateIncentiveSchemes();

        //Check the status of incentiveScheme
        incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);

        boolean isActive = (incentiveSchemeDBBean.getActiveScheme() == 1);
        Assert.assertFalse(isActive);
    }


    @Test
    public void validateIncentiveSchemesTestSix()throws Exception{

        Date currentDate = CommonUtils.getCurrentDate();

        //Today - X
        Date startDate = new Date((currentDate.getTime() + 86400000));

        //Today - Y
        Date endDate =  new Date((currentDate.getTime() + 86400000*2));

        //Add incentiveScheme
        IncentiveSchemeDBBean incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);
        incentiveSchemeDBBean.setActiveScheme((byte) 0);
        incentiveSchemeDBBean.setSchemeEnabled((byte) 1);
        incentiveSchemeDBBean.setValidityStartDate(startDate);
        incentiveSchemeDBBean.setValidityStartDate(endDate);

        //Update incentiveScheme
        incentiveSchemeDao.updateIncentiveScheme(incentiveSchemeDBBean);

        //Run validator
        referralEngineJob.validateIncentiveSchemes();

        //Check the status of incentiveScheme
        incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);

        boolean isActive = (incentiveSchemeDBBean.getActiveScheme() == 1);
        Assert.assertTrue(isActive);
    }


    @Test
    public void validateIncentiveSchemesTestSeven()throws Exception{

        Date currentDate = CommonUtils.getCurrentDate();

        //Yesterday
        Date startDate = new Date((currentDate.getTime() - 86400000));

        //Yesterday
        Date endDate =  new Date((currentDate.getTime() - 86400000));

        //Add incentiveScheme
        IncentiveSchemeDBBean incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);
        incentiveSchemeDBBean.setActiveScheme((byte) 0);
        incentiveSchemeDBBean.setSchemeEnabled((byte) 1);
        incentiveSchemeDBBean.setValidityStartDate(startDate);
        incentiveSchemeDBBean.setValidityStartDate(endDate);

        //Update incentiveScheme
        incentiveSchemeDao.updateIncentiveScheme(incentiveSchemeDBBean);

        //Run validator
        referralEngineJob.validateIncentiveSchemes();

        //Check the status of incentiveScheme
        incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);

        boolean isActive = (incentiveSchemeDBBean.getActiveScheme() == 1);
        Assert.assertTrue(isActive);
    }

    @After
    public void deleteIncentiveSchemeEntries() throws Exception{

        //Delete all the entries in incentive Scheme table
        validCountryDao.deleteValidCountry(validCountryDao.getValidCountry(validCountryId));
        incentiveSchemeDao.deleteIncentiveScheme(incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId));
    }

    private IncentiveSchemeDBBean getIncentiveScheme(Date startDate, Date endDate, byte schemeEnabled, byte isActive){

        IncentiveSchemeDBBean incentiveSchemeDBBean = new IncentiveSchemeDBBean();
        incentiveSchemeDBBean.setActiveScheme((byte)0);
        incentiveSchemeDBBean.setSchemeEnabled((byte)1);
        incentiveSchemeDBBean.setValidityEndDate(endDate);
        incentiveSchemeDBBean.setValidityStartDate(startDate);

        return incentiveSchemeDBBean;
    }
}
