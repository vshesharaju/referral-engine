package com.gigsky.backend.customer;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.rest.bean.Customer;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.util.GSTokenUtil;
import com.gigsky.util.GSUtil;
import com.gigsky.util.HttpClientUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vinayr on 11/01/17.
 */
public class CustomerManagementImpl implements CustomerManagementInterface {

    private static final Logger logger = LoggerFactory.getLogger(CustomerManagementImpl.class);

    @Autowired
    ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    GSTokenUtil gsTokenUtil;

    @Autowired
    Configurations configurations;

    @Autowired
    HttpClientUtil httpClientUtil;

    public Customer getCustomerDetails(int gsCustomerId, String token, int tenantId) throws BackendRequestException
    {

        //Token is not sent, then use the token stored in DB
        if(token == null) {
            token = gsTokenUtil.getValidToken(tenantId);
        }

        //Create URL string for call account details api
        String url = configurations.getBackendBaseUrl() + "account/" + gsCustomerId;

        long timeOutInMs = configurations.getRequestTimeOut();

        //Create headers
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Basic " +token);
        headers.put(GSUtil.TENANT_ID_HEADER, String.valueOf(tenantId));

        try {

            //Call the api, and get the response
            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                    httpClientUtil.sendHttpRequest("GET", url, null, null, headers, timeOutInMs);

            //Check for null
            if( gsRestHttpResponse != null ){

                //Deserialize the response body
                Customer customer = (Customer)HttpClientUtil.convertToBean(gsRestHttpResponse.getResponseBody(), Customer.class);

                // If backend not return any response, returning as backend request failure
                if(customer == null) {
                    throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);

                    // if response has type error
                } else if("error".equals(customer.getType())) {

                    // If response has errorCode as 2001 should throw invalid token
                    if(customer.getErrorInt() == 2001) {
                        throw new BackendRequestException(ErrorCode.INVALID_TOKEN);
                    } else {
                        // any other error types should return backed request failure.
                        throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
                    }
                }

                return customer;
            }

            return null;
        }
        catch (Exception e){

            logger.error("GetCustomerDetails exception: " + e, e);
            if(e instanceof BackendRequestException){
                 throw new BackendRequestException(((BackendRequestException) e).getErrorCode());
            } else{
                throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
            }
        }
    }

}
