package com.gigsky.backend.customer;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.database.bean.MockCustomerDBBean;
import com.gigsky.database.dao.MockCustomerDao;
import com.gigsky.rest.bean.Customer;
import com.gigsky.rest.exception.ErrorCode;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by vinayr on 11/01/17.
 */
public class MockCustomerManagementImpl implements CustomerManagementInterface {

    @Autowired
    private MockCustomerDao mockCustomerDao;

    public Customer getCustomerDetails(int gsCustomerId, String token, int tenantId) throws BackendRequestException
    {
        //Fetch the customer details from the MockCustomer table
        MockCustomerDBBean mockCustomerDBBean = mockCustomerDao.getCustomer(gsCustomerId, tenantId);

        if (mockCustomerDBBean != null)
        {
            //Populate the Customer bean
            Customer customer = new Customer();
            customer.setCustomerId(gsCustomerId);
            customer.setEmailId(mockCustomerDBBean.getEmail());
            customer.setFirstName(mockCustomerDBBean.getFirstName());
            customer.setLastName(mockCustomerDBBean.getLastName());
            customer.setCountry(mockCustomerDBBean.getCountry());
            customer.setPreferredCurrency(mockCustomerDBBean.getPreferredCurrency());
            customer.setAccountActivationStatus(mockCustomerDBBean.getStatus());
            customer.setCreatedOn(mockCustomerDBBean.getCreatedOn());

            return customer;
        } else {
            throw new BackendRequestException(ErrorCode.CUSTOMER_DOES_NOT_EXIST);
        }
    }
}
