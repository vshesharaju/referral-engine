package com.gigsky.backend.customer;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.rest.bean.Customer;

/**
 * Created by vinayr on 11/01/17.
 */
public interface CustomerManagementInterface {

    //TODO: To call this API at one place once accountInfo change event is implemented
    public Customer getCustomerDetails(int gsCustomerId, String token, int tenantId) throws BackendRequestException;
}
