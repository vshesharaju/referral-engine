package com.gigsky.backend.beans;

/**
 * Created by pradeepragav on 28/05/19.
 */
public class MockSimDBBean {
    private int id;
    private String simId;
    private Integer mockCustomerId;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSimId() {
        return simId;
    }

    public void setSimId(String simId) {
        this.simId = simId;
    }

    public int getMockCustomerId() {
        return mockCustomerId;
    }

    public void setMockCustomerId(int mockCustomerId) {
        this.mockCustomerId = mockCustomerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MockSimDBBean that = (MockSimDBBean) o;

        if (id != that.id) return false;
        if (simId != null ? !simId.equals(that.simId) : that.simId != null) return false;
        if (mockCustomerId != null ? !mockCustomerId.equals(that.mockCustomerId) : that.mockCustomerId != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (simId != null ? simId.hashCode() : 0);
        result = 31 * result + (mockCustomerId != null ? mockCustomerId.hashCode() : 0);
        return result;
    }

}
