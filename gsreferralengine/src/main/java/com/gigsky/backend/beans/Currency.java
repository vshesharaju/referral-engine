package com.gigsky.backend.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by pradeepragav on 30/03/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Currency {

    private String type;
    private String code;
    private String currency;
    private String currencySymbol;
    private Float usdMultiplier;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    public Float getUsdMultiplier() {
        return usdMultiplier;
    }

    public void setUsdMultiplier(Float usdMultiplier) {
        this.usdMultiplier = usdMultiplier;
    }
}
