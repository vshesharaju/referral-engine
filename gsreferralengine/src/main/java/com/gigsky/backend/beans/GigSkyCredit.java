package com.gigsky.backend.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by pradeepragav on 15/02/17.
 *
 * Used for
 * "AddGigskyCredit" request and
 * "DeductGigskyCredit" request
 */

@JsonInclude( JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class GigSkyCredit {

    private String type;
    private Float gigskyCredit;
    private Float gigskyBalance;
    private String currency;
    private String creditType;
    private String creditExpiry;
    private Integer creditTxnId;
    private String description;
    private Integer gigskyCreditId;
    private List<Integer> creditTxnIds;
    private Integer errorInt;

    public Integer getErrorInt() {
        return errorInt;
    }

    public void setErrorInt(Integer errorInt) {
        this.errorInt = errorInt;
    }

    public Float getGigskyCredit() {
        return gigskyCredit;
    }

    public void setGigskyCredit(Float gigskyCredit) {
        this.gigskyCredit = gigskyCredit;
    }

    public Float getGigskyBalance() {
        return gigskyBalance;
    }

    public void setGigskyBalance(Float gigskyBalance) {
        this.gigskyBalance = gigskyBalance;
    }

    public List<Integer> getCreditTxnIds() {
        return creditTxnIds;
    }

    public void setCreditTxnIds(List<Integer> creditTxnIds) {
        this.creditTxnIds = creditTxnIds;
    }

    public Integer getCreditTxnId() {
        return creditTxnId;
    }

    public void setCreditTxnId(Integer creditTxnId) {
        this.creditTxnId = creditTxnId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCreditType() {
        return creditType;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public String getCreditExpiry() {
        return creditExpiry;
    }

    public void setCreditExpiry(String creditExpiry) {
        this.creditExpiry = creditExpiry;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getGigskyCreditId() {
        return gigskyCreditId;
    }

    public void setGigskyCreditId(Integer gigskyCreditId) {
        this.gigskyCreditId = gigskyCreditId;
    }
}
