package com.gigsky.backend.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by pradeepragav on 29/03/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class SupportedCurrencies {

    String type;
    List<Currency>  list;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Currency> getList() {
        return list;
    }

    public void setList(List<Currency> list) {
        this.list = list;
    }

}
