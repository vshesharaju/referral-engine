package com.gigsky.backend.ngw;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.backend.ngw.beans.CustomerDetail;
import com.gigsky.backend.ngw.beans.Option;

import java.util.List;

/**
 * Created by vinayr on 03/02/17.
 */
public class MockNgwManagementImpl implements NgwManagementInterface {

    //To send email to customer
    public  void sendEmail(String emailType, List<String> emailIds, String language, Option option, int tenantId) throws BackendRequestException
    {

        return;
    }

    @Override
    public boolean isNGWServerRunning() throws BackendRequestException{

        return true;
    }

    @Override
    public void deleteCustomer(CustomerDetail customerDetail, int tenantId) throws BackendRequestException {
        return;
    }
}
