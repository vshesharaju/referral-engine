package com.gigsky.backend.ngw.beans;

/**
 * Created by vinayr on 03/02/17.
 */
public class Option {

    // Options for Email notification referral
    private String customerName;
    private String customerEmail;
    private String newCustomerName;
    private String newCustomerEmail;
    private int advocateAmount;
    private int referralAmount;
    private String referralCode;
    private String currency;
    private int creditLimit;
    private int creditWarningLimit;
    private int gsCustomerId;
    private int creditDeduct;
    private String csn;
    private int promoAmount;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getNewCustomerName() {
        return newCustomerName;
    }

    public void setNewCustomerName(String newCustomerName) {
        this.newCustomerName = newCustomerName;
    }

    public String getNewCustomerEmail() {
        return newCustomerEmail;
    }

    public void setNewCustomerEmail(String newCustomerEmail) {
        this.newCustomerEmail = newCustomerEmail;
    }

    public int getAdvocateAmount() {
        return advocateAmount;
    }

    public void setAdvocateAmount(int advocateAmount) {
        this.advocateAmount = advocateAmount;
    }

    public int getReferralAmount() {
        return referralAmount;
    }

    public void setReferralAmount(int referralAmount) {
        this.referralAmount = referralAmount;
    }

    public int getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(int creditLimit) {
        this.creditLimit = creditLimit;
    }

    public int getCreditWarningLimit() {
        return creditWarningLimit;
    }

    public void setCreditWarningLimit(int creditWarningLimit) {
        this.creditWarningLimit = creditWarningLimit;
    }

    public int getGsCustomerId() {
        return gsCustomerId;
    }

    public void setGsCustomerId(int gsCustomerId) {
        this.gsCustomerId = gsCustomerId;
    }

    public int getCreditDeduct() {
        return creditDeduct;
    }

    public void setCreditDeduct(int creditDeduct) {
        this.creditDeduct = creditDeduct;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCsn() {
        return csn;
    }

    public void setCsn(String csn) {
        this.csn = csn;
    }

    public int getPromoAmount() {
        return promoAmount;
    }

    public void setPromoAmount(int promoAmount) {
        this.promoAmount = promoAmount;
    }
}
