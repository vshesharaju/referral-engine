package com.gigsky.backend.ngw;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.backend.ngw.beans.CustomerDetail;
import com.gigsky.backend.ngw.beans.Option;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.PreferenceDBBean;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.PreferenceDao;
import com.gigsky.database.dao.TenantConfigurationsKeyValueDao;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.util.GSTaskUtil;
import com.gigsky.util.GSTokenUtil;
import com.gigsky.util.GSUtil;
import com.gigsky.util.HttpClientUtil;
import com.gigsky.util.bean.Event;
import org.apache.commons.httpclient.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vinayr on 03/02/17.
 */
public class NgwManagementImpl implements NgwManagementInterface {

    private static final Logger logger = LoggerFactory.getLogger(NgwManagementImpl.class);

    @Autowired
    ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    PreferenceDao preferenceDao;

    @Autowired
    Configurations configurations;

    @Autowired
    HttpClientUtil httpClientUtil;

    @Autowired
    GSTaskUtil gsTaskUtil;

    @Autowired
    GSTokenUtil gsTokenUtil;

    @Autowired
    TenantConfigurationsKeyValueDao tenantConfigurationsKeyValueDao;

    //To send email to customer
    public  void sendEmail(String emailType, List<String> emailIds, String language, Option option, int tenantId) throws BackendRequestException
    {

        //If referral engine enabled, will process sending emails.
        // Should support promotions email tenant specific..  Soon
        if(gsTaskUtil.checkReferralEnable(tenantId))
        {
            //If email has optout "true", we are ignoring those emails for final list
            List<String>  emailsList = finalListEmails(emailIds, tenantId);

            // if emails list size while sending request to NGW
            if(emailsList.size() > 0) {
                try {

                    // preparing event post request
                    Event event = new Event();
                    event.setType("EventDetail");
                    event.setEventType("EMAIL");
                    event.setSubEventType(emailType);
                    event.setEmailIds(emailIds);
                    event.setLanguage(language);
                    event.setOptions(option);

                    // events creation url for NGW
                    String createEventUrl = configurations.getNGWBaseUrl() + "events";

                    // fetch request time out from configuration key values, if it's not available set default value
                    long requestTimeOut = configurations.getRequestTimeOut();

                    //Create headers
                    Map<String, String> headers = new HashMap<String, String>();
                    headers.put(GSUtil.TENANT_ID_HEADER, String.valueOf(tenantId));

                    // sending email event to notification gateway server
                    httpClientUtil.sendHttpRequest("POST", createEventUrl, null, event, headers, requestTimeOut);

                } catch(Exception e) {
                    logger.error("send email for notification gateway API exception: " +e, e);
                    String exceptionString = "send email for notification gateway API exception: " +e;

                    throw new BackendRequestException(ErrorCode.GATEWAY_REQUEST_FAILURE);
                }
            }
        }

    }

    @Override
    public boolean isNGWServerRunning() throws BackendRequestException{

        try {
            // getting NGW base url from configuration key values
            String buildInfoUrl = configurations.getNGWBaseUrl() + "info";

            // fetch request time out from configuration key values, if it's not available set default value
            long requestTimeOut = configurations.getRequestTimeOut();

            HttpClientUtil.GSRestHttpResponse response = httpClientUtil.sendHttpRequest("GET", buildInfoUrl, null, requestTimeOut);
            if(response.getHttpCode() == HttpStatus.SC_OK) {
                return true;
            }
        } catch(Exception e) {
            logger.error("getting Notification Gateway API info exception: " +e, e);

            String exceptionString = "getting Notification Gateway API info exception: " +e;

            throw new BackendRequestException(ErrorCode.GATEWAY_REQUEST_FAILURE);
        }
        return false;

    }

    public void deleteCustomer(CustomerDetail customerDetail, int tenantId) throws BackendRequestException {
        //Call notification gateway's 'Delete customer details' REST API
        String url = configurations.getNGWBaseUrl() + "customers";

        //Get token
        String token = gsTokenUtil.getValidToken(tenantId);

        long timeOutInMs = configurations.getRequestTimeOut();

        //Create headers
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Basic " +token);
        headers.put(GSUtil.TENANT_ID_HEADER, String.valueOf(tenantId));
        String method = "DELETE";

        try
        {
            //Call the API with headers and post body
            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                    httpClientUtil.sendHttpRequest(method, url, null, customerDetail, headers, timeOutInMs);

            if(gsRestHttpResponse != null && gsRestHttpResponse.getHttpCode() == HttpStatus.SC_OK)
            {
                //API Successful
                logger.info("Delete Customer API successful");
            }
            else
            {
                logger.info("Delete Customer API failed");
                throw new BackendRequestException(ErrorCode.GATEWAY_REQUEST_FAILURE);
            }
        }
        catch (Exception e)
        {
            logger.info("Delete Customer API failed");
            throw new BackendRequestException(ErrorCode.GATEWAY_REQUEST_FAILURE);
        }
    }


    private List<String> finalListEmails(List<String> emailIds, int tenantId) {

        //Get adminEmailId using configurationKeyValues
        String adminEmailId = tenantConfigurationsKeyValueDao
                .getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.GS_ADMIN_EMAIL_ID, tenantId);

        List<String> finalEmailsList = new ArrayList<String>();

        for(String emailId: emailIds){

            if(!isEmailOptOut(emailId, adminEmailId, tenantId)) {
                finalEmailsList.add(emailId);
            }
        }
        return finalEmailsList;
    }


    private boolean isEmailOptOut(String emailId, String adminEmailId, int tenantId) {

        // If email is is admin email, optout should be false.
        if(emailId.equals(adminEmailId))
            return false;

        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByEmail(emailId, tenantId);
        PreferenceDBBean preferenceDBBean = preferenceDao.getPreferenceByKey(customerDBBean.getId(), Configurations.PreferrenceKeyType.OPT_OUT);
        return preferenceDBBean.getValue().equals("true");
    }

}
