package com.gigsky.backend.ngw.beans;

/**
 * Created by pradeepragav on 12/06/18.
 */
//{
//        "type": "CustomerDetail",
//        "oldEmailId": "abc@gs.com",
//        "anonymousEmailId": "xyz@xxx.com"
//        }
public class CustomerDetail {

   String type;
   String oldEmailId;
   String anonymousEmailId;

   public CustomerDetail(String oldEmailId, String anonymousEmailId)
   {
       this.type = "CustomerDetail";
       this.anonymousEmailId = anonymousEmailId;
       this.oldEmailId = oldEmailId;
   }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getOldEmailId() {
        return oldEmailId;
    }

    public void setOldEmailId(String oldEmailId) {
        this.oldEmailId = oldEmailId;
    }

    public String getAnonymousEmailId() {
        return anonymousEmailId;
    }

    public void setAnonymousEmailId(String anonymousEmailId) {
        this.anonymousEmailId = anonymousEmailId;
    }
}
