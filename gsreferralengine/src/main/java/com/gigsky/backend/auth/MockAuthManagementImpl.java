package com.gigsky.backend.auth;

import com.gigsky.backend.beans.MockSimDBBean;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.MockCustomerDBBean;
import com.gigsky.database.dao.MockCustomerDao;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by vinayr on 11/01/17.
 */
public class MockAuthManagementImpl implements AuthManagementInterface {

    @Autowired
    private MockCustomerDao mockCustomerDao;

    private static final Logger logger = LoggerFactory.getLogger(MockAuthManagementImpl.class);

    //Validate the token by fetching the customer details from the mock customer table
    public boolean validateCustomerToken(String token, int tenantId) throws ServiceException
    {
        //Get the customer details based on token
        MockCustomerDBBean mockCustomerDBBean = mockCustomerDao.getCustomer(token, tenantId);
        if (mockCustomerDBBean == null)
        {
            logger.error("Customer doesn't exists");
            throw new ServiceException(ErrorCode.CUSTOMER_DOES_NOT_EXIST);
        }

        return true;
    }

    //Validate the token by fetching the customer details from the mock customer table
    public boolean validateCustomerToken(String token, int gsCustomerId, int tenantId) throws ServiceException
    {
        MockCustomerDBBean customer = mockCustomerDao.getCustomer(gsCustomerId, tenantId);
        if(customer == null)
        {
            logger.error("Customer doesn't exists");
            throw new ServiceException(ErrorCode.CUSTOMER_DOES_NOT_EXIST);
        } else
        {
            if(!customer.getToken().equals(token))
            {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean validateCustomerSimId(String token, int gsCustomerId, int tenantId, String simId) {

        List<MockSimDBBean> mockSims = mockCustomerDao.getMockSims(gsCustomerId);

        for(MockSimDBBean mockSim : mockSims)
        {
            if(mockSim.getSimId().equals(simId)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean validateAdminPermission(String token, int tenantId) throws ServiceException
    {
        //Get the customer details based on token
        MockCustomerDBBean mockCustomerDBBean = mockCustomerDao.getCustomer(token, tenantId);
        if (mockCustomerDBBean != null && mockCustomerDBBean.getPermission().equals(Configurations.PERMISSION_GIGSKY_REFERRAL_SCHEME_EDIT))
        {
            return true;
        }
        return false;
    }

    @Override
    public boolean validateAdminReadPermission(String token, int tenantId) throws ServiceException
    {
        //Get the customer details based on token
        MockCustomerDBBean mockCustomerDBBean = mockCustomerDao.getCustomer(token, tenantId);
        if (mockCustomerDBBean != null &&
                (mockCustomerDBBean.getPermission().equals(Configurations.PERMISSION_GIGSKY_REFERRAL_SCHEME_READ)||
                mockCustomerDBBean.getPermission().equals(Configurations.PERMISSION_GIGSKY_REFERRAL_SCHEME_EDIT)))
        {
            return true;
        }
        return false;
    }
}
