package com.gigsky.backend.auth;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.service.exception.ServiceException;

/**
 * Created by vinayr on 11/01/17.
 */
public interface AuthManagementInterface {

    public boolean validateCustomerToken(String token, int tenantId) throws ServiceException, BackendRequestException;

    public boolean validateCustomerToken(String token, int gsCustomerId, int tenantId) throws ServiceException, BackendRequestException;

    public boolean validateCustomerSimId(String token, int gsCustomerId, int tenantId, String simId) throws BackendRequestException;

    public boolean validateAdminPermission(String token, int tenantId) throws ServiceException, BackendRequestException;

    public boolean validateAdminReadPermission(String token, int tenantId) throws ServiceException, BackendRequestException;
}
