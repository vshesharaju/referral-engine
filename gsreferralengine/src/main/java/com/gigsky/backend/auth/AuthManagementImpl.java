package com.gigsky.backend.auth;

import com.gigsky.backend.beans.RolesResponse;
import com.gigsky.backend.beans.SimAssociationStatus;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSUtil;
import com.gigsky.util.HttpClientUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by vinayr on 11/01/17.
 */
public class AuthManagementImpl implements AuthManagementInterface {

    private static final Logger logger = LoggerFactory.getLogger(AuthManagementImpl.class);

    //Response of successful request of Get roles on user API
    private static final String ROLES_SUCCESS = "RolesResponse";

    private static final String ASSOCIATED_CURRENT_ACCOUNT = "ASSOCIATED_CURRENT_ACCOUNT";

    @Autowired
    ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    Configurations configurations;

    @Autowired
    HttpClientUtil httpClientUtil;

    public boolean validateCustomerToken(String token, int tenantId) throws ServiceException, BackendRequestException
    {
        //Call "Get User Account Role" api
        RolesResponse rolesResponse = getResponse(token, tenantId);

        //If we get the response, then the token is valid
        if(rolesResponse != null &&
                ROLES_SUCCESS.equals(rolesResponse.getType()))
            return true;

        return false;
    }

    public boolean validateCustomerToken(String token, int gsCustomerId, int tenantId) throws ServiceException, BackendRequestException
    {
        return validateCustomerToken(token, tenantId);
    }


    @Override
    public boolean validateCustomerSimId(String token, int gsCustomerId, int tenantId, String simId) throws BackendRequestException {

        String url = configurations.getBackendBaseUrl()+"account/"+gsCustomerId+"/SIM/"+simId+"/associationStatus";

        //Create headers map
        Map<String, String> headers = new HashMap<String, String>();
        headers.put(GSUtil.AUTHENTICATION_HEADER, "Basic " +token);
        headers.put(GSUtil.TENANT_ID_HEADER, Integer.toString(tenantId));

        //Get the timeout in ms from ConfigurationsKeyValue
        long timeOutInMs = configurations.getRequestTimeOut();

        try {

            //Call the api
            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                    httpClientUtil.sendHttpRequest("GET", url, null, null, headers, timeOutInMs);

            if (gsRestHttpResponse != null) {

                //Deserialize the response
                SimAssociationStatus simAssociationStatus =
                        (SimAssociationStatus) HttpClientUtil.convertToBean(gsRestHttpResponse.getResponseBody(), SimAssociationStatus.class);

                if(simAssociationStatus == null || (simAssociationStatus != null && "error".equals(simAssociationStatus.getType())))
                {
                    throw new BackendRequestException((ErrorCode.BACKEND_REQUEST_FAILURE));
                }
                else
                {
                    if(ASSOCIATED_CURRENT_ACCOUNT.equals(simAssociationStatus.getStatus()))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

        }
        catch (Exception e){

            logger.error(" SIM association status api exception " + e, e);

            if(e instanceof BackendRequestException){
                throw new BackendRequestException(((BackendRequestException) e).getErrorCode());
            } else{
                throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
            }
        }

        return false;
    }

    @Override
    public boolean validateAdminPermission(String token, int tenantId) throws ServiceException, BackendRequestException
    {
        //Call "Get User Account Role" api
        RolesResponse rolesResponse = getResponse(token, tenantId);

        if(rolesResponse != null){

            //Response is not error
            if(ROLES_SUCCESS.equals(rolesResponse.getType())){

                //Get the permission list from the response
                List permissionsList = (List) rolesResponse.getRole_group().get("permissionsList");

                //Check if edit permission is in the user's permission list
                if(permissionsList != null &&
                        permissionsList.size() != 0 &&
                            permissionsList.contains(Configurations.PERMISSION_GIGSKY_REFERRAL_SCHEME_EDIT)){
                    return true;
                }
            }
        }


        return false;
    }

    @Override
    public boolean validateAdminReadPermission(String token, int tenantId) throws ServiceException, BackendRequestException
    {
        //Call "Get User Account Role" api
        RolesResponse rolesResponse = getResponse(token, tenantId);

        if(rolesResponse != null){

            //Response is not error
            if(ROLES_SUCCESS.equals(rolesResponse.getType())){

                //Get the permission list from the response
                List permissionsList = (List) rolesResponse.getRole_group().get("permissionsList");

                //Check if edit permission is in the user's permission list
                if(permissionsList != null &&
                        permissionsList.size() != 0 &&
                        (permissionsList.contains(Configurations.PERMISSION_GIGSKY_REFERRAL_SCHEME_EDIT)||
                        permissionsList.contains(Configurations.PERMISSION_GIGSKY_REFERRAL_SCHEME_READ))){
                    return true;
                }
            }
        }


        return false;
    }

    //Get response for the api get user roles
    private RolesResponse getResponse(String token, int tenantId) throws BackendRequestException {

        ////Get url required for get user roles api
        String url = configurations.getBackendBaseUrl() + "account/roles";

        //Create headers map
        Map<String, String> headers = new HashMap<String, String>();
        headers.put(GSUtil.AUTHENTICATION_HEADER, "Basic " +token);
        headers.put(GSUtil.TENANT_ID_HEADER, Integer.toString(tenantId));

        //Get the timeout in ms from ConfigurationsKeyValue
        long timeOutInMs = configurations.getRequestTimeOut();

        try{

            //Call the api
            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                    httpClientUtil.sendHttpRequest("GET", url, null, null, headers, timeOutInMs);

            if(gsRestHttpResponse != null){

                //Deserialize the response
                RolesResponse rolesResponse =
                        (RolesResponse)HttpClientUtil.convertToBean(gsRestHttpResponse.getResponseBody(), RolesResponse.class);

                // If backend not return any response, returning as backend request failure
                if(rolesResponse == null) {
                    throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);

                // if response has type error
                } else if("error".equals(rolesResponse.getType())) {

                    // If response has errorCode as 2001 should return null,
                    // it will take care of handling invalid token
                    if(rolesResponse.getErrorInt() == 2001) {
                        return null;
                    } else {
                        // any other error types should return backed request failure.
                        throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
                    }
                }

                return rolesResponse;
            }
        }
        catch (Exception e){

            logger.error(" User roles api exception " + e, e);

            if(e instanceof BackendRequestException){
                throw new BackendRequestException(((BackendRequestException) e).getErrorCode());
            } else{
                throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
            }
        }

        return null;
    }
}
