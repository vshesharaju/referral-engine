package com.gigsky.backend.Info;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.util.HttpClientUtil;
import org.apache.commons.httpclient.HttpStatus;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by prsubbareddy on 10/02/17.
 */
public class BackendInfoManagementImpl implements BackendInfoManagementInterface {

    private static final Logger logger = LoggerFactory.getLogger(BackendInfoManagementImpl.class);

    @Autowired
    Configurations configurations;

    @Autowired
    HttpClientUtil httpClientUtil;

    public boolean verifyBackendServerIsRunning() throws BackendRequestException {

        try {
            // getting Backend base url from configuration key values
            String buildInfoUrl = configurations.getBackendBaseUrl() + "info";

            // fetch request time out from configuration key values, if it's not available set default value
            long requestTimeOut = configurations.getRequestTimeOut();

            HttpClientUtil.GSRestHttpResponse response = httpClientUtil.sendHttpRequest("GET", buildInfoUrl, null, requestTimeOut);
            if(response.getHttpCode() == HttpStatus.SC_OK) {
                return true;
            }
        } catch(Exception e) {
            logger.error("getting backend API info exception: " +e, e);
            String exceptionString = "getting backend API info exception: " +e;
            throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
        }
        return false;
    }

}
