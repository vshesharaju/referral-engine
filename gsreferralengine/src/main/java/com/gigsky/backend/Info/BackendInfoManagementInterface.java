package com.gigsky.backend.Info;

import com.gigsky.backend.exception.BackendRequestException;

/**
 * Created by prsubbareddy on 10/02/17.
 */
public interface BackendInfoManagementInterface {

    boolean verifyBackendServerIsRunning() throws BackendRequestException;

}
