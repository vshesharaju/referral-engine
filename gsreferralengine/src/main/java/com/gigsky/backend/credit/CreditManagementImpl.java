package com.gigsky.backend.credit;

import com.gigsky.backend.beans.GigSkyCredit;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.PreferenceDBBean;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.PreferenceDao;
import com.gigsky.rest.bean.Credit;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.util.GSTokenUtil;
import com.gigsky.util.GSUtil;
import com.gigsky.util.HttpClientUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by vinayr on 11/01/17.
 */
public class CreditManagementImpl implements CreditManagementInterface {

    private static final Logger logger = LoggerFactory.getLogger(CreditManagementImpl.class);

    public static final String CREDIT_ADD_REQ_TYPE = "AddGigskyCredit";

    public static final String CREDIT_DEDUCT_REQ_TYPE = "DeductGigskyCredit";

    public static final String CREDIT_DETAILS_REQ_TYPE = "GigskyCreditResponse";

    @Autowired
    Configurations configurations;

    @Autowired
    GSTokenUtil gsTokenUtil;

    @Autowired
    ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    PreferenceDao preferenceDao;

    @Autowired
    HttpClientUtil httpClientUtil;


    @Override
    public Credit getCreditDetailsOfCustomer(int gsCustomerId, int tenantId) throws BackendRequestException{


        //Get creditBalance details url using backend url
        String creditBalanceURL = "account/" + gsCustomerId + "/credit/gigskyCredit" + "?details=CREDIT_BALANCE";

        return callBackendCreditApi("GET", creditBalanceURL, null, CREDIT_DETAILS_REQ_TYPE, tenantId);
    }


    public Credit getCreditDetails(int gsCustomerId, int creditId, int tenantId) throws BackendRequestException
    {

        String baseUrl = configurations.getBackendBaseUrl();
        String url = baseUrl + "account/"+ gsCustomerId + "/credit/gigskyCredit/"+ creditId;

        long timeOut = configurations.getRequestTimeOut();

        try {

            //Get header map which includes Authorization token
            Map<String, String> headerMap = getHeaderMap(tenantId);

            //Call the api
            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse = httpClientUtil.sendHttpRequest("GET", url, null, null, headerMap, timeOut);

            //If the API call is successful
            if(gsRestHttpResponse != null
                    && gsRestHttpResponse.getErrResponseBody() == null){

                //Convert the responseBody string to object
                Credit creditResponse =
                        (Credit) HttpClientUtil.convertToBean(gsRestHttpResponse.getResponseBody(), Credit.class);


                //If error return null
                if("error".equals(creditResponse.getType())){

                    return null;
                }
                return  creditResponse;
            }

        }
        catch (Exception e){

            logger.error(" Get credit details error " + e, e);
            String exceptionString = " Get credit details error " + e;
            throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
        }
        return null;
    }

    @Override
    public Credit deductCredit(int gsCustomerId, int creditId, int tenantId) throws BackendRequestException{

        //Create requestCrest (Credit bean) with all the necessary values
        GigSkyCredit requestGigskyCredit = new GigSkyCredit();

        requestGigskyCredit.setType(CREDIT_DEDUCT_REQ_TYPE);
        requestGigskyCredit.setGigskyCreditId(creditId);

        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, tenantId);

        //TODO description
        // Check If customer language other than en, fetch localized string.
        requestGigskyCredit.setDescription("GigSky credit deduct");

        //Get preferred currency from preference entry for customer
        PreferenceDBBean preferenceDBBean = null;

        if(customerDBBean != null){
            preferenceDBBean = preferenceDao.getPreferenceByKey(customerDBBean.getId(), Configurations.PreferrenceKeyType.PREFERRED_CURRENCY);
        }

        if(preferenceDBBean != null){
            requestGigskyCredit.setCurrency(preferenceDBBean.getValue());
        }
        else {
            requestGigskyCredit.setCurrency(Configurations.DEFAULT_CURRENCY_CODE);
        }

        //Get the url for calling "Add credit" api
        String deductCreditUrl = "account/" + gsCustomerId + "/credit/gigskyCredit";

        //Call the api (POST) and get the response
        return callBackendCreditApi("POST", deductCreditUrl, requestGigskyCredit, CREDIT_DEDUCT_REQ_TYPE, tenantId);
    }

    @Override
    public Credit addCredit(int gsCustomerId, Credit credit, int tenantId) throws BackendRequestException{


        //Get the url for calling "Add credit" api
        String addCreditUrl = "account/" + gsCustomerId + "/credit/gigskyCredit";

        //Create the request object
        GigSkyCredit requestCredit = new GigSkyCredit();

        //Set type
        requestCredit.setType(CREDIT_ADD_REQ_TYPE);

        requestCredit.setCreditType(credit.getCreditType());
        requestCredit.setCurrency(credit.getCurrency());
        requestCredit.setGigskyCredit(credit.getCreditBalance());

        if(credit.getExpiry() != null) {
            requestCredit.setCreditExpiry(CommonUtils.convertTimestampToString(credit.getExpiry()));
        }
        requestCredit.setDescription(credit.getDescription());

        return callBackendCreditApi("POST", addCreditUrl, requestCredit, CREDIT_ADD_REQ_TYPE, tenantId);
    }

    private Map<String, String> getHeaderMap(int tenantId){

        //Get authorization token
        String token = gsTokenUtil.getValidToken(tenantId);

        //Create headers map
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Basic " +token);
        headers.put(GSUtil.TENANT_ID_HEADER, String.valueOf(tenantId));

        return headers;
    }

    private Credit callBackendCreditApi
            (String method, String specificUrl, Object requestBody, String requestType, int tenantId) throws BackendRequestException{

        //Get backend base url
        String baseURL = configurations.getBackendBaseUrl();

        //Get apiUrl
        String apiUrl = baseURL + specificUrl;

        //Get header map which will have authorization token
        Map<String, String> headersMap = getHeaderMap(tenantId);

        //Get request timeout
        long timeOut = configurations.getRequestTimeOut();

        //Call the api and get the response
        try{

            HttpClientUtil.GSRestHttpResponse gsRestHttpResponse = httpClientUtil.sendHttpRequest(method, apiUrl, null, requestBody, headersMap, timeOut);

            //Check whether response is null or error
            if(gsRestHttpResponse != null){

                //Convert the response body string to response object
                GigSkyCredit gigSkyCredit = (GigSkyCredit) HttpClientUtil.convertToBean(gsRestHttpResponse.getResponseBody(), GigSkyCredit.class);

                if(gigSkyCredit != null){

                    return convertCreditFromGigskyCredit(requestType, gigSkyCredit);
                }
            }
        }
        catch (Exception e){

            logger.error(" Get credit details api call exception " + e, e);
            throw new BackendRequestException(ErrorCode.BACKEND_REQUEST_FAILURE);
        }

        return null;
    }

    //This method maps the data from GigSkyCredit(API Response POJO) to the Credit(POJO used by the calling method)
    private Credit convertCreditFromGigskyCredit(String requestType, GigSkyCredit responseBody){

        Credit credit = new Credit();

        if ("error".equals(responseBody.getType())) {

            if((CREDIT_ADD_REQ_TYPE.equals(requestType))) {

                //For error case, check the errorInt
                int errorInt = responseBody.getErrorInt();

                //If the errorInt is 7017 then mark the status as FAILED_MAX (reached max credit amount)
                if (errorInt == 7017) {
                    credit.setType(Configurations.CreditAddResponseStatus.MAX_CREDIT_LIMIT_REACHED);
                }
            }
            else {
                return null;
            }
        }

        //responseType
        String type = responseBody.getType();
        if(StringUtils.isNotEmpty(type) && !"error".equals(responseBody.getType()))
            credit.setType(type);


        //Currency
        String currency = responseBody.getCurrency();
        if(StringUtils.isNotEmpty(currency))
            credit.setCurrency(currency);

        //TransactionId
        Integer creditTxnId = responseBody.getCreditTxnId();
        if(creditTxnId != null)
            credit.setCreditTxnId(responseBody.getCreditTxnId());

        //CreditId
        Integer creditId = responseBody.getGigskyCreditId();
        if(creditId != null)
            credit.setCreditId(creditId);

        //Balance
        Float gsCredit = responseBody.getGigskyCredit();
        if(gsCredit != null)
            credit.setCreditBalance(gsCredit);

        //Balance
        Float gigSkyCreditBalance = responseBody.getGigskyBalance();
        if(gigSkyCreditBalance != null)
            credit.setCreditBalance(gigSkyCreditBalance);

        //ErrorInt
        Integer errorInt = responseBody.getErrorInt();
        if(errorInt != null)
            credit.setErrorInt(errorInt);

        return credit;
    }
}
