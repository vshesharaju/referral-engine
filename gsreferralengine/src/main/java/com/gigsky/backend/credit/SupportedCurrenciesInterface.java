package com.gigsky.backend.credit;

/**
 * Created by pradeepragav on 29/03/17.
 */
public interface SupportedCurrenciesInterface {

    public void updateSupportedCurrencies(int tenantId);
}
