package com.gigsky.backend.credit;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.MockCreditDBBean;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.database.dao.MockCreditDao;
import com.gigsky.rest.bean.Credit;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by vinayr on 11/01/17.
 */
public class MockCreditManangementImpl implements CreditManagementInterface {

    @Autowired
    private MockCreditDao mockCreditDao;

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    @Override
    public Credit getCreditDetailsOfCustomer(int gsCustomerId, int tenantId){

        //Get credit balance
        float creditBalance = mockCreditDao.getCreditBalanceOfCustomer(gsCustomerId);

        //If mockCredit entries are there
        if(creditBalance != -1){

            //Create credit object
            Credit credit = new Credit();

            //Set parameters
            credit.setCreditBalance(creditBalance);
            credit.setType("GigskyCreditResponse");
            credit.setCurrency("USD");
            return credit;
        }
        return null;
    }

    @Override
    //Get the credit details from the mock credit table
    public Credit getCreditDetails(int gsCustomerId, int creditId, int tenantId)
    {
        //Fetch the credit details
        MockCreditDBBean mockCreditDBBean = mockCreditDao.getCredit(creditId);
        if (mockCreditDBBean != null) {
            Credit credit = new Credit();
            credit.setCreditId(creditId);
            credit.setAddedAmount(mockCreditDBBean.getBalance());
            credit.setCreditType(mockCreditDBBean.getCreditType());
            credit.setExpiry(mockCreditDBBean.getExpiryTime());
            credit.setCurrency(mockCreditDBBean.getCurrency());
            credit.setCreditTxnId(1234);
            return credit;
        }
        return null;
    }

    @Override
    public Credit deductCredit(int gsCustomerId, int creditId, int tenantId) {

        //Get mockCredit bean
        MockCreditDBBean mockCreditDBBean = mockCreditDao.getCredit(creditId);
        if(mockCreditDBBean != null){
            //Check for the gsCustomerId
            if(mockCreditDBBean.getMockcustomerId() == gsCustomerId){

                //Set balance as Zero
                mockCreditDBBean.setBalance((float)0);

                //Update mockCredit using dao
                mockCreditDao.updateCredit(mockCreditDBBean);
                return null;
            }
        }
        return null;
    }

    @Override
    public Credit addCredit(int gsCustomerId, Credit credit, int tenantId){


        //For testing purpose
        if(shouldCreditAddFail()) return null;

        // adding credit for gsCustomerId
        MockCreditDBBean mockCreditDBBean = new MockCreditDBBean();
        mockCreditDBBean.setCreditType(credit.getCreditType());
        mockCreditDBBean.setMockcustomerId(gsCustomerId);
        mockCreditDBBean.setBalance(credit.getCreditBalance());
        mockCreditDBBean.setExpiryTime(credit.getExpiry());
        mockCreditDBBean.setCurrency(credit.getCurrency());

        int creditId = mockCreditDao.addCredit(mockCreditDBBean);

        credit = getCreditDetails(gsCustomerId, creditId, tenantId);

        //Set type
        credit.setType(Configurations.CreditAddResponseStatus.SUCCESS);

        return credit;
    }

    //Check for API_FAILURE_MODE key in configs
    private boolean shouldCreditAddFail(){

        //Get value for key CREDIT_ADD_FAILURE_MODE
        String creditAddFailure = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.API_FAILURE_MODE);

        //If creditAddFailureMode is set to true
        if("true".equals(creditAddFailure)){

            //Set credit response to null, so the invitation entry will be marked as credit all failed
            return true;
        }

        return false;
    }
}
