package com.gigsky.backend.credit;

import com.gigsky.backend.beans.Currency;
import com.gigsky.backend.beans.SupportedCurrencies;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.SupportedCurrencyDBBean;
import com.gigsky.database.dao.SupportedCurrenciesDao;
import com.gigsky.util.HttpClientUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by pradeepragav on 29/03/17.
 */
public class SupportedCurrenciesImpl implements SupportedCurrenciesInterface {

    private static final Logger logger = LoggerFactory.getLogger(SupportedCurrenciesImpl.class);


    @Autowired
    private HttpClientUtil httpClientUtil;

    @Autowired
    private Configurations configurations;

    @Autowired
    private SupportedCurrenciesDao supportedCurrenciesDao;

    @Override
    public void updateSupportedCurrencies(int tenantId) {

        String apiURL = configurations.getBackendBaseUrl() + "supportedCurrencies";

        long timeOut = configurations.getRequestTimeOut();

        //Call getSupportedCurrencies API
        HttpClientUtil.GSRestHttpResponse gsRestHttpResponse =
                httpClientUtil.sendHttpRequest("GET", apiURL, null, null, null, timeOut);

        if(gsRestHttpResponse != null){

            SupportedCurrencies supportedCurrencies =(SupportedCurrencies) HttpClientUtil
                    .convertToBean(gsRestHttpResponse.getResponseBody(),SupportedCurrencies.class);

            if(supportedCurrencies != null &&
                    "SupportedCurrencies".equals(supportedCurrencies.getType())){

                for (Currency currencyEntry : supportedCurrencies.getList()){

                    //Set currency
                    String currencyCode = currencyEntry.getCode();

                    //Get supportedCurrency entry
                    SupportedCurrencyDBBean supportedCurrencyDBBean = supportedCurrenciesDao.getSupportedCurrencyDetails(currencyCode, tenantId);

                    //If there is no entry, the above dao method returns empty entry (not null)
                    supportedCurrencyDBBean.setCurrencyCode(currencyCode);

                    //Set usdMultiplier
                    Float usdMultiplier = currencyEntry.getUsdMultiplier();
                    supportedCurrencyDBBean.setUsdMultiplier(usdMultiplier);

                    //set tenantId
                    supportedCurrencyDBBean.setTenantId(tenantId);

                    //Update using dao
                    supportedCurrenciesDao.updateSupportedCurrencyDetails(supportedCurrencyDBBean);
                }

                logger.info("+++++Updated supportedCurrencies+++++");
            }
        }
    }
}