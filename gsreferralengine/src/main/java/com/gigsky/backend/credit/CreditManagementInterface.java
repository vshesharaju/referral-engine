package com.gigsky.backend.credit;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.rest.bean.Credit;

/**
 * Created by vinayr on 11/01/17.
 */
public interface CreditManagementInterface {

    public Credit getCreditDetails(int gsCustomerId, int creditId, int tenantId) throws BackendRequestException;

    public Credit deductCredit(int gsCustomerId, int creditId, int tenantId) throws BackendRequestException;

    public Credit addCredit(int gsCustomerId, Credit credit, int tenantId) throws BackendRequestException;

    public Credit getCreditDetailsOfCustomer(int gsCustomerId, int tenantId)throws BackendRequestException;
}
