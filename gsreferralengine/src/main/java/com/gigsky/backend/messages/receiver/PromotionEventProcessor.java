package com.gigsky.backend.messages.receiver;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.database.dao.TenantConfigurationsKeyValueDao;
import com.gigsky.database.dao.TenantDao;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.ValidationService;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.task.PromoEventsTask;
import com.gigsky.util.GSUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

/**
 * Created by pradeepragav on 01/12/17.
 */
public class PromotionEventProcessor extends EventMessageProcessor {

    private static final Logger logger = LoggerFactory.getLogger(PromotionEventProcessor.class);

    @Autowired
    private SIMActivationPromoProcessor simActivationPromoProcessor;

    @Autowired
    private SignUpPromoEventProcessor signUpPromoEventProcessor;

    @Autowired
    private TenantConfigurationsKeyValueDao tenantConfigurationsKeyValueDao;

    @Autowired
    private GSUtil gsUtil;

    @Override
    public void handleEvents(EventMessageData messageData, String eventData, String eventMetaData)
            throws IOException, ClassNotFoundException, BackendRequestException, ServiceException, ResourceValidationException
    {

        int tenantId = gsUtil.extractTenantIdFromMessageData(messageData);

        //Check for promotionEnable in Configuration key values
        String promoEnable = tenantConfigurationsKeyValueDao.getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.PROMOTION_ENABLE, tenantId);
        if(promoEnable != null && promoEnable.equals("true"))
        {
            //Check for event type
            String eventType = getEventType(messageData);

            if(eventType.equals(Configurations.ReferralEventType.SIM_ACTIVATION_EVENT))
            {
                simActivationPromoProcessor.handleEvents(messageData, eventData, eventMetaData);
            }
            else if(eventType.equals(Configurations.ReferralEventType.SIGNUP_EVENT))
            {
                signUpPromoEventProcessor.handleEvents(messageData, eventData, eventMetaData);
            }
            //TODO : For other events handle if there is a requirement
        }

        return;
    }
}
