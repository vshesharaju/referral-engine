package com.gigsky.backend.messages.dbbeans;

import java.sql.Timestamp;

/**
 * Created by pradeepragav on 20/01/17.
 */
public class QueueDetailsDBBean {
    private long id;
    private String exchangeName;
    private String queueName;
    private String routingKey;
    private Timestamp createTime;
    private Timestamp updateTime;
    private MessageServerDBBean serverDBBean;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public MessageServerDBBean getServerDBBean() {
        return serverDBBean;
    }

    public void setServerDBBean(MessageServerDBBean serverDBBean) {
        this.serverDBBean = serverDBBean;
    }
}
