package com.gigsky.backend.messages.receiver;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.InvitationDBBean;
import com.gigsky.database.bean.PromotionDBBean;
import com.gigsky.database.bean.ReferralEventDBBean;
import com.gigsky.database.dao.*;
import com.gigsky.messaging.beans.*;
import com.gigsky.rest.bean.Account;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.AccountService;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSTaskUtil;
import com.gigsky.util.GSTokenUtil;
import com.gigsky.util.GSUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

/**
 * Created by pradeepragav on 05/04/17.
 */
public class ReferralEventMessageProcessor extends EventMessageProcessor {

    private static final Logger logger = LoggerFactory.getLogger(ReferralEventMessageProcessor.class);

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private ReferralEventDao referralEventDao;

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    private InvitationDao invitationDao;

    @Autowired
    private AccountService accountService;

    @Autowired
    private GSTokenUtil tokenUtil;

    @Autowired
    private GSUtil gsUtil;

    @Autowired
    private PromotionDao promotionDao;

    @Override
    public void handleEvents(EventMessageData messageData, String eventData, String eventMetaData)
            throws IOException, ClassNotFoundException, BackendRequestException, ServiceException, ResourceValidationException
    {

        //Not storing creditExpiry and creditExpired
        if(messageData instanceof CreditExpiry){

            return;
        }

        //Get gigsky backend's customerId
        long gsCustomerId = messageData.getCustomerId();

        //Set the status to NEW
        String referralEventStatus = Configurations.StatusType.STATUS_NEW;

        //Get the eventType
        String eventType = getEventType(messageData);

        int customerId;

        int tenantId = gsUtil.extractTenantIdFromMessageData(messageData);

        //Get gigsky referral's customer details using gsCustomerId
        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, tenantId);

        //Check whether the customer Entry is there in referral db
        if(customerDBBean != null){

            customerId = customerDBBean.getId();

            if(checkForDuplicates(customerDBBean, messageData, eventType)){

                //Set the status to Duplicate entry
                referralEventStatus = Configurations.StatusType.DUPLICATE_ENTRY;
            }
            //If it is a free plan mark the event as complete
            else if(messageData instanceof PlanPurchase || messageData instanceof SubscriptionExpiry){

                //Check whether these two events be processed
                if(checkIfPlanPurchaseEventIsAlreadyCredited(customerDBBean)
                        || checkIfFreePlan(messageData))
                    referralEventStatus = Configurations.StatusType.STATUS_COMPLETE;
            }
        }
        else {

            //New user, there is no customer entry in referral DB
            if(isBetaMode()){

                //It is in beta mode
                //There is no customer entry
                //Check if it is signUp event with referral code
                if(messageData instanceof UserSignup){

                    UserSignup userSignup = (UserSignup) messageData;
                    if(StringUtils.isEmpty(userSignup.getReferralCode())){
                        return;
                    }
                }
                else {
                    //Other event
                    return;
                }
            }
            customerId = createCustomerDBBean(messageData, gsCustomerId);
        }

        //Create ReferralEventDBBean
        ReferralEventDBBean referralEventDBBean = new ReferralEventDBBean();

        //Set referral customerId which is obtained using gsCustomerId
        referralEventDBBean.setCustomerId(customerId);

        //Set event data this will contain the eventMessage,
        referralEventDBBean.setEventData(eventData);
        referralEventDBBean.setEventType(eventType);
        referralEventDBBean.setStatus(referralEventStatus);
        referralEventDBBean.setEventMetaData(eventMetaData);
        referralEventDBBean.setTenantId(tenantId);

        //Mark event status message as not processed (Due to planPurchase event (already credited))
        if(Configurations.StatusType.STATUS_COMPLETE.equals(referralEventStatus)){
            referralEventDBBean.setStatusMessage(Configurations.StatusType.NOT_PROCESSED);
        }

        //Store the data using referralEventDao
        referralEventDao.addReferralEvent(referralEventDBBean);
    }

    private boolean isBetaMode(){

        String isBetaMode = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.BETA_MODE_ENABLE);

        if(StringUtils.isNotEmpty(isBetaMode))
            return "true".equals(isBetaMode);

        //Default value is false
        return false;
    }


    //If advocate is already credit
    //Or If signupEvent is without referral
    private boolean checkIfPlanPurchaseEventIsAlreadyCredited(CustomerDBBean customerDBBean) throws IOException, ClassNotFoundException {

        //Check for signUp event and the referralCode
        ReferralEventDBBean referralEventDBBean = referralEventDao.getSignUpReferralEvent(customerDBBean.getId(), customerDBBean.getTenantId());

        // Fetch customer details
        //CustomerDBBean customerDBBean = customerDao.getCustomerInfo(customerId);

        //Get advocateId
        Integer advocateId = null;
        if(customerDBBean != null ) {
            advocateId = customerDBBean.getAdvocateId();
        }

        if(referralEventDBBean != null){

            //Get event data (String)
            String eventData = referralEventDBBean.getEventData();

            //Get event data (Deserialize)
            EventMessageData eventMessageData = GSTaskUtil.getMessageData(eventData, Configurations.ReferralEventType.SIGNUP_EVENT);

            UserSignup userSignup = (UserSignup) eventMessageData;

            //If no referral code, then mark it as NOT_PROCESSED and change customer status to USING_SERVICE
            if(StringUtils.isEmpty(userSignup.getReferralCode()) && advocateId == null){

                customerDBBean.setStatus(Configurations.CustomerStatus.STATUS_USING_SERVICE);
                customerDao.updateCustomerInfo(customerDBBean);
                return true;
            }

            //If referralCode is entered
            if(!StringUtils.isEmpty(userSignup.getReferralCode()))
            {
                //Check if it is Promo code
                PromotionDBBean promotion = promotionDao.getPromotion(userSignup.getReferralCode(), customerDBBean.getTenantId());
                if(promotion!=null)
                {
                    return true;
                }
            }
        }


        //Check if the customer's advocate is already credited
        if(advocateId != null){

            CustomerDBBean advocateCustomer = customerDao.getCustomerInfo(advocateId, customerDBBean.getTenantId());

            //Check if the advocate is deleted
            if(!advocateCustomer.isDeleted())
            {
                InvitationDBBean invitationDBBean = invitationDao.getInvitationByTargetCustomerId(customerDBBean.getId());

                Integer advCreditId = invitationDBBean.getAdvocateCreditId();
                if(advCreditId != null){

                    //Set referralEvent as complete, he is already rewarded
                    return true;
                }
            }
            else {
                //Advocate account is deleted
                //Skip referral credit add
                return true;
            }
        }
        return false;
    }

    private boolean checkIfFreePlan(EventMessageData eventMessageData){

        if(eventMessageData instanceof PlanPurchase){

            PlanPurchase planPurchase = (PlanPurchase) eventMessageData;

            if(planPurchase.isFreePlan()){
                return true;
            }
        }
//                    else {
//
//                        SubscriptionExpiry subscriptionExpiry = (SubscriptionExpiry) eventMessageData;
//                        if(subscriptionExpiry.isFreePlan()){
//                            return true;
//                        }
//                    }
        return false;
    }

    @Override
    protected boolean checkForDuplicates(CustomerDBBean customerDBBean, EventMessageData eventMessageData, String eventType)
    {

        ReferralEventDBBean referralEventDBBean = null;

        if(eventMessageData instanceof UserSignup){

            //Check if there are signUp events for the same customerId
            referralEventDBBean = referralEventDao.getSignUpReferralEvent(customerDBBean.getId(), customerDBBean.getTenantId());

        }
        else {

            String keyValuePairString = null;

            if(eventMessageData instanceof SimActivation){

                //Get simActivation event object, to get iccid
                SimActivation simActivation = (SimActivation) eventMessageData;

                //SIMActivation, if gigsky sim only one entry using iccid for customerId
                if(Configurations.SIMType.GS_SIM_50.equals(simActivation.getSimType()) || Configurations.SIMType.GIGSKY_SIM.equals(simActivation.getSimType())) {
                    keyValuePairString = getUniqueEventDataAttribute("iccID",simActivation.getIccID(), true);
                }
            }
            else if(eventMessageData instanceof SubscriptionExpiry){

                //Plan expired, same subscription id for customerId

                //Get planExpiry object, to get subscriptionId
                SubscriptionExpiry planExpiry = (SubscriptionExpiry) eventMessageData;

                keyValuePairString = getUniqueEventDataAttribute("subscriptionId",planExpiry.getSubscriptionId().toString(), false);
            }
            else if(eventMessageData instanceof PlanPurchase){

                //Get planPurchaseEvent object, to get TransactionId
                PlanPurchase planPurchase = (PlanPurchase) eventMessageData;

                keyValuePairString = getUniqueEventDataAttribute("transactionId",planPurchase.getTransactionId().toString(), false);
            }

            if(keyValuePairString != null){

                referralEventDBBean = referralEventDao.getDuplicateEvent(eventType, customerDBBean, keyValuePairString);
            }
        }

        //If there is an entry in entry, return true
        if(referralEventDBBean != null){
            return true;
        }

        return false;
    }



    //Get account details and add customer entry
    private int createCustomerDBBean(EventMessageData messageData, long gsCustomerId) throws BackendRequestException, ServiceException, ResourceValidationException {

        //Create CustomerDBBean
        CustomerDBBean customerDBBean = new CustomerDBBean();

        //gsCustomerId is common
        customerDBBean.setGsCustomerId(gsCustomerId);

        int tenantId = gsUtil.extractTenantIdFromMessageData(messageData);
        customerDBBean.setTenantId(tenantId);

        //If the event is of type UserSignUp, we get emailId, status and gsCustomerId of the customer
        if(messageData instanceof UserSignup) {

            UserSignup userSignup = (UserSignup) messageData;

            //Assign parameters to CustomerDBBean
            customerDBBean.setEmailId(userSignup.getEmailId());
            customerDBBean.setTenantId(tenantId);
            customerDBBean.setStatus(userSignup.getAccountActivationStatus());

            //Add CustomerDBBean using dao
            //Get id of added customer
            return customerDao.createCustomerInfo(customerDBBean);
        }

        //Get token
        String token = tokenUtil.getValidToken(tenantId);

        //Get account details,
        //This call fetches account details
        //Adds an entry in customer table
        //Creates referral code
        //Sets referral status as signup without referral
        Account account = accountService.getAccountDetails((int)gsCustomerId, token, tenantId);

        //Get id of added customer
        int customerID = customerDao.getCustomerIdForBackendCustomerId(gsCustomerId, tenantId);


        return customerID;
    }
}
