package com.gigsky.backend.messages;

import com.gigsky.backend.messages.dbbeans.MessageServerDBBean;
import com.gigsky.backend.messages.dbbeans.MessagingQueueDetails;
import com.gigsky.backend.messages.dbbeans.QueueDetailsDBBean;
import com.gigsky.database.bean.MessageServerClusterNodeDBBean;
import com.rabbitmq.client.Address;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pradeepragav on 20/01/17.
 */
public class MessagingFactory {


    public static Connection getConnection(MessagingQueueDetails queueDetails) throws Exception {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setUsername(queueDetails.getUserName());
            factory.setPassword(queueDetails.getPassword());

            if (StringUtils.isNotEmpty(queueDetails.getVirtualHost())) {
                factory.setVirtualHost(queueDetails.getVirtualHost());
            }

            //get the address list for connectivity
            List<Address> addressList = getAddressList(queueDetails.getClusterNodes());
            Connection connection = factory.newConnection(addressList);
            return connection;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    public static List<Address> getAddressList(List<MessageServerClusterNodeDBBean> clusterNodes) {

        List<Address> addressList = new ArrayList<Address>();
        for (MessageServerClusterNodeDBBean clusterNode : clusterNodes) {
            if (clusterNode.getPort() == null || clusterNode.getPort().intValue() <= 0) {
                Address address = new Address(clusterNode.getHost());
                addressList.add(address);
            }
            else {
                Address address = new Address(clusterNode.getHost(), clusterNode.getPort());
                addressList.add(address);
            }
        }
        return addressList;
    }

    public static Connection getRemoteConnection(QueueDetailsDBBean queueDetails) throws Exception {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            MessageServerDBBean serverDetails = queueDetails.getServerDBBean();
            factory.setHost(serverDetails.getHost());
//            factory.setVirtualHost(serverDetails.getUsername());
            factory.setUsername(serverDetails.getUsername());
            factory.setPassword(serverDetails.getPassword());

            Connection connection = factory.newConnection();
            return connection;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }
}
