package com.gigsky.backend.messages.receiver;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.ReferralEventDBBean;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.ReferralEventDao;
import com.gigsky.messaging.beans.*;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;

/**
 * Created by pradeepragav on 17/05/18.
 */
public class DeleteAccountEventProcessor extends EventMessageProcessor{

    private static final Logger logger = LoggerFactory.getLogger(DeleteAccountEventProcessor.class);

    @Autowired
    private ReferralEventDao referralEventDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private GSUtil gsUtil;

    //Overridden by the subclasses
    public void handleEvents(EventMessageData messageData, String eventData, String eventMetaData)
            throws IOException, ClassNotFoundException, BackendRequestException, ServiceException, ResourceValidationException
    {
        String status = Configurations.StatusType.STATUS_NEW;

        int tenantId = gsUtil.extractTenantIdFromMessageData(messageData);

        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(messageData.getCustomerId(), tenantId);
        if(customerDBBean != null)
        {
            //Check for duplicate entry
            if(checkForDuplicates(customerDBBean, messageData, messageData.getObjectType()))
            {
                //Duplicate event
                status = Configurations.StatusType.DUPLICATE_ENTRY;
            }
        }
        else
        {
            DeleteAccountInfo deleteAccount =(DeleteAccountInfo) messageData;

            //If not create a customer entry which is marked as deleted, mark the status as processed
            customerDBBean = new CustomerDBBean();
            customerDBBean.setGsCustomerId(messageData.getCustomerId());
            customerDBBean.setDeleted(true);
            customerDBBean.setEmailId(deleteAccount.getNewEmailId());
            customerDBBean.setTenantId(tenantId);
            customerDao.createCustomerInfo(customerDBBean);
            status = Configurations.StatusType.STATUS_COMPLETE;
            eventData = null;
        }

        //Store in referralEvent table
        ReferralEventDBBean referralEventDBBean = new ReferralEventDBBean();
        referralEventDBBean.setCustomerId(customerDBBean.getId());
        referralEventDBBean.setEventData(eventData);
        referralEventDBBean.setEventMetaData(null);
        referralEventDBBean.setRetryCount(0);
        referralEventDBBean.setEventType(getEventType(messageData));
        referralEventDBBean.setStatus(status);
        referralEventDBBean.setTenantId(tenantId);
        referralEventDao.addReferralEvent(referralEventDBBean);
    }

    protected boolean checkForDuplicates(CustomerDBBean customerDBBean, EventMessageData eventMessageData, String eventType)
    {
        //Get delete account events of the customer
        List<ReferralEventDBBean> referralEventsForCustomerByEventType =
                referralEventDao.getReferralEventsForCustomerByEventType(customerDBBean.getId(),
                        getEventType(eventMessageData), customerDBBean.getTenantId());

        if(referralEventsForCustomerByEventType.size() > 0)
            return true;

        return false;
    }

}
