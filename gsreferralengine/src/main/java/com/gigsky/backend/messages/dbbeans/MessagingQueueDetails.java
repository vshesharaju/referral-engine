package com.gigsky.backend.messages.dbbeans;

import com.gigsky.database.bean.MessageServerClusterNodeDBBean;

import java.util.List;

/**
 * Created by pradeepragav on 10/04/17.
 */
public class MessagingQueueDetails {

    private int queueId;
    private String queueName;
    private String exchangeName;
    private String routingKey;
    private String clusterName;
    private String userName;
    private String password;
    private String virtualHost;
    private int clusterId;
    private String encVersion;
    private List<MessageServerClusterNodeDBBean> clusterNodes;

    public int getQueueId() {
        return queueId;
    }

    public void setQueueId(int queueId) {
        this.queueId = queueId;
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    public String getRoutingKey() {
        return routingKey;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }

    public String getClusterName() {
        return clusterName;
    }

    public void setClusterName(String clusterName) {
        this.clusterName = clusterName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVirtualHost() {
        return virtualHost;
    }

    public void setVirtualHost(String virtualHost) {
        this.virtualHost = virtualHost;
    }

    public int getClusterId() {
        return clusterId;
    }

    public void setClusterId(int clusterId) {
        this.clusterId = clusterId;
    }

    public String getEncVersion() {
        return encVersion;
    }

    public void setEncVersion(String encVersion) {
        this.encVersion = encVersion;
    }

    public List<MessageServerClusterNodeDBBean> getClusterNodes() {
        return clusterNodes;
    }

    public void setClusterNodes(List<MessageServerClusterNodeDBBean> clusterNodes) {
        this.clusterNodes = clusterNodes;
    }

    @Override
    public String toString() {
        return "MessagingQueueDetails{" +
                "queueId=" + queueId +
                ", queueName='" + queueName + '\'' +
                ", exchangeName='" + exchangeName + '\'' +
                ", routingKey='" + routingKey + '\'' +
                ", clusterName='" + clusterName + '\'' +
                ", virtualHost='" + virtualHost + '\'' +
                ", clusterId=" + clusterId +
                ", clusterNodes=" + clusterNodes +
                '}';
    }
}
