package com.gigsky.backend.messages.receiver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigsky.backend.messages.MessagingFactory;
import com.gigsky.backend.messages.dbbeans.MessagingQueueDetails;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.MessageServerQueueDao;
import com.gigsky.gscommon.util.encryption.EncryptionFactory;
import com.gigsky.gscommon.util.encryption.GigSkyEncryptor;
import com.gigsky.messaging.EventMessage;
import com.gigsky.messaging.EventMessageComposer;
import com.gigsky.messaging.beans.*;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSUtil;
import com.rabbitmq.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by pradeepragav on 20/01/17.
 *
 * 1. Listens to the rabbitMQ message queue for referral events (Sign up, plan purchase ...)
 * 2. Stores the event message details in referral event table
 * 3. Also checks for duplicate messages and stores as duplicate entry
 * 4. For plan purchase event it marks as complete if the customer has already redeemed the credit through referral prgm
 */
public class EventMessageReceiver implements NotificationReceiver {

    private static final Logger logger = LoggerFactory.getLogger(EventMessageReceiver.class);

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public static final boolean DURABLE = true;

    public static final boolean ACK = true;

    private static Connection connection;

    private static Channel channel;

    @Autowired
    private EncryptionFactory encryptionFactory;

    @Autowired
    private ReferralEventMessageProcessor referralEventMessageProcessor;

    @Autowired
    private PromotionEventProcessor promotionEventProcessor;

    @Autowired
    private HealthCheckEventProcessor healthCheckEventProcessor;

    @Autowired
    private DeleteAccountEventProcessor deleteAccountEventProcessor;

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    private MessageServerQueueDao messageServerQueueDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private GSUtil gsUtil;

    public void consume() throws InterruptedException, TimeoutException, IOException {

        try {

            //Get queue name from configurationsDao
            String queueName = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.MQ_QUEUE_NAME);

            //Get messagingQueueDetails
            MessagingQueueDetails messagingQueueDetails = messageServerQueueDao.getMessagingQueueDetails(queueName);

            //Get connection using MessageFactory
            connection = MessagingFactory.getConnection(messagingQueueDetails);

            //Create channel
            channel = connection.createChannel();

            logger.info("EventMessageReciever createChannel called");

            //Get encryptor based on version
            final GigSkyEncryptor encryptor = encryptionFactory.getEncryptor(Configurations.LATEST_ENCRYPTION_VERSION);

            logger.info("Queue configs" + "ExchangeName :" +messagingQueueDetails.getExchangeName() + " RoutingKey :" +messagingQueueDetails.getRoutingKey() + " QueueName :" +messagingQueueDetails.getQueueName());

            //Start listening to queue
            Consumer consumer = new DefaultConsumer(channel) {

                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                        throws IOException {

                    //Convert messageByte to string
                    String encryptedMessage = new String(body, "UTF-8");

                    //Check if the message is Fresh
                    if (!envelope.isRedeliver()) {


                        //Decrypt the message
                        String message = encryptor.decryptString(encryptedMessage);

                        //Get eventMessage from the string message
                        EventMessage eventMessage = objectMapper.readValue(message, EventMessage.class);

                        logger.info(" [x] Received message eventType: " + eventMessage.getEventType() +" and occured time: "+ eventMessage.getEventOccurredTime() + " and messageId: " + eventMessage.getMessageId() + " , " + (envelope.isRedeliver() ? "REDELIVER" : "FRESH"));

                        try {

                            String eventData = eventMessage.getData();

                            //Get messageData using EventMessageComposer
                            EventMessageData messageData = EventMessageComposer.getMessageData(eventMessage);

                            // Process health check events and return
                            if(messageData instanceof HealthCheckEvent)
                            {
                                healthCheckEventProcessor.handleEvents(messageData, eventData, message);
                                return;
                            }


                            // Get the account type from events and process only if account type "GIGSKY"
                            String accountType = messageData.getAccountType();
                            if((accountType != null && accountType.length() > 0 && !"GIGSKY".equals(accountType))
                                    //Check if the event's customer is deleted
                                    || (isCustomerDeleted(messageData)))
                            {
                                logger.info("Ignoring event which is not gigsky account");
                                return;
                            }

                            if(messageData instanceof DeleteAccountInfo)
                            {
                                deleteAccountEventProcessor.handleEvents(messageData, eventData, message);
                                return;
                            }

                            //Process referral events
                            referralEventMessageProcessor.handleEvents(messageData, eventData, message);

                            //Process promotion events
                            promotionEventProcessor.handleEvents(messageData, eventData, message);

                        } catch (Exception e) {
                            logger.error("Exception occured while receiving the message", e);
                            e.printStackTrace();
                        }
                    }
                }
            };

            channel.basicConsume(queueName, ACK, consumer);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private boolean isCustomerDeleted(EventMessageData eventMessageData) throws ResourceValidationException, ServiceException
    {
        Long gsCustomerId = eventMessageData.getCustomerId();
        int tenantId = gsUtil.extractTenantIdFromMessageData(eventMessageData);
        CustomerDBBean customerInfoByGsCustomerId = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, tenantId);
        if(customerInfoByGsCustomerId != null && customerInfoByGsCustomerId.isDeleted())
        {
            return true;
        }

        return false;
    }

    public void shutDown() throws IOException, TimeoutException {

        //Called when the contextListener is destroyed

        logger.info("++ e ++");

        //Close RabbitMQ channel
        channel.close();

        //Close RabbitMQ connection
        connection.close();

        logger.info("++ RabbitMQ consumer closed connections++");
    }

}
