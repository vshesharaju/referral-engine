package com.gigsky.backend.messages.receiver;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.messaging.beans.*;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.NoSuchElementException;

/**
 * Created by pradeepragav on 01/12/17.
 * Common/Super class for handling the events sent from RabbitMQ
 * The class is responsible for storing valid events into the DB for further processing
 */
public class EventMessageProcessor
{

    private static final Logger logger = LoggerFactory.getLogger(EventMessageProcessor.class);

    //Overridden by the subclasses
    public void handleEvents(EventMessageData messageData, String eventData, String eventMetaData)
            throws IOException, ClassNotFoundException, BackendRequestException, ServiceException, ResourceValidationException
    {
        return;
    }

    protected String getEventType(EventMessageData eventMessageData){

        if(eventMessageData instanceof UserSignup){

            return Configurations.ReferralEventType.SIGNUP_EVENT;
        }
        else if(eventMessageData instanceof SimActivation){

            return Configurations.ReferralEventType.SIM_ACTIVATION_EVENT;
        }
        else if(eventMessageData instanceof PlanPurchase){

            return Configurations.ReferralEventType.PURCHASE_EVENT;
        }
        else if(eventMessageData instanceof SubscriptionExpiry){

            return Configurations.ReferralEventType.PLAN_EXPIRED_EVENT;
        }
        else if(eventMessageData instanceof EmailIDChange){

            return Configurations.ReferralEventType.EMAIL_ID_CHANGE_EVENT;
        }
        else if(eventMessageData instanceof CurrencyChange){

            return Configurations.ReferralEventType.CURRENCY_CHANGE_EVENT;
        } else if(eventMessageData instanceof AccountInformationChange) {

            return Configurations.ReferralEventType.ACCOUNT_INFORMATION_CHANGE_EVENT;
        } else if(eventMessageData instanceof HealthCheckEvent) {

            return Configurations.ReferralEventType.HEALTH_CHECK_EVENT;
        }
        else if(eventMessageData instanceof DeleteAccountInfo)
        {
            return Configurations.ReferralEventType.ACCOUNT_DELETE_EVENT;
        }
        throw new NoSuchElementException("The event type is not specified");
    }


    protected boolean checkForDuplicates(CustomerDBBean customerDBBean, EventMessageData eventMessageData, String eventType)
    {
        return false;
    }

    protected String getUniqueEventDataAttribute(String key, String value, boolean isValueAString){

        StringBuffer keyValuePairString = new StringBuffer();
        keyValuePairString.append("\"");
        keyValuePairString.append(key);
        keyValuePairString.append("\"");
        keyValuePairString.append(":");

        //If the value is string, append "
        if(isValueAString)
            keyValuePairString.append("\"");

        keyValuePairString.append(value);

        //If the value is string, append "
        if(isValueAString)
            keyValuePairString.append("\"");

        return keyValuePairString.toString();
    }

}
