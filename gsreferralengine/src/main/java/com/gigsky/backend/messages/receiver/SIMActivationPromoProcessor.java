package com.gigsky.backend.messages.receiver;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.PromoEventDBBean;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.PromoEventDao;
import com.gigsky.database.dao.PromoIccidDao;
import com.gigsky.database.dao.PromotionDao;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.messaging.beans.SimActivation;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.AccountService;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSTaskUtil;
import com.gigsky.util.GSTokenUtil;
import com.gigsky.util.GSUtil;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

/**
 * Created by pradeepragav on 01/12/17.
 */
public class SIMActivationPromoProcessor extends EventMessageProcessor
{

    @Autowired
    private PromoIccidDao promoIccidDao;

    @Autowired
    private PromotionDao promotionDao;

    private static final Logger logger = LoggerFactory.getLogger(SIMActivationPromoProcessor.class);

    private String countryLanguageList = "{\"list\":[" +
            "{\"type\":\"country\",\"name\":\"Japan\",\"code\":\"JP\",\"langCode\":\"ja\"}," +
            "{\"type\":\"country\",\"name\":\"Spain\",\"code\":\"ES\",\"langCode\":\"es\"}," +
            "{\"type\":\"country\",\"name\":\"France\",\"code\":\"FR\",\"langCode\":\"fr\"}," +
            "{\"type\":\"country\",\"name\":\"French Guiana\",\"code\":\"GF\",\"langCode\":\"fr\"}," +
            "{\"type\":\"country\",\"name\":\"Martinique\",\"code\":\"MQ\",\"langCode\":\"fr\"}," +
            "{\"type\":\"country\",\"name\":\"Guadeloupe\",\"code\":\"GP\",\"langCode\":\"fr\"}," +
            "{\"type\":\"country\",\"name\":\"Italy\",\"code\":\"IT\",\"langCode\":\"it\"}," +
            "{\"type\":\"country\",\"name\":\"San Marino\",\"code\":\"SM\",\"langCode\":\"it\"}," +
            "{\"type\":\"country\",\"name\":\"China\",\"code\":\"CN\",\"langCode\":\"zh\"}," +
            "{\"type\":\"country\",\"name\":\"Hong Kong\",\"code\":\"HK\",\"langCode\":\"zh\"}," +
            "{\"type\":\"country\",\"name\":\"Taiwan\",\"code\":\"TW\",\"langCode\":\"zh\"}," +
            "{\"type\":\"country\",\"name\":\"Germany\",\"code\":\"DE\",\"langCode\":\"de\"}]}";

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private PromoEventDao promoEventDao;

    @Autowired
    private GSTokenUtil gsTokenUtil;

    @Autowired
    AccountService accountService;

    @Autowired
    private GSUtil gsUtil;

    @Autowired
    private GSTaskUtil gsTaskUtil;

    @Override
    public void handleEvents(EventMessageData messageData, String eventData, String eventMetaData)
            throws IOException, ClassNotFoundException, BackendRequestException, ServiceException, ResourceValidationException
    {
        //Check for SIM type
        SimActivation simActivation = (SimActivation) messageData;

        //Check for SIM type for 3.0 SIM (GIGSKY_SIM) or 5.0 SIM (GS_SIM_50)
        if(Configurations.SIMType.GS_SIM_50.equals(simActivation.getSimType()) ||
                Configurations.SIMType.GIGSKY_SIM.equals(simActivation.getSimType()))
        {

            int tenantId = gsUtil.extractTenantIdFromMessageData(messageData);

            CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(simActivation.getCustomerId(), tenantId);

            String eventStatus = Configurations.StatusType.STATUS_NEW;


            if(customerDBBean != null)
            {
                //Check for duplicate event entry in PromoEvent table
                if(checkForDuplicates(customerDBBean, messageData, null))
                {
                    eventStatus = Configurations.StatusType.DUPLICATE_ENTRY;
                }
            }
            else
            {
                //No customer Entry, fetch customer details and add entry in customer table
                logger.info("New customer entry is creating for customer in SIMActivationPromoProcessor flow");
                // Get account details will handle generation of new customer entry and referral code
                accountService.getAccountDetails(simActivation.getCustomerId().intValue(), gsTokenUtil.getValidToken(tenantId), tenantId);

                // Fetching customer DB info using gs customer id
                customerDBBean = customerDao.getCustomerInfoByGsCustomerId(simActivation.getCustomerId().intValue(), tenantId);

                // update language code for customer using gs customer id
                accountService.updateLanguageForCustomer(simActivation.getCustomerId().intValue(), getLanguageUsingCountryCode(customerDBBean.getCountry()), tenantId);
            }

            //Validate promotion, if invalid mark as NOT_PROCESSED
            if(!gsTaskUtil.isValidIccIdPromoCode(simActivation.getIccID(), tenantId))
            {
                eventStatus = Configurations.StatusType.NOT_PROCESSED;
            }

            //Create PromoEvent bean
            PromoEventDBBean promoEventDBBean = new PromoEventDBBean();
            // id is from Customer DB bean
            promoEventDBBean.setCustomerId(customerDBBean.getId());
            promoEventDBBean.setEventData(eventData);
            promoEventDBBean.setEventMetaData(eventMetaData);
            promoEventDBBean.setEventType(Configurations.PromoEventType.SIM_ACTIVATION_EVENT);
            promoEventDBBean.setStatus(eventStatus);
            promoEventDBBean.setTenantId(tenantId);

            //Store event with appropriate status message
            promoEventDao.addPromoEvent(promoEventDBBean);
        }
    }

    @Override
    /**
     *   Only for SIM 5.0 or GigSky SIM. Not for ACME SIM
     */
    protected boolean checkForDuplicates(CustomerDBBean customerDBBean,
                                         EventMessageData eventMessageData,
                                         String eventType)
    {
        //Get simActivation event object, to get iccid
        SimActivation simActivation = (SimActivation) eventMessageData;

        //SIMActivation, if gigsky sim only one entry using iccid for customerId
        String keyValuePairString =
                getUniqueEventDataAttribute("iccID",simActivation.getIccID(), true);

        //Check there is already SIM activation event received for the customer using promoTableDao
        PromoEventDBBean promoEventDBBean = promoEventDao.getPromoEvent(customerDBBean.getId(),
                Configurations.PromoEventType.SIM_ACTIVATION_EVENT,
                keyValuePairString, customerDBBean.getTenantId());

        return (promoEventDBBean != null);
    }

    public String getLanguageUsingCountryCode(String countryCode) {

        try {
            JSONObject countriesObject = new JSONObject(countryLanguageList);
            JSONArray countriesList = countriesObject.getJSONArray("list");

            for(int i = 0; i < countriesList.length(); i++) {

                JSONObject countriesInfo =  countriesList.getJSONObject(i);
                String cCode = countriesInfo.optString("code");
                if(countryCode.toLowerCase().equals(cCode.toLowerCase())) {
                    return countriesInfo.optString("langCode");
                }
            }
        } catch (Exception e){
            logger.error("Getting language code from countries language for country code:" + countryCode, e);
        }

        return "en";
    }

}
