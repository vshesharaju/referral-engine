package com.gigsky.backend.messages.receiver;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * Created by pradeepragav on 20/01/17.
 */
public interface NotificationReceiver {

    void consume() throws InterruptedException, TimeoutException, IOException;
}
