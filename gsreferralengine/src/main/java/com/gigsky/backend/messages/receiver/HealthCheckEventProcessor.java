package com.gigsky.backend.messages.receiver;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.HealthCheckEventDBBean;
import com.gigsky.database.dao.HealthCheckEventDao;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.service.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;

/**
 * Created by prsubbareddy on 21/12/17.
 *
 * HealthCheckEventProcessor will process all health check events and
 * store in Health Check DB
 *
 */
public class HealthCheckEventProcessor extends EventMessageProcessor {

    @Autowired
    private HealthCheckEventDao healthCheckEventDao;

    @Override
    public void handleEvents(EventMessageData messageData, String eventData, String eventMetaData)
            throws IOException, ClassNotFoundException, BackendRequestException, ServiceException
    {

        //Check for event type
        String eventType = getEventType(messageData);

        if(Configurations.ReferralEventType.HEALTH_CHECK_EVENT.equals(eventType))
        {
            // We will be having only one entry or else no entries
            HealthCheckEventDBBean healthCheckEventDBBean = healthCheckEventDao.getHealthCheckEvent();

            if(healthCheckEventDBBean != null)
            {
                healthCheckEventDBBean.setEventMetaData(eventMetaData);
                healthCheckEventDBBean.setEventReceivedTime(CommonUtils.getCurrentTimestamp());
                healthCheckEventDao.updateHealthCheckEventInfo(healthCheckEventDBBean);
            } else {

                healthCheckEventDBBean = new HealthCheckEventDBBean();
                healthCheckEventDBBean.setEventType(eventType);
                healthCheckEventDBBean.setEventMetaData(eventMetaData);
                healthCheckEventDBBean.setEventReceivedTime(CommonUtils.getCurrentTimestamp());
                healthCheckEventDao.addHealthCheckEventInfo(healthCheckEventDBBean);
            }
        }

    }
}
