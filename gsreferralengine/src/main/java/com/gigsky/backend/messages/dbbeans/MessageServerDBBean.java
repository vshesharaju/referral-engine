package com.gigsky.backend.messages.dbbeans;

import java.sql.Timestamp;

/**
 * Created by pradeepragav on 20/01/17.
 */
public class MessageServerDBBean {
    private long id;            //| int(11)      | NO   | PRI | NULL              |
    private String host;        //| varchar(200) | NO   |     | NULL              |
    private String port;        //| varchar(45)  | NO   |     | NULL              |
    private String username;    //| varchar(100) | YES  |     | NULL              |
    private String password;    //| varchar(250) | YES  |     | NULL              |
    private String virtualHost; //| varchar(200) | YES  |     | NULL              |
    private String encVersion;  //| varchar(10)  | YES  |     | NULL              |
    private Timestamp createTime;  //| timestamp    | YES  |     | CURRENT_TIMESTAMP |
    private Timestamp updateTime;  //| timestamp    | YES  |     | NULL              |

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVirtualHost() {
        return virtualHost;
    }

    public void setVirtualHost(String virtualHost) {
        this.virtualHost = virtualHost;
    }

    public String getEncVersion() {
        return encVersion;
    }

    public void setEncVersion(String encVersion) {
        this.encVersion = encVersion;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }
}
