package com.gigsky.backend.messages.receiver;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.PromoEventDBBean;
import com.gigsky.database.bean.PromotionDBBean;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.PromoEventDao;
import com.gigsky.database.dao.PromotionDao;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.messaging.beans.UserSignup;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.util.List;

/**
 * Created by pradeepragav on 29/01/19.
 */
public class SignUpPromoEventProcessor extends EventMessageProcessor {

    private static final Logger logger = LoggerFactory.getLogger(SignUpPromoEventProcessor.class);

    @Autowired
    CustomerDao customerDao;

    @Autowired
    PromotionDao promotionDao;

    @Autowired
    PromoEventDao promoEventDao;

    @Autowired
    private GSUtil gsUtil;

    @Override
    public void handleEvents(EventMessageData messageData, String eventData, String eventMetaData)
            throws IOException, ClassNotFoundException, BackendRequestException, ServiceException, ResourceValidationException
    {
        UserSignup userSignup = (UserSignup)messageData;

        int tenantId = gsUtil.extractTenantIdFromMessageData(messageData);
        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(messageData.getCustomerId().intValue(), tenantId);

        String eventStatus = Configurations.StatusType.STATUS_NEW;

        if(customerDBBean != null)
        {
            //Check for duplicates
            if(checkForDuplicates(customerDBBean, messageData, Configurations.ReferralEventType.SIGNUP_EVENT))
            {
                eventStatus = Configurations.StatusType.DUPLICATE_ENTRY;
            }
        }
        else
        {
            customerDBBean = new CustomerDBBean();
            customerDBBean.setTenantId(tenantId);
            customerDBBean.setGsCustomerId(userSignup.getCustomerId());
            customerDBBean.setEmailId(userSignup.getEmailId());
            customerDBBean.setStatus(userSignup.getAccountActivationStatus());
            customerDao.createCustomerInfo(customerDBBean);
        }

        //Check if the referralCode is a valid promotion
        if(!validatePromotion(userSignup.getReferralCode(), tenantId))
        {
            eventStatus = Configurations.StatusType.STATUS_COMPLETE;
        }

        //Store the event in Promotion Table
        //Create PromoEvent bean
        PromoEventDBBean promoEventDBBean = new PromoEventDBBean();
        // id is from Customer DB bean
        promoEventDBBean.setCustomerId(customerDBBean.getId());
        promoEventDBBean.setEventData(eventData);
        promoEventDBBean.setEventMetaData(eventMetaData);
        promoEventDBBean.setEventType(Configurations.PromoEventType.SIGNUP_EVENT);
        promoEventDBBean.setStatus(eventStatus);
        promoEventDBBean.setTenantId(tenantId);

        //Store event with appropriate status message
        promoEventDao.addPromoEvent(promoEventDBBean);
    }


    @Override
    protected boolean checkForDuplicates(CustomerDBBean customerDBBean, EventMessageData eventMessageData, String eventType) {

        List<PromoEventDBBean> promoEvents = promoEventDao.getPromoEvent(customerDBBean.getId(), customerDBBean.getTenantId(), eventType);
        if(promoEvents != null && promoEvents.size() > 0)
        {
            return true;
        }

        return false;
    }


    private boolean validatePromotion(String promoCode, int tenantId)
    {
        if(promoCode == null)
            return false;

        PromotionDBBean promotion = promotionDao.getPromotion(promoCode, tenantId);

        //Check only active promo
        if (promotion != null && promotion.getActivePromo() == 1) {
            return true;
        }

        return false;
    }
}
