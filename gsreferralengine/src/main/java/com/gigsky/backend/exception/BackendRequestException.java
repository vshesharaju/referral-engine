package com.gigsky.backend.exception;

import com.gigsky.exception.ReferralException;
import com.gigsky.rest.exception.ErrorCode;

/**
 * Created by pradeepragav on 22/02/17.
 */
public class BackendRequestException extends ReferralException {

    public BackendRequestException(ErrorCode code)
    {
        super(code);
    }

    public BackendRequestException(String message)
    {
        super(message);
    }

    public BackendRequestException(String message, Exception e)
    {
        super(message, e);
    }

    public BackendRequestException(Exception e)
    {
        super(e);
    }
}
