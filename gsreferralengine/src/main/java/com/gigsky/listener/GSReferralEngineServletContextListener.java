package com.gigsky.listener;

import com.gigsky.backend.messages.receiver.EventMessageReceiver;
import com.gigsky.common.Configurations;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.executor.*;
import com.gigsky.scheduler.AutowiringSpringBeanJobFactory;
import com.gigsky.scheduler.ReferralQueueMonitorScheduler;
import com.gigsky.scheduler.ReferralEngineJob;
import org.apache.commons.lang.StringUtils;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

/**
 * Created by pradeepragav on 24/01/17.
 * Servlet context listener which is used to init event handlers
 */
public class GSReferralEngineServletContextListener implements ServletContextListener{

    private static final Logger logger = LoggerFactory.getLogger(GSReferralEngineServletContextListener.class);

    private static Scheduler scheduler;
    private static Scheduler referralQueueMonitorScheduler;


    private static final String DEFAULT_SCHEME_CRON_EXPRESSION = "0 0 0 1/1 * ? *";
    private static final String DEFAULT_REFERRAL_QUEUE_MONITOR_CRON_EXPRESSION = "0 0/10 * 1/1 * ? *";

    @Autowired
    private EventMessageReceiver eventMessageReceiver;

    @Autowired
    private TimerEventsExecutor timerEventsExecutor;

    @Autowired
    private RetryCreditExecutor retryCreditExecutor;

    @Autowired
    private ReferralProgramEmailExecutor referralProgramEmailExecutor;

    @Autowired
    private PromotionEventExecutor promotionEventExecutor;

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {

        logger.debug("initializing context ...");

        //Get Spring’s context
        ApplicationContext context =  WebApplicationContextUtils.getRequiredWebApplicationContext(servletContextEvent.getServletContext());

        //Get BeanFactory
        AutowireCapableBeanFactory beanFactory = context.getAutowireCapableBeanFactory();

        //Autowire
        beanFactory.autowireBean(this);

        try {
            startExecutingEvents();
            startRetryCreditProcess();
            startReferralProgramProcess();
            startSchemeValidityScheduler();
            startPromotionEventTaskScheduler();
            startReferralQueueMonitorScheduler();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

        logger.debug("context initialized");
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

        logger.debug("context contextDestroyed++");


        try {
            stopExecutingEvents();
            stopRetryCreditProcess();
            stopReferralProgramProcess();
            stopSchemeValidityScheduler();
            stopPromotionEvenTaskScheduler();
            stopReferralQueueMonitorScheduler();
        } catch (SchedulerException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.debug("context contextDestroyed--");
    }

    //Start the processing of events periodically
    private void startExecutingEvents() throws ExecutionException, InterruptedException {
        try {

            timerEventsExecutor.start();

            eventMessageReceiver.consume();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (TimeoutException e) {
            e.printStackTrace();
        }
    }

    //Stopping the processing of events
    private void stopExecutingEvents() throws IOException, TimeoutException {

        timerEventsExecutor.shutdown();

        //Stop listening to message queue
        eventMessageReceiver.shutDown();
    }

    //Start the processing of retry credit process periodically
    private void startRetryCreditProcess() throws ExecutionException, InterruptedException {
        try {

            retryCreditExecutor.start();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Stopping the processing of retry credit
    private void stopRetryCreditProcess() {
        retryCreditExecutor.shutdown();
    }

    //Start the processing of retry credit process periodically
    private void startReferralProgramProcess() throws ExecutionException, InterruptedException {
        try {

            referralProgramEmailExecutor.start();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    //Stopping the processing of retry credit
    private void stopReferralProgramProcess() {
        referralProgramEmailExecutor.shutdown();
    }

    private void startSchemeValidityScheduler() throws SchedulerException {

        //Create jobDetail for specifying the class where the task has to be performed
        JobDetail jobdetail = JobBuilder.newJob(ReferralEngineJob.class)
                .withIdentity("schemeValidityScheduler", "schemeGroup").build();

        String cronExpression = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.SCHEME_CRON_EXPRESSION);

        if(StringUtils.isEmpty(cronExpression)){

            cronExpression = DEFAULT_SCHEME_CRON_EXPRESSION;
        }

        //Create a trigger using CronScheduleBuilder which
        //uses a cron expression to create a trigger
        Trigger trigger = TriggerBuilder
                .newTrigger()
                .withIdentity("schemeValiditySchedulerTrigger", "schemeTriggerGroup")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronExpression).inTimeZone(TimeZone.getTimeZone("UTC")))
                .build();

        //Initialize an instance of Scheduler
        scheduler = new StdSchedulerFactory().getScheduler();

        AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);

        //Bean jobfactory to autowire objects into job class
        scheduler.setJobFactory(jobFactory);

        //Start the scheduler
        scheduler.start();

        //Schedule the job using the JobDetail instance and the Trigger instance
        scheduler.scheduleJob(jobdetail, trigger);

    }

    private void stopSchemeValidityScheduler() throws SchedulerException {

        //Stop the scheduler
        scheduler.shutdown(true);
    }

    private void startPromotionEventTaskScheduler()
    {
        try {
            promotionEventExecutor.start();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void stopPromotionEvenTaskScheduler()
    {
        promotionEventExecutor.shutdown();
    }

    private void startReferralQueueMonitorScheduler() throws SchedulerException {

        //Create jobDetail for specifying the class where the task has to be performed
        JobDetail jobdetail = JobBuilder.newJob(ReferralQueueMonitorScheduler.class)
                .withIdentity("referralQueueMonitorScheduler", "referralQueueMonitorGroup").build();

        String cronExpression = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.REFERRAL_QUEUE_MONITOR_CRON_EXPRESSION);

        if(StringUtils.isEmpty(cronExpression)){

            cronExpression = DEFAULT_REFERRAL_QUEUE_MONITOR_CRON_EXPRESSION;
        }

        //Create a trigger using CronScheduleBuilder which
        //uses a cron expression to create a trigger
        Trigger trigger = TriggerBuilder
                .newTrigger()
                .withIdentity("referralQueueMonitorSchedulerTrigger", "referralQueueMonitorTriggerGroup")
                .withSchedule(
                        CronScheduleBuilder.cronSchedule(cronExpression).inTimeZone(TimeZone.getTimeZone("UTC")))
                .build();

        //Initialize an instance of Scheduler
        referralQueueMonitorScheduler = new StdSchedulerFactory().getScheduler();

        AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);

        //Bean job factory to autowire objects into job class
        referralQueueMonitorScheduler.setJobFactory(jobFactory);

        //Start the referral queue scheduler
        referralQueueMonitorScheduler.start();

        //Schedule the job using the JobDetail instance and the Trigger instance
        referralQueueMonitorScheduler.scheduleJob(jobdetail, trigger);
    }

    private void stopReferralQueueMonitorScheduler() throws SchedulerException {
        //Stop the referral queue scheduler
        referralQueueMonitorScheduler.shutdown(true);
    }

}
