package com.gigsky.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigsky.common.Configurations;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.database.dao.TenantConfigurationsKeyValueDao;
import com.gigsky.rest.bean.ErrorResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by jeyarajs on 08/12/15.
 */
public class HttpClientUtil {

    private static final String LOG_TAG = HttpClientUtil.class.getName();
    private static final Logger mLogger = Logger.getLogger(LOG_TAG);

    @Autowired
    GSTokenUtil gsTokenUtil;

    @Autowired
    ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    private TenantConfigurationsKeyValueDao tenantConfigurationsKeyValueDao;

    public class GSRestHttpResponse {

        private int httpCode = HttpStatus.SC_INTERNAL_SERVER_ERROR;
        private ErrorResponse errResponseBody = null;
        private String responseBody = null;

        public int getHttpCode() {
            return httpCode;
        }
        public void setHttpCode(int httpCode) {
            this.httpCode = httpCode;
        }
        public ErrorResponse getErrResponseBody() {
            return errResponseBody;
        }
        public void setErrResponseBody(ErrorResponse errResponseBody) {
            this.errResponseBody = errResponseBody;
        }
        public String getResponseBody() {
            return responseBody;
        }
        public void setResponseBody(String responseBody) {
            this.responseBody = responseBody;
        }

        @Override
        public String toString() {
            return "GSHttpResponse [httpCode=" + httpCode + ", errResponseBody=" + errResponseBody + ", responseBody=" + responseBody + "]";
        }

    }

    public GSRestHttpResponse sendHttpRequest(String aMethod, String aUrl, Object aBody, long aTimeoutMS) {
        return sendHttpRequest(aMethod, aUrl, null, aBody, null, aTimeoutMS);
    }

    private static Client mClient = null;
    private static int mClientTimeoutMS = 30 * 1000;

    static {
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        mClient = Client.create(clientConfig);

		/*
		 * Connection timeout can not be controlled through DB to take advantage of optimization
		 */
        mClient.setConnectTimeout(mClientTimeoutMS);
        mClient.setReadTimeout(mClientTimeoutMS);
    }

    /**
     *
     * @param aMethod
     * @param aUrl
     * @param aBody
     * @return
     */
    public GSRestHttpResponse sendHttpRequest(String aMethod, String aUrl, MultivaluedMap<String, String> aQueryParams, Object aBody, Map<String, String> aHeaders, long aTimeoutMS) {

        mLogger.debug("sendHttpRequest+++: "+aMethod+" "+aUrl);

        GSRestHttpResponse lResponse = new GSRestHttpResponse();
        WebResource.Builder builder = null;
        WebResource webResource = null;
        ClientResponse response = null;

        try {

			/*convert input object to string*/
            String reqBodyStr = null;
            if(aBody != null) {
                ObjectMapper lMapper = new ObjectMapper();
                lMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
                reqBodyStr = lMapper.writeValueAsString(aBody);
                //mLogger.debug("RequestBody:"+reqBodyStr);
            }

			/*Form http request*/
            if(aTimeoutMS != mClientTimeoutMS) {
                mLogger.debug("Http Client Timeout set; old ms:"+mClientTimeoutMS+" new ms:"+aTimeoutMS);
                mClient.setConnectTimeout((int) aTimeoutMS);
                mClient.setReadTimeout((int) aTimeoutMS);
                mClientTimeoutMS = (int) aTimeoutMS;
            }

            webResource = mClient.resource(aUrl);
            webResource.setProperty("Content-Type", "application/json;charset=UTF-8");
			/*insert query string*/
            if(aQueryParams != null) {
                webResource = webResource.queryParams(aQueryParams);
            }

            builder = webResource.accept("application/json;charset=UTF-8").type(MediaType.APPLICATION_JSON);

			/*insert headers*/
            if(aHeaders != null) {
                for (Map.Entry<String, String> entry : aHeaders.entrySet()) {
                    String header = entry.getKey();
                    String value = entry.getValue();
                    builder = builder.header(header, value);
                }
            }

            if(aMethod.compareToIgnoreCase("GET") == 0) {
                response = builder.get(ClientResponse.class);
            } else if(aMethod.compareToIgnoreCase("POST") == 0) {
                response = builder.post(ClientResponse.class, reqBodyStr);
            } else if(aMethod.compareToIgnoreCase("PUT") == 0) {
                response = builder.put(ClientResponse.class, reqBodyStr);
            } else if(aMethod.compareToIgnoreCase("DELETE") == 0) {

                if(StringUtils.isNotEmpty(reqBodyStr))
                    response = builder.delete(ClientResponse.class, reqBodyStr);
                else
                    response = builder.delete(ClientResponse.class);
            }

            lResponse.setHttpCode(response.getStatus());
            lResponse.setResponseBody(response.getEntity(String.class));

            if (response.getStatus() != ClientResponse.Status.OK.getStatusCode()) {
                mLogger.error("http error : "+response.getStatus() + " for URL: " + aUrl);

                JSONObject obj = new JSONObject(lResponse.getResponseBody());
                //Consider this case as invalid token or expired token and retry login
                if(obj != null && checkIfTokenBelongToAdmin(aHeaders) && obj.getInt("errorInt") ==  2001){
                    int tenantId = fetchTenantId(aHeaders);

                    //Token expired case retry request with new token
                    String token = gsTokenUtil.getTokenByLogin(tenantId);
                    if(token != null)
                    {
                        // update token to Database configurations
                        gsTokenUtil.updateTokenForConfigurations(token, tenantId);

                        // retry request with new token
                        return retryRequestIfTokenExpire(aMethod, aUrl, aBody, aTimeoutMS, token, tenantId);
                    }
                }
            }

            //mLogger.debug("httpResponse:"+lResponse.toString());

        } catch (JsonMappingException e) {
            lResponse.setHttpCode(HttpStatus.SC_BAD_REQUEST);
            mLogger.error("Exception for URL: " + aUrl);
        } catch (IOException e) {
            lResponse.setHttpCode(HttpStatus.SC_FORBIDDEN);
            mLogger.error("Exception for URL: " + aUrl);
        } catch (Exception e) {
            lResponse.setHttpCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            mLogger.error("Exception for URL: " + aUrl);
        }

        finally {

            try {
                if(response != null) {
                    response.close();
                }
            } catch(Exception e) {
                mLogger.error(null, e);
            }

        }

        mLogger.debug("sendHttpRequest---");
        return lResponse;
    }

    public GSRestHttpResponse retryRequestIfTokenExpire(String aMethod, String aUrl, Object aBody, long aTimeoutMS, String token, int tenantId) {

        //Create headers map
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", "Basic " +token);
        headers.put(GSUtil.TENANT_ID_HEADER, String.valueOf(tenantId));

        return sendHttpRequest(aMethod, aUrl, null, aBody, headers, aTimeoutMS);
    }


    /**
     *
     * @param <T>
     * @param aBody
     * @param aTargetClass
     * @return
     */
    public static <T> Object convertToBean(String aBody, Class<T> aTargetClass) {

        ObjectMapper lMapper = new ObjectMapper();
        try {

            // setting format
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            lMapper.setDateFormat(format);

            return lMapper.readValue(aBody, aTargetClass);
        } catch (Exception e) {
            mLogger.error("Exception:"+aBody, e);
        }
        return null;
    }


    public synchronized static void shutdown() {
        mLogger.debug("shutdown+++");
        try {
            if(mClient != null) {
                mClient.destroy();
            }
        } catch(Exception e) {
            mLogger.error(null, e);
        }
        mLogger.debug("shutdown---");
    }

    private boolean checkIfTokenBelongToAdmin(Map<String, String> aHeaders){

        if(aHeaders == null) return false;

        //Get token string from the aHeaders map
        String tokenWithBasicStr = aHeaders.get("Authorization");

        //If there is no token in header
        if(tokenWithBasicStr == null) return false;

        //Split the string, "Basic token"
        String[] tokenSubStr = tokenWithBasicStr.split(" ");

        //Get the token from the array of strings
        String token = tokenSubStr[1];

        int tenantId = fetchTenantId(aHeaders);

        //Get admin token from configurationsKey table
        String adminToken = tenantConfigurationsKeyValueDao.getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.ADMIN_TOKEN, tenantId);

        //If adminToken is null, then it is not a adminToken. As it is not created at first
        if(StringUtils.isEmpty(adminToken)) return false;

        //Compare the tokens
        return (adminToken.equals(token));
    }

    private int fetchTenantId(Map<String, String> aHeaders) {
        // Fetching default tenant id from config key values
        int tenantId = Integer.parseInt(configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.DEFAULT_TENANT_ID, "1"));

        if(aHeaders != null){
            String tenantIdHeader = aHeaders.get(GSUtil.TENANT_ID_HEADER);

            if(StringUtils.isNotEmpty(tenantIdHeader)){
                tenantId = Integer.parseInt(tenantIdHeader);
            }
        }

        return  tenantId;
    }
}
