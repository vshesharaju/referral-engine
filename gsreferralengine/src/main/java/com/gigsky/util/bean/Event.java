package com.gigsky.util.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gigsky.backend.ngw.beans.Option;

import java.util.List;

/**
 * Created by prsubbareddy on 15/02/17.
 */

@JsonInclude( JsonInclude.Include.NON_NULL)
public class Event {

    private String type;
    private String eventType;
    private String subEventType;
    private List<String> emailIds;
    private String language;
    private Option options;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getSubEventType() {
        return subEventType;
    }

    public void setSubEventType(String subEventType) {
        this.subEventType = subEventType;
    }

    public List<String> getEmailIds() {
        return emailIds;
    }

    public void setEmailIds(List<String> emailIds) {
        this.emailIds = emailIds;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Option getOptions() {
        return options;
    }

    public void setOptions(Option options) {
        this.options = options;
    }


}
