package com.gigsky.util.bean;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by prsubbareddy on 09/03/17.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
public class LoginResponse {

    private String type;
    private String token;
    private String expiryDate;
    private String customerId;
    private boolean forcePasswordChange;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public boolean isForcePasswordChange() {
        return forcePasswordChange;
    }

    public void setForcePasswordChange(boolean forcePasswordChange) {
        this.forcePasswordChange = forcePasswordChange;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
