package com.gigsky.util.bean;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by prsubbareddy on 14/02/17.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
public class AccountLogin {

    private String type;
    private String loginType;
    private String emailId;
    private String password;
    private String token;

    public void setType(String type){
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}