package com.gigsky.util;

import com.gigsky.backend.credit.CreditManagementInterface;
import com.gigsky.backend.customer.CustomerManagementInterface;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import com.gigsky.rest.bean.Credit;
import com.gigsky.service.AccountService;
import com.gigsky.service.exception.ServiceException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

/**
 * Created by pradeepragav on 03/02/17.
 */
public class GSAddCreditUtil {

    private final static String REF_ADV_DESC = "For referring";
    private final static String REF_SIGNUP_DESC = "For signing up with invite code";

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    private IncentiveSchemeDao incentiveSchemeDao;

    @Autowired
    private PreferenceDao preferenceDao;

    @Autowired
    private CreditManagementInterface creditManagementInterface;

    @Autowired
    private SupportedCurrenciesDao supportedCurrenciesDao;

    @Autowired
    CustomerManagementInterface customerManagementInterface;

    @Autowired
    GSTokenUtil gsTokenUtil;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    AccountService accountService;

    public static final String CREDIT_DETAILS_RESPONSE = "GigskyCreditResponse";

    //Check for ReferralCreditAdd, argument as type Configurations.ReferralEventType
    public boolean isReferralCreditAdd(String eventType){

        //Get value for AddCreditMode from configurationKeyValueDao
        String valueForAddCreditMethod = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.REFERRAL_CREDIT_ADD);

        if(valueForAddCreditMethod.equals(eventType)){
            return true;
        }
        return false;
    }

    //Check for AdvocateCreditAdd, argument as type Configurations.ReferralEventType
    public boolean isAdvocateCreditAdd(String eventType){

        //Get value for AddCreditMode from configurationKeyValueDao
        String valueForAddCreditMethod = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.ADVOCATE_CREDIT_ADD);

        if(valueForAddCreditMethod.equals(eventType)){
            return true;
        }
        return false;
    }

    public Timestamp getReferralCreditExpiry(){

        //Get current date
        Date currentDate = CommonUtils.getCurrentDate();

        //Get october 1st date (currYr-10-1 00:00:00)
        Calendar date = Calendar.getInstance();
        date.set(Calendar.MONTH, 9);
        date.set(Calendar.DAY_OF_MONTH, 1);
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        //Get java.sql.Date
        Date octoberDate = new java.sql.Date(date.getTimeInMillis());

        //Set month as December(currYr-12-31 23:59:59)
        Calendar expiryDate = Calendar.getInstance();
        expiryDate.set(Calendar.MONTH, 11);
        expiryDate.set(Calendar.DAY_OF_MONTH, 31);
        expiryDate.set(Calendar.HOUR_OF_DAY, 23);
        expiryDate.set(Calendar.MINUTE, 59);
        expiryDate.set(Calendar.SECOND, 59);
        expiryDate.set(Calendar.MILLISECOND, 59);


        //Check whether it is before october 1st
        if(octoberDate.compareTo(currentDate) <= 0)
        {
            //Set year
            expiryDate.set(Calendar.YEAR,expiryDate.get(Calendar.YEAR) + 1);
        }
        return new Timestamp(expiryDate.getTimeInMillis());
    }

    public Timestamp getReferralCreditExpiry(Integer expiryPeriodInDays){

        //Get current date
        Date currentDate = CommonUtils.getCurrentDate();

        Calendar date = Calendar.getInstance();

        if(expiryPeriodInDays < 0){
            return new Timestamp(date.getTimeInMillis());
        }

        date.add(Calendar.DAY_OF_MONTH, expiryPeriodInDays);

          //Check whether it is after currentDate date
        if(currentDate.compareTo(date.getTime()) > 0)
        {
            return null;
        }

        return new Timestamp(date.getTimeInMillis());

    }

    public Credit getGigSkyCreditDetailsOfCustomer(long gsCustomerId, int tenantId) throws BackendRequestException {

        //Check the credit balance of the advocate(Backend Call)
        return creditManagementInterface.getCreditDetailsOfCustomer((int)gsCustomerId, tenantId);
    }

    public boolean shouldAdminBeAlertedForAddingCredit(long gsCustomerId, int tenantId) throws BackendRequestException {

        //Check the credit balance of the advocate(Backend Call)
        Credit credit = getGigSkyCreditDetailsOfCustomer(gsCustomerId, tenantId);

        //Api call is successful
        if (credit != null &&
                CREDIT_DETAILS_RESPONSE.equals(credit.getType())){

            float creditBalance =  credit.getCreditBalance();

            // Get credit warning amount from Configurations
            float alertLimit = getCreditWarningAmount(credit.getCurrency(), tenantId);

            //Compare the ALERT credit amount with creditBalance
            if(creditBalance >= alertLimit){

                return true;
            }
        }

        return false;
    }

    public float getCreditWarningAmount(String currency, int tenantId) {

        //Get the ALERT value from Config table(in USD)
        String strFloat =  configurationKeyValueDao
                .getConfigurationValue(Configurations.ConfigurationKeyValueType.ALERT_CREDIT_AMOUNT);

        //Convert String into float
        float alertLimit = Float.parseFloat(strFloat);

        //Check if currency is not USD
        if(!"USD".equals(currency)){

            //Get the usdMultiplier
            SupportedCurrencyDBBean supportedCurrencyDBBean =
                    supportedCurrenciesDao.getSupportedCurrencyDetails(currency, tenantId);

            float usdMultiplier = supportedCurrencyDBBean.getUsdMultiplier().floatValue();

            //Update the alert Limit
            alertLimit = alertLimit * usdMultiplier;
        }

        return alertLimit;
    }

    public Credit getCreditDetailsForCustomer(CustomerDBBean customerDBBean, ReferralCodeDBBean referralCodeDBBean, boolean isAdvocate) throws ServiceException, BackendRequestException {

        //Get customer currency
        PreferenceDBBean preferenceDBBean;

        Integer customerId = (isAdvocate) ? customerDBBean.getAdvocateId() : customerDBBean.getId();

        preferenceDBBean = preferenceDao.getPreferenceByKey(customerId, Configurations.PreferrenceKeyType.PREFERRED_CURRENCY);

        //Get active incentive scheme
        Map<String, Object> incentiveSchemeMap = incentiveSchemeDao.getActiveSchemeForReferralCode(referralCodeDBBean.getReferralCode(), preferenceDBBean.getValue(), customerDBBean.getTenantId());

        //Create credit bean
        Credit credit = new Credit();

        List<CreditSchemeDBBean> creditSchemeDBBeans = (ArrayList<CreditSchemeDBBean>)incentiveSchemeMap.get(Configurations.CREDIT_SCHEME_BEAN);
        CreditSchemeDBBean creditSchemeDBBean =  creditSchemeDBBeans.get(0);

        String creditType;

        if(isAdvocate){
            //Set balance
            credit.setCreditBalance(creditSchemeDBBean.getAdvocateAmount());

            creditType = Configurations.CreditType.REF_ADV;

            // Getting backend customer info for name
            long gsCustomerId = customerDBBean.getGsCustomerId();

            String customerName = (customerDBBean.getFirstName() != null) ? customerDBBean.getFirstName() : (accountService.getCustomerDetails(gsCustomerId, customerDBBean.getTenantId()) != null)? accountService.getCustomerDetails(gsCustomerId, customerDBBean.getTenantId()).getFirstName(): null;

            // if customer name is null, then fetch customer email
            if(StringUtils.isEmpty(customerName)){
                customerName = customerDBBean.getEmailId();
            }

            // Check If advocate language other than en, fetch localized string.
            String advocateDesc = REF_ADV_DESC  + " "+customerName;
            credit.setDescription(advocateDesc);
        }
        else {
            //Set balance
            credit.setCreditBalance(creditSchemeDBBean.getNewCustomerAmount());

            creditType = Configurations.CreditType.REF_SIGNUP;

            // Check If customer language other than en, fetch localized string.
            credit.setDescription(REF_SIGNUP_DESC);
        }

        //Set preferredCurrency
        credit.setCurrency(preferenceDBBean.getValue());

        //Set creditType
        credit.setCreditType(creditType);

        IncentiveSchemeDBBean incentiveSchemeDBBean = (IncentiveSchemeDBBean)incentiveSchemeMap.get(Configurations.INCENTIVE_SCHEME_BEAN);
        Integer expiryPeriod = incentiveSchemeDBBean.getReferralCreditExpiryPeriod();

        credit.setExpiry(getReferralCreditExpiry(expiryPeriod));

        //Set values using incentiveScheme for this referralCode
//        credit.setExpiry(creditExpiry);
        return credit;
    }
}
