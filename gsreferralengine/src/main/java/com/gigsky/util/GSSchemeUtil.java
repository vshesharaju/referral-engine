package com.gigsky.util;

import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.common.GigskyDuration;
import com.gigsky.database.bean.PromoCreditDBBean;
import com.gigsky.database.bean.PromoHistoryDBBean;
import com.gigsky.database.dao.SupportedCurrenciesDao;
import com.gigsky.rest.bean.Credit;
import com.gigsky.rest.exception.ResourceValidationException;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by shwethars on 11/01/19.
 */
public class GSSchemeUtil {

    @Autowired
    private SupportedCurrenciesDao supportedCurrenciesDao;

    public static List<String> updateCountryCodesToUpperCase(List<String> countryList)
    {
        List<String> uppercaseCountriesList = new ArrayList<String>();
        if(countryList != null) {

            for(int i = 0; i < countryList.size(); i++)
            {
                String countryCode = countryList.get(i);
                uppercaseCountriesList.add(countryCode.toUpperCase());
            }

        }
        return uppercaseCountriesList;
    }


    //Returns true if current time is within the parameter start time and end time
    public static boolean utilMethodDoesTimelineMatchesCurrentTimeline(java.sql.Date startDate, java.sql.Date endDate){

        //Get currentDate
        java.sql.Date currentDate = CommonUtils.getCurrentDate();

        //Compare current date with startDate and endDate
        return (currentDate.compareTo(startDate) >= 0 && currentDate.compareTo(endDate) <= 0);
    }

    public static boolean isValidStartDate(String startDate, String endDate, boolean isAddFlow) throws ResourceValidationException {

        boolean isValidStartDate = true;

        if(isAddFlow && (startDate == null || !CommonUtils.isValidDateFormat(startDate))) {
            isValidStartDate = false;

        } else if((startDate != null && !CommonUtils.isValidDateFormat(startDate)) || ((startDate == null && endDate != null))) {
            isValidStartDate = false;
        }

        return isValidStartDate;
    }

    public static boolean isValidEndDate(String startDate, String endDate, boolean isAddFlow) throws ParseException {

        if(isAddFlow && (endDate == null || !CommonUtils.isValidDateFormat(endDate))) {
            return false;
        }
        else if ((startDate != null && endDate == null) || (startDate != null && endDate != null && !CommonUtils.isValidDateFormat(endDate))) {
            return false;
        }

        // Check added to avoid crash If both start and end dates are not null for update case
        if(startDate != null && endDate != null) {

            Date startDateTime = CommonUtils.convertStringToDate(startDate);
            Date endDateTime = CommonUtils.convertStringToDate(endDate);

            // End date should be greater than start date and greater than or equals to current date
            if(startDateTime.compareTo(endDateTime) > 0 || endDateTime.compareTo(CommonUtils.getCurrentDate()) < 0) {
                return false;
            }
        }

        return true;
    }

    private static boolean isValueInteger(int value) {
        return (value == Math.round(value));
    }

    public static boolean isValidExpiryPeriod(Integer expiryPeriod, boolean isAddFlow) {
        if(isAddFlow && (expiryPeriod == null || expiryPeriod == 0 || expiryPeriod <= 0 || ! isValueInteger(expiryPeriod))) {
            return false;
        } else if(expiryPeriod != null && (expiryPeriod <= 0  || ! isValueInteger(expiryPeriod))) {
            return false;
        }
        return true;
    }

    public static boolean isValidMaxRedeemCount(Integer maxRedeemCount, boolean isAddFlow)
    {
        if(isAddFlow && (maxRedeemCount == null || maxRedeemCount == 0 || maxRedeemCount <= 0 || ! isValueInteger(maxRedeemCount))) {
            return false;
        } else if(maxRedeemCount != null && (maxRedeemCount <= 0  || ! isValueInteger(maxRedeemCount))) {
            return false;
        }
        return true;
    }


    public static boolean isValidCreditAmount(String currency, float amount) throws ResourceValidationException {
        return (amount <= 0 || (!currency.equals("JPY") && !isValidAmountWithDecimals(amount)) || (currency.equals("JPY") && !isValidAmountWithOutDecimals(amount)));
    }

    private static boolean isValidAmountWithDecimals(float amount)
    {
        // Other than JPY currency, all currency amounts will have decimals
        String[] splitter = Float.toString(amount).split("\\.");
        int decimalLength = splitter[1].length();  // After Decimal Count
        return (decimalLength <= 2);
    }

    private static boolean isValidAmountWithOutDecimals(float amount)
    {
        // for JPY currency, Amount won't have decimals
        String[] splitter = Float.toString(amount).split("\\.");
        String decimalSplit = splitter[1];  // After Decimal string
        return (decimalSplit == null ||  decimalSplit.equals("0") || decimalSplit.equals("00"));
    }

    public static boolean doesValidityTimeClash(Date validityStartOfNewScheme, Date validityEndOfNewScheme ,Date validityStartOfExistingScheme,Date validityEndOfExistingScheme){

        //The range of the new scheme should not clash with the range of the existing scheme

        //The start date of new scheme should be after the end date of existing scheme OR
        //The end date of the scheme should be before the start date of the existing scheme

        if ((!(validityStartOfNewScheme.after(validityEndOfExistingScheme) ||
                validityEndOfNewScheme.before(validityStartOfExistingScheme))))
            return true;
        else
            return false;
    }

    public static boolean validateIsEnabled(String s) throws ResourceValidationException
    {
        return !(!s.equals("true") && !s.equals("false"));
    }

    public static Credit prepareCreditDetails(String currency, PromoCreditDBBean promoCreditDBBean)
    {
        //Create credit bean
        Credit credit = new Credit();
        credit.setCurrency(currency);
        credit.setCreditBalance(promoCreditDBBean.getCreditAmount());
        credit.setCreditType(Configurations.CreditType.PROMOTION);
        credit.setDescription(Configurations.CreditDesc.PROMO_CREDIT_DESC);
        return credit;
    }

    public static Credit prepareCreditDetails(String currency, PromoCreditDBBean promoCreditDBBean, int expiryPeriodInDays)
    {
        //Create credit bean
        Credit credit = new Credit();
        credit.setCurrency(currency);
        credit.setCreditBalance(promoCreditDBBean.getCreditAmount());
        credit.setCreditType(Configurations.CreditType.PROMOTION);

        //Get current date
        Calendar expiryDate = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        expiryDate.add(Calendar.DATE, expiryPeriodInDays);
        credit.setExpiry(new Timestamp(expiryDate.getTimeInMillis()));
        credit.setDescription(Configurations.CreditDesc.PROMO_CREDIT_DESC);
        return credit;
    }

    public static PromoHistoryDBBean getPromoHistoryBean(int gsCustomerId, String currency, int promoId, int promoEventId,
                                                   String iccid, Float creditAmount, int transactionId, String emailStatus, int retryCount, String status)
    {

        PromoHistoryDBBean promoHistoryDBBean = new PromoHistoryDBBean();
        promoHistoryDBBean.setGsCustomerId((long) gsCustomerId);
        promoHistoryDBBean.setCurrency(currency);
        promoHistoryDBBean.setPromotionId(promoId);
        if(promoEventId != 0){
            promoHistoryDBBean.setPromoEventId(promoEventId);
        }
        promoHistoryDBBean.setIccid(iccid);
        promoHistoryDBBean.setCreditAmount(creditAmount);
        promoHistoryDBBean.setCreditTxnId(transactionId);
        promoHistoryDBBean.setEmailNotifStatus(emailStatus);
        promoHistoryDBBean.setRetryCount(retryCount);
        promoHistoryDBBean.setStatus(status);
        return promoHistoryDBBean;
    }

    public boolean isValidCurrency(String currency, int tenantId) throws ResourceValidationException
    {
        List<String> supportedCurrencies = supportedCurrenciesDao.getSupportedCurrencies(tenantId);

        return !(currency == null || supportedCurrencies == null || !supportedCurrencies.contains(currency));
    }


}
