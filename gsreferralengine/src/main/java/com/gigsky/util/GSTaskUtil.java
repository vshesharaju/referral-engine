package com.gigsky.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigsky.backend.Info.BackendInfoManagementInterface;
import com.gigsky.backend.credit.CreditManagementInterface;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.backend.ngw.NgwManagementInterface;
import com.gigsky.backend.ngw.beans.Option;
import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.common.GigskyDuration;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.referralCode.ReferralCodeManagement;
import com.gigsky.rest.bean.Credit;
import com.gigsky.rest.bean.ReferralCode;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.AccountService;
import com.gigsky.service.exception.ServiceException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by prsubbareddy on 01/02/17.
 */
public class GSTaskUtil {

    private static final Logger logger = LoggerFactory.getLogger(GSTaskUtil.class);

    private static String DEFAULT_SIGNUP_EVENT_DELAY_MS = "000000000200000";

    @Autowired
    private CreditManagementInterface creditManagementInterface;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    private TenantConfigurationsKeyValueDao tenantConfigurationsKeyValueDao;

    @Autowired
    private PreferenceDao preferenceDao;

    @Autowired
    private GSAddCreditUtil gsAddCreditUtil;

    @Autowired
    private InvitationDao invitationDao;

    @Autowired
    private ReferralCodeDao referralCodeDao;

    @Autowired
    private ReferralEventDao referralEventDao;

    @Autowired
    private ReferralCodeManagement referralCodeManagement;

    @Autowired
    private BackendInfoManagementInterface backendInfoManagementInterface;

    @Autowired
    private NgwManagementInterface ngwManagementInterface;

    @Autowired
    GSTokenUtil gsTokenUtil;

    @Autowired
    private AccountService accountService;

    @Autowired
    private PromotionDao promotionDao;

    @Autowired
    private GSUtil gsUtil;

    @Autowired
    private PromoIccidDao promoIccidDao;

    //Create Preference details
    public void createPreference(int customerId, String pKey, String value) {

        PreferenceDBBean preferenceDBBean = preferenceDao.getPreferenceByKey(customerId, pKey);
        if (preferenceDBBean == null) {
            preferenceDBBean = new PreferenceDBBean();
            preferenceDBBean.setCustomerId(customerId);
            preferenceDBBean.setPkey(pKey);
            preferenceDBBean.setValue(value);
            preferenceDao.addPreference(preferenceDBBean);
        }
        else
        {
            preferenceDBBean.setValue(value);
            preferenceDao.updatePreference(preferenceDBBean);
        }
    }

    public ReferralCodeDBBean createReferralCodeForCustomer(CustomerDBBean customerDBBean) throws ServiceException {

        //Synchronize referralCode generation
        synchronized (this) {
            //Check if the Referral Code for DEFAULT channel is available
            ReferralCodeDBBean referralCodeDBBean = referralCodeDao.getActiveReferralCodeForChannel(customerDBBean.getId(), Configurations.ReferralCodeChannelType.DEFAULT);

            //If the referral code is not generated for DEFAULT channel then generate it
            if (referralCodeDBBean == null) {
                //Create the referral code for the active scheme for the new customer
                ReferralCode referralCode = new ReferralCode();
                referralCode.setCustomerId(customerDBBean.getId());
                referralCode.setEmailId(customerDBBean.getEmailId());
                referralCode.setChannel(Configurations.ReferralCodeChannelType.DEFAULT);

                referralCodeDBBean = referralCodeManagement.createReferralCodeForActiveScheme(referralCode, customerDBBean);
                int referralCodeId = referralCodeDao.addReferralCode(referralCodeDBBean);

                //Add an entry in the referral code history table with the scheme details
                ReferralCodeHistoryDBBean referralCodeHistoryDBBean = referralCodeManagement.createReferralCodeHistory(referralCodeId, referralCodeDBBean.getIncentiveSchemeId());
                referralCodeDao.addReferralCodeHistory(referralCodeHistoryDBBean);
            }

            return referralCodeDBBean;
        }
    }

    public boolean checkReferralEnable(int tenantId) {

        String isEnabledStr = tenantConfigurationsKeyValueDao.getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.REFERRAL_ENABLE, tenantId);

        return ("true".equals(isEnabledStr));
    }

    public static <T extends EventMessageData> T getMessageData(String eventData, String eventType) throws IOException, ClassNotFoundException {

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonData = eventData;
        String objectType = eventTypeToObjectTypeMappingForMessagingBeans(eventType);
        Class classOfJsonInstance = Class.forName("com.gigsky.messaging.beans." + objectType);
        Object eventMessageData = objectMapper.readValue(jsonData, classOfJsonInstance);
        return (T) eventMessageData;
    }

    public static String eventTypeToObjectTypeMappingForMessagingBeans(String eventType) {

        if (Configurations.ReferralEventType.SIGNUP_EVENT.equals(eventType)) {
            return Configurations.MessageBeansObjectType.SIGNUP_EVENT;
        } else if (Configurations.ReferralEventType.SIM_ACTIVATION_EVENT.equals(eventType)) {
            return Configurations.MessageBeansObjectType.SIM_ACTIVATION_EVENT;
        } else if (Configurations.ReferralEventType.PURCHASE_EVENT.equals(eventType)) {
            return Configurations.MessageBeansObjectType.PURCHASE_EVENT;
        } else if (Configurations.ReferralEventType.PLAN_EXPIRED_EVENT.equals(eventType)) {
            return Configurations.MessageBeansObjectType.PLAN_EXPIRED_EVENT;
        } else if (Configurations.ReferralEventType.CREDIT_NEARING_EXPIRY_EVENT.equals(eventType)
                || Configurations.ReferralEventType.CREDIT_EXPIRED_EVENT.equals(eventType)) {
            return Configurations.MessageBeansObjectType.CREDIT_EXPIRED_EVENT;
        } else if (Configurations.ReferralEventType.CURRENCY_CHANGE_EVENT.equals(eventType)) {
            return Configurations.MessageBeansObjectType.CURRENCY_CHANGE_EVENT;
        } else if (Configurations.ReferralEventType.EMAIL_ID_CHANGE_EVENT.equals(eventType)) {
            return Configurations.MessageBeansObjectType.EMAIL_ID_CHANGE_EVENT;
        } else if(Configurations.ReferralEventType.ACCOUNT_INFORMATION_CHANGE_EVENT.equals(eventType)) {
            return Configurations.MessageBeansObjectType.ACCOUNT_INFORMATION_CHANGE_EVENT;
        }
        else if(Configurations.ReferralEventType.ACCOUNT_DELETE_EVENT.equals(eventType))
        {
            return Configurations.MessageBeansObjectType.ACCOUNT_DELETE_EVENT;
        }
        return "";
    }


    public void addCredit(ReferralEventDBBean referralEventDBBean) throws Exception {

        String eventType = referralEventDBBean.getEventType();
        EventMessageData eventMessageData = GSTaskUtil.getMessageData(referralEventDBBean.getEventData(), eventType);

        //Check referralProgram enable
        if (checkReferralEnable(referralEventDBBean.getTenantId())) {

            //Check the event type
            if (Configurations.ReferralEventType.PURCHASE_EVENT.equals(eventType) || Configurations.ReferralEventType.PLAN_EXPIRED_EVENT.equals(eventType)) {
                //Advocate credit add
                if (gsAddCreditUtil.isAdvocateCreditAdd(eventType)) {
                    addCredit(eventMessageData, true);
                }

            } else {
                //New customer credit add
                if (gsAddCreditUtil.isReferralCreditAdd(eventType)) {
                    addCredit(eventMessageData, false);
                }
            }
        } else {

            //Get customerId using referralEventDBBean>eventData>gsCustomerId
            long gsCustomerId = eventMessageData.getCustomerId();
            CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, referralEventDBBean.getTenantId());

            if (Configurations.ReferralEventType.SIGNUP_EVENT.equals(eventType)) {

                // update customer status as SIGNUP_WITHOUT_REFERRAL
                customerDBBean.setReferralStatus(Configurations.ReferralStatus.SIGNUP_WITHOUT_REFERRAL);
                customerDao.updateCustomerInfo(customerDBBean);

            } else if(Configurations.ReferralEventType.PURCHASE_EVENT.equals(eventType)) {

                //Get invitation for newCustomerId
                InvitationDBBean invitationDBBean = invitationDao.getInvitationByTargetCustomerId(customerDBBean.getId());
                if(invitationDBBean != null) {
                    invitationDBBean.setStatus(Configurations.CustomerStatus.STATUS_USING_SERVICE);
                    invitationDao.updateInvitation(invitationDBBean);
                }
            }

        }

        // update referral event with status as COMPLETE
        referralEventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
        referralEventDao.updateReferralEvent(referralEventDBBean);
    }

    // This method will take care to add credit for advocate or referral
    public void addCredit(EventMessageData eventmessageData, boolean isAdvocate) throws ServiceException, BackendRequestException, ResourceValidationException {

        //Get invitation using advocateId
        long gsCustomerId = eventmessageData.getCustomerId();
        int tenantId = gsUtil.extractTenantIdFromMessageData(eventmessageData);

        //Get customerDBBean of the customerID
        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, tenantId);

        //Get advocateId using newGsreCustomerId
        int advocateCustomerId = customerDBBean.getAdvocateId();

        //Check if advocateId is not set
        if (advocateCustomerId != 0) {

            //Get invitation for newCustomerId
            InvitationDBBean invitationDBBean = invitationDao.getInvitationByTargetCustomerId(customerDBBean.getId());

            processAddCreditForCustomer(invitationDBBean, customerDBBean, isAdvocate, null);
        }
    }

    // this method will take care to retry add credit for advocate or referral
    public void retryCredit(InvitationDBBean invitationDBBean, boolean isAdvocate, int retryCount) throws ServiceException, BackendRequestException {

        // Fetching customer bean using invitation target customer id
        int customerId = invitationDBBean.getTargetCustomerId();
        CustomerDBBean customerDBBean = customerDao.getCustomerInfoWithOutTenantId(customerId);

        //If customer is deleted, then
        if(customerDBBean.isDeleted())
        {
            return;
        }
        // handle add credit for retry by passing details
        processAddCreditForCustomer(invitationDBBean, customerDBBean, isAdvocate, retryCount);

    }

    //The parameter retryCount is not null if called from retryEventService
    public void processAddCreditForCustomer(InvitationDBBean invitationDBBean, CustomerDBBean customerDBBean, boolean isAdvocate, Integer retryCount) throws ServiceException, BackendRequestException {

        //Get referralCodeDBBean using referralCodeId of invitation
        ReferralCodeDBBean referralCodeDBBean = referralCodeDao.getReferralCode(invitationDBBean.getReferralCodeId());

        //Get credit details that is being added with credit balance, credit expiry
        Credit credit = gsAddCreditUtil.getCreditDetailsForCustomer(customerDBBean, referralCodeDBBean, isAdvocate);

        long gsCustomerIdLong = customerDBBean.getGsCustomerId();

        // Referee email fetching to send options for advocate credit success
        String refereeEmail = customerDBBean.getEmailId();

        if (isAdvocate) {

            //Get advocateId
            int advocateCustomerId = customerDBBean.getAdvocateId();

            //Get customerDBBean of advocate
            customerDBBean = customerDao.getCustomerInfo(advocateCustomerId, customerDBBean.getTenantId());

            //Get advocate gsCustomerId
            gsCustomerIdLong = customerDBBean.getGsCustomerId();
        }

        //Add credit for the customer
        Credit creditResponse = creditManagementInterface.addCredit((int) gsCustomerIdLong, credit, customerDBBean.getTenantId());

        //After adding credit
        //1. If the creditBalance should be checked with the ALERT value
        //   and send alert email for admin
        //2. If the add credit fails because of 7017, send email to customers

        //If credit add is successful
        if (creditResponse != null &&
                (Configurations.CreditAddResponseStatus.SUCCESS.equals(creditResponse.getType()))) {

            if (isAdvocate) {
                //Advocate

                //Add admin token to fetch customer details
                long gsCustomerId = customerDBBean.getGsCustomerId();
                CustomerDBBean customerInfoBackend = accountService.getCustomerDetails(gsCustomerId, customerDBBean.getTenantId());

                //Update invitation with advocate credit id
                invitationDBBean.setAdvocateCreditId(creditResponse.getCreditId());
                invitationDBBean.setAdvocateCreditTxnId(creditResponse.getCreditTxnId());
                invitationDBBean.setAdvocateCreditStatus(Configurations.StatusType.CREDIT_ADD_SUCCESS);

                // update invitation status to USING_SERVICE when advocate credit added
                invitationDBBean.setStatus(Configurations.CustomerStatus.STATUS_USING_SERVICE);

                //send email to advocate for advocate credit success
                List emailIds = new ArrayList();
                emailIds.add(customerDBBean.getEmailId());

                Option option = new Option();
                if(customerInfoBackend.getFirstName() != null) {
                    option.setCustomerName(customerInfoBackend.getFirstName());
                }
                option.setCustomerEmail(customerDBBean.getEmailId());
                option.setNewCustomerEmail(refereeEmail);
                option.setAdvocateAmount((int) credit.getCreditBalance());
                option.setCurrency(credit.getCurrency());
                option.setGsCustomerId((int) gsCustomerId);

                String language = (StringUtils.isNotEmpty(customerDBBean.getLanguage())) ? customerDBBean.getLanguage() :
                        tenantConfigurationsKeyValueDao.getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.TENANT_DEF_LANG,customerDBBean.getTenantId());

                ngwManagementInterface.sendEmail(Configurations.EmailType.CREDIT_ADDED, emailIds, language, option, customerDBBean.getTenantId());

                //Check referralCredit alert
                if (gsAddCreditUtil.shouldAdminBeAlertedForAddingCredit(gsCustomerIdLong, customerDBBean.getTenantId())) {

                    //Get adminEmailId using configurationKeyValue table
                    String adminEmailId = tenantConfigurationsKeyValueDao
                            .getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.GS_ADMIN_EMAIL_ID, customerDBBean.getTenantId());

                    // Getting credit warning limit
                    float creditWarningAmount = gsAddCreditUtil.getCreditWarningAmount(credit.getCurrency(), customerDBBean.getTenantId());

                    //Set email options
                    Option emailOption = new Option();
                    emailOption.setCustomerEmail(customerDBBean.getEmailId());
                    emailOption.setCreditWarningLimit((int) creditWarningAmount);

                    if (credit.getCurrency() != null)
                        emailOption.setCurrency(credit.getCurrency());

                    emailIds = new ArrayList();
                    emailIds.add(adminEmailId);

                    ngwManagementInterface.sendEmail(Configurations.EmailType.INTERNAL_CREDIT_WARNING, emailIds, "en", emailOption, customerDBBean.getTenantId());
                }
            } else {

                //Referee
                //Update invitation with referral credit id
                invitationDBBean.setReferralCreditId(creditResponse.getCreditId());
                invitationDBBean.setReferralCreditTxnId(creditResponse.getCreditTxnId());
                invitationDBBean.setReferralCreditStatus(Configurations.StatusType.CREDIT_ADD_SUCCESS);

                //Set customer referralStatus
                customerDBBean.setReferralStatus(Configurations.ReferralStatus.SIGNUP_WITH_REFERRAL);
            }
        } else {

            //Failed due to MAX credit reach
            if (creditResponse != null &&
                    Configurations.CreditAddResponseStatus.MAX_CREDIT_LIMIT_REACHED.equals(creditResponse.getType())) {

//                Option creditLimitOptions = new Option();
//                creditLimitOptions.setCustomerEmail(customerDBBean.getEmailId());
//                creditLimitOptions.setCreditLimit(1000);
//
//                List customerEmail = new ArrayList();
//                //Customer emailId
//                customerEmail.add(customerDBBean.getEmailId());
//
//                // sending email to customer for failed credit bcz of credit already reached MAX amount
//                ngwManagementInterface.sendEmail(Configurations.EmailType.CREDIT_LIMIT, customerEmail, customerDBBean.getLanguage(), creditLimitOptions);


                Credit creditDetailsOfCustomer = gsAddCreditUtil.getGigSkyCreditDetailsOfCustomer(customerDBBean.getGsCustomerId(), customerDBBean.getTenantId());

                //Get adminEmailId using configurationKeyValue table
                String adminEmailId = tenantConfigurationsKeyValueDao
                        .getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.GS_ADMIN_EMAIL_ID, customerDBBean.getTenantId());

                //Admin EmailId
                List adminInternalEmail = new ArrayList();
                adminInternalEmail.add(adminEmailId);

                Option creditAlertOption = new Option();
                creditAlertOption.setCustomerEmail(customerDBBean.getEmailId());
                float creditBalance = creditDetailsOfCustomer.getCreditBalance();
                creditAlertOption.setCreditLimit((int) creditBalance);
                creditAlertOption.setCurrency(credit.getCurrency());

                // sending email to customer for failed credit bcz of credit already reached MAX amount
                ngwManagementInterface.sendEmail(Configurations.EmailType.INTERNAL_CREDIT_ALERT, adminInternalEmail, "en", creditAlertOption, customerDBBean.getTenantId());

            }

            //Failure case, update the invitation entries
            if (isAdvocate) {

                //Advocate
                invitationDBBean.setAdvocateCreditStatus(Configurations.StatusType.CREDIT_ADD_FAILED);
                // if retry count not null, updating retry count
                if (retryCount != null) {
                    retryCount++;
                    invitationDBBean.setAdvocateRetryCount(retryCount);
                }
            } else {

                //Referee
                invitationDBBean.setReferralCreditStatus(Configurations.StatusType.CREDIT_ADD_FAILED);

                // if retry count not null, updating retry count
                if (retryCount != null) {
                    retryCount++;
                    invitationDBBean.setReferralRetryCount(retryCount);
                }

                customerDBBean.setReferralStatus(Configurations.StatusType.CREDIT_ADD_FAILED);
            }
        }

        //Update invitation details
        invitationDao.updateInvitation(invitationDBBean);

        //Update the customer referral status
        customerDao.updateCustomerInfo(customerDBBean);
    }

    public boolean checkPromotionEnable(int tenantId) {

        String isEnabledStr = tenantConfigurationsKeyValueDao.getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.PROMOTION_ENABLE, tenantId);

        return StringUtils.equals(isEnabledStr, "true");
    }

    public boolean IfBackendServersRunning() {
        try {
            //Check backend server available and NGW server availability
            boolean isBackendServerRunning = backendInfoManagementInterface.verifyBackendServerIsRunning();
            boolean isNgwServerRunning = ngwManagementInterface.isNGWServerRunning();

            if (isBackendServerRunning && isNgwServerRunning) {
                return true;
            }
        } catch (Exception e) {
            logger.error("GSTaskUtil check referral process exception: " + e,e);
        }
        return false;
    }


    public boolean checkIfSignUpEventIsProcessed(ReferralEventDBBean referralEventDBBean) {

        //Get customerId from the referralEventDbBean
        int customerId = referralEventDBBean.getCustomerId();

        ReferralEventDBBean referralCreditAddEvent = referralEventDao.getSignUpReferralEvent(customerId, referralEventDBBean.getTenantId());

        //If there is a signUp event
        if (referralCreditAddEvent != null) {

            //Check the status of signUpEvent, new/in process
            if(Configurations.StatusType.STATUS_NEW.equals(referralCreditAddEvent.getStatus()) ||
                    Configurations.StatusType.STATUS_IN_PROCESS.equals(referralCreditAddEvent.getStatus())) {

                //Update the referralEvent status as NEW
                referralEventDBBean.setStatus(Configurations.StatusType.STATUS_NEW);

                referralEventDao.updateReferralEvent(referralEventDBBean);
                return false;
            }
        }
        else {
            //SignUp event is not there

            //Get customerBean using customerId
            CustomerDBBean customerDBBean = customerDao.getCustomerInfo(customerId, referralEventDBBean.getTenantId());

            //If accountCreationTime is null then consider creationTime
            Timestamp accountCreationTime =
                    (customerDBBean.getAccountCreationTime() != null) ?
                            customerDBBean.getAccountCreationTime() : customerDBBean.getCreateTime();

            //Compare the accountCreationTime and decide whether the planPurchaseEvent should wait for the signUpEvent
            if(accountCreationTime != null)
            {
                //Convert Timestamp into milliSeconds
                long accountCreationTimeInMS = accountCreationTime.getTime();
                long currentTimeInMS = CommonUtils.getCurrentTimestamp().getTime();

                String allowedEventDelay = configurationKeyValueDao.
                        getConfigurationValue(Configurations.ConfigurationKeyValueType.SIGNUP_EVENT_DELAY_TIME_MS,
                                DEFAULT_SIGNUP_EVENT_DELAY_MS);

                //Get the accepted delay between purchase and signUp event from configurationKeyValue DB
                GigskyDuration delayBetweenPurchaseAndSignupEvent = new GigskyDuration(allowedEventDelay);

                //Convert into milliSeconds
                long delayInMS = delayBetweenPurchaseAndSignupEvent.convertTimeInMilliSeconds();

                //Compare accountCreationTime
                long differenceOfCurrentAndAccCreationTime = currentTimeInMS - accountCreationTimeInMS;

                //Return false if difference is greater than the delay
                if(differenceOfCurrentAndAccCreationTime > delayInMS)
                {
                    //Mark the event status as SIGNUP_EVENT_NOT_FOUND
                    referralEventDBBean.setStatus(Configurations.StatusType.SIGNUP_EVENT_NOTFOUND);

                    referralEventDao.updateReferralEvent(referralEventDBBean);

                    //Update the customer status as using service
                    customerDBBean.setStatus(Configurations.CustomerStatus.STATUS_USING_SERVICE);
                    customerDao.updateCustomerInfo(customerDBBean);
                    return false;
                }
                //Else continue to wait for signUp event
            }

            //Add credit should be done, But the signUp event is not there
            //Wait for signUp event
            //Update the retry count
            int presentRetryCount = referralEventDBBean.getRetryCount();

            if(presentRetryCount < Configurations.MAX_SIGNUP_WAIT_COUNT){

                //Increment retryCount
                presentRetryCount++;

                //Update referralEvent entry with new retry count
                referralEventDBBean.setRetryCount(presentRetryCount);

                //And status as NEW
                referralEventDBBean.setStatus(Configurations.StatusType.STATUS_NEW);

                referralEventDao.updateReferralEvent(referralEventDBBean);
                return false;
            }
            else {

                //retryCount reached max retry count

                //Mark the event status as SIGNUP_EVENT_NOT_FOUND
                referralEventDBBean.setStatus(Configurations.StatusType.SIGNUP_EVENT_NOTFOUND);

                //Mark the event status as failed after retries
                referralEventDBBean.setStatusMessage(Configurations.StatusType.STATUS_FAILED_AFTER_RETRIES);

                referralEventDao.updateReferralEvent(referralEventDBBean);

                //Update the customer status as using service
                customerDBBean.setStatus(Configurations.CustomerStatus.STATUS_USING_SERVICE);
                customerDao.updateCustomerInfo(customerDBBean);
                return false;
            }
        }
        //Go ahead and add credit
        return true;
    }

    public InvitationDBBean createInvitationEntry(CustomerDBBean customerDBBean,
                                                  ReferralCodeDBBean referralCodeDBBean,
                                                  Timestamp createdOn)
    {
        InvitationDBBean invitationDBBean = new InvitationDBBean();
        invitationDBBean.setEmailId(customerDBBean.getEmailId());
        invitationDBBean.setInviteTime(null);
        invitationDBBean.setTargetCustomerId(customerDBBean.getId());
        invitationDBBean.setReferralCodeId(referralCodeDBBean.getId());
        invitationDBBean.setStatus(Configurations.CustomerStatus.STATUS_SIGNED_UP);
        invitationDBBean.setAccountCreatedOn(createdOn);

        invitationDao.addInvitation(invitationDBBean);

        return invitationDBBean;
    }



    public boolean isMaxReferredCountReached(ReferralCodeDBBean referralCodeBean)
    {

        if(referralCodeBean != null)
        {
            /*
            Introduced max referred count check for general referral codes
             and max promoted count check for promotional codes
            */
            int maxPromotedCount = Integer.valueOf(configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.MAX_PROMOTED_COUNT));
            int maxReferredCount = Integer.valueOf(configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.MAX_REFERRED_COUNT));

            int referredCount = 0;
            byte isPromotionalCode = 0;

            referredCount = ((referralCodeBean.getReferredCount() == null) ? referredCount : referralCodeBean.getReferredCount());
            isPromotionalCode = ((referralCodeBean.getIsPromotionalCode() == null) ? isPromotionalCode : referralCodeBean.getIsPromotionalCode());

            if(isPromotionalCode == 1) {
                if(referredCount >= maxPromotedCount)
                    return true;
            } else{
                if(referredCount >= maxReferredCount && !isResetReferredTimeReached(referralCodeBean))
                    return true;
            }
        }

        return false;
    }

    private boolean isResetReferredTimeReached(ReferralCodeDBBean referralCodeDBBean)
    {
        long resetReferredTimeInMS;
        String resetReferredTime = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.RESET_TIME_REFERRED_COUNT);
        if(StringUtils.isEmpty(resetReferredTime)) {
            resetReferredTimeInMS = Configurations.DEFAULT_RESET_TIME_REFERRED_COUNT;
        }else{
            resetReferredTimeInMS = new GigskyDuration(resetReferredTime).convertTimeInMilliSeconds();
        }

        if(referralCodeDBBean.getFirstReferredTime() != null) {
            // difference time between current time to first referred time
            long diffTime = CommonUtils.getCurrentTimestampInUTC().getTime() - referralCodeDBBean.getFirstReferredTime().getTime();
            return diffTime >= resetReferredTimeInMS;
        }

        return false;
    }

    public boolean isValidPromoCode(String promoCode, int tenantId) throws ServiceException {
        PromotionDBBean promotionDBBean = promotionDao.getPromotion(promoCode, tenantId);

        if(promotionDBBean != null && promotionDBBean.getActivePromo() ==1)
        {
            //Check if currentRedeemCount reached maxRedeemCount
            if(promotionDBBean.getCurrentRedeemCount() >= promotionDBBean.getMaxRedeemCount())
            {
                logger.error("Promo code reached max redeem count");
                throw new ServiceException(ErrorCode.PROMOTION_REACHED_MAX_REDEEM_COUNT);
            }
            else
            {
                return true;
            }
        }
        return false;
    }

    public boolean isValidReferralCode(String referralCode, int tenantId)
    {
        ReferralCodeDBBean referralCodeDBBean =
                referralCodeDao.getReferralCodeInfoByReferralCode(referralCode, tenantId);

        if(referralCodeDBBean != null && referralCodeDBBean.getIsActive()!=0)
        {
            //If used as promo code
            if(referralCodeDBBean.getIsPromotionalCode() != null
                    && referralCodeDBBean.getIsPromotionalCode() == 1)
            {
                int maxPromotedCount =
                        Integer.valueOf(configurationKeyValueDao.getConfigurationValue(Configurations
                                .ConfigurationKeyValueType.MAX_PROMOTED_COUNT));

                //If referredCount lt MAX_PROMOTED_COUNT return true, else false
                if(referralCodeDBBean.getReferredCount()!=null)
                {
                    if(referralCodeDBBean.getReferredCount() < maxPromotedCount)
                    {
                        return true;
                    }
                }
                else
                    return true;
            }
            else {
                return true;
            }
        }

        return false;
    }


    //Only for SDS
    public boolean isValidIccIdPromoCode(String iccid, int tenantId)
    {
        PromoIccidDBBean promoIccidDBBean = promoIccidDao.getPromoIccid(iccid);

        if(promoIccidDBBean != null)
        {
            PromotionDBBean promotion = promotionDao.getPromotion(promoIccidDBBean.getPromotionId(), tenantId);
            //Iccid range matches
            //Check validity
            Date promotionEndDate = promotion.getEndDate();
            Date promotionStartDate = promotion.getStartDate();
            Date currentDate = CommonUtils.getCurrentDate();
            if((currentDate.before(promotionEndDate) || currentDate.equals(promotionEndDate)) &&
                    (currentDate.after(promotionStartDate) || currentDate.equals(promotionStartDate)))
            {
                return true;
            }
            logger.info("Validating promotion for iccid - " + iccid + " Expired promo with promoCode - "+ promotion.getPromoCode());
        }

        logger.info("Validating promotion for iccid - " + iccid + " Valid promotion NOT_FOUND");
        return false;
    }

    //to check weather referral-emails are enabled
    public boolean checkReferralEmailEnable(int tenantId) {
        String isEnabledStr = tenantConfigurationsKeyValueDao.getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.REFERRAL_EMAIL_ENABLE, tenantId);

        return StringUtils.equals(isEnabledStr, "true");
    }
}
