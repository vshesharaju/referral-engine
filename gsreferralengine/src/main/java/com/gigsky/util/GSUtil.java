package com.gigsky.util;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.TenantDBBean;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.database.dao.TenantDao;
import com.gigsky.filter.RequestUtils;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.ValidationService;
import com.gigsky.service.exception.ServiceException;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import java.util.List;

/**
 * Created by prsubbareddy on 18/01/17.
 */
public class GSUtil {

    @Autowired
    private ValidationService validationService;

    @Autowired
    private TenantDao tenantDao;

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    public static final String AUTHENTICATION_HEADER = "Authorization";
    private static final String ACCEPT_LANGUAGE_HEADER = "Accept-Language";
    public static final String TENANT_ID_HEADER = "tenantsId";
    public static final String JSON = "JSON";
    public static final String CSV = "CSV";
    public static final String PROMO_USAGE_FILE_NAME = "PROMO_USAGE_OF_PROMO_ID";
    public static final String BASIC_FIELD = "Basic";



    private static String localeStrings = "{\"strings\":\n" +
            "  [\n" +
            "    {\"string\" : \"For referring\", \"localize\":\n" +
            "    [ {\"langCode\" : \"de\",  \"localizedString\" : \"Für das Empfehlen von\"},\n" +
            "      {\"langCode\" : \"es\",  \"localizedString\" : \"Por haber recomendado a\"},\n" +
            "      {\"langCode\" : \"fr\",  \"localizedString\" : \"Pour parrainer\"},\n" +
            "      {\"langCode\" : \"it\",  \"localizedString\" : \"Per riferimento\"},\n" +
            "      {\"langCode\" : \"ja\",  \"localizedString\" : \"のご紹介で\"},\n" +
            "      {\"langCode\" : \"zh\",  \"localizedString\" : \"关于推荐\"},\n" +
            "      {\"langCode\" : \"zh-HK\",  \"localizedString\" : \"推薦\"},\n" +
            "      {\"langCode\" : \"zh-TW\",  \"localizedString\" : \"推薦\"}\n" +
            "      ]},\n" +
            "    {\"string\" : \"For signing up with invite code\", \"localize\":\n" +
            "    [ {\"langCode\" : \"de\",  \"localizedString\" : \"Für das Anmelden mit einem Einladungscode\"},\n" +
            "      {\"langCode\" : \"es\",  \"localizedString\" : \"Para registrarte con un código de invitación\"},\n" +
            "      {\"langCode\" : \"fr\",  \"localizedString\" : \"Inscrivez-vous avec votre code d'invitation\"},\n" +
            "      {\"langCode\" : \"it\",  \"localizedString\" : \"Per iscriverti con un codice di invito\"},\n" +
            "      {\"langCode\" : \"ja\",  \"localizedString\" : \"紹介コードによるご登録で\"},\n" +
            "      {\"langCode\" : \"zh\",  \"localizedString\" : \"关于用邀请代码注册\"},\n" +
            "      {\"langCode\" : \"zh-HK\",  \"localizedString\" : \"使用邀請碼註冊\"},\n" +
            "      {\"langCode\" : \"zh-TW\",  \"localizedString\" : \"使用邀請碼註冊\"}\n" +
            "      ]},\n" +
            "    {\"string\" : \"GigSky credit deduct\", \"localize\":\n" +
            "    [ {\"langCode\" : \"de\",  \"localizedString\" : \"GigSky Gutschriftenabzug\"},\n" +
            "      {\"langCode\" : \"es\",  \"localizedString\" : \"Descuento de crédito de GigSky\"},\n" +
            "      {\"langCode\" : \"fr\",  \"localizedString\" : \"Déduction de crédit GigSky\"},\n" +
            "      {\"langCode\" : \"it\",  \"localizedString\" : \"Dedurre credito GigSky\"},\n" +
            "      {\"langCode\" : \"ja\",  \"localizedString\" : \"GigSky クレジット控除\"},\n" +
            "      {\"langCode\" : \"zh\",  \"localizedString\" : \"GigSky 积分扣除\"},\n" +
            "      {\"langCode\" : \"zh-HK\",  \"localizedString\" : \"GigSky 積分扣除\"},\n" +
            "      {\"langCode\" : \"zh-TW\",  \"localizedString\" : \"GigSky 積分扣除\"}\n" +
            "      ]}\n" +
            "    ]\n" +
            "}";

    private static final Logger logger = LoggerFactory.getLogger(GSUtil.class);

    public static String extractToken(HttpHeaders headers, ValidationService validationService) throws ResourceValidationException {
        String token = null;
        List<String> headerValues = headers.getRequestHeader(AUTHENTICATION_HEADER);

        if(headerValues == null ||(headerValues != null && headerValues.size() <= 0))
        {
            logger.error("GSUtil extractToken Token missing");
            throw new ResourceValidationException(ErrorCode.TOKEN_MISSING);
        }

        if(headerValues != null && headerValues.size() > 0)
        {
            token = headerValues.get(0);
        }

        validationService.validateToken(token);
        token = token.substring(6);
        return token;
    }

    public int extractTenantId(HttpHeaders headers) throws ServiceException, ResourceValidationException{
        return extractTenantId(headers.getRequestHeaders());
    }

    public int extractTenantId(MultivaluedMap<String,String> headers) throws ServiceException, ResourceValidationException{
        List<String> headerValues = headers.get(TENANT_ID_HEADER);

        // Fetching default tenant id from config key values
        int tenantId = Integer.parseInt(configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.DEFAULT_TENANT_ID, "1"));

        if(headerValues != null && headerValues.size() > 0) {

            String tId = headerValues.get(0);
            if(StringUtils.isNotEmpty(tId)) {
                tenantId = Integer.parseInt(tId);
                validationService.validateTenantId(tenantId);
            } else {
                logger.error("GSUtil Tenant id is invalid");
                throw new ServiceException(ErrorCode.INVALID_TENANT_ID);
            }
        }

        // Validation for tenant id:  Tenant id is present or not in database
        TenantDBBean tenantDBBean = tenantDao.getTenantInfo(tenantId);
        if(tenantDBBean == null)
        {
            logger.error("GSUtil Tenant id is invalid");
            throw new ServiceException(ErrorCode.INVALID_TENANT_ID);
        }

        return tenantId;
    }


    public int extractTenantIdFromMessageData(EventMessageData messageData) throws ServiceException, ResourceValidationException{

        // Fetching default tenant id from config key values
        int tenantId = Integer.parseInt(configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.DEFAULT_TENANT_ID, "1"));

        if(messageData != null && messageData.getTenantsId() != null) {

            tenantId= messageData.getTenantsId();
            validationService.validateTenantId(tenantId);
        }

        // Validation for tenant id:  Tenant id is present or not in database
        TenantDBBean tenantDBBean = tenantDao.getTenantInfo(tenantId);
        if(tenantDBBean == null)
        {
            logger.error("GSUtil Tenant id is invalid");
            throw new ServiceException(ErrorCode.INVALID_TENANT_ID);
        }

        return tenantId;
    }

    public static String extractLanguage(HttpHeaders headers)
    {
        String language = null;
        List<String> headerValues = headers.getRequestHeader(ACCEPT_LANGUAGE_HEADER);

        if(headerValues != null && headerValues.size() > 0)
        {
            language = headerValues.get(0);
        }
        return language;
    }

    public String getTokenFromString(String basicToken) throws ResourceValidationException {

        if(basicToken == null ||(basicToken != null && basicToken.length() <= 0))
        {
            logger.error("GSUtil extractToken Token missing");
            throw new ResourceValidationException(ErrorCode.TOKEN_MISSING);
        }
        if (basicToken.startsWith(BASIC_FIELD)) {
            return basicToken.substring(BASIC_FIELD.length()).trim();
        }
        return basicToken;
    }

    public static boolean checkIsTokenAvailable(HttpHeaders headers){

        List<String> headerValues = headers.getRequestHeader(AUTHENTICATION_HEADER);
        if(headerValues != null && headerValues.size() > 0) {
            String token = headerValues.get(0);

            if(token != null && token.length() > 0) {
                return true;
            }
        }
        return false;
    }

    public static String getLocaleLanguage() {

        String locale = RequestUtils.getLocale();
        logger.info("GSUtil original Locale :" + locale);

        if(locale != null && (locale.length() == 2 || (locale.length() == 5 && locale.contains("-")))) {
            return locale;

        } else if((locale != null) && (locale.length() > 5)){

            locale = locale.substring(0, 5);

            if(locale.contains("-")){
                return locale;
            } else {
                locale = locale.substring(0, 2);
                return locale;
            }

        }

        return StringUtils.EMPTY;
    }

    public static String getLocalizedString(String engString, String lang) {

        try {
            // Convert locale strings to JSON object
            JSONObject obj = new JSONObject(localeStrings);

            // Fetch Strings array from input json object data
            JSONArray stringArray = obj.getJSONArray("strings");

            for(int i = 0; i < stringArray.length(); i ++) {

                // Fetching objData from string array
                JSONObject objData = stringArray.getJSONObject(i);

                // Check if engString matches with data String
                if(objData.optString("string").equals(engString)) {

                    // Fetch localised string array for eng string
                    JSONArray localisedArray = objData.getJSONArray("localize");
                    for(int j = 0; j < localisedArray.length(); j ++) {

                        JSONObject localizeObjData = localisedArray.getJSONObject(j);

                        // check if lang matches with localised string language
                        // Fetch localised string
                        if(localizeObjData.optString("langCode").equals(lang)) {
                            return localizeObjData.optString("localizedString");
                        }
                    }
                }
            }
        }
        catch(Exception e) {
            logger.error("GSUtil localisation parsing error " + e.toString(),e);
        }

        return engString;
    }

}
