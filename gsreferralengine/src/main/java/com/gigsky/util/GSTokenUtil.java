package com.gigsky.util;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.TenantConfigurationsKeyValueDBBean;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.database.dao.TenantConfigurationsKeyValueDao;
import com.gigsky.gscommon.util.encryption.EncryptionFactory;
import com.gigsky.gscommon.util.encryption.GigSkyEncryptor;
import com.gigsky.util.bean.AccountLogin;
import com.gigsky.util.bean.LoginResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by prsubbareddy on 14/02/17.
 */
public class GSTokenUtil {

    private static final Logger logger = LoggerFactory.getLogger(GSTokenUtil.class);

    @Autowired
    private EncryptionFactory encryptionFactory;

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    private Configurations configurations;

    @Autowired
    private TenantConfigurationsKeyValueDao tenantConfigurationsKeyValueDao;

    @Autowired
    HttpClientUtil httpClientUtil;

    public String getValidToken(int tenantId) {

        String engineMode = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.ENGINE_MODE,"TEST");
        // If engine mode is TEST we will return token as null, In mock APIs case we are not depending on Token
        if(engineMode != null && engineMode.equals("TEST")) {
            return null;
        }

        // fetch token from configurations
        String token = tenantConfigurationsKeyValueDao.getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.ADMIN_TOKEN, tenantId);

        if(token == null || token.length() == 0) {
            //call login and fetch token
            token = getTokenByLogin(tenantId);

            if(token != null) {
                // update token to Database configurations
                createTokenForConfigurations(token, tenantId);
            }
        }
        return token;
    }

    public void createTokenForConfigurations(String token, int tenantId) {
        // create token entry in configurations
        TenantConfigurationsKeyValueDBBean tenantConfigKeyValueBean = new TenantConfigurationsKeyValueDBBean();
        tenantConfigKeyValueBean.setCkey(Configurations.TenantConfigurationKeyValueType.ADMIN_TOKEN);
        tenantConfigKeyValueBean.setValue(token);
        tenantConfigKeyValueBean.setTenantId(tenantId);
        tenantConfigurationsKeyValueDao.setTenantConfigurationValue(tenantConfigKeyValueBean);
    }

    public void updateTokenForConfigurations(String token, int tenantId) {
        // update token to Database configurations
        TenantConfigurationsKeyValueDBBean tenantConfigKeyValueBean = new TenantConfigurationsKeyValueDBBean();
        tenantConfigKeyValueBean.setCkey(Configurations.TenantConfigurationKeyValueType.ADMIN_TOKEN);
        tenantConfigKeyValueBean.setValue(token);
        tenantConfigKeyValueBean.setTenantId(tenantId);
        tenantConfigurationsKeyValueDao.updateTenantConfigurationValue(tenantConfigKeyValueBean);
    }


    public String getTokenByLogin(int tenantId) {

        String loginUrl = configurations.getBackendBaseUrl() + "account/login";

        long timeOutInMs = configurations.getRequestTimeOut();

        // initialising encryption
        GigSkyEncryptor encryptor = encryptionFactory.getEncryptor(Configurations.LATEST_ENCRYPTION_VERSION);

        // fetching encrypted admin email and password from configuration key values
        String encryptedEmailId = tenantConfigurationsKeyValueDao.getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.ADMIN_EMAIL, tenantId);
        String encryptedPassword = tenantConfigurationsKeyValueDao.getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.ADMIN_PASSWORD, tenantId);

        //decrypting admin email and password
        String emailId = encryptor.decryptString(encryptedEmailId); // decrypt admin email
        String password = encryptor.decryptString(encryptedPassword); //decrypt admin password

        try {

            // preparing request for account login
            AccountLogin loginReq = new AccountLogin();
            loginReq.setType("AccountLogin");
            loginReq.setLoginType("NORMAL");
            loginReq.setEmailId(emailId);
            loginReq.setPassword(password);


            //Create headers map
            Map<String, String> headerMap = new HashMap<String, String>();
            headerMap.put(GSUtil.TENANT_ID_HEADER, String.valueOf(tenantId));

            // call backend login and get token
            HttpClientUtil.GSRestHttpResponse response = httpClientUtil.sendHttpRequest("POST", loginUrl, null, loginReq, headerMap, timeOutInMs);

//            HttpClientUtil.GSRestHttpResponse response = httpClientUtil.sendHttpRequest("POST", loginUrl, loginReq, timeOutInMs);

            if(response != null) {

                LoginResponse loginResponse = (LoginResponse) HttpClientUtil.convertToBean(response.getResponseBody(), LoginResponse.class);
                return loginResponse.getToken();
            }

        } catch (Exception e) {
            logger.error("Login for token exception: " + e,e);
        }

        return null;
    }

}
