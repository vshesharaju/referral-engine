package com.gigsky.util;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.gigsky.task.SimActivationEventTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Timestamp;

/**
 * Created by pradeepragav on 14/02/17.
 */
public class CustomerDateAndTimeDeserialize extends JsonDeserializer<Timestamp> {

    private static final Logger logger = LoggerFactory.getLogger(CustomerDateAndTimeDeserialize.class);

    @Override
    public Timestamp deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JsonProcessingException {

        String str = jsonParser.getText().trim();
        try {
            return Timestamp.valueOf(str);
        } catch (Exception e) {
            logger.error("Exception : ", e);
        }
        return null;
    }
}
