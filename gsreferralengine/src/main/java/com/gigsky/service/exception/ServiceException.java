package com.gigsky.service.exception;

import com.gigsky.exception.ReferralException;
import com.gigsky.rest.exception.ErrorCode;

/**
 * Created by pradeepragav on 22/02/17.
 */
public class ServiceException extends ReferralException {

    public ServiceException(ErrorCode code)
    {
        super(code);
    }

    public ServiceException(String message)
    {
        super(message);
    }

    public ServiceException(String message, Exception e)
    {
        super(message, e);
    }

    public ServiceException(ErrorCode errorCode, String message)
    {
        super(errorCode,message);
    }

    public ServiceException(Exception e)
    {
        super(e);
    }
}
