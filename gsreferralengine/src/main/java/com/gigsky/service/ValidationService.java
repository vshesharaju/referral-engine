package com.gigsky.service;

import com.gigsky.rest.bean.PageInfo;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.rest.exception.ResourceValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ValidationService {

	private static final Logger logger = LoggerFactory.getLogger(ValidationService.class);

	public void validateStartIndexAndCount(int startIndex, int count) {
		if(startIndex < 0) {
			logger.error("ValidationService validateStartIndexAndCount start index [" + startIndex + "] is invalid ");
			throw new IllegalArgumentException("start index [" + startIndex + "] is invalid");
		}
		if(count < 0) {
			logger.error("ValidationService validateStartIndexAndCount count [" + count + "] is invalid ");
			throw new IllegalArgumentException("count [" + count + "] is invalid");
		}
	}

	public void validateId(int id) {
		if(id <= 0) {
			logger.error("ValidationService validateId Id [" + id + "] is invalid");
			throw new IllegalArgumentException("Id [" + id + "] is invalid");
		}
	}

	public void validateToken(String authCode) throws ResourceValidationException
	{
		if(authCode == null || authCode.length() < 38 || !authCode.startsWith("Basic "))
		{
			logger.error("ValidationService validateToken Token is invalid");
			throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
		}

		String token = authCode.substring(6);

		if(token.length() != 32)
		{
			logger.error("ValidationService validateToken Token is invalid");
			throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
		}
	}

	public void validatePage(PageInfo info)
	{
		if(info.getStart() < 0 || info.getCount() < 0)
		{
			logger.error("ValidationService validatePage Invalid page parameters");
			throw new IllegalArgumentException("Invalid page parameters");
		}
	}


	public void validateTenantId(int tenantId) throws ResourceValidationException {
		if(tenantId <= 0) {
			logger.error("ValidationService validateTenantId [" + tenantId + "] is invalid");
			throw new ResourceValidationException(ErrorCode.INVALID_TENANT_ID);
		}
	}


}
