package com.gigsky.service;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.ReferralEventDBBean;
import com.gigsky.database.dao.ReferralEventDao;
import com.gigsky.scheduler.BaseSchedulerImpl;
import com.gigsky.task.*;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * Created by prsubbareddy on 30/01/17.
 */
public class EventService extends BaseSchedulerImpl {

    private static final Logger logger = LoggerFactory.getLogger(EventService.class);

    private static final int DEFAULT_EVENTS_PROCESSING_COUNT = 500;
    private static final int DEFAULT_EVENTS_QUEUE_SIZE = 500;

    @Autowired
    ReferralEventDao referralEventDao;

    @Autowired
    ReferralEventTaskFactory referralEventFactory;

    @Override
    public void initialize() {
        logger.debug("EventService:initialize++");
        super.init(DEFAULT_EVENTS_QUEUE_SIZE);
    }

    @Override
    public void startProcess()
    {
        int eventsCount = DEFAULT_EVENTS_PROCESSING_COUNT;
        int pendingTasks;
        String eventsCountValue = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.EVENTS_PROCESSING_COUNT);

        if(StringUtils.isNotEmpty(eventsCountValue))
        {
            eventsCount = Integer.parseInt(eventsCountValue);
        }

        synchronized (this) {
            pendingTasks = threadPoolExecutor.getQueue().size();
        }

        eventsCount -= pendingTasks;

        if (eventsCount > 0)
        {
            // Processing Events for STATUS_NEW status
            handleEventsProcessForStatusNew(eventsCount);
        }

    }

    private void handleEventsProcessForStatusNew(int eventsCount)
    {

        List<ReferralEventDBBean> events = referralEventDao.getReferralEventsByStatus(Configurations.StatusType.STATUS_NEW, eventsCount);

        for(ReferralEventDBBean event: events){

            event.setStatus(Configurations.StatusType.STATUS_IN_PROCESS);
            referralEventDao.updateReferralEvent(event);

            if(Configurations.ReferralEventType.SIGNUP_EVENT.equals(event.getEventType()))
            {
                SignUpEventTask signUpEventTask = (SignUpEventTask) referralEventFactory.createReferralEventTask("SIGN_UP_TASK");
                signUpEventTask.setReferralEvent(event);
                threadPoolExecutor.submit(signUpEventTask);
            }
            else if(Configurations.ReferralEventType.PURCHASE_EVENT.equals(event.getEventType()))
            {
                PlanPurchaseEventTask planPurchaseEventTask = (PlanPurchaseEventTask) referralEventFactory.createReferralEventTask("PLAN_PURCHASE_TASK");
                planPurchaseEventTask.setReferralEvent(event);
                threadPoolExecutor.submit(planPurchaseEventTask);
            }
            else if(Configurations.ReferralEventType.PLAN_EXPIRED_EVENT.equals(event.getEventType()))
            {
                PlanExpiredEventTask planExpiredEventTask = (PlanExpiredEventTask) referralEventFactory.createReferralEventTask("PLAN_EXPIRED_TASK");
                planExpiredEventTask.setReferralEvent(event);
                threadPoolExecutor.submit(planExpiredEventTask);
            }
            else if(Configurations.ReferralEventType.SIM_ACTIVATION_EVENT.equals(event.getEventType()))
            {
                SimActivationEventTask simActivationEventTask = (SimActivationEventTask) referralEventFactory.createReferralEventTask("SIM_ACTIVATION_TASK");
                simActivationEventTask.setReferralEvent(event);
                threadPoolExecutor.submit(simActivationEventTask);
            }
            else if(Configurations.ReferralEventType.CREDIT_NEARING_EXPIRY_EVENT.equals(event.getEventType()))
            {
                CreditNearingExpiryEventTask creditNearingExpiryEventTask = (CreditNearingExpiryEventTask) referralEventFactory.createReferralEventTask("CREDIT_NEAR_EXPIRY_TASK");
                creditNearingExpiryEventTask.setReferralEvent(event);
                threadPoolExecutor.submit(creditNearingExpiryEventTask);
            }
            else if(Configurations.ReferralEventType.CREDIT_EXPIRED_EVENT.equals(event.getEventType()))
            {
                CreditExpiredEventTask creditExpiredEventTask = (CreditExpiredEventTask) referralEventFactory.createReferralEventTask("CREDIT_EXPIRED_TASK");
                creditExpiredEventTask.setReferralEvent(event);
                threadPoolExecutor.submit(creditExpiredEventTask);
            }
            else if(Configurations.ReferralEventType.CURRENCY_CHANGE_EVENT.equals(event.getEventType()))
            {
                CurrencyChangeEventTask currencyChangeEventTask = (CurrencyChangeEventTask) referralEventFactory.createReferralEventTask("CURRENCY_CHANGE_TASK");
                currencyChangeEventTask.setReferralEvent(event);
                threadPoolExecutor.submit(currencyChangeEventTask);
            }
            else if(Configurations.ReferralEventType.EMAIL_ID_CHANGE_EVENT.equals(event.getEventType()))
            {
                EmailChangeEventTask emailChangeEventTask = (EmailChangeEventTask) referralEventFactory.createReferralEventTask("EMAIL_CHANGE_TASK");
                emailChangeEventTask.setReferralEvent(event);
                threadPoolExecutor.submit(emailChangeEventTask);
            }
            else if(Configurations.ReferralEventType.ACCOUNT_INFORMATION_CHANGE_EVENT.equals(event.getEventType()))
            {
                AccountInformationChangeEventTask accountInfoChangeEventTask = (AccountInformationChangeEventTask) referralEventFactory.createReferralEventTask("ACCOUNT_INFO_CHANGE_TASK");
                accountInfoChangeEventTask.setReferralEvent(event);
                threadPoolExecutor.submit(accountInfoChangeEventTask);
            }
            else if(Configurations.ReferralEventType.ACCOUNT_DELETE_EVENT.equals(event.getEventType()))
            {
                DeleteAccountEventTask deleteAccountEventTask = (DeleteAccountEventTask) referralEventFactory.createReferralEventTask("DELETE_ACCOUNT_TASK");
                deleteAccountEventTask.setReferralEvent(event);
                threadPoolExecutor.submit(deleteAccountEventTask);
            }
        }
    }

    @Override
    public void destroy(){
        logger.debug("EventService:destroy++");
        super.destroy();
    }

}