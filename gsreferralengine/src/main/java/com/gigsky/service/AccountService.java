package com.gigsky.service;

import com.gigsky.backend.customer.CustomerManagementInterface;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.PreferenceDBBean;
import com.gigsky.database.bean.ReferralCodeDBBean;
import com.gigsky.database.bean.ReferralCodeHistoryDBBean;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.PreferenceDao;
import com.gigsky.database.dao.ReferralCodeDao;
import com.gigsky.referralCode.ReferralCodeManagement;
import com.gigsky.rest.bean.Account;
import com.gigsky.rest.bean.Customer;
import com.gigsky.rest.bean.OptOut;
import com.gigsky.rest.bean.ReferralCode;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSTaskUtil;
import com.gigsky.util.GSTokenUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by vinayr on 11/01/17.
 */
public class AccountService {

    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);

    @Autowired
    private CustomerManagementInterface customerManagementInterface;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private ReferralCodeDao referralCodeDao;

    @Autowired
    private PreferenceDao preferenceDao;

    @Autowired
    private ReferralCodeManagement referralCodeManagement;

    @Autowired
    private ConfigurationKeyValueDao configDao;

    @Autowired
    GSTaskUtil gsTaskUtil;

    @Autowired
    GSTokenUtil gsTokenUtil;

    //Get Account Details
    public Account getAccountDetails(int gsCustomerId, String token, int tenantId) throws ServiceException, BackendRequestException {

        //Validate the token and get the customer information from the backend
        Customer customer = customerManagementInterface.getCustomerDetails(gsCustomerId, token, tenantId);

        //Check if the customer is already present in the referral engine
        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, tenantId);

        customerDBBean = convertCustomerBean(tenantId, customer, customerDBBean);
        upsertCustomer(customerDBBean);

        //Add Preferred Currency to Customer Preferences
        gsTaskUtil.createPreference(customerDBBean.getId(), Configurations.PreferrenceKeyType.PREFERRED_CURRENCY, customer.getPreferredCurrency());

        //Add optout preference for customer as "false" by default
        gsTaskUtil.createPreference(customerDBBean.getId(), Configurations.PreferrenceKeyType.OPT_OUT, "false");

        //Check if the Referral Code for DEFAULT channel is available
        ReferralCodeDBBean referralCodeDBBean = referralCodeDao.getActiveReferralCodeForChannel(customerDBBean.getId(), Configurations.ReferralCodeChannelType.DEFAULT);

        //If the referral code is not generated for DEFAULT channel then generate it
        if (referralCodeDBBean == null) {
            referralCodeDBBean = createReferralCode(customerDBBean, Configurations.ReferralCodeChannelType.DEFAULT);

            int referralCodeId = referralCodeDao.addReferralCode(referralCodeDBBean);

            referralCodeDBBean = referralCodeDao.getActiveReferralCodeForChannel(customerDBBean.getId(), Configurations.ReferralCodeChannelType.DEFAULT);

            //Add an entry in the referral code history table with the scheme details
            ReferralCodeHistoryDBBean referralCodeHistoryDBBean = referralCodeManagement.createReferralCodeHistory(referralCodeId, referralCodeDBBean.getIncentiveSchemeId());
            referralCodeDao.addReferralCodeHistory(referralCodeHistoryDBBean);
        } else {
            //Update existing referralCode if it is not mapped to Active incentiveScheme
            referralCodeManagement.updateExistingReferralCode(referralCodeDBBean, customerDBBean.getCountry());
        }

        //Populate the account details
        Account accountDetails = createAccountDetails(customerDBBean, customer, referralCodeDBBean.getReferralCode());
        return accountDetails;
    }


    //set OptOut
    public void setOptOut(final int customerId, String optOut) throws ServiceException {

        //Get preferences for customer using customerId
        PreferenceDBBean preferenceDBBean = preferenceDao.getPreferenceByKey(customerId, Configurations.PreferrenceKeyType.OPT_OUT);

        if (preferenceDBBean != null) {

            //set opt out value
            preferenceDBBean.setValue(optOut);

            //update preference using dao
            preferenceDao.updatePreference(preferenceDBBean);
        } else {
            //Create preference for OPT_OUT key
            gsTaskUtil.createPreference(customerId, Configurations.PreferrenceKeyType.OPT_OUT, optOut);
        }
    }

    // get OptOut
    public OptOut getOptOut(final int customerId) throws ServiceException {

        //Get OptOut preferences for customer using customerId
        PreferenceDBBean preferenceDBBean = preferenceDao.getPreferenceByKey(customerId, Configurations.PreferrenceKeyType.OPT_OUT);

        if (preferenceDBBean != null) {

            // construct OptOut response using preference Bean
            boolean optValue = preferenceDBBean.getValue().equals("true") ? true : false;

            OptOut optOut = new OptOut();
            optOut.setType("InviteOptOut");
            optOut.setOptOut(optValue);
            return optOut;

        } else {
            logger.error("OptOut entry not found");
            throw new ServiceException(ErrorCode.REFERRAL_OPT_OUT_ENTRY_NOT_FOUND);
        }

    }

    public CustomerDBBean getCustomerDetails(long gsCustomerId, int tenantId) throws BackendRequestException, ServiceException {
        //Check if there is a customer entry with gsCustomerId
        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, tenantId);

        if (customerDBBean.getAccountCreationTime() == null ||
                customerDBBean.getFirstName() == null) {
            Customer customer = customerManagementInterface.getCustomerDetails((int) gsCustomerId, gsTokenUtil.getValidToken(tenantId), tenantId);

            customerDBBean.setAccountCreationTime(customer.getCreatedOn());
            customerDBBean.setFirstName(customer.getFirstName());
            customerDBBean.setLastName(customer.getLastName());

            //If country has changed, update referralCode and incentiveScheme mapping
            if (!customerDBBean.getCountry().equals(customer.getCountry())) {
                customerDBBean.setCountry(customer.getCountry());

                ReferralCodeDBBean referralCodeDBBean = referralCodeDao.getActiveReferralCodeForChannel(customerDBBean.getId(), Configurations.ReferralCodeChannelType.DEFAULT);
                referralCodeManagement.updateExistingReferralCode(referralCodeDBBean, customerDBBean.getCountry());
            }

            customerDao.updateCustomerInfo(customerDBBean);
        }


        return customerDBBean;
    }


    public void updateLanguageForCustomer(int gsCustomerId, String language, int tenantId) {

        //Check if the customer is already present in the referral engine
        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, tenantId);

        //Update the language of the customer if language is not set or different
        if (StringUtils.isEmpty(customerDBBean.getLanguage()) ||
                (StringUtils.isNotEmpty(customerDBBean.getLanguage()) && !(customerDBBean.getLanguage().equals(language)))) {
            customerDBBean.setLanguage(language);
            customerDao.updateCustomerInfo(customerDBBean);
        }

    }

    //Create customer bean
    private CustomerDBBean createCustomer(Customer customer) {
        CustomerDBBean customerDBBean = new CustomerDBBean();
        customerDBBean.setGsCustomerId(customer.getCustomerId());
        customerDBBean.setEmailId(customer.getEmailId());
        customerDBBean.setAccountCreationTime(customer.getCreatedOn());
        customerDBBean.setFirstName(customer.getFirstName());
        customerDBBean.setLastName(customer.getLastName());

        //Set the Backend Customer Status
        customerDBBean.setStatus(customer.getAccountActivationStatus());
        customerDBBean.setCountry(customer.getCountry());

        return customerDBBean;
    }

    //Create customer bean
    private CustomerDBBean convertCustomerBean(int tenantId, Customer fromCustomer, CustomerDBBean toCustomerBean) {

        if (toCustomerBean == null) {
            toCustomerBean = new CustomerDBBean();
            toCustomerBean.setReferralStatus(Configurations.ReferralStatus.SIGNUP_WITHOUT_REFERRAL);
        }

        //GsCustomerId
        if (toCustomerBean.getGsCustomerId() == null) {
            toCustomerBean.setGsCustomerId(fromCustomer.getCustomerId());
            toCustomerBean.setTenantId(tenantId);
        }

        //Email
        if (StringUtils.isNotEmpty(fromCustomer.getEmailId())) {
            toCustomerBean.setEmailId(fromCustomer.getEmailId());
        }

        //FirstName
        if (StringUtils.isNotEmpty(fromCustomer.getFirstName())) {
            toCustomerBean.setFirstName(fromCustomer.getFirstName());
        }

        //LastName
        if (StringUtils.isNotEmpty(fromCustomer.getLastName())) {
            toCustomerBean.setLastName(fromCustomer.getLastName());
        }

        //Country
        if (StringUtils.isNotEmpty(fromCustomer.getCountry())) {
            toCustomerBean.setCountry(fromCustomer.getCountry());
        }

        //Activation status
        if (StringUtils.isEmpty(toCustomerBean.getStatus())) {
            if (StringUtils.isNotEmpty(fromCustomer.getAccountActivationStatus())) {
                toCustomerBean.setStatus(fromCustomer.getAccountActivationStatus());
            }
        }

        //Set referral status as SIGNUP_WITHOUT_REFERRAL if referral status was not set
        if (StringUtils.isEmpty(toCustomerBean.getReferralStatus())) {
            toCustomerBean.setReferralStatus(Configurations.ReferralStatus.SIGNUP_WITHOUT_REFERRAL);
        }

        if (toCustomerBean.getAccountCreationTime() == null) {
            toCustomerBean.setAccountCreationTime(fromCustomer.getCreatedOn());
        }

        return toCustomerBean;
    }

    private void upsertCustomer(CustomerDBBean customerDBBean) {

        if (customerDBBean.getId() != 0) {
            //Update customer
            customerDao.updateCustomerInfo(customerDBBean);

        } else {
            //Insert customer
            customerDao.createCustomerInfo(customerDBBean);
        }
    }


    //Create Account details
    private Account createAccountDetails(CustomerDBBean customerDBBean, Customer customer, String referralCode) {
        Account accountDetails = new Account();
        accountDetails.setEmailId(customerDBBean.getEmailId());
        accountDetails.setPreferredCurrency(customer.getPreferredCurrency());
        accountDetails.setFirstName(customer.getFirstName());
        accountDetails.setLastName(customer.getLastName());

        accountDetails.setReferralCode(referralCode);

        //Construct the referral URL
        String referralBaseUrl = configDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.REFERRAL_BASE_URL, "https://www.gigsky.com/");
        String referralUrl = referralBaseUrl + referralCode;
        accountDetails.setReferralUrl(referralUrl);
        accountDetails.setReferralStatus(customerDBBean.getReferralStatus());

        return accountDetails;
    }

    //Create Referral Code
    private ReferralCodeDBBean createReferralCode(CustomerDBBean customerDBBean, String channel) throws ServiceException {
        //Create the referral code for the active scheme for the customer
        ReferralCode referralCode = new ReferralCode();
        referralCode.setCustomerId(customerDBBean.getId());
        referralCode.setEmailId(customerDBBean.getEmailId());
        referralCode.setChannel(Configurations.ReferralCodeChannelType.DEFAULT);

        return referralCodeManagement.createReferralCodeForActiveScheme(referralCode, customerDBBean);
    }

}
