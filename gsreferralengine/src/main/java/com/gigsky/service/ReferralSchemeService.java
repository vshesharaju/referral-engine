package com.gigsky.service;

import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CreditSchemeDBBean;
import com.gigsky.database.bean.IncentiveSchemeDBBean;
import com.gigsky.database.bean.ValidCountryDBBean;
import com.gigsky.database.dao.CreditSchemeDao;
import com.gigsky.database.dao.IncentiveSchemeDao;
import com.gigsky.database.dao.SupportedCurrenciesDao;
import com.gigsky.database.dao.ValidCountryDao;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.rest.bean.CreditScheme;
import com.gigsky.rest.bean.PageInfo;
import com.gigsky.rest.bean.Scheme;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSSchemeUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by pradeepragav on 12/01/17.
 */
public class ReferralSchemeService {

    private static final Logger logger = LoggerFactory.getLogger(ReferralSchemeService.class);

    @Autowired
    private IncentiveSchemeDao incentiveSchemeDao;

    @Autowired
    private ValidCountryDao validCountryDao;

    @Autowired
    private CreditSchemeDao creditSchemeDao;

    @Autowired
    private SupportedCurrenciesDao supportedCurrenciesDao;

    //Add IncentiveScheme
    public Scheme addIncentiveScheme(Scheme schemeDetails, int tenantId) throws ServiceException, DatabaseException, ParseException {

        //Check if scheme can be added
        validateSchemeForAdd(schemeDetails, tenantId);

        //Incentive scheme can be added

        //Convert String to date
        Date newSchemeStartDate = CommonUtils.convertStringToDate(schemeDetails.getValidityStart());
        Date newSchemeEndDate = CommonUtils.convertStringToDate(schemeDetails.getValidityEnd());

        //Create IncentiveSchemeDBBean using Scheme
        IncentiveSchemeDBBean newIncentiveSchemeDBBean = new IncentiveSchemeDBBean();
        newIncentiveSchemeDBBean.setValidityStartDate(newSchemeStartDate);
        newIncentiveSchemeDBBean.setValidityEndDate(newSchemeEndDate);
        newIncentiveSchemeDBBean.setAdvocateCreditExpiryPeriod(schemeDetails.getAdvocateCreditExpiryPeriod());
        newIncentiveSchemeDBBean.setReferralCreditExpiryPeriod(schemeDetails.getReferralCreditExpiryPeriod());
        newIncentiveSchemeDBBean.setSchemeEnabled((schemeDetails.getSchemeEnabled())? (byte)1 :(byte)0);
        newIncentiveSchemeDBBean.setTenantId(tenantId);

        //For each country in countries create and add ValidCountryDBBean
        List<String> countries = schemeDetails.getCountry();

        //Create a List of ValidaCountries using countries
        List<ValidCountryDBBean> newValidCountryDBBeanList = new ArrayList<ValidCountryDBBean>();
        for(String country : countries){

            //Create ValidCountryDBBean using Scheme
            ValidCountryDBBean newValidCountryDBBean = new ValidCountryDBBean();
            newValidCountryDBBean.setCountry(country);

            //Add ValidCountryDBBean
            newValidCountryDBBeanList.add(newValidCountryDBBean);
        }

        //Create CreditSchemeDBBean using Scheme
        List<CreditScheme> newCreditSchemeList = schemeDetails.getCreditScheme();

        //If the scheme is enabled then
        if(schemeDetails.getSchemeEnabled()) {

            //Check if this scheme should be set as active
            byte activeScheme = (GSSchemeUtil.utilMethodDoesTimelineMatchesCurrentTimeline(
                    newIncentiveSchemeDBBean.getValidityStartDate(),
                    newIncentiveSchemeDBBean.getValidityEndDate())) ? (byte)1: (byte)0;

            //Set activeScheme
            newIncentiveSchemeDBBean.setActiveScheme(activeScheme);
        }


        //Create List CreditSchemeDBBeanList
        List<CreditSchemeDBBean> newCreditSchemeDBBeanList = getListOfCreditSchemeDBBeansFrom(newCreditSchemeList);

        incentiveSchemeDao.addIncentiveScheme(newIncentiveSchemeDBBean,newValidCountryDBBeanList, newCreditSchemeDBBeanList);

        //Create outputSchemeDetails and return
        Scheme outputSchemeDetails = new Scheme("SchemeDetail",
                newIncentiveSchemeDBBean.getId(),
                schemeDetails.getValidityStart(),
                schemeDetails.getValidityEnd(),
                schemeDetails.getCountry(),
                newIncentiveSchemeDBBean.getAdvocateCreditExpiryPeriod(),
                newIncentiveSchemeDBBean.getReferralCreditExpiryPeriod(),
                (newIncentiveSchemeDBBean.getSchemeEnabled()==1)? true : false,
                schemeDetails.getCreditScheme());

        return outputSchemeDetails;
    }

    //Get IncentiveScheme
    public Scheme getIncentiveScheme(int incentiveSchemeId, int tenantId) throws ServiceException{

        //Get incentiveSchemeDBBean
        IncentiveSchemeDBBean incentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);

        //If null throw error
        if(incentiveSchemeDBBean == null){
            logger.error("IncentiveScheme entry missing for incentiveSchemeId: "+incentiveSchemeId);
            throw new ServiceException(ErrorCode.REFERRAL_SCHEME_NOT_FOUND);
        }

        //Get validCountryDBBean
        List<ValidCountryDBBean> validCountryDBBeanList = validCountryDao.getValidCountriesForIncentiveSchemeId(incentiveSchemeId);

        //Create list<String> countries from validCountryDBBeanLis
        List<String> countriesOfIncentiveScheme = getCountries(validCountryDBBeanList);

        //Get creditSchemeDBBean
        List<CreditSchemeDBBean> creditSchemeDBBeanList = creditSchemeDao.getCreditSchemesForIncentiveScheme(incentiveSchemeId);

        //Create CreditScheme bean using creditSchemeDBBeanList
        List<CreditScheme> creditSchemeList = getCreditSchemes(creditSchemeDBBeanList);

        String validityStartDate = incentiveSchemeDBBean.getValidityStartDate().toString();

        String validityEndDate = incentiveSchemeDBBean.getValidityEndDate().toString();

        //Create scheme object using incentiveSchemeDBBean
        Scheme outputSchemeDetails = new Scheme("SchemeDetail",
                incentiveSchemeDBBean.getId(),
                validityStartDate,
                validityEndDate,
                countriesOfIncentiveScheme,
                incentiveSchemeDBBean.getAdvocateCreditExpiryPeriod(),
                incentiveSchemeDBBean.getReferralCreditExpiryPeriod(),
                (incentiveSchemeDBBean.getSchemeEnabled()==1)? true : false,
                creditSchemeList);
        return outputSchemeDetails;
    }

    //Update IncentiveScheme
    public Scheme updateIncentiveScheme(int incentiveSchemeId, Scheme schemeDetails, int tenantId) throws ServiceException, DatabaseException, ParseException {

        //Create InvitationDBBean using Scheme bean
        IncentiveSchemeDBBean existingIncentiveSchemeDBBean = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);

        //If incentiveScheme is not found
        if(existingIncentiveSchemeDBBean==null){
            //Throw error
            logger.error("IncentiveScheme entry missing for incentiveSchemeId: "+incentiveSchemeId);
            throw new ServiceException(ErrorCode.REFERRAL_SCHEME_NOT_FOUND);
        }

        //Check if incentiveScheme sent is eligible for updating
        validateSchemeForUpdate(schemeDetails, existingIncentiveSchemeDBBean, tenantId);

        Date newSchemeStartDate = null;
        Date newSchemeEndDate = null;

        //Convert String to date
        if(StringUtils.isNotEmpty(schemeDetails.getValidityStart())){
            newSchemeStartDate = CommonUtils.convertStringToDate(schemeDetails.getValidityStart());
            existingIncentiveSchemeDBBean.setValidityStartDate(newSchemeStartDate);
        }

        if(StringUtils.isNotEmpty(schemeDetails.getValidityEnd())){
            newSchemeEndDate = CommonUtils.convertStringToDate(schemeDetails.getValidityEnd());
            existingIncentiveSchemeDBBean.setValidityEndDate(newSchemeEndDate);
        }

        //Set parameters to existingIncentiveSchemeDBBean as per schemeDetails
        if(schemeDetails.getReferralCreditExpiryPeriod() != null && schemeDetails.getReferralCreditExpiryPeriod() != 0){
            existingIncentiveSchemeDBBean.setReferralCreditExpiryPeriod(schemeDetails.getReferralCreditExpiryPeriod());
        }

        if(schemeDetails.getAdvocateCreditExpiryPeriod() != null && schemeDetails.getAdvocateCreditExpiryPeriod() != 0){
            existingIncentiveSchemeDBBean.setAdvocateCreditExpiryPeriod(schemeDetails.getAdvocateCreditExpiryPeriod());
        }

        if(schemeDetails.getSchemeEnabled() != null ) {
            existingIncentiveSchemeDBBean.setSchemeEnabled((schemeDetails.getSchemeEnabled()) ? (byte) 1 : (byte) 0);
        }

        //Create a List of ValidaCountries using countries
        List<ValidCountryDBBean> newValidCountryDBBeanList = null;

        if(CollectionUtils.isNotEmpty(schemeDetails.getCountry())){

            //For each country in countries create and add ValidCountryDBBean
            List<String> countries = schemeDetails.getCountry();
            newValidCountryDBBeanList = new ArrayList<ValidCountryDBBean>();
            for(String country : countries){

                //Create ValidCountryDBBean using Scheme
                ValidCountryDBBean newValidCountryDBBean = new ValidCountryDBBean();
                newValidCountryDBBean.setCountry(country);

                //Add ValidCountryDBBean
                newValidCountryDBBeanList.add(newValidCountryDBBean);
            }
        }

        //Create List CreditSchemeDBBeanList
        List<CreditSchemeDBBean> newCreditSchemeDBBeanList = null;

        if(CollectionUtils.isNotEmpty(schemeDetails.getCreditScheme())){
            List<CreditScheme> newCreditSchemeList = schemeDetails.getCreditScheme();
            //Create List CreditSchemeDBBeanList
            newCreditSchemeDBBeanList = getListOfCreditSchemeDBBeansFrom(newCreditSchemeList);
        }

        //If the scheme is enabled
        if(schemeDetails.getSchemeEnabled()!= null ){

            byte activeScheme = 0;
            if(schemeDetails.getSchemeEnabled()){

                //Check if this scheme should be set as active
                activeScheme = (GSSchemeUtil.utilMethodDoesTimelineMatchesCurrentTimeline(
                        existingIncentiveSchemeDBBean.getValidityStartDate(),
                        existingIncentiveSchemeDBBean.getValidityEndDate()))
                        ? (byte)1: (byte)0;
            }

            //Set activeScheme
            existingIncentiveSchemeDBBean.setActiveScheme(activeScheme);
        }


        //Update using dao
        incentiveSchemeDao.updateIncentiveScheme(existingIncentiveSchemeDBBean, newValidCountryDBBeanList, newCreditSchemeDBBeanList);

        //Get incentiveScheme
        Map<String ,Object> activeReferralSchemeMap = incentiveSchemeDao.getIncentiveSchemeDetails(incentiveSchemeId, tenantId);

        //Create outputSchemeDetails and return
        Scheme outputSchemeDetails = getSchemeUsingIncentiveSchemeMap(activeReferralSchemeMap);
        return outputSchemeDetails;
    }


    //Delete IncentiveScheme
    public void deleteIncentiveScheme(int incentiveSchemeId, int tenantId) throws ServiceException{

        //Check if incentiveScheme exists
        IncentiveSchemeDBBean existingIncentiveScheme = incentiveSchemeDao.getIncentiveScheme(incentiveSchemeId, tenantId);

        if(existingIncentiveScheme==null){
            //Log error
            logger.error("Incentive scheme not found for incentiveSchemeId :"+incentiveSchemeId);

            //Throw error
            throw new ServiceException(ErrorCode.REFERRAL_SCHEME_NOT_FOUND);
        }

        //Delete the incentiveScheme using dao
        incentiveSchemeDao.deleteIncentiveScheme(existingIncentiveScheme);
    }

    //Get all incentive schemes
    public List<Scheme> getAllIncentiveScheme(String countryCode, String isEnabled, PageInfo inputPageInfo, PageInfo outputPageInfo, String date, int tenantId) throws ServiceException, ParseException{

        //Get all incentive schemes for this country
        List<IncentiveSchemeDBBean> incentiveSchemeDBBeanList = incentiveSchemeDao.getIncentiveSchemes(countryCode, isEnabled, inputPageInfo, tenantId);

        //Check for null
        if(CollectionUtils.isEmpty(incentiveSchemeDBBeanList)){

            //Get totalCount, in case only totalCount is required
            //Configure outputPageInfo
            int totalCount = incentiveSchemeDao.getCountOfIncentiveSchemes(countryCode, isEnabled, date, tenantId);

            outputPageInfo.setCount(totalCount);
            outputPageInfo.setStart(inputPageInfo.getStart());

            if(totalCount == 0)
                logger.error("Incentive schemes not found for country");

            return new ArrayList<Scheme>();
        }

        // updating list of incentive schemes, based on date.
        if(date != null) {
            incentiveSchemeDBBeanList = incentiveSchemeDao.getAllIncentiveSchemesByDate(incentiveSchemeDBBeanList, date);
        }

        List<Scheme> outPutScheme = new ArrayList<Scheme>();

        for(IncentiveSchemeDBBean incentiveSchemeDBBean : incentiveSchemeDBBeanList){

            //Get ValidCountries for this incentiveScheme
            List<ValidCountryDBBean> validCountryDBBeans = validCountryDao.getValidCountriesForIncentiveSchemeId(incentiveSchemeDBBean.getId());

            //Get CreditSchemes for this incentiveScheme
            List<CreditSchemeDBBean> creditSchemeDBBeans = creditSchemeDao.getCreditSchemesForIncentiveScheme(incentiveSchemeDBBean.getId());

            //Create Scheme pojo
            List<String> countries = getCountries(validCountryDBBeans);
            List<CreditScheme> creditSchemeList = getCreditSchemes(creditSchemeDBBeans);

            String validityStartDate = incentiveSchemeDBBean.getValidityStartDate().toString();
            String validityEndDate = incentiveSchemeDBBean.getValidityEndDate().toString();


            //Create scheme object using incentiveSchemeDBBean
            Scheme outputSchemeDetails = new Scheme("SchemeDetail",
                    incentiveSchemeDBBean.getId(),
                    validityStartDate,
                    validityEndDate,
                    countries,
                    incentiveSchemeDBBean.getAdvocateCreditExpiryPeriod(),
                    incentiveSchemeDBBean.getReferralCreditExpiryPeriod(),
                    (incentiveSchemeDBBean.getSchemeEnabled()==1)? true : false,
                    creditSchemeList);

            //Add scheme to outPutScheme
            outPutScheme.add(outputSchemeDetails);
        }

        //Configure outputPageInfo
        int totalCount =
                (CollectionUtils.isEmpty(incentiveSchemeDBBeanList)) ?
                        0 : incentiveSchemeDao.getCountOfIncentiveSchemes(countryCode, isEnabled, date, tenantId);

        outputPageInfo.setCount(totalCount);
        outputPageInfo.setStart(inputPageInfo.getStart());

        return outPutScheme;
    }

    //Get active scheme
    public Scheme getActiveScheme(String countryCode , String preferredCurrency, int tenantId) throws ServiceException{

        Map<String ,Object> activeReferralSchemeMap = incentiveSchemeDao.getActiveScheme(countryCode, preferredCurrency, tenantId);

        //Create scheme using activeReferralSchemeMap
        Scheme outputSchemeDetails = getSchemeUsingIncentiveSchemeMap(activeReferralSchemeMap);
        return outputSchemeDetails;
    }

    //Get referral scheme details
    public Scheme getSchemeForReferralSchemeDetails(String referralCode, String preferredCurrency, int tenantId) throws ServiceException{

        Map<String, Object> referralSchemeDetails = incentiveSchemeDao.getActiveSchemeForReferralCode(referralCode, preferredCurrency, tenantId);

        if(referralSchemeDetails == null){
            logger.error("Incentive scheme not found");
            throw new ServiceException(ErrorCode.REFERRAL_SCHEME_NOT_FOUND);
        }

        //Create scheme using getSchemeUsingIncentiveSchemeMap
        Scheme outputSchemeDetails = getSchemeUsingIncentiveSchemeMap(referralSchemeDetails);
        return outputSchemeDetails;
    }

    /*IncentiveScheme Utility methods*/

    private void validateSchemeForAdd(Scheme scheme, int tenantId) throws ServiceException, ParseException {

        int incentiveSchemeId = scheme.getIncentiveSchemeId();

        //Check if the scheme is active or inactive
        if(scheme.getSchemeEnabled() != null && scheme.getSchemeEnabled()){

            List<String> countries = scheme.getCountry();
            List<IncentiveSchemeDBBean> incentiveSchemeDBBeanList;

            //Check if there are any active scheme present
            incentiveSchemeDBBeanList = incentiveSchemeDao.getEnabledSchemeForCountries(countries, tenantId);


            if(incentiveSchemeDBBeanList!=null){
                //For each incentiveScheme check whether the ValidityStart of new Scheme clashes

                //Get validity start and end dates of the new scheme
                Date validityStartOfNewScheme = CommonUtils.convertStringToDate(scheme.getValidityStart());
                Date validityEndOfNewScheme = CommonUtils.convertStringToDate(scheme.getValidityEnd());

                for (IncentiveSchemeDBBean incentiveSchemeDBBean : incentiveSchemeDBBeanList){

                    //Get validity start and end dates of the existing scheme
                    Date validityStartOfExistingScheme = incentiveSchemeDBBean.getValidityStartDate();
                    Date validityEndOfExistingScheme = incentiveSchemeDBBean.getValidityEndDate();

                    if((incentiveSchemeDBBean.getId()!=incentiveSchemeId)
                            //IncentiveSchemeId is passed for updating, If the incentiveScheme is not the same which is being updated then we will not allow updating the incentiveScheme
                            && GSSchemeUtil.doesValidityTimeClash(validityStartOfNewScheme, validityEndOfNewScheme,validityStartOfExistingScheme, validityEndOfExistingScheme)){

                        //Validity time clashes with the existing scheme
                        logger.error("Incentive scheme cannot be added");
                        throw new ServiceException(ErrorCode.REFERRAL_SCHEME_ALREADY_EXISTS);
                    }
                }
            }
        }
    }

    private void validateSchemeForUpdate(Scheme scheme, IncentiveSchemeDBBean incentiveSchemeDBBean, int tenantId) throws ServiceException, ParseException {

        //Create a Scheme object with no null values
        Scheme schemeWithNoNullValues = new Scheme();

        schemeWithNoNullValues.setType(scheme.getType());

        //Set incentiveSchemeID
        schemeWithNoNullValues.setIncentiveSchemeId(incentiveSchemeDBBean.getId());

        //Get validityStartDate, if the newScheme does not have then take it from existing incentiveScheme
        String validityStartDate = (StringUtils.isEmpty(scheme.getValidityStart()) ?
                CommonUtils.getDateInString(incentiveSchemeDBBean.getValidityStartDate()) : scheme.getValidityStart());

        //Set validityStart string
        schemeWithNoNullValues.setValidityStart(validityStartDate);

        //Get validityStartDate, if the newScheme does not have then take it from existing incentiveScheme
        String validityEndDate = (StringUtils.isEmpty(scheme.getValidityEnd()) ?
                CommonUtils.getDateInString(incentiveSchemeDBBean.getValidityEndDate()) : scheme.getValidityEnd());

        //Set validityEnd string
        schemeWithNoNullValues.setValidityEnd(validityEndDate);

        //Get countries
        List<String > countries = (CollectionUtils.isEmpty(scheme.getCountry()) ?
                getCountries(validCountryDao.getValidCountriesForIncentiveSchemeId(incentiveSchemeDBBean.getId())) : scheme.getCountry()) ;

        //Set countries for this scheme
        schemeWithNoNullValues.setCountry(countries);

        //Set advocateCreditExpiryPeriod
        Integer advocateCreditExpiryPeriod = (scheme.getAdvocateCreditExpiryPeriod() == null) ?
                incentiveSchemeDBBean.getAdvocateCreditExpiryPeriod() : scheme.getAdvocateCreditExpiryPeriod();

        schemeWithNoNullValues.setAdvocateCreditExpiryPeriod(advocateCreditExpiryPeriod);

        //Set referralCreditExpiryPeriod
        Integer referralCreditExpiryPeriod = (scheme.getReferralCreditExpiryPeriod() == null) ?
                incentiveSchemeDBBean.getReferralCreditExpiryPeriod() : scheme.getReferralCreditExpiryPeriod();

        schemeWithNoNullValues.setReferralCreditExpiryPeriod(referralCreditExpiryPeriod);

        //Set schemeEnabled
        Boolean schemeEnabled = (scheme.getSchemeEnabled() == null) ? (incentiveSchemeDBBean.getSchemeEnabled() == 1) : scheme.getSchemeEnabled();

        schemeWithNoNullValues.setSchemeEnabled(schemeEnabled);

        schemeWithNoNullValues.setCreditScheme((CollectionUtils.isEmpty(scheme.getCreditScheme())) ?
                getCreditSchemes(creditSchemeDao.getCreditSchemesForIncentiveScheme(incentiveSchemeDBBean.getId())) : scheme.getCreditScheme() );

        validateSchemeForAdd(schemeWithNoNullValues, tenantId);
    }



    private List<String> getCountries(List<ValidCountryDBBean> validCountryDBBeanList){

        //Create countries list
        List<String> countries = new ArrayList<String>();

        //Add countries to List
        for (ValidCountryDBBean validCountryDBBean : validCountryDBBeanList){

            String country = validCountryDBBean.getCountry();

            countries.add(country);

        }

        return countries;
    }

    private List<CreditScheme> getCreditSchemes(List<CreditSchemeDBBean> creditSchemeDBBeanList){

        //Create CreditScheme list
        List<CreditScheme> creditSchemeList = new ArrayList<CreditScheme>();

        //Add CreditScheme
        for(CreditSchemeDBBean creditSchemeDBBean : creditSchemeDBBeanList){
            //Create CreditScheme
            CreditScheme creditScheme = new CreditScheme(creditSchemeDBBean);
            //Add CreditScheme to List
            creditSchemeList.add(creditScheme);
        }
        return creditSchemeList;
    }

    private Scheme getSchemeUsingIncentiveSchemeMap(Map<String, Object> incentiveSchemeMap){

        //Get the contents of the Hashmap
        IncentiveSchemeDBBean activeIncentiveSchemeDBBean = (IncentiveSchemeDBBean) incentiveSchemeMap.get(Configurations.INCENTIVE_SCHEME_BEAN);

        List<ValidCountryDBBean> validCountryDBBeans = (List<ValidCountryDBBean>) incentiveSchemeMap.get(Configurations.VALIDCOUNTRY_BEAN_LIST);

        List<CreditSchemeDBBean> creditSchemeDBBeans = (List<CreditSchemeDBBean>) incentiveSchemeMap.get(Configurations.CREDIT_SCHEME_BEAN);

        //Get list of countries from validCountryDBBeans
        List<String> countries = getCountries(validCountryDBBeans);

        //Get list of CreditScheme from creditSchemeDBBeans
        List<CreditScheme> creditSchemeList = getCreditSchemes(creditSchemeDBBeans);

        //Convert date to string
        String validityStartDate = activeIncentiveSchemeDBBean.getValidityStartDate().toString();

        String validityEndDate = activeIncentiveSchemeDBBean.getValidityEndDate().toString();

        //Create scheme using activeReferralSchemeMap
        Scheme scheme = new Scheme("SchemeDetail",
                activeIncentiveSchemeDBBean.getId(),
                validityStartDate,
                validityEndDate,
                countries,
                activeIncentiveSchemeDBBean.getAdvocateCreditExpiryPeriod(),
                activeIncentiveSchemeDBBean.getReferralCreditExpiryPeriod(),
                (activeIncentiveSchemeDBBean.getSchemeEnabled()==1)? true : false,
                creditSchemeList);
        return scheme;
    }

    private List<CreditSchemeDBBean> getListOfCreditSchemeDBBeansFrom(List<CreditScheme> creditSchemes){

        List<CreditSchemeDBBean> newCreditSchemeDBBeanList = new ArrayList<CreditSchemeDBBean>();

        //For each CreditScheme in newCreditSchemeList create CreditSchemeDBBean and add
        for(CreditScheme newCreditScheme : creditSchemes){

            CreditSchemeDBBean creditSchemeDBBean = new CreditSchemeDBBean();
            creditSchemeDBBean.setCurrency(newCreditScheme.getCurrency());
            creditSchemeDBBean.setNewCustomerAmount(newCreditScheme.getNewCustomerAmount());
            creditSchemeDBBean.setAdvocateAmount(newCreditScheme.getAdvocateAmount());

            //Add to list
            newCreditSchemeDBBeanList.add(creditSchemeDBBean);
        }

        return newCreditSchemeDBBeanList;
    }


    //validate whether the currencies present in credit scheme are only the supported currencies of that tenant
    public boolean isValidCreditScheme(List<CreditScheme> schemeList, int tenantId){
        if(CollectionUtils.isEmpty(schemeList)){
            return false;
        }

        List<String> supportedCurrencies = supportedCurrenciesDao.getSupportedCurrencies(tenantId);

        if(supportedCurrencies.size() != schemeList.size()){
            return false;
        }

        return true;

    }

}
