package com.gigsky.service;

import com.gigsky.backend.credit.CreditManagementInterface;
import com.gigsky.backend.customer.CustomerManagementInterface;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.InvitationDBBean;
import com.gigsky.database.bean.ReferralCodeDBBean;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.InvitationDao;
import com.gigsky.database.dao.ReferralCodeDao;
import com.gigsky.rest.bean.Credit;
import com.gigsky.rest.bean.Invitation;
import com.gigsky.rest.bean.PageInfo;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSTokenUtil;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prsubbareddy on 06/01/17.
 */
public class ReferralInviteService {

    private static final Logger logger = LoggerFactory.getLogger(ReferralInviteService.class);

    @Autowired
    private InvitationDao invitationDao;

    @Autowired
    private ReferralCodeDao referralCodeDao;

    @Autowired
    private CreditManagementInterface creditManagementInterface;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    GSTokenUtil tokenUtil;

    @Autowired
    CustomerManagementInterface customerManagementInterface;


    public Invitation getInvite(int gsCustomerId, int inviteId, int tenantId) throws ServiceException, BackendRequestException {
        //Get invitation
        InvitationDBBean invitationDBBean = invitationDao.getInvitationById(inviteId);

        //If entry is missing
        if(invitationDBBean == null){
            logger.error("Invitation with inviteId: "+inviteId+"not found");
            throw new ServiceException(ErrorCode.REFERRAL_INVITE_ENTRY_NOT_FOUND);
        }

        //Check if the invite belongs to the same customer
        if(checkIfInviteDoesNotBelongToCustomer(gsCustomerId, invitationDBBean, tenantId)){

            logger.error("Invitation with inviteId: "+inviteId+"not found");
            throw new ServiceException(ErrorCode.REFERRAL_INVITE_ENTRY_NOT_FOUND);
        }

        return getInvitation(invitationDBBean, tenantId);
    }


    public List<Invitation> getInvitationsForCustomer(int customerId, PageInfo inputPageInfo, PageInfo outputPageInfo, int tenantId) throws ServiceException, BackendRequestException {

        //Get list of InvitationDBBean
        List<InvitationDBBean> invitationDBBeans = invitationDao.getInvitationsForCustomer(customerId,inputPageInfo.getStart(),inputPageInfo.getCount(), tenantId);

        if(CollectionUtils.isEmpty(invitationDBBeans)){

            //In case of start and end index are set to Zero, return totalCount
            //Configure outputPageInfo
            int totalCount = invitationDao.getInvitationCountForCustomer(customerId, tenantId);

            outputPageInfo.setCount(totalCount);
            outputPageInfo.setStart(inputPageInfo.getStart());

            return new ArrayList<Invitation>();
        }

        //Create a list of Invitation based on parameters
        List<Invitation> outputInvitations = new ArrayList<Invitation>();

        for(InvitationDBBean invitationDBBean : invitationDBBeans){

            outputInvitations.add(getInvitation(invitationDBBean, tenantId));
        }

        //Configure outputPageInfo
        int totalCount = invitationDao.getInvitationCountForCustomer(customerId, tenantId);

        outputPageInfo.setCount(totalCount);
        outputPageInfo.setStart(inputPageInfo.getStart());

        return outputInvitations;
    }


    /* ReferralInviteService Utility methods */
    private boolean checkIfInviteDoesNotBelongToCustomer(int gsCustomerId, InvitationDBBean invitationDBBean, int tenantId){

        //Get customerId from gsCustomerID
        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, tenantId);

        int customerId = customerDBBean.getId();

        //Get referralCodeId
        int referralCodeId = invitationDBBean.getReferralCodeId();

        //Get referralCodeDbBean
        ReferralCodeDBBean referralCodeDBBean = referralCodeDao.getReferralCode(referralCodeId);

        //ReferralCode's customerId is not equal to customerId passed
        if (referralCodeDBBean.getCustomerId() != customerId){
            return true;
        }
        return false;
    }

    private Invitation getInvitation(InvitationDBBean invitationDBBean, int tenantId) throws BackendRequestException {

        Invitation invitation = new Invitation(invitationDBBean);

        CustomerDBBean customerDBBean = customerDao.getCustomerInfo(invitationDBBean.getTargetCustomerId(), tenantId);

        long gsCustomerId = customerDBBean.getGsCustomerId();

        //Set advocate credit details
        if(invitationDBBean.getAdvocateCreditId() != null) {

            //Get credit details
            Credit credit = creditManagementInterface.getCreditDetails((int)gsCustomerId, invitationDBBean.getAdvocateCreditId(), tenantId);

            invitation.setAdvocateAmount(credit.getAddedAmount());
            invitation.setAdvocateCurrency(credit.getCurrency());
        }

        return invitation;
    }
}
