package com.gigsky.service;

import com.gigsky.backend.credit.CreditManagementInterface;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.rest.bean.*;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSSchemeUtil;
import com.gigsky.util.GSTaskUtil;
import com.gigsky.util.GSUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by shwethars on 10/01/19.
 */
public class PromotionService {

    private static final Logger logger = LoggerFactory.getLogger(PromotionService.class);

    @Autowired
    private PromotionDao promotionDao;

    @Autowired
    private PromoCountryDao promoCountryDao;

    @Autowired
    private PromoCreditDao promoCreditDao;

    @Autowired
    private ReferralCodeDao referralCodeDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private PreferenceDao preferenceDao;

    @Autowired
    private GSTaskUtil gsTaskUtil;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CreditManagementInterface creditManagementInterface;

    @Autowired
    private PromoHistoryDao promoHistoryDao;

    @Autowired
    private IncentiveSchemeDao incentiveSchemeDao;
    
    @Autowired
    private SupportedCurrenciesDao supportedCurrenciesDao;

    @Autowired
    private ReferralEventDao referralEventDao;

    @Autowired
    private PromoEventDao promoEventDao;

    @Autowired
    private ErrorStringDao errorStringDao;

    public Promotion addPromotion(Promotion promoDetails, int tenantId) throws ServiceException, DatabaseException, ParseException {

        //Check if promotion can be added
        validatePromoCode(promoDetails, tenantId);

        //Promotion can be added

        //Convert String to date
        Date startDate = CommonUtils.convertStringToDate(promoDetails.getStartDate());
        Date endDate = CommonUtils.convertStringToDate(promoDetails.getEndDate());

        //Create PromotionDBBean using Promotion
        PromotionDBBean promotionDBBean = new PromotionDBBean();

        promotionDBBean.setName(promoDetails.getName());
        promotionDBBean.setPromoCode(promoDetails.getPromoCode());
        promotionDBBean.setStartDate(startDate);
        promotionDBBean.setEndDate(endDate);
        promotionDBBean.setCreditExpiryPeriod(promoDetails.getCreditExpiryPeriod());
        promotionDBBean.setMaxRedeemCount(promoDetails.getMaxRedeemCount());
        promotionDBBean.setPromoEnabled((promoDetails.getPromoEnabled())? (byte)1 :(byte)0);
//        promotionDBBean.setActivePromo();
        promotionDBBean.setTenantId(tenantId);

        //For each country in countries create and add PromoCountryDBBean
        List<String> countries = promoDetails.getCountry();

        //Create a List of promoCountries using countries
        List<PromoCountryDBBean> newPromoCountryDBBeanList = new ArrayList<PromoCountryDBBean>();
        for(String country : countries){

            //Create PromoCountryDBBean from Promotion
            PromoCountryDBBean newPromoCountryDBBean = new PromoCountryDBBean();
            newPromoCountryDBBean.setCountry(country);

            //Add PromoCountryDBBean
            newPromoCountryDBBeanList.add(newPromoCountryDBBean);
        }

        //Create PromoCredit from Promotion
        List<PromoCredit> newPromoCreditList = promoDetails.getPromoCredit();

        //If the promotion is enabled then
        if(promoDetails.getPromoEnabled()) {
            
            //Check if this promotion should be set as active
            byte activePromotion = (GSSchemeUtil.utilMethodDoesTimelineMatchesCurrentTimeline(
                    promotionDBBean.getStartDate(),
                    promotionDBBean.getEndDate())) ? (byte)1: (byte)0;

            //Set activePromotion
            promotionDBBean.setActivePromo(activePromotion);
        }else{
//            logger.info("Set active to false");
            promotionDBBean.setActivePromo((byte)0);
        }


        //Create List PromoCreditDBBeanList
        List<PromoCreditDBBean> newPromoCreditDBBeanList = getPromoCreditDBBeanList(newPromoCreditList);

        int insertedPromotionId = promotionDao.addPromotion(promotionDBBean, newPromoCountryDBBeanList, newPromoCreditDBBeanList);

        PromotionDBBean insertedPromotionDBBean = promotionDao.getPromotion(insertedPromotionId, tenantId);

        //Create outputPromoDetails and return
        return  new Promotion("PromoDetail",
                insertedPromotionDBBean.getName(),
                insertedPromotionDBBean.getPromoCode(),
                insertedPromotionDBBean.getId(),
                promoDetails.getStartDate(),
                promoDetails.getEndDate(),
                promoDetails.getCountry(),
                insertedPromotionDBBean.getCreditExpiryPeriod(),
                (insertedPromotionDBBean.getPromoEnabled()==1),
                promoDetails.getPromoCredit(),
                insertedPromotionDBBean.getCurrentRedeemCount(),
                insertedPromotionDBBean.getMaxRedeemCount());

    }

    public Promotion updatePromotion(int promoId, Promotion promoDetails, int tenantId) throws ServiceException, DatabaseException, ParseException {

        //Create PromotionDBBean using Promotion bean
        PromotionDBBean existingPromotionDBBean = promotionDao.getPromotion(promoId, tenantId);

        //If promotion is not found
        if(existingPromotionDBBean==null){
            logger.error("Promotion entry missing for promoId: "+promoId);
            throw new ServiceException(ErrorCode.PROMOTION_NOT_FOUND);
        }

        //validate promo code
        if(StringUtils.isNotEmpty(promoDetails.getPromoCode()) && !StringUtils.equals(promoDetails.getPromoCode(), existingPromotionDBBean.getPromoCode())){
            validatePromoCode(promoDetails, tenantId);
        }

        //Convert String to promoName
        if(StringUtils.isNotEmpty(promoDetails.getName())){
            existingPromotionDBBean.setName(promoDetails.getName());
        }


        //Convert String to promoCode
        if(StringUtils.isNotEmpty(promoDetails.getPromoCode())){
            existingPromotionDBBean.setPromoCode(promoDetails.getPromoCode());
        }

        //Convert String to date
        if(StringUtils.isNotEmpty(promoDetails.getStartDate())){
            Date startDate = CommonUtils.convertStringToDate(promoDetails.getStartDate());
            existingPromotionDBBean.setStartDate(startDate);
        }

        if(StringUtils.isNotEmpty(promoDetails.getEndDate())){
            Date endDate = CommonUtils.convertStringToDate(promoDetails.getEndDate());
            existingPromotionDBBean.setEndDate(endDate);
        }

        //Set parameters to existingPromotionDBBean as per promoDetails
        if(promoDetails.getCreditExpiryPeriod() != null && promoDetails.getCreditExpiryPeriod() != 0){
            existingPromotionDBBean.setCreditExpiryPeriod(promoDetails.getCreditExpiryPeriod());
        }

        if(promoDetails.getMaxRedeemCount() != null && promoDetails.getMaxRedeemCount() != 0){

            if(existingPromotionDBBean.getCurrentRedeemCount() > promoDetails.getMaxRedeemCount())
            {
                logger.error("Promotion requires valid max redeem count");
                throw new ServiceException(ErrorCode.PROMOTION_CODE_REQUIRES_VALID_MAX_REDEEM_COUNT);
            }
            else
            {
                existingPromotionDBBean.setMaxRedeemCount(promoDetails.getMaxRedeemCount());
            }
        }
        

        if(promoDetails.getPromoEnabled() != null ) {
            existingPromotionDBBean.setPromoEnabled((promoDetails.getPromoEnabled()) ? (byte) 1 : (byte) 0);
        }

        //Create a List of PromoCountries using countries
        List<PromoCountryDBBean> newPromoCountryDBBeanList =null;

        if(CollectionUtils.isNotEmpty(promoDetails.getCountry())){

            //For each country in countries create and add PromoCountryDBBean
            List<String> countries = promoDetails.getCountry();
            newPromoCountryDBBeanList = new ArrayList<PromoCountryDBBean>();
            for(String country : countries){

                //Create PromoCountryDBBean from Promotion
                PromoCountryDBBean newPromoCountryDBBean = new PromoCountryDBBean();
                newPromoCountryDBBean.setCountry(country);

                //Add PromoCountryDBBean
                newPromoCountryDBBeanList.add(newPromoCountryDBBean);
            }
        }

        List<PromoCreditDBBean> newPromoCreditDBBeanList = null;

        if(CollectionUtils.isNotEmpty(promoDetails.getPromoCredit())){
            List<PromoCredit> newPromoCreditList = promoDetails.getPromoCredit();
            
            newPromoCreditDBBeanList = getPromoCreditDBBeanList(newPromoCreditList);
        }

        //If the promotion is enabled
        if(promoDetails.getPromoEnabled()!= null ){
            byte activePromotion = 0;
            if(promoDetails.getPromoEnabled()){

                //Check if this promotion should be set as active
                activePromotion = (GSSchemeUtil.utilMethodDoesTimelineMatchesCurrentTimeline(
                        existingPromotionDBBean.getStartDate(),
                        existingPromotionDBBean.getEndDate()))
                        ? (byte)1: (byte)0;
            }

            //Set activePromotion
            existingPromotionDBBean.setActivePromo(activePromotion);
        }


        //Update using dao
        promotionDao.updatePromotion(existingPromotionDBBean, newPromoCountryDBBeanList, newPromoCreditDBBeanList);

        //Get promotion
        Map<String ,Object> promotionDetailsMap = promotionDao.getPromotionDetails(promoId, tenantId);
        
        return getPromotionFromDetailsMap(promotionDetailsMap);
    }

    public void deletePromotion(int promoId, int tenantId) throws ServiceException, DatabaseException, ParseException {

        PromotionDBBean existingPromotionDBBean = promotionDao.getPromotion(promoId, tenantId);

        //If promotion is not found
        if(existingPromotionDBBean==null){
            logger.error("Promotion entry missing for promoId: "+promoId);
            throw new ServiceException(ErrorCode.PROMOTION_NOT_FOUND);
        }

        promotionDao.deletePromotion(existingPromotionDBBean);


    }

    public Promotion getPromotion(int promoId, int tenantId) throws ServiceException, DatabaseException, ParseException {

        Map<String ,Object> promotionDetailsMap = promotionDao.getPromotionDetails(promoId, tenantId);

        if(promotionDetailsMap == null){
            logger.error("Promotion entry missing for promoId: "+promoId);
            throw new ServiceException(ErrorCode.PROMOTION_NOT_FOUND);
        }

        return getPromotionFromDetailsMap(promotionDetailsMap);
    }

    public Promotion getPromotionByPromoCode(String promoCode, int tenantId) throws ServiceException, DatabaseException, ParseException {

        Map<String ,Object> promotionDetailsMap = promotionDao.getPromotionByPromoCode(promoCode, tenantId);

        if(promotionDetailsMap == null){
            logger.error("Promotion entry missing for promoCode: "+ promoCode);
            throw new ServiceException(ErrorCode.PROMOTION_NOT_FOUND);
        }

        return getPromotionFromDetailsMap(promotionDetailsMap);
    }

    public PromoHistoryList getPromoHistoryByPromoId(Integer promoId,String sortBy, PageInfo inputPageInfo)
            throws ServiceException, DatabaseException, ParseException {

        //Get List of all Customers
        List<PromoUsageResultSet> promoHistoryDBBeanList  = promoHistoryDao.getPromoUsageList(promoId, sortBy, inputPageInfo);

        PromoHistoryList promoHistoryList = new PromoHistoryList("PromoUsageList");
        //Check for null
        if(CollectionUtils.isEmpty(promoHistoryDBBeanList)){
            logger.error("No promo usage found for promoCode: "+ promoId);

            //check if exists in promotion entries
            PromotionDBBean promotionBean = promotionDao.getPromotionByPromoId(promoId);
            if(promotionBean != null) {
                //Get totalCount, in case only totalCount is required
                int totalCount = promoHistoryDao.getCountOfPromoUsage(promoId, sortBy);
                promoHistoryList.setPromotionId(promoId);
                promoHistoryList.setStartIndex(inputPageInfo.getStart());
                promoHistoryList.setCount(totalCount);
                promoHistoryList.setList(Collections.<PromoHistory>emptyList());
                return promoHistoryList;
            }
            else
                logger.error("No promotion present for promoId : "+promoId);
                throw new ServiceException(ErrorCode.PROMOTION_NOT_FOUND);
        }

        boolean flag = true;
        List<PromoHistory> outputPromoHistoryList = new ArrayList<PromoHistory>();

        for(PromoUsageResultSet promoHistDBBean : promoHistoryDBBeanList){
                PromoHistoryDBBean promotionDBBeans = promoHistDBBean.getPromoHistoryDBBean();
                Integer redeemCount = promoHistDBBean.getRedeemCount();
                String firstName = promoHistDBBean.getFirstName();
                String lastName = promoHistDBBean.getLastName();
                String emailId = promoHistDBBean.getEmailId();
                
                if (flag) {
                    //set promoId and redeemCount once
                    promoHistoryList.setPromotionId(promotionDBBeans.getPromotionId());
                    promoHistoryList.setCurrentRedeemCount(redeemCount);
                    flag = false;
                }
                //Create Promotion object using promoHistoryDBBean
                PromoHistory outputPromoHistory = new PromoHistory("PromoUsage");
                outputPromoHistory.setCustomerId(promotionDBBeans.getGsCustomerId());
                outputPromoHistory.setIccId(promotionDBBeans.getIccid());
                outputPromoHistory.setCreditAmount(promotionDBBeans.getCreditAmount());
                outputPromoHistory.setCurrency(promotionDBBeans.getCurrency());
                outputPromoHistory.setCreditId(promotionDBBeans.getCreditId());
                outputPromoHistory.setFirstName(firstName);
                outputPromoHistory.setLastName(lastName);
                outputPromoHistory.setEmailId(emailId);
                outputPromoHistory.setCreditTransactionId(promotionDBBeans.getCreditTxnId());
                outputPromoHistory.setRedemptionTime(CommonUtils.convertTimestampToString(promotionDBBeans.getCreateTime()));
                //Add promotion to outputPromotion
            outputPromoHistoryList.add(outputPromoHistory);

            }

        int totalCount =
                (CollectionUtils.isEmpty(promoHistoryDBBeanList)) ?
                        0 : promoHistoryDao.getCountOfPromoUsage(promoId,sortBy);

        promoHistoryList.setStartIndex(inputPageInfo.getStart());
        promoHistoryList.setCount(promoHistoryDBBeanList.size());
        promoHistoryList.setTotalCount(totalCount);
        promoHistoryList.setList(outputPromoHistoryList);
        return promoHistoryList;
    }

    public List<Promotion> getAllPromotions(String countryCode, String isEnabled, String search, PageInfo inputPageInfo, PageInfo outputPageInfo, String date, int tenantId) throws ServiceException, ParseException{

        //Get all promotions for this country
        List<PromotionDBBean> promotionDBBeanList = promotionDao.getPromotions(countryCode, isEnabled, search, inputPageInfo, tenantId);

        //Check for null
        if(CollectionUtils.isEmpty(promotionDBBeanList)){

            //Get totalCount, in case only totalCount is required
            //Configure outputPageInfo
            int totalCount = promotionDao.getCountOfPromotions(countryCode, search, isEnabled, date, tenantId);

            if(totalCount == 0)
                logger.error("Promotions not found for country "+ countryCode);
            outputPageInfo.setCount(totalCount);
            outputPageInfo.setStart(inputPageInfo.getStart());

            return new ArrayList<Promotion>();
        }

//         updating list of promotions, based on date.
        if(date != null) {
            promotionDBBeanList = promotionDao.getAllPromotionsByDate(promotionDBBeanList, date);
        }

        List<Promotion> outputPromotion = new ArrayList<Promotion>();

        for(PromotionDBBean promotionDBBean : promotionDBBeanList){

            //Get promoCountries for this promotion
            List<PromoCountryDBBean> promoCountryDBBeans = promoCountryDao.getPromoCountries(promotionDBBean.getId());

            //Get promoCredits for this promotion
            List<PromoCreditDBBean> promoCreditDBBeans = promoCreditDao.getPromoCreditList(promotionDBBean.getId());

            //Create Promotion
            List<String> countries = getCountries(promoCountryDBBeans);
            List<PromoCredit> promoCreditList = getPromoCredits(promoCreditDBBeans);


            String startDate = promotionDBBean.getStartDate().toString();
            String endDate = promotionDBBean.getEndDate().toString();


            //Create Promotion object using promotionDBBean
            Promotion outputPromoHistory = new Promotion("PromoDetail",
                    promotionDBBean.getName(),
                    promotionDBBean.getPromoCode(),
                    promotionDBBean.getId(),
                    startDate,
                    endDate,
                    countries,
                    promotionDBBean.getCreditExpiryPeriod(),
                    (promotionDBBean.getPromoEnabled()==1),
                    promoCreditList,
                    promotionDBBean.getCurrentRedeemCount(),
                    promotionDBBean.getMaxRedeemCount()
            );

            //Add promotion to outputPromotion
            outputPromotion.add(outputPromoHistory);
        }

        //Configure outputPageInfo
        int totalCount =
                (CollectionUtils.isEmpty(promotionDBBeanList)) ?
                        0 : promotionDao.getCountOfPromotions(countryCode, search, isEnabled, date, tenantId);

        outputPageInfo.setCount(totalCount);
        outputPageInfo.setStart(inputPageInfo.getStart());

        return outputPromotion;
    }


    //validates the code based on whether promotion/referral enabled for that tenant & checks if there exists valid promotion/referral scheme or not
    //validateOnlyPromoCode - flag to validate only promo code
    public boolean isValidCode(String code, int tenantId, boolean validateOnlyPromoCode) throws ServiceException, DatabaseException, ParseException {

        //Check if the promotion enabled for tenantId
        //add isActive check - make it as getActivePromotions
        //Check if the referral enabled for tenantId

        if(gsTaskUtil.checkPromotionEnable(tenantId) && gsTaskUtil.isValidPromoCode(code, tenantId)){
            return true;
        }

        if(!validateOnlyPromoCode && isValidReferralCode(code, tenantId)){
            return true;
        }

        return false;
    }

    public boolean isValidReferralCode(String code, int tenantId)
    {
        if(gsTaskUtil.checkReferralEnable(tenantId) && gsTaskUtil.isValidReferralCode(code, tenantId))
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public boolean isValidCode(String codeType, String code, int tenantId) throws ParseException, DatabaseException, ServiceException {

        if(StringUtils.isEmpty(codeType))
        {
            if(!isValidCode(code, tenantId, false))
            {
                throw new ServiceException(ErrorCode.INVALID_CODE);
            }
            else
            {
                return true;
            }
        }
        else if("REFERRAL".equalsIgnoreCase(codeType))
        {
            if(!isValidReferralCode(code, tenantId))
            {
               throw new ServiceException(ErrorCode.INVALID_REFERRAL_CODE);
            }
            else
            {
                return true;
            }
        }
        else if("PROMOTION".equalsIgnoreCase(codeType))
        {
            if(!isValidCode(code, tenantId, true))
            {
                throw new ServiceException(ErrorCode.INVALID_PROMO_CODE);
            }
            else
            {
                return true;
            }
        }
        else
        {
            return false;
        }
    }


    public CodeInfo applyPromoCode(CodeInfo codeInfo, String token, int tenantId, int gsCustomerId, String simId)
            throws Exception {

        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, tenantId);

        // If customer details not found then fetch from gsBackend
        if(customerDBBean == null){
            accountService.getAccountDetails(gsCustomerId, token, tenantId);

            customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, tenantId);
        }

        // If preferred currency is empty then take currency from customer preference db
        if (StringUtils.isEmpty(codeInfo.getCurrency())) {

            //Get account details
            accountService.getAccountDetails(gsCustomerId, token, tenantId);

            //Get updated customer details from db
            customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, tenantId);

            //Get preferred currency
            PreferenceDBBean currencyPreference = preferenceDao
                    .getPreferenceByKey(customerDBBean.getId(), Configurations.PreferrenceKeyType.PREFERRED_CURRENCY);

            //Set currency
            codeInfo.setCurrency(currencyPreference.getValue());
        }

//        synchronized (this)
        {

            String countryOfRes = customerDBBean.getCountry();
            List<PromotionDBBean> promotionDBBeans = promotionDao.getEnabledPromotionForCountryWithPromoCode(codeInfo.getCode(),countryOfRes, tenantId);

            PromotionDBBean promotionDBBean = null;

            if(CollectionUtils.isEmpty(promotionDBBeans)){
                promotionDBBean = promotionDao.getDefaultPromotionByPromoCode(codeInfo.getCode(), tenantId);
            }else{

                for(PromotionDBBean promoDBBean: promotionDBBeans){
                    if(StringUtils.equalsIgnoreCase(promoDBBean.getPromoCode(), codeInfo.getCode())){
                        promotionDBBean = promoDBBean;
                    }
                }
            }

            if(promotionDBBean == null){
                logger.error("Promo code requires valid code");
                throw new ServiceException(ErrorCode.INVALID_PROMO_CODE);
            }

            if(!comparePromotionValidity(promotionDBBean.getStartDate(), promotionDBBean.getEndDate())){
                logger.error("Promo code requires valid code");
                throw new ServiceException(ErrorCode.INVALID_PROMO_CODE);
            }

            List<PromoHistoryDBBean> promoHistoryDBBeans = promoHistoryDao.getPromotionHistoryList(promotionDBBean.getId(), simId);

            if(promoHistoryDBBeans != null && promoHistoryDBBeans.size() != 0){

                PromoHistoryDBBean promoHistoryDBBean = promoHistoryDBBeans.get(0);
                if(!(Configurations.PromoEventsTaskStatus.API_FAILED.equals(promoHistoryDBBean.getStatus())))
                {
                    logger.error("Promo code is already applied");
                    throw new ServiceException(ErrorCode.PROMO_CODE_ALREADY_APPLIED);
                }
            }

            //Create session and transaction
            Session session = promotionDao.getSession();
            Transaction transaction = session.beginTransaction();
            logger.info("Transaction started for promotion entry with promoId: "
                    + promotionDBBean.getId() + ", customerId :" + customerDBBean.getId());

            //Get promotion with write lock
            PromotionDBBean lockedPromotionDBBean = promotionDao.getPromotion(session, promotionDBBean.getId(), tenantId);

            //Check currentRedeemCount
            if(lockedPromotionDBBean.getCurrentRedeemCount() >= lockedPromotionDBBean.getMaxRedeemCount())
            {
                promotionDao.closeSession(session);

                logger.error("Promo code redemption reached max redeem count");
                throw new ServiceException(ErrorCode.PROMOTION_REACHED_MAX_REDEEM_COUNT);
            }

            PromoCreditDBBean promoCreditDBBean = promoCreditDao.getPromoCredit(session, lockedPromotionDBBean.getId(), codeInfo.getCurrency());

            //Check if minimumCartAmount is less than cartAmount
            Float minimumCartAmount = promoCreditDBBean.getMinimumCartAmount();
            if(codeInfo.getCartAmount()!=null
                    && codeInfo.getCartAmount() < minimumCartAmount)
            {
                promotionDao.closeSession(session);

                logger.error("Promo code redemption cartAmount is less than minimum cart amount");

                String locale = GSUtil.getLocaleLanguage();
                ErrorStringDBBean errorStringByErrorId = errorStringDao.getErrorStringByErrorId(ErrorCode.CART_AMOUNT_LESS_THAN_MINIMUM_CART_AMOUNT.statusCode, locale);

                String formattedMessage = errorStringByErrorId.getErrorString();
                String currency = promoCreditDBBean.getCurrency();
                if("JPY".equals(currency))
                {
                    currency = "¥";
                }
                else if("USD".equals(currency))
                {
                    currency = "US$ ";
                }
                else if("EUR".equals(currency))
                {
                    currency = "€";
                }
                else if("GBP".equals(currency))
                {
                    currency = "£";
                }

                String message = String.format(formattedMessage, currency, promoCreditDBBean.getMinimumCartAmount());
                if(message.contains("¥"))
                {
                    message = message.replace(".00","");
                }
                throw new ServiceException(ErrorCode.CART_AMOUNT_LESS_THAN_MINIMUM_CART_AMOUNT,message);
            }

            if(codeInfo.getValidationMode()!= null && codeInfo.getValidationMode()==true)
            {
                promotionDao.closeSession(session);
                return new CodeInfo("ApplyCode", promoCreditDBBean.getCreditAmount(), codeInfo.getCurrency(), null);
            }

            try {

                //Create credit bean
                Credit credit = GSSchemeUtil.prepareCreditDetails(codeInfo.getCurrency(), promoCreditDBBean, lockedPromotionDBBean.getCreditExpiryPeriod());

                //Add credit API call
                Credit creditResponse = creditManagementInterface.addCredit(gsCustomerId, credit, tenantId);

                PromoHistoryDBBean promoHistoryDBBean =
                        GSSchemeUtil.getPromoHistoryBean(
                                gsCustomerId, codeInfo.getCurrency(),
                                lockedPromotionDBBean.getId(), 0,
                                simId, promoCreditDBBean.getCreditAmount(),
                                0, Configurations.EmailStatus.NOT_SENT,
                                0, Configurations.PromoEventsTaskStatus.COMPLETE
                        );


                String status = null;
                if (creditResponse != null &&
                        (Configurations.CreditAddResponseStatus.SUCCESS.equals(creditResponse.getType())))
                {

                    logger.info("Apply Promo Code - add credit SUCCESS for GS CustomerId " + gsCustomerId);

                    //Set credit transaction
                    promoHistoryDBBean.setCreditTxnId(creditResponse.getCreditTxnId());
                    //Set credit Id
                    promoHistoryDBBean.setCreditId(creditResponse.getCreditId());
                    //Set status
                    promoHistoryDBBean.setStatus(Configurations.ApplyPromoCodeStatus.COMPLETE);
                    status = Configurations.ApplyPromoCodeStatus.COMPLETE;

                    //Update current redeem count
                    lockedPromotionDBBean.setCurrentRedeemCount(lockedPromotionDBBean.getCurrentRedeemCount()+1);
                    promotionDao.updatePromotion(session, lockedPromotionDBBean);
                }
                else
                {
                    //Mark promoHistory entry with different status. So RetryAddCredit won't pick this up
                    //Credit add failed
                    logger.info("Apply Promo Code - add credit FAILED for GS CustomerId " + gsCustomerId );
                    promoHistoryDBBean.setStatus(Configurations.PromoEventsTaskStatus.API_FAILED);
                    promoHistoryDao.addPromotionHistory(session,promoHistoryDBBean);

                    transaction.commit();
                    transaction = null;
                    logger.info("Transaction complete for promotion entry with promoId: " + promotionDBBean.getId());

                    throw new ServiceException(ErrorCode.ADD_PROMO_CODE_FAILED);
                }


                promoHistoryDao.addPromotionHistory(session,promoHistoryDBBean);

                transaction.commit();
                transaction = null;
                logger.info("Transaction complete for promotion entry with promoId: " + promotionDBBean.getId());
                return  new CodeInfo("ApplyCode", promoCreditDBBean.getCreditAmount(), codeInfo.getCurrency(), status);
            }
            catch (Exception exception)
            {

                logger.info("Add promotion failed for gsCustomerId : "+gsCustomerId, exception);
                if(transaction!=null)
                {
                    transaction.rollback();
                }
                throw exception;
            }
            finally {
                //Close session
                promotionDao.closeSession(session);
            }
        }
    }

    public CodeInfo checkCodeStatus(String code, String token, int gsCustomerId, int tenantId) throws ServiceException, BackendRequestException, ParseException, DatabaseException{

        Account account = accountService.getAccountDetails(gsCustomerId, token, tenantId);

        int customerId = customerDao.getCustomerIdForBackendCustomerId(gsCustomerId, tenantId);

        PreferenceDBBean currencyPreference = preferenceDao
                .getPreferenceByKey(customerId, Configurations.PreferrenceKeyType.PREFERRED_CURRENCY);

        String status = account.getReferralStatus();
        String currency = currencyPreference.getValue();
        Float amount = null;

        if(isValidCode(code, tenantId, true)){ //validate promo code

            PromotionDBBean promotionDBBean = promotionDao.getPromotion(code, tenantId);

            //TODO : getPromotionHistoryByPromoId can return multiple entries
            List<PromoHistoryDBBean> promotionHistoryBeans = promoHistoryDao.getPromotionHistoryByPromoId((long) gsCustomerId, promotionDBBean.getId());

            //check if the code is already applied then fetch currency and amount based on that
            if(promotionHistoryBeans != null && promotionHistoryBeans.size()!=0){

                PromoHistoryDBBean promoHistoryDBBean = promotionHistoryBeans.get(0);

                if(StringUtils.equals(promoHistoryDBBean.getStatus(), Configurations.ApplyPromoCodeStatus.CREDIT_ADD_FAILED)){
                    status = promoHistoryDBBean.getStatus();
                }
                currency = promoHistoryDBBean.getCurrency();
                amount = promoHistoryDBBean.getCreditAmount();
            }
            else{ //fetch amount based on the active promotion by promo code

                PromoCreditDBBean promoCreditDBBean = promotionDao.getPromoCreditFromActivePromotion(code, tenantId, currency);
                if(promoCreditDBBean == null){
                    logger.error("Invalid code - no promo credit for promo code: "+ code + " currency: "+ currency);
                    throw new ServiceException(ErrorCode.INVALID_CODE);
                }

                List<PromoEventDBBean> promoEvents = promoEventDao.getPromoEvent(customerId, tenantId, Configurations.ReferralEventType.SIGNUP_EVENT);
                if(!CollectionUtils.isEmpty(promoEvents))
                {
                    PromoEventDBBean promoEventDBBean = promoEvents.get(0);
                    if((Configurations.StatusType.STATUS_NEW.equals(promoEventDBBean.getStatus()) ||
                            Configurations.StatusType.STATUS_IN_PROCESS.equals(promoEventDBBean.getStatus())))
                    {
                        status = Configurations.ReferralStatus.CREDIT_ADD_IN_PROCESS;
                    }
                }

                amount = promoCreditDBBean.getCreditAmount();
            }

        }else if(isValidCode(code, tenantId, false)){ //if the above condition fails then we validate referral code

            //fetch amount from the Active scheme
            CreditSchemeDBBean creditSchemeDBBean = incentiveSchemeDao.getCreditSchemeFromActiveScheme(code.toLowerCase(), currency, tenantId);
            if(creditSchemeDBBean == null){
                logger.error("Invalid code - no referral credit for referral code: "+ code + " currency: "+ currency);
                throw new ServiceException(ErrorCode.INVALID_CODE);
            }

            ReferralEventDBBean signUpReferralEvent = referralEventDao.getSignUpReferralEvent(customerId, tenantId);

            if(signUpReferralEvent!=null && (Configurations.StatusType.STATUS_NEW.equals(signUpReferralEvent.getStatus()) ||
                    Configurations.StatusType.STATUS_IN_PROCESS.equals(signUpReferralEvent.getStatus())))
            {
                status = Configurations.ReferralStatus.CREDIT_ADD_IN_PROCESS;
            }

            amount = creditSchemeDBBean.getNewCustomerAmount();

        }else{
            logger.error("Invalid code : "+ code);
            throw new ServiceException(ErrorCode.INVALID_CODE);
        }

        return new CodeInfo("CodeStatus", amount, currency, status);
    }

    private void validatePromoCode(Promotion promotion, int tenantId) throws ServiceException, ParseException {
        //check if promo code is in referral db
        if((promotionDao.getPromotion(promotion.getPromoCode(), tenantId) != null) || (referralCodeDao.getReferralCodeInfoByReferralCode(promotion.getPromoCode(), tenantId) != null) ){
            logger.error("Promo code already exists");
            throw new ServiceException(ErrorCode.PROMO_CODE_ALREADY_EXISTS);
        }
    }

    private List<PromoCreditDBBean> getPromoCreditDBBeanList(List<PromoCredit> promoCredits){
        List<PromoCreditDBBean> newPromoCreditDBBeanList = new ArrayList<PromoCreditDBBean>();

        //For each PromoCredit in newPromoCreditList create PromoCreditDBBean and add
        for(PromoCredit newPromoCredit : promoCredits){

            PromoCreditDBBean promoCreditDBBean = new PromoCreditDBBean();
            promoCreditDBBean.setCurrency(newPromoCredit.getCurrency());
            promoCreditDBBean.setCreditAmount(newPromoCredit.getCreditAmount());
            promoCreditDBBean.setMinimumCartAmount(newPromoCredit.getMinimumCartAmount());

            //Add to list
            newPromoCreditDBBeanList.add(promoCreditDBBean);
        }

        return newPromoCreditDBBeanList;
    }

    private Promotion getPromotionFromDetailsMap(Map<String, Object> promotionDetailsMap){

        //Get the contents of the Hashmap
        PromotionDBBean activePromotionDBBean = (PromotionDBBean) promotionDetailsMap.get(Configurations.PROMOTION_BEAN);

        List<PromoCountryDBBean> promoCountryDBBeans = (List<PromoCountryDBBean>) promotionDetailsMap.get(Configurations.PROMO_COUNTRY_BEAN_LIST);

        List<PromoCreditDBBean> promoCreditDBBeans = (List<PromoCreditDBBean>) promotionDetailsMap.get(Configurations.PROMO_CREDIT_BEAN);

        //Get list of countries from promoCountryDBBeans
        List<String> countries = getCountries(promoCountryDBBeans);

        //Get list of PromoCredit from promoCreditDBBeans
        List<PromoCredit> promoCreditList = getPromoCredits(promoCreditDBBeans);

        //Convert date to string
        String startDate = activePromotionDBBean.getStartDate().toString();

        String endDate = activePromotionDBBean.getEndDate().toString();

        return new Promotion("PromoDetail",
                activePromotionDBBean.getName(),
                activePromotionDBBean.getPromoCode(),
                activePromotionDBBean.getId(),
                startDate,
                endDate,
                countries,
                activePromotionDBBean.getCreditExpiryPeriod(),
                (activePromotionDBBean.getPromoEnabled()==1),
                promoCreditList,
                activePromotionDBBean.getCurrentRedeemCount(),
                activePromotionDBBean.getMaxRedeemCount()
                );
    }

    private List<String> getCountries(List<PromoCountryDBBean> promoCountryDBBeanList){

        //Create countries list
        List<String> countries = new ArrayList<String>();

        //Add countries to List
        for (PromoCountryDBBean promoCountryDBBean : promoCountryDBBeanList){

            String country = promoCountryDBBean.getCountry();

            countries.add(country);

        }

        return countries;
    }

    private List<PromoCredit> getPromoCredits(List<PromoCreditDBBean> promoCreditDBBeanList){

        //Create PromoCredit list
        List<PromoCredit> PromoCreditList = new ArrayList<PromoCredit>();

        //Add PromoCredit
        for(PromoCreditDBBean promoCreditDBBean : promoCreditDBBeanList){
            //Create PromoCredit
            PromoCredit promoCredit = new PromoCredit(promoCreditDBBean);
            //Add PromoCredit to List
            PromoCreditList.add(promoCredit);
        }
        return PromoCreditList;
    }

    private boolean comparePromotionValidity(Date startDate, Date endDate){

        //Get currentDate
        Date currentDate = CommonUtils.getCurrentDate();

        //If current date is less than the start date
        if(startDate.compareTo(currentDate) > 0){

            return false;
        }

        //If end date is less than the current date
        if(currentDate.compareTo(endDate) > 0){

            return false;
        }

        return true;
    }

    //validate whether the currencies present in promo credit are only the supported currencies of that tenant
    public boolean isValidPromoCreditDetails(List<PromoCredit> promoCredits, int tenantId){
        if(CollectionUtils.isEmpty(promoCredits)){
            return false;
        }

        List<String> supportedCurrencies = supportedCurrenciesDao.getSupportedCurrencies(tenantId);

        if(supportedCurrencies.size() != promoCredits.size()){
            return false;
        }

        return true;

    }

}
