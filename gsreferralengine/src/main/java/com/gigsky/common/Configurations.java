package com.gigsky.common;

import com.gigsky.database.dao.ConfigurationKeyValueDao;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by prsubbareddy on 06/01/17.
 */
public class Configurations {

    @Autowired
    ConfigurationKeyValueDao configurationKeyValueDao;

    public enum ErrorMsgType {
        SYSTEM_ERROR("SYSTEM_ERROR :");

        String string = null;
        ErrorMsgType(String str) {
            string = str;
        }

        @Override
        public String toString() {
            return string;
        }
    }

    //Gigsky supported error languages
    private final static String[] supportedLocale_GigSky =  new String[] {"en","it","ja","de","fr","es","zh","zh-HK","zh-TW"};
    //KDDI supported error languages
    private final static String[] supportedLocale_KDDI = new String[] {"en","ja"};
    //Constants
    public static final long DEFAULT_REQUEST_TIMEOUT_MS = 5000;
    public static final int DEFAULT_MAX_CREDIT_RETRY_COUNT = 1;
    public static final int MAX_SIGNUP_WAIT_COUNT = 3;
    public static int MILLISECONDS_IN_A_DAY = 86400000;
    public static int MILLISECONDS_FOR_TEN_MINUTES = 10*60*1000;

    //ReferralScheme related
    public static int MAX_RESEND_COUNT = 3;
    public static final int GIGSKY_TENANT_ID = 1;
    public static final int KDDI_TENANT_ID = 2;

    public static String DEFAULT_COUNTRY_CODE = "ALL";
    public static String DEFAULT_CURRENCY_CODE = "USD";
    public static String DEFAULT_LANGUAGE_CODE = "en";

    public static final int REFERRAL_CODE_MIN_LENGTH = 4;
    public static final int REFERRAL_CODE_MAX_LENGTH = 9;

    //Active referralScheme map keys
    public static String INCENTIVE_SCHEME_BEAN = "IncentiveSchemeDBBean";
    public static String VALIDCOUNTRY_BEAN_LIST = "ValidCountryDBBeans";
    public static String CREDIT_SCHEME_BEAN = "CreditSchemeDBBean";

    //Active promotion map keys
    public static String PROMOTION_BEAN = "PromotionDBBean";
    public static String PROMO_COUNTRY_BEAN_LIST = "PromoCountryDBBeans";
    public static String PROMO_CREDIT_BEAN = "PromoCreditDBBean";

    public static final String CREATED_ON_MAX_DURATION = "CREATED_ON_MAX_DURATION";
    public static final long DEFAULT_RESET_TIME_REFERRED_COUNT = 31536000000l;

    public static String PERMISSION_GIGSKY_REFERRAL_SCHEME_EDIT = "GIGSKY_REFERRAL_SCHEME_EDIT";
    public static String PERMISSION_GIGSKY_REFERRAL_SCHEME_READ = "GIGSKY_REFERRAL_SCHEME_READ";
    /* Encryption version for GigSky Encryptor */
    public static final String LATEST_ENCRYPTION_VERSION = "1.0.1";

    public static class ConfigurationKeyValueType {

        // Referral engine and backend APIs configurations
        public static final String ENGINE_MODE = "ENGINE_MODE";
        public static final String BACKEND_BASE_URL = "BACKEND_BASE_URL";
        public static final String NGW_BASE_URL = "NGW_BASE_URL";
        public static final String REFERRAL_BASE_URL = "REFERRAL_BASE_URL";
        public static final String REQUEST_TIMEOUT_MS = "REQUEST_TIMEOUT_MS";
        public static final String ALERT_CREDIT_AMOUNT = "ALERT_CREDIT_AMOUNT";

        // Add Credit configurations
        public static final String REFERRAL_CREDIT_ADD = "REFERRAL_CREDIT_ADD";
        public static final String ADVOCATE_CREDIT_ADD = "ADVOCATE_CREDIT_ADD";

        // executors related configurations
        public static final String REFERRAL_PROGRAM_EXEC_TIME_MS = "REFERRAL_PROGRAM_EXEC_TIME_MS";
        public static final String EVENT_EXEC_TIME_MS = "EVENT_EXEC_TIME_MS";
        public static final String RETRY_CREDIT_EXEC_TIME_MS = "RETRY_CREDIT_EXEC_TIME_MS";
        public static final String SHUTDOWN_WAIT_TIME_MS = "SHUTDOWN_WAIT_TIME_MS";
        public static final String SIGNUP_EVENT_DELAY_TIME_MS = "SIGNUP_EVENT_DELAY_TIME_MS";

        // schedulers related configurations
        public static final String REFERRAL_SEND_EMAIL_COUNT = "REFERRAL_SEND_EMAIL_COUNT";
        public static final String REFERRAL_INITIAL_DELAY = "REFERRAL_INITIAL_DELAY";
        public static final String REFERRAL_RESEND_DELAY = "REFERRAL_RESEND_DELAY";
        public static final String RETRY_CREDIT_COUNT = "RETRY_CREDIT_COUNT";
        public static final String EVENTS_PROCESSING_COUNT = "EVENTS_PROCESSING_COUNT";
        public static final String REFERRAL_PROCESSING_THREADS = "REFERRAL_PROCESSING_THREADS";

        // rabbit MQ configurations
        public static final String MQ_QUEUE_NAME = "MQ_QUEUE_NAME";
        public static final String MQ_HOST_NAME = "MQ_HOST_NAME";
        public static final String MQ_USER_NAME = "MQ_USER_NAME";
        public static final String MQ_PASSWORD = "MQ_PASSWORD";
        public static final String MQ_EXCHANGE_NAME = "MQ_EXCHANGE_NAME";
        public static final String MQ_ROUTING_KEY = "MQ_ROUTING_KEY";

        //Cron expression
        public static final String SCHEME_CRON_EXPRESSION = "SCHEME_CRON_EXPRESSION";
        public static final String REFERRAL_QUEUE_MONITOR_CRON_EXPRESSION = "REFERRAL_QUEUE_MONITOR_CRON_EXPRESSION";
        public static final String REFERRAL_QUEUE_MAX_DELAY_IN_MS = "REFERRAL_QUEUE_MAX_DELAY_IN_MS";

        //Beta mode true or false value
        public static final String BETA_MODE_ENABLE = "BETA_MODE_ENABLE";

        //API request fail mode, used for testing failure cases
        public static final String API_FAILURE_MODE = "API_FAILURE_MODE";

        public static final String PROMO_EVENTS_COUNT = "PROMO_EVENTS_COUNT";
        public static final String MAX_DELETE_ACCOUNT = "MAX_DELETE_ACCOUNT";


        /*
            Configurations will helpful to restrict referral and promo referral codes
         */
        public static final String MAX_REFERRED_COUNT = "MAX_REFERRED_COUNT";
        public static final String RESET_TIME_REFERRED_COUNT = "RESET_TIME_REFERRED_COUNT";
        public static final String MAX_PROMOTED_COUNT = "MAX_PROMOTED_COUNT";
        public static final String MAX_CREDIT_RETRY_COUNT = "MAX_CREDIT_RETRY_COUNT";

        public static final String DEFAULT_TENANT_ID = "DEFAULT_TENANT_ID";
    }

    public static class TenantConfigurationKeyValueType{

        // admin related Configurations
        public static final String ADMIN_EMAIL = "ADMIN_EMAIL";
        public static final String ADMIN_PASSWORD = "ADMIN_PASSWORD";
        public static final String ADMIN_TOKEN = "ADMIN_TOKEN";

        // admin email to send emails
        public static final String GS_ADMIN_EMAIL_ID = "GS_ADMIN_EMAIL_ID";

        //Referral Enable with Value = "true"/"false"
        public static final String REFERRAL_ENABLE = "REFERRAL_ENABLE";

        //Promotions Enable with Value = "true"/"false"
        public static final String PROMOTION_ENABLE = "PROMOTION_ENABLE";

        //GLOBAL-Promotions Enable with Value = "true"/"false"
        public static final String REFERRAL_EMAIL_ENABLE = "REFERRAL_EMAIL_ENABLE";

        //Tenant Default Language
        public static final String TENANT_DEF_LANG = "TENANT_DEF_LANG";
    }

    public static class ReferralCodeChannelType {
        public static final String DEFAULT = "DEFAULT";
        public static final String EMAIL = "EMAIL";
        public static final String FACEBOOK = "FACEBOOK";
        public static final String TWITTER = "TWITTER";
    }

    public static class PreferrenceKeyType{
        public static final String OPT_OUT = "OPT_OUT";
        public static final String PREFERRED_CURRENCY = "PREFERRED_CURRENCY";
    }


    public static class ReferralEventType{
        public static final String SIGNUP_EVENT = "SignupEvent";
        public static final String PURCHASE_EVENT = "PlanPurchaseEvent";
        public static final String PLAN_EXPIRED_EVENT = "SubscriptionExpiredEvent";
        public static final String SIM_ACTIVATION_EVENT = "SIMActivationEvent";
        public static final String CREDIT_NEARING_EXPIRY_EVENT = "CreditNearExpiryEvent";
        public static final String CREDIT_EXPIRED_EVENT = "CreditExpiryEvent";
        public static final String CURRENCY_CHANGE_EVENT = "CurrencyChangeEvent";
        public static final String EMAIL_ID_CHANGE_EVENT = "EmailIdChangeEvent";
        public static final String ACCOUNT_INFORMATION_CHANGE_EVENT = "AccountInformationChange";
        public static final String HEALTH_CHECK_EVENT = "HealthCheckEvent";
        public static final String ACCOUNT_DELETE_EVENT = "DeleteAccountEvent";
    }

    public static class PromoEventType
    {
        public static final String SIGNUP_EVENT = "SignupEvent";
        public static final String SIM_ACTIVATION_EVENT = "SIMActivationEvent";
    }

    public static class EmailStatus
    {
        public static final String SENT = "SENT";
        public static final String NOT_SENT = "NOT_SENT";
    }


    public static class StatusType {
        public static final String STATUS_NEW = "NEW";
        public static final String STATUS_IN_PROCESS = "IN_PROCESS";
        public static final String STATUS_COMPLETE = "COMPLETE";
        public static final String STATUS_FAILED = "FAILED";
        public static final String STATUS_FAILED_AFTER_RETRIES = "FAILED_AFTER_RETRIES";
        public static final String CREDIT_ADD_FAILED = "CREDIT_ADD_FAILED";
        public static final String CREDIT_ADD_SUCCESS = "CREDIT_ADD_SUCCESS";
        public static final String CREDIT_DEDUCT_FAILED = "CREDIT_DEDUCT_FAILED";
        public static final String CREDIT_DEDUCT_SUCCESS = "CREDIT_DEDUCT_SUCCESS";
        public static final String CREDIT_ADD_FAILED_AFTER_RETRIES = "CREDIT_ADD_FAILED_AFTER_RETRIES";
        public static final String CREDIT_DEDUCT_FAILED_AFTER_RETRIES = "CREDIT_DEDUCT_FAILED_AFTER_RETRIES";
        public static final String DUPLICATE_ENTRY = "DUPLICATE_ENTRY";
        public static final String CREDIT_DEDUCTED = "CREDIT_DEDUCTED";
        public static final String NOT_PROCESSED = "NOT_PROCESSED";
        public static final String SIGNUP_EVENT_NOTFOUND = "SIGNUP_EVENT_NOTFOUND";
        public static final String CREDIT_NOT_ADDED_FRAUD_DETECTED = "CREDIT_NOT_ADDED_FRAUD_DETECTED";
        public static final String FAILED_API = "FAILED_API";
    }

    public static class PromoEventsTaskStatus
    {
        public static final String FAILED = "FAILED";
        public static final String EXPIRED_PROMOTION = "EXPIRED_PROMOTION";
        public static final String CREDIT_ADD_FAILED = "CREDIT_ADD_FAILED";
        public static final String CREDIT_ADD_SUCCESS = "CREDIT_ADD_SUCCESS";
        public static final String COMPLETE = "COMPLETE";
        public static final String NOT_PROCESSED = "NOT_PROCESSED";
        public static final String API_FAILED = "API_FAILED";
        public static final String REACHED_MAX_REDEEM_COUNT= "REACHED_MAX_REDEEM_COUNT";

    }

    public static class ApplyPromoCodeStatus
    {

        public static final String CREDIT_ADD_FAILED = "CREDIT_ADD_FAILED";
        public static final String COMPLETE = "COMPLETE";

    }


    public static class ReferralStatus {
        public static final String SIGNUP_WITH_REFERRAL = "SIGNUP_WITH_REFERRAL";
        public static final String SIGNUP_WITHOUT_REFERRAL = "SIGNUP_WITHOUT_REFERRAL";
        public static final String SIGNUP_WITH_INVALID_REFERRAL = "SIGNUP_WITH_INVALID_REFERRAL";
        public static final String CREDIT_ADD_IN_PROCESS = "CREDIT_ADD_IN_PROCESS";
        public static final String SIGNUP_WITH_PROMOTION= "SIGNUP_WITH_PROMOTION";
    }

    //SIM type
    public static class SIMType{
        public static final String GS_SIM_50 = "GS_SIM_50";
        public static final String ACME_SIM = "ACME_SIM";
        public static final String GIGSKY_SIM = "GIGSKY_SIM";
        public static final String ACME_GSMA_SIM = "ACME_GSMA_SIM";
        public static final String MSFT_GSMA_ESIM = "MSFT_GSMA_ESIM";
        public static final String ACME_GSMA_ESIM_V2 = "ACME_GSMA_ESIM_V2";
        public static final String ACME_GSMA_ESIM_V2_KDDI = "ACME_GSMA_ESIM_V2_KDDI";
        public static final String ANDROID_GSMA_ESIM = "ANDROID_GSMA_ESIM";
        //TODO : Add android eSIM also
    }

    public static class CustomerStatus {
        public static final String STATUS_SIGNED_UP = "SIGNED_UP";
        public static final String STATUS_USING_SERVICE = "USING_SERVICE";
        public static final String STATUS_SIM_ACTIVATED = "SIM_ACTIVATED";
    }

    public static class CreditType {
        public static final String PROMOTION = "PROMOTION_CREDIT";
        public static final String REF_ADV = "REFERRAL_ADVOCATE";
        public static final String REF_SIGNUP = "REFERRAL_SIGNUP";
        public static final String GENERAL = "GENERAL";

    }

    public static class CreditDesc{
        // use for iccid promotion credit description
        public static final String PROMO_CREDIT_DESC = "Promotion credit";
    }



    public static class EmailType{
        public static final String REFERRAL_INVITE = "REFERRAL_INITIATION";
        public static final String NEW_CUSTOMER_SIGNUP = "NEW_CUSTOMER_SIGNUP";
        public static final String CREDIT_ADDED = "ADVOCATE_CREDIT_ADDED";
        public static final String CREDIT_LIMIT = "CREDIT_LIMIT";
        public static final String INTERNAL_CREDIT_ALERT = "INTERNAL_CREDIT_ALERT";
        public static final String INTERNAL_CREDIT_DEDUCT = "INTERNAL_CREDIT_DEDUCT";
        public static final String INTERNAL_CREDIT_WARNING = "INTERNAL_CREDIT_WARNING";
        public static final String ICCID_PROMOTION_CREDIT = "ICCID_PROMOTION_CREDIT";
        public static final String RABBIT_MQ_SERVER_UP = "RABBIT_MQ_SERVER_UP";
        public static final String RABBIT_MQ_SERVER_DOWN = "RABBIT_MQ_SERVER_DOWN";
        public static final String DELETED_ACCOUNTS_THRESHOLD = "DELETED_ACCOUNTS_THRESHOLD";

        // remove this email
        public static final String CREDIT_EXPIRED = "CREDIT_EXPIRED";
    }

    public static class MessageBeansObjectType{

        public static final String SIGNUP_EVENT = "UserSignup";
        public static final String PURCHASE_EVENT = "PlanPurchase";
        public static final String PLAN_EXPIRED_EVENT = "SubscriptionExpiry";
        public static final String SIM_ACTIVATION_EVENT = "SimActivation";
        public static final String CREDIT_NEARING_EXPIRY_EVENT = "CreditExpiry";
        public static final String CREDIT_EXPIRED_EVENT = "CreditExpiry";
        public static final String CURRENCY_CHANGE_EVENT = "CurrencyChange";
        public static final String EMAIL_ID_CHANGE_EVENT = "EmailIDChange";
        public static final String ACCOUNT_INFORMATION_CHANGE_EVENT = "AccountInformationChange";
        public static final String ACCOUNT_DELETE_EVENT = "DeleteAccountInfo";

    }

    //Get list of language supported based on the tenantsId
    public static List<String> getSupportedLocale(int tenantId){
        if(GIGSKY_TENANT_ID == tenantId){
            return Arrays.asList(supportedLocale_GigSky);
        }else if(KDDI_TENANT_ID == tenantId){
            return Arrays.asList(supportedLocale_KDDI);
        }
        return Collections.EMPTY_LIST;
    }

    //Credit add response status
    public static class CreditAddResponseStatus{

        public static final String SUCCESS = "AddGigskyCreditResponse";
        public static final String FAILED = "error";
        //FAILED_MAX is used to indicate that credit add failed because of credit balance reached max (errorInt : 7017)
        public static final String MAX_CREDIT_LIMIT_REACHED = "MAX_CREDIT_LIMIT_REACHED";
    }

    public String getBackendBaseUrl() {
        return configurationKeyValueDao.getConfigurationValue(ConfigurationKeyValueType.BACKEND_BASE_URL);
    }

    public String getNGWBaseUrl() {
        return configurationKeyValueDao.getConfigurationValue(ConfigurationKeyValueType.NGW_BASE_URL);
    }

    public long getRequestTimeOut(){

        //Use default value
        long timeOutInMs = DEFAULT_REQUEST_TIMEOUT_MS;

        //Get timeout from db
        String timeOutInMsStr = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.REQUEST_TIMEOUT_MS);

        if(StringUtils.isNotEmpty(timeOutInMsStr)){

            //Convert string to long
            timeOutInMs = new GigskyDuration(timeOutInMsStr).convertTimeInMilliSeconds();
        }

        return timeOutInMs;
    }

}
