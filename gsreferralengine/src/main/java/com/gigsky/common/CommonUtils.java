package com.gigsky.common;

import com.gigsky.rest.bean.PageInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.hibernate.Query;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by prsubbareddy on 06/01/17.
 */
public class CommonUtils {

    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String UTC_TIME_ZONE_ID = "UTC";
    public static final TimeZone UTC_TIME_ZONE = TimeZone.getTimeZone(UTC_TIME_ZONE_ID);

    private static final Logger logger = LoggerFactory.getLogger(CommonUtils.class);

    public static Timestamp getCurrentTimestampInUTC() {

        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));

        String currentTimeUTC = dateFormatGmt.format(new java.util.Date());
        Timestamp currentTimestamp = Timestamp.valueOf(currentTimeUTC);
        return currentTimestamp;
    }

    public static Timestamp getCurrentTimestamp() {

        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentTime = dateFormatGmt.format(new java.util.Date());
        Timestamp currentTimestamp = Timestamp.valueOf(currentTime);
        return currentTimestamp;
    }

    public static Timestamp convertLocalTimeToUTcTime(Timestamp localTimeStamp) throws ParseException {

        String localTime = convertTimestampToString(localTimeStamp);

        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = dateFormatGmt.parse(localTime);

        String currentTimeUTC = dateFormatGmt.format(date);
        Timestamp timestamp = Timestamp.valueOf(currentTimeUTC);
        return timestamp;
    }

    public static Timestamp convertMilliSecondsToTimeStampInUTC(long milliSeconds) {

        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
        String time = dateFormatGmt.format(new Date(milliSeconds));

        Timestamp timestamp = Timestamp.valueOf(time);
        return timestamp;
    }

    public static Timestamp convertMilliSecondsToTimeStamp(long milliSeconds) {

        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = dateFormatGmt.format(new Date(milliSeconds));

        Timestamp timestamp = Timestamp.valueOf(time);
        return timestamp;
    }

    public static String convertTimestampToString(Timestamp timestamp) {

        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormatGmt.format(timestamp);
    }

    public static boolean isValidDateFormat(String value) {
        java.util.Date date = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (ParseException ex) {
            logger.error("isValidDateFormat exception",ex);
            ex.printStackTrace();
        }
        return date != null;
    }

    //Date utils

    public static java.sql.Date getCurrentDate() {

        //TODO : Check while comparing
//        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        // Today
        Calendar date = Calendar.getInstance();
        // Reset hour, minutes, seconds and millis
        date.set(Calendar.HOUR_OF_DAY, 0);
        date.set(Calendar.MINUTE, 0);
        date.set(Calendar.SECOND, 0);
        date.set(Calendar.MILLISECOND, 0);

        java.sql.Date currentDate = new java.sql.Date(date.getTimeInMillis());
//         getUTCFormattedDateString(date, DATE_FORMAT);
        return currentDate;
    }

    public static String getDateInString(java.sql.Date date){
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd");

        String currentTimeUTC = dateFormatGmt.format(date);
        return currentTimeUTC;
    }

    public static java.sql.Date convertStringToDate(String stringDate) throws ParseException {

        //TODO : Check while comparing
//        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));

        java.util.Date date = dateFormatGmt.parse(stringDate);
        //Convert util date to sql date
        java.sql.Date sqlDate = new java.sql.Date(date.getTime());

        //Set time of the date to 00:00:00,
        //As dates are used to compare each other
        sqlDate = setTimeOfDateToMidNight(sqlDate);

        return sqlDate;
    }

    private static java.sql.Date setTimeOfDateToMidNight(java.sql.Date date){

        //Use calender lib for setting the time of the date
        Calendar now = Calendar.getInstance();
        now.setTime(date);
        now.set(Calendar.HOUR, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.HOUR_OF_DAY, 0);

        //Get date out of calendar instance
        Date utilDate =  now.getTime();

        //Convert java.util.Date to java.sql.Date
        java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

        return sqlDate;
    }

    public static String getUTCFormattedDateString(Calendar calendarToBeConverted, String inputDateFormat) {
        if (calendarToBeConverted == null) {
            return null;
        }
        else {
            return getUTCFormattedDateString(calendarToBeConverted.getTime(), inputDateFormat);
        }
    }

    public static String getUTCFormattedDateStringFromTimestamp(Timestamp timestampToBeConverted, String inputDateFormat) {
        if (timestampToBeConverted == null) {
            return null;
        }
        else {
            return getUTCFormattedDateString(timestampToBeConverted, inputDateFormat);
        }
    }

    public static List getQueryResponse(PageInfo inputPageInfo,Query query){
        List queryOutput;
        if(inputPageInfo!= null ){

            //If the startIndex and count is intentionally set to zero
            if(inputPageInfo.getCount() == 0 && inputPageInfo.getStart() == 0) return null;

            queryOutput = query.
                    setMaxResults(inputPageInfo.getCount()).
                    setFirstResult(inputPageInfo.getStart())
                    .list();
        }
        //Paging disabled
        else {
            queryOutput = query.list();
        }

        return queryOutput;
    }


    public static String getUTCFormattedDateString(Date dateToBeConverted, String inputDateFormat) {
        if (dateToBeConverted == null) {
            return null;
        }

        SimpleDateFormat dateFormat = null;
        if (StringUtils.isNotEmpty(inputDateFormat)) {
            dateFormat = new SimpleDateFormat(inputDateFormat);
        }
        else {
            dateFormat = new SimpleDateFormat(DATE_FORMAT);
        }

        dateFormat.setTimeZone(UTC_TIME_ZONE);
        return dateFormat.format(dateToBeConverted);
    }

    //To convert from UTC string to date instance
    public static Date convertFromUTCToTargetTimeZone(String inputUTCDateString, String targetTimeZone) {
        if (StringUtils.isEmpty(inputUTCDateString)) {
            return null;
        }
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            dateFormat.setLenient(false);
            dateFormat.setTimeZone(UTC_TIME_ZONE);
            Date convertedUTCDate = dateFormat.parse(inputUTCDateString);
            if (StringUtils.isNotEmpty(targetTimeZone)) {
                dateFormat.setTimeZone(TimeZone.getTimeZone(targetTimeZone));
                return dateFormat.parse(dateFormat.format(convertedUTCDate));
            }
            return dateFormat.parse(dateFormat.format(convertedUTCDate));
        }
        catch (ParseException pe) {
            throw new IllegalArgumentException(inputUTCDateString + " is invalid date");
        }
    }
}
