package com.gigsky.database.exception;

import com.gigsky.exception.ReferralException;
import com.gigsky.rest.exception.ErrorCode;

/**
 * Created by pradeepragav on 16/01/17.
 */
public class DatabaseException extends ReferralException {

    public DatabaseException(ErrorCode code)
    {
        super(code);
    }

    public DatabaseException(String message)
    {
        super(message);
    }

    public DatabaseException(String message, Exception e)
    {
        super(message, e);
    }

    public DatabaseException(Exception e)
    {
        super(e);
    }
}
