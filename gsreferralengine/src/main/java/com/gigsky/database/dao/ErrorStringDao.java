package com.gigsky.database.dao;

import com.gigsky.database.bean.ErrorStringDBBean;
import com.gigsky.database.bean.ErrorStringLocaleDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ErrorStringDao extends AbstractDao {

	private static final Logger logger = LoggerFactory.getLogger(ErrorStringDao.class);


	public ErrorStringDBBean getErrorStringByErrorId(int errorId) {

		Session session = openSession();

		try {

			return (ErrorStringDBBean) session.createCriteria(ErrorStringDBBean.class)
					.add(Restrictions.eq("errorId", errorId))
					.uniqueResult();
		} finally {
			closeSession(session);
		}
	}

	public ErrorStringDBBean getErrorStringByErrorId(int errorId, String locale) {

		Session session = openSession();
		try {

			// Fetch error string bean by passing error id
			ErrorStringDBBean errorStringDBBean = getErrorStringByErrorId(errorId);

			// If language other than english, Fetch related localised string for error string
			if (!locale.equals("en")) {
				ErrorStringLocaleDBBean errorStringLocaleDBBean = (ErrorStringLocaleDBBean) session.createCriteria(ErrorStringLocaleDBBean.class)
						.add(Restrictions.eq("errorStringId", errorStringDBBean.getId()))
						.add(Restrictions.eq("language", locale))
						.uniqueResult();

				if (errorStringLocaleDBBean != null) {
					errorStringDBBean.setErrorString(errorStringLocaleDBBean.getErrorString());
				}
			}

			return errorStringDBBean;

		} finally {
			closeSession(session);
		}
	}
}