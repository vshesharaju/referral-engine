package com.gigsky.database.dao;

import com.gigsky.database.bean.CustomerCsnMappingDBBean;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by pradeepragav on 05/01/17.
 */
public class CustomerCSNMappingDao extends AbstractDao {
    private static final Logger logger = LoggerFactory.getLogger(CustomerCSNMappingDao.class);

    //Add  customerCSNMapping
    public int addCustomerCSNMapping(CustomerCsnMappingDBBean customerCsnMappingDBBean){
        Session session = openSession();
        try {
            session.save(customerCsnMappingDBBean);
            return customerCsnMappingDBBean.getId();
        }
        finally {
            closeSession(session);
        }
    }

    //Delete customerCSNMapping
    public void deleteCustomerCSNMapping(CustomerCsnMappingDBBean customerCsnMappingDBBean){
        Session session = openSession();
        try {
            session.delete(customerCsnMappingDBBean);
        }
        finally {
            closeSession(session);
        }

    }

    public void deleteCustomerCSNMappingByGsCustomerId(int gsCustomerId)
    {
        Session session = openSession();
        try
        {
            String queryStr = "delete CustomerCsnMappingDBBean where gsCustomerId = :customerId";

            Query query = session.createQuery(queryStr).setParameter("customerId", gsCustomerId);
            query.executeUpdate();

        }
        finally {
            closeSession(session);
        }
    }

    //Update customerCSNMapping
    public void updateCustomerCSNMapping(CustomerCsnMappingDBBean customerCsnMappingDBBean){
        Session session = openSession();
        try {
            session.saveOrUpdate(customerCsnMappingDBBean);
        }
        finally {
            closeSession(session);
        }
    }

    //Get customerCsnMapping by Id
    public CustomerCsnMappingDBBean getCustomerCsnMappingbyId(int customerCsnMappingId){
        Session session = openSession();
        try {
            CustomerCsnMappingDBBean customerCsnMappingDBBean = (CustomerCsnMappingDBBean) session.createCriteria(CustomerCsnMappingDBBean.class)
                    .add(Restrictions.eq("id",customerCsnMappingId))
                    .uniqueResult();
            if(customerCsnMappingDBBean !=null){
                return customerCsnMappingDBBean;
            }
            logger.warn("CustomerCsnMapping entry missing for customerCsnMappingId:"+ customerCsnMappingId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    //Get customerCsnMappingDBBean by gsCustomerId
    public CustomerCsnMappingDBBean getCustomerCsnMappingByCsn(String CSN, int tenantId){

        Session session = openSession();

        try {

            CustomerCsnMappingDBBean customerCsnMappingDBBean =  (CustomerCsnMappingDBBean)session.createCriteria(CustomerCsnMappingDBBean.class)
                    .add(Restrictions.eq("csn",CSN))
                    .add(Restrictions.eq("tenantId",tenantId))
                    .uniqueResult();

            if(customerCsnMappingDBBean != null){
                return customerCsnMappingDBBean;
            }

            logger.warn("CustomerCsnMappings entry not found for csn : "+ CSN);
            return null;
        }
        finally {

            closeSession(session);
        }
    }

}
