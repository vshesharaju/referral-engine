package com.gigsky.database.dao;

import com.gigsky.database.bean.SupportedCurrencyDBBean;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pradeepragav on 28/03/17.
 */
public class SupportedCurrenciesDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(SupportedCurrenciesDao.class);

    //Get
    public SupportedCurrencyDBBean getSupportedCurrencyDetails(String currencyCode, int tenantId){

        Session session = openSession();

        try {

            SupportedCurrencyDBBean supportedCurrencyDBBean =
                    (SupportedCurrencyDBBean) session.createCriteria(SupportedCurrencyDBBean.class)
                    .add(Restrictions.eq("currencyCode",currencyCode))
                    .add(Restrictions.eq("tenantId",tenantId))
                    .uniqueResult();

            if(supportedCurrencyDBBean != null)
                return supportedCurrencyDBBean;

            logger.warn("Supported currency entry not found for currency :" + currencyCode);

            return new SupportedCurrencyDBBean();
        }
        finally {

            closeSession(session);
        }
    }

    //Get supported Currencies
    public List<String> getSupportedCurrencies(int tenantId){

        Session session = openSession();

        try {

            List supportedCurrenciesDbBeans = session.createCriteria(SupportedCurrencyDBBean.class)
                    .add(Restrictions.eq("tenantId",tenantId))
                    .list();

            if(CollectionUtils.isNotEmpty(supportedCurrenciesDbBeans)){

                //New list of strings
                List<String> supportedCurrencies = new ArrayList<String>();

                //Type cast
                List<SupportedCurrencyDBBean> supportedCurrencyDBBeans = supportedCurrenciesDbBeans;

                //Add currencies into list of strings
                for(SupportedCurrencyDBBean supportedCurrencyDBBean : supportedCurrencyDBBeans){

                    supportedCurrencies.add(supportedCurrencyDBBean.getCurrencyCode());
                }

                return supportedCurrencies;
            }
        }
        finally {
            closeSession(session);
        }
        return null;
    }

    //Update
    public void updateSupportedCurrencyDetails(SupportedCurrencyDBBean supportedCurrencyDBBean){

        Session session = openSession();

        try {

            session.saveOrUpdate(supportedCurrencyDBBean);
        }
        finally {
            closeSession(session);
        }
    }

    //Add
//    public void addSupportedCurrenciesDetails(SupportedCurrencyDBBean supportedCurrencyDBBean){
//
//        Session session = openSession();
//
//        try {
//            session.save(supportedCurrencyDBBean);
//        }
//        finally {
//            closeSession(session);
//        }
//
//    }
//
//    //Delete
//    public void deleteSupportedCurrencies(SupportedCurrencyDBBean supportedCurrencyDBBean){
//
//        Session session = openSession();
//
//        try {
//            session.delete(supportedCurrencyDBBean);
//        }
//        finally {
//            closeSession(session);
//        }
//
//    }
}
