package com.gigsky.database.dao;

import com.gigsky.database.bean.CountryDBBean;
import com.gigsky.database.bean.ValidCountryDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by pradeepragav on 05/01/17.
 */
public class ValidCountryDao extends AbstractDao {
    private static final Logger logger = LoggerFactory.getLogger(ValidCountryDao.class);

    //Add validCountry
    public int addValidCountry(ValidCountryDBBean validCountryDBBean){
        Session session = openSession();
        try {
            session.save(validCountryDBBean);
            return validCountryDBBean.getId();
        }
        finally {
            closeSession(session);
        }
    }

    //Delete validCountry
    public void deleteValidCountry(ValidCountryDBBean validCountryDBBean){
        Session session = openSession();
        try {
            session.delete(validCountryDBBean);
        }
        finally {
            closeSession(session);
        }
    }

    //Update validCountry
    public void updateValidCountry(ValidCountryDBBean validCountryDBBean){
        Session session = openSession();
        try {
            session.saveOrUpdate(validCountryDBBean);
        }
        finally {
            closeSession(session);
        }
    }

    //Get validContryById
    public ValidCountryDBBean getValidCountry(int validCountryId){
        Session session = openSession();

        try
        {

            ValidCountryDBBean validCountryDBBean = (ValidCountryDBBean) session.createCriteria(ValidCountryDBBean.class)
                    .add(Restrictions.eq("id", validCountryId))
                    .uniqueResult();

            if (validCountryDBBean != null)
            {
                return validCountryDBBean;
            }

            logger.warn("ValidCountry entry is missing for " + validCountryId);

            return null;
        }

        finally {
            closeSession(session);
        }
    }

    //Get validCountriesForIncentiveScheme
    public List<ValidCountryDBBean> getValidCountriesForIncentiveSchemeId(int incentiveSchemeId){
        Session session = openSession();
        try {
            List validCountryDBBeans = session.createCriteria(ValidCountryDBBean.class)
                    .add(Restrictions.eq("incentiveSchemeId",incentiveSchemeId))
                    .addOrder(Order.asc("id"))
                    .list();

            if(validCountryDBBeans!=null){
                return validCountryDBBeans;
            }
            logger.warn("ValidCountry entry missing for incentiveScheme :"+ incentiveSchemeId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }



    public CountryDBBean getCountryByCountryCode(String countryCode)
    {
        Session session = openSession();

        try
        {
            CountryDBBean countryDBBean = (CountryDBBean) session.createCriteria(CountryDBBean.class)
                    .add(Restrictions.eq("countryCode", countryCode))
                    .uniqueResult();

            if (countryDBBean != null)
            {
                return countryDBBean;
            }

            logger.warn("Country entry is missing for " + countryCode);

            return null;
        }

        finally {
            closeSession(session);
        }
    }
}
