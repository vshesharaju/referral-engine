package com.gigsky.database.dao;

import com.gigsky.database.bean.PromoIccidDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by prsubbareddy on 30/11/17.
 */
public class PromoIccidDao extends AbstractDao{

    private static final Logger logger = LoggerFactory.getLogger(PromoIccidDao.class);

    // Add Promo ICCID
//    public int addPromoIccid(PromoIccidDBBean promoIccidBean) {
//        Session session = openSession();
//
//        try {
//            session.save(promoIccidBean);
//            return promoIccidBean.getId();
//        } finally {
//            closeSession(session);
//        }
//    }
//
//
//    // Update Promo ICCID
//    public void updatePromoIccid(PromoIccidDBBean promoIccidBean) {
//        Session session = openSession();
//
//        try {
//            session.saveOrUpdate(promoIccidBean);
//        } finally {
//            closeSession(session);
//        }
//    }
//
//
//    // Delete Promo ICCID
//    public void deletePromoIccid(PromoIccidDBBean promoIccidBean) {
//        Session session = openSession();
//
//        try {
//            session.delete(promoIccidBean);
//        } finally {
//            closeSession(session);
//        }
//    }


    // Get Promo ICCID by id
    public PromoIccidDBBean getPromoIccid(String iccid) {
        Session session = openSession();
        try {
            PromoIccidDBBean promoIccidBean = (PromoIccidDBBean) session.createCriteria(PromoIccidDBBean.class)
                    .add(Restrictions.and(Restrictions.ge("iccidEnd", iccid), Restrictions.le("iccidStart", iccid)))
                    .uniqueResult();
            if (promoIccidBean != null) {
                return promoIccidBean;
            }
            logger.warn("PromoIccid entry missing for iccid: " + iccid);
            return null;
        } finally {
            closeSession(session);
        }
    }


    // Get Promo ICCID by id
    public PromoIccidDBBean getPromoIccid(Session session, String iccid) {

            PromoIccidDBBean promoIccidBean = (PromoIccidDBBean) session.createCriteria(PromoIccidDBBean.class)
                    .add(Restrictions.and(Restrictions.ge("iccidEnd", iccid), Restrictions.le("iccidStart", iccid)))
                    .uniqueResult();
            if (promoIccidBean != null) {
                return promoIccidBean;
            }
            logger.warn("PromoIccid entry missing for iccid: " + iccid);
            return null;

    }

}
