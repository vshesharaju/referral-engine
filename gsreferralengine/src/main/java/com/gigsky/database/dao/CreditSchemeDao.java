package com.gigsky.database.dao;

import com.gigsky.database.bean.CreditSchemeDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by prsubbareddy on 22/12/16.
 */
public class CreditSchemeDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(CreditSchemeDao.class);

    // Add credit scheme info
    public int addCreditScheme(CreditSchemeDBBean creditSchemeDBBean)
    {
        Session session = openSession();

        try
        {
            session.save(creditSchemeDBBean);
            return creditSchemeDBBean.getId();
        }
        finally
        {
            closeSession(session);
        }
    }

    // Update credit scheme details for the creditSchemeId
    public void updateCreditScheme(CreditSchemeDBBean creditSchemeDBBean)
    {
        Session session = openSession();

        try
        {
            session.saveOrUpdate(creditSchemeDBBean);
        }

        finally {
            closeSession(session);
        }
    }

    // Fetch credit scheme details for the input creditSchemeId
    public CreditSchemeDBBean getCreditScheme(int creditSchemeId)
    {
        Session session = openSession();

        try
        {

            CreditSchemeDBBean creditScheme = (CreditSchemeDBBean) session.createCriteria(CreditSchemeDBBean.class)
                    .add(Restrictions.eq("id", creditSchemeId))
                    .uniqueResult();

            if (creditScheme != null)
            {
                return creditScheme;
            }

            logger.warn("CreditScheme entry is missing for " + creditSchemeId);

            return null;
        }

        finally {
            closeSession(session);
        }
    }

    // Fetch credit scheme details for the input incentiveSchemeId and currency
    public CreditSchemeDBBean getCreditScheme(int incentiveSchemeId, String currency)
    {
        Session session = openSession();

        try
        {

            CreditSchemeDBBean creditScheme = (CreditSchemeDBBean) session.createCriteria(CreditSchemeDBBean.class)
                    .add(Restrictions.eq("incentiveSchemeId", incentiveSchemeId))
                    .add(Restrictions.eq("currency", currency))
                    .uniqueResult();

            if (creditScheme != null)
            {
                return creditScheme;
            }

            logger.warn("CreditScheme entry is missing for " + creditScheme);

            return null;
        }

        finally {
            closeSession(session);
        }
    }

    // Get incentiveScheme by incentiveScheme Id
    public List<CreditSchemeDBBean> getCreditSchemesForIncentiveScheme(int incentiveSchemeId){
        Session session = openSession();

        try {
            List creditSchemeDBBeans = session.createCriteria(CreditSchemeDBBean.class)
                    .add(Restrictions.eq("incentiveSchemeId",incentiveSchemeId))
                    .addOrder(Order.asc("id"))
                    .list();

            if(creditSchemeDBBeans!=null){
                return creditSchemeDBBeans;
            }
            logger.warn("CreditSchemes entry missing for incentiveSchemeId: "+incentiveSchemeId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    // Delete credit scheme details for the creditSchemeId
    public void deleteCreditScheme(CreditSchemeDBBean creditSchemeDBBean)
    {
        Session session = openSession();

        try
        {
            session.delete(creditSchemeDBBean);
        }

        finally {
            closeSession(session);
        }
    }

}
