package com.gigsky.database.dao;

import com.gigsky.database.bean.TenantConfigurationsKeyValueDBBean;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by shwethars on 25/01/19.
 */
public class TenantConfigurationsKeyValueDao extends AbstractDao  {

    private static final Logger logger = LoggerFactory.getLogger(TenantConfigurationsKeyValueDao.class);

    public String getTenantConfigurationValue(String configKey, int tenantId) {
        Session session = openSession();
        try {
            String configValue = (String) session.createCriteria(TenantConfigurationsKeyValueDBBean.class)
                    .setProjection(Projections.projectionList().add(Projections.property("value")))
                    .add(Restrictions.eq("ckey", configKey))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .uniqueResult();

            if (StringUtils.isNotEmpty(configValue)) {
                return configValue;
            }
            logger.warn("Tenant Configuration entry is missing for " + configKey+ " for tenantId "+tenantId);
            return StringUtils.EMPTY;
        }
        finally {
            closeSession(session);
        }
    }

    //Get configurationValue by configKey and defaultvalue
    public String getTenantConfigurationValue(String configKey, String defaultValue, int tenantId) {
        String configurationValue = getTenantConfigurationValue(configKey, tenantId);
        if (StringUtils.isEmpty(configurationValue)) {
            configurationValue = defaultValue;
        }
        return configurationValue;
    }

    public String setTenantConfigurationValue(TenantConfigurationsKeyValueDBBean tenantConfigurationsKeyValue)
    {
        Session session = openSession();

        try
        {
            session.save(tenantConfigurationsKeyValue);
            return tenantConfigurationsKeyValue.getCkey();
        }

        finally {
            closeSession(session);
        }
    }

    public String updateTenantConfigurationValue(TenantConfigurationsKeyValueDBBean tenantConfigurationsKeyValue)
    {
        Session session = openSession();

        try
        {
            session.saveOrUpdate(tenantConfigurationsKeyValue);
            return tenantConfigurationsKeyValue.getCkey();
        }

        finally {
            closeSession(session);
        }
    }
}
