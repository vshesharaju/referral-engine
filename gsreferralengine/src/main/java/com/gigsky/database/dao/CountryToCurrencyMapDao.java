package com.gigsky.database.dao;

import com.gigsky.database.bean.CountryToCurrencyMapDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by pradeepragav on 19/01/17.
 */
public class CountryToCurrencyMapDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(CountryToCurrencyMapDao.class);

    //Get countryToCurrencyMap
    public String getCurrency(String country){
        String currency;

        Session session = openSession();
        try {

            //Get countryToCurrencyDBBean for this country
            CountryToCurrencyMapDBBean countryToCurrencyMapDBBean =(CountryToCurrencyMapDBBean) session.createCriteria(CountryToCurrencyMapDBBean.class)
                    .add(Restrictions.eq("countryCode",country))
                    .uniqueResult();

            //If there is an entry
            if(countryToCurrencyMapDBBean != null){

                //Return the currency for this country
                currency = countryToCurrencyMapDBBean.getCurrencyCode();
                return currency;
            }

            logger.warn("CountryToCurrency missing for country: "+country);
            return null;
        }
        finally {

            closeSession(session);
        }
    }
}
