package com.gigsky.database.dao;

import com.gigsky.database.bean.PreferenceDBBean;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by pradeepragav on 04/01/17.
 */
public class PreferenceDao extends AbstractDao{
    private static final Logger logger = LoggerFactory.getLogger(PreferenceDao.class);

    //Add preferrence
    public int addPreference(PreferenceDBBean preferenceDBBean){
        Session session = openSession();

        try {
            session.save(preferenceDBBean);
            return preferenceDBBean.getId();
        }
        catch (ConstraintViolationException constraintException)
        {
            logger.warn("Create preferrence Exception :", constraintException);
            session.close();

            PreferenceDBBean preferenceByKey = getPreferenceByKey(preferenceDBBean.getCustomerId(), preferenceDBBean.getPkey());

            if(preferenceByKey!=null)
            {
                preferenceByKey.setValue(preferenceDBBean.getValue());
                updatePreference(preferenceByKey);
                return preferenceByKey.getId();
            }
            else
            {
                return  -1;
            }
        }
        catch (Exception e)
        {
            logger.error("Create preferrence Exception :", e);
            return -1;
        }
        finally {
            closeSession(session);
        }
    }

    //Delete preferrence
//    public void deletePreference(PreferenceDBBean preferenceDBBean){
//        Session session = openSession();
//
//        try{
//            session.delete(preferenceDBBean);
//        }
//        finally {
//            closeSession(session);
//        }
//    }

    /*Deletes all the preferences for a customer*/
    public void deletePreferences(int customerId)
    {
        Session session = openSession();
        try {
            String queryStr = "delete PreferenceDBBean where customerId = :customerId";

            Query query = session.createQuery(queryStr).setParameter("customerId", customerId);
            query.executeUpdate();
        }
        finally {
            closeSession(session);
        }
    }


    //Get preference by id
    public PreferenceDBBean getPreference(int preferenceId){
        Session session = openSession();
        try {
            PreferenceDBBean preferenceDBBean = (PreferenceDBBean) session.createCriteria(PreferenceDBBean.class).
                    add(Restrictions.eq("id",preferenceId))
                    .uniqueResult();
            if(preferenceDBBean!=null){
                return preferenceDBBean;
            }
            logger.warn("Preference entry missing for preferenceId: "+preferenceId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    // Get preference using preferenceKey
    public PreferenceDBBean getPreferenceByKey(int customerId,String pKey){

        Session session =openSession();
        try {
            PreferenceDBBean preferenceDBBean=(PreferenceDBBean) session.createCriteria(PreferenceDBBean.class)
                    .add(Restrictions.eq("customerId",customerId))
                    .add(Restrictions.eq("pkey",pKey))
                    .uniqueResult();
            if(preferenceDBBean!=null){
                return preferenceDBBean;
            }

            logger.warn("Preference entry missing for pKey: "+pKey);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    public PreferenceDBBean getPreferenceByKey(Session session, int customerId,String pKey){

            PreferenceDBBean preferenceDBBean=(PreferenceDBBean) session.createCriteria(PreferenceDBBean.class)
                    .add(Restrictions.eq("customerId",customerId))
                    .add(Restrictions.eq("pkey",pKey))
                    .uniqueResult();
            if(preferenceDBBean!=null){
                return preferenceDBBean;
            }

            logger.warn("Preference entry missing for pKey: "+pKey);
            return null;
    }

    //Get preferrence
    public List<PreferenceDBBean> getPreferencesForCustomer(int customerId){
        Session session = openSession();

        try {
            List<PreferenceDBBean> preferenceDBBeans = (List<PreferenceDBBean>) session.createCriteria(PreferenceDBBean.class)
                    .add(Restrictions.eq("customerId",customerId))
                    .addOrder(Order.asc("id")).list();

            if(preferenceDBBeans != null){
                return preferenceDBBeans;
            }
            logger.warn("Preferrence entry is missing for customerId: "+ customerId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    //Update preferrence
    public void updatePreference(PreferenceDBBean preferenceDBBean){
        Session session = openSession();
        try {
            session.saveOrUpdate(preferenceDBBean);
        }
        finally {
            closeSession(session);
        }
    }
}
