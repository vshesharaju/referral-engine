package com.gigsky.database.dao;

import com.gigsky.database.bean.ReferralAnalyticsDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by pradeepragav on 05/01/17.
 */
public class ReferralAnalyticsDao extends AbstractDao {
    private static final Logger logger = LoggerFactory.getLogger(ReferralAnalyticsDao.class);

    //Add referralAnalytics
    public int addReferralAnalytics(ReferralAnalyticsDBBean referralAnalyticsDBBean){
        Session session = openSession();
        try {
            session.save(referralAnalyticsDBBean);
            return referralAnalyticsDBBean.getId();
        }
        finally {
            closeSession(session);
        }
    }

    //Delete referralanalytics
    public void deleteReferralAnalytics(ReferralAnalyticsDBBean referralAnalyticsDBBean){
        Session session = openSession();
        try {
            session.delete(referralAnalyticsDBBean);
        }
        finally {
            closeSession(session);
        }
    }

    //Update referralAnalytics
    public void updateReferralAnalytics(ReferralAnalyticsDBBean referralAnalyticsDBBean){
        Session session = openSession();
        try {
            session.saveOrUpdate(referralAnalyticsDBBean);
        }
        finally {
            closeSession(session);
        }
    }

    //Get referralAnalytics by id
    public ReferralAnalyticsDBBean getReferralAnalytics(int referralAnalyticsId){
        Session session = openSession();
        try {
            ReferralAnalyticsDBBean referralAnalyticsDBBean = (ReferralAnalyticsDBBean) session.createCriteria(ReferralAnalyticsDBBean.class).
                    add(Restrictions.eq("id",referralAnalyticsId))
                    .uniqueResult();
            if(referralAnalyticsDBBean!=null){
                return referralAnalyticsDBBean;
            }
            logger.warn("ReferralAnalytics entry missing for referralAnalyticsId: "+referralAnalyticsId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }
}
