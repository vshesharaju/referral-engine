package com.gigsky.database.dao;

import com.gigsky.database.bean.MessageServerClusterNodeDBBean;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by pradeepragav on 10/04/17.
 */
public class MessageServerClusterNodeDao extends AbstractDao {

    public List<MessageServerClusterNodeDBBean> getNodesForCluster(int clusterId){

        Session session = openSession();
        try {
            List messageClusterNodes = session.createCriteria(MessageServerClusterNodeDBBean.class)
                    .add(Restrictions.eq("messageServerClusterId", clusterId))
                    .list();

            if(CollectionUtils.isNotEmpty(messageClusterNodes)){
                return messageClusterNodes;
            }
        }
        finally {
            closeSession(session);
        }
        return null;
    }
}
