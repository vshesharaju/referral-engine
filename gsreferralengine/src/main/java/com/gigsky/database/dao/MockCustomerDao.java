package com.gigsky.database.dao;

import com.gigsky.backend.beans.MockSimDBBean;
import com.gigsky.database.bean.MockCustomerDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by vinayr on 16/01/17.
 */
public class MockCustomerDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(MockCustomerDao.class);

    // Add customer info
    public int addCustomer(MockCustomerDBBean mockCustomerDBBean)
    {
        Session session = openSession();

        try
        {
            session.save(mockCustomerDBBean);
            return mockCustomerDBBean.getId();
        }
        finally
        {
            closeSession(session);
        }
    }
//
//    // Update customer info
//    public void updateCustomer(MockCustomerDBBean mockCustomerDBBean)
//    {
//        Session session = openSession();
//
//        try
//        {
//            session.saveOrUpdate(mockCustomerDBBean);
//        }
//
//        finally {
//            closeSession(session);
//        }
//    }

    // Fetch customer info for customer id
    public MockCustomerDBBean getCustomer(int customerId, int tenantId)
    {
        Session session = openSession();

        try
        {

            MockCustomerDBBean mockCustomerDBBean = (MockCustomerDBBean) session.createCriteria(MockCustomerDBBean.class)
                    .add(Restrictions.eq("id", customerId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .uniqueResult();

            if (mockCustomerDBBean != null)
            {
                return mockCustomerDBBean;
            }

            logger.warn("Mock customer entry is missing for " + customerId);

            return null;
        }

        finally {
            closeSession(session);
        }
    }

    // Fetch customer info for token
    public MockCustomerDBBean getCustomer(String token, int tenantId)
    {
        Session session = openSession();

        try
        {
            MockCustomerDBBean mockCustomerDBBean = (MockCustomerDBBean) session.createCriteria(MockCustomerDBBean.class)
                    .add(Restrictions.eq("token", token))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .uniqueResult();

            if (mockCustomerDBBean != null)
            {
                return mockCustomerDBBean;
            }

            logger.warn("Mock customer entry is missing for " + token);

            return null;
        }

        finally {
            closeSession(session);
        }
    }

    // Fetch customer info for token
//    public MockCustomerDBBean getCustomer(String token, int gsCustomerId)
//    {
//        Session session = openSession();
//
//        try
//        {
//            MockCustomerDBBean mockCustomerDBBean = (MockCustomerDBBean) session.createCriteria(MockCustomerDBBean.class)
//                    .add(Restrictions.eq("token", token))
//                    .add(Restrictions.eq("id", gsCustomerId))
//                    .uniqueResult();
//
//            if (mockCustomerDBBean != null)
//            {
//                return mockCustomerDBBean;
//            }
//
//            logger.warn("Mock customer entry is missing for " + token);
//
//            return null;
//        }
//
//        finally {
//            closeSession(session);
//        }
//    }
//
//    // Delete customer info
    public void deleteCustomer(MockCustomerDBBean mockCustomerDBBean)
    {
        Session session = openSession();

        try
        {
            session.delete(mockCustomerDBBean);
        }

        finally {
            closeSession(session);
        }
    }

    public int addMockSim(MockSimDBBean mockSimDBBean)
    {
        Session session = openSession();

        try {
            session.save(mockSimDBBean);
            return mockSimDBBean.getId();
        }
        finally {
            closeSession(session);
        }
    }

    public List<MockSimDBBean> getMockSims(int mockCustomerId)
    {
        Session session = openSession();

        try {

            List mockSimDBBeans = (List) session.createCriteria(MockSimDBBean.class)
                    .add(Restrictions.eq("mockCustomerId", new Integer(mockCustomerId)))
                    .list();

            if(mockSimDBBeans != null)
            {
                return mockSimDBBeans;
            }

            logger.warn("Mock Sims entry is missing for " + mockCustomerId);
            return null;

        }
        finally {
            closeSession(session);
        }
    }


}
