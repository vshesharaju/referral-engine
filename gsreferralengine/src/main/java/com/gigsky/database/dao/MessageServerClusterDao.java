package com.gigsky.database.dao;

import com.gigsky.database.bean.MessageServerClusterDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 * Created by pradeepragav on 10/04/17.
 */
public class MessageServerClusterDao extends AbstractDao{

    public MessageServerClusterDBBean getMessageCluster(int mclusterId){

        Session session = openSession();

        try {

            MessageServerClusterDBBean messageServerClusterDBBean = (MessageServerClusterDBBean)session.createCriteria(MessageServerClusterDBBean.class)
                    .add(Restrictions.eq("id", mclusterId))
                    .uniqueResult();

            if(messageServerClusterDBBean!= null){
                return messageServerClusterDBBean;
            }
        }
        finally {
            closeSession(session);
        }
        return null;
    }
}
