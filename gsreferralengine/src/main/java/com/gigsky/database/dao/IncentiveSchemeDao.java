package com.gigsky.database.dao;

import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.referralCode.ReferralCodeManagement;
import com.gigsky.rest.bean.CreditScheme;
import com.gigsky.rest.bean.PageInfo;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.exception.ServiceException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by prsubbareddy on 22/12/16.
 */
public class IncentiveSchemeDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(IncentiveSchemeDao.class);

    @Autowired
    private CountryToCurrencyMapDao countryToCurrencyMapDao;

    @Autowired
    private ValidCountryDao validCountryDao;

    @Autowired
    private CreditSchemeDao creditSchemeDao;

    @Autowired
    private ReferralCodeManagement referralCodeManagement;

    @Autowired
    private ReferralCodeDao referralCodeDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private PreferenceDao preferenceDao;

    // Add incentive scheme info
    public int addIncentiveScheme(IncentiveSchemeDBBean incentiveSchemeDBBean) {
        Session session = openSession();

        try {
            session.save(incentiveSchemeDBBean);
            return incentiveSchemeDBBean.getId();
        } finally {
            closeSession(session);
        }
    }

    // Update incentive scheme details for the incentive scheme id
    public void updateIncentiveScheme(IncentiveSchemeDBBean incentiveSchemeDBBean) {
        Session session = openSession();

        try {
            session.saveOrUpdate(incentiveSchemeDBBean);
        } finally {
            closeSession(session);
        }
    }

    // Get incentive scheme details for the input incentiveSchemeId
    public IncentiveSchemeDBBean getIncentiveScheme(int incentiveSchemeId, int tenantId) {
        Session session = openSession();

        try {

            IncentiveSchemeDBBean incentiveSchemeDBBean = (IncentiveSchemeDBBean) session.createCriteria(IncentiveSchemeDBBean.class)
                    .add(Restrictions.eq("id", incentiveSchemeId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .uniqueResult();

            if (incentiveSchemeDBBean != null) {
                return incentiveSchemeDBBean;
            }

            logger.warn("IncentiveScheme entry is missing for " + incentiveSchemeId);

            return null;
        } finally {
            closeSession(session);
        }
    }

    //Gets incentiveScheme, validCountries and creditSchemes for this incentiveScheme
    public Map<String, Object> getIncentiveSchemeDetails(int incentiveSchemeId, int tenantId){

        Session session = openSession();

        try {

            //Create a map
            Map<String, Object> activeReferralSchemeMap;

            IncentiveSchemeDBBean incentiveSchemeDBBean = (IncentiveSchemeDBBean) session.createCriteria(IncentiveSchemeDBBean.class)
                    .add(Restrictions.eq("id", incentiveSchemeId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .uniqueResult();

            if(incentiveSchemeDBBean != null){

                activeReferralSchemeMap = fetchValidCountryAndCreditSchemesFromIncentiveBean(incentiveSchemeDBBean);

                if(activeReferralSchemeMap != null){

                    return activeReferralSchemeMap;
                }
            }

            logger.warn("IncentiveScheme entry is missing for incentiveSchemeId" + incentiveSchemeId);

            return null;

        }
        finally {
            closeSession(session);
        }
    }

    // Get active scheme for countryCode
    public IncentiveSchemeDBBean getActiveSchemeForCountry(String country, int tenantId) {

        Session session = openSession();

        try {
            String query = "select I from IncentiveSchemeDBBean I, ValidCountryDBBean V where " +
                    "V.country =:countryCode and " +
                    "I.activeScheme=:isActive and "+
                    "I.tenantId=:tenantId and "+
                    "V.incentiveSchemeId = I.id";

            //Since there can be one or more active schemes for the same country but having different validity, We get list incentiveSchemes
            IncentiveSchemeDBBean incentiveSchemeDBBean = (IncentiveSchemeDBBean)session.createQuery(query)
                    .setParameter("isActive", (byte) 1)
                    .setParameter("countryCode", country)
                    .setParameter("tenantId", tenantId)
                    .uniqueResult();

            if(incentiveSchemeDBBean != null) {

                return incentiveSchemeDBBean;
            }

            logger.warn("IncentiveScheme entry missing for country " + country);
            return null;
        } finally {
            closeSession(session);
        }
    }

    // Get active schemes for countries
    public List<IncentiveSchemeDBBean> getEnabledSchemeForCountries(List<String> countries, int tenantId) {

        Session session = openSession();

        try {

            List incentiveSchemeDBBean = session.createQuery("select I from IncentiveSchemeDBBean I, ValidCountryDBBean V " +
                    " where V.country in (:countryCodes)" +
                    " and I.schemeEnabled =:isEnabled " +
                    " and I.tenantId =:tenantId " +
                    " and V.incentiveSchemeId = I.id ")
                    .setParameterList("countryCodes", countries)
                    .setParameter("isEnabled", (byte) 1)
                    .setParameter("tenantId", tenantId)
                    .list();

            if (incentiveSchemeDBBean != null) {
                return incentiveSchemeDBBean;
            }
            logger.warn("IncentiveScheme entry is missing for " + countries);

            return null;
        } finally {
            closeSession(session);
        }
    }

    // Get active scheme for countryCode and currency
    public Map<String, Object> getActiveScheme(String countryCode, String preferedCurrency, int tenantId) {

        Session session = openSession();
        try {

            //Create a map
            Map<String, Object> activeReferralSchemeMap;

            //Country is null, then default country
            countryCode = (StringUtils.isNotEmpty(countryCode))? countryCode : Configurations.DEFAULT_COUNTRY_CODE;

            //PreferredCurrency is null, then preferredCurrency is fetched from countryToCurrencyMap for this country
            preferedCurrency = (StringUtils.isNotEmpty(preferedCurrency))? preferedCurrency : countryToCurrencyMapDao.getCurrency(countryCode);

            preferedCurrency = (StringUtils.isNotEmpty(preferedCurrency))? preferedCurrency : Configurations.DEFAULT_CURRENCY_CODE;

            IncentiveSchemeDBBean incentiveSchemeDBBean = getActiveSchemeForCountry(countryCode, tenantId);

            if(incentiveSchemeDBBean != null){

                activeReferralSchemeMap = fetchValidCountryAndCreditSchemesFromIncentiveBean(incentiveSchemeDBBean, preferedCurrency);

                if(activeReferralSchemeMap != null){

                    return activeReferralSchemeMap;
                }
            }
            else {

                //If incentiveScheme for the specified country is not found, then fetch the default incentiveScheme
                incentiveSchemeDBBean = getActiveSchemeForCountry(Configurations.DEFAULT_COUNTRY_CODE, tenantId);

                activeReferralSchemeMap = fetchValidCountryAndCreditSchemesFromIncentiveBean(incentiveSchemeDBBean, preferedCurrency);

                if(activeReferralSchemeMap != null){

                    return activeReferralSchemeMap;
                }
            }

            logger.warn("IncentiveScheme not found");
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    public Map<String, Object> getActiveSchemeForReferralCode(String referralCode, String preferredCurrency, int tenantId) throws ServiceException {

        Session session = openSession();

        try {

            Map<String , Object> incentiveSchemeMap;

            //Get customerDBBean using referralCode
            CustomerDBBean customerDBBean = customerDao.getCustomerInfoByReferralCode(referralCode, tenantId);

            //Check if the customerDBBean is null
            if(customerDBBean == null ){

                logger.warn("Customer entry not found for referralCode : " + referralCode);
                return null;
            }

            if(preferredCurrency == null){

                //Fetch advocate currency by getting preferenceDBBean for customer and preference key
                PreferenceDBBean preferenceDBBean =  preferenceDao.getPreferenceByKey(customerDBBean.getId(), Configurations.PreferrenceKeyType.PREFERRED_CURRENCY);

                if(preferenceDBBean == null){

                    //If preferred currency is null, use default currency code
                    preferredCurrency = Configurations.DEFAULT_CURRENCY_CODE;
                }
                else {
                    preferredCurrency = preferenceDBBean.getValue();
                }
            }

            //Get referralCodeDBBean
            ReferralCodeDBBean referralCodeDBBean = referralCodeDao.getReferralCodeInfoByReferralCode(referralCode, customerDBBean.getTenantId());

            //Check whether referralScheme is mapped to active incentive scheme and if not map it to active incentive scheme
            referralCodeManagement.updateExistingReferralCode(referralCodeDBBean, customerDBBean.getCountry());

            //Fetch active scheme for this referralCode
            String stringQuery = "select I from IncentiveSchemeDBBean I, ReferralCodeDBBean R where " +
                            "R.incentiveSchemeId = I.id and " +
                            "R.referralCode =:referralCode and " +
                            "I.activeScheme =:isActive";

            //Fetch active incentive scheme for referral Code
            IncentiveSchemeDBBean incentiveSchemeDBBean = (IncentiveSchemeDBBean) session.createQuery(stringQuery)
                            .setParameter("referralCode", referralCode)
                            .setParameter("isActive", (byte) 1)
                            .uniqueResult();

            //Check for null
            if(incentiveSchemeDBBean != null){

                incentiveSchemeMap = fetchValidCountryAndCreditSchemesFromIncentiveBean(incentiveSchemeDBBean, preferredCurrency);

                if(incentiveSchemeMap != null){

                    return incentiveSchemeMap;
                }
            }

            logger.warn("IncentiveScheme entry missing for referral code " + referralCode);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    public CreditSchemeDBBean getCreditSchemeFromActiveScheme(String referralCode, String preferredCurrency, int tenantId) throws ServiceException {

        Session session = openSession();

        try {

            //Get customerDBBean using referralCode
            CustomerDBBean customerDBBean = customerDao.getCustomerInfoByReferralCode(referralCode, tenantId);

            //Check if the customerDBBean is null
            if(customerDBBean == null ){

                logger.warn("Customer entry not found for referralCode : " + referralCode);
                return null;
            }

            if(preferredCurrency == null){

                //Fetch advocate currency by getting preferenceDBBean for customer and preference key
                PreferenceDBBean preferenceDBBean =  preferenceDao.getPreferenceByKey(customerDBBean.getId(), Configurations.PreferrenceKeyType.PREFERRED_CURRENCY);

                if(preferenceDBBean == null){

                    //If preferred currency is null, use default currency code
                    preferredCurrency = Configurations.DEFAULT_CURRENCY_CODE;
                }
                else {
                    preferredCurrency = preferenceDBBean.getValue();
                }
            }

            //Get referralCodeDBBean
            ReferralCodeDBBean referralCodeDBBean = referralCodeDao.getReferralCodeInfoByReferralCode(referralCode, customerDBBean.getTenantId());

            //Check whether referralScheme is mapped to active incentive scheme and if not map it to active incentive scheme
            referralCodeManagement.updateExistingReferralCode(referralCodeDBBean, customerDBBean.getCountry());

            //Fetch active scheme for this referralCode
            String stringQuery = "select I from IncentiveSchemeDBBean I, ReferralCodeDBBean R where " +
                    "R.incentiveSchemeId = I.id and " +
                    "R.referralCode =:referralCode and " +
                    "I.activeScheme =:isActive";

            //Fetch active incentive scheme for referral Code
            IncentiveSchemeDBBean incentiveSchemeDBBean = (IncentiveSchemeDBBean) session.createQuery(stringQuery)
                    .setParameter("referralCode", referralCode)
                    .setParameter("isActive", (byte) 1)
                    .uniqueResult();

            //Check for null
            if(incentiveSchemeDBBean != null){

                CreditSchemeDBBean creditSchemeDBBean = creditSchemeDao.getCreditScheme(incentiveSchemeDBBean.getId(), preferredCurrency);

                if(creditSchemeDBBean != null){

                    return creditSchemeDBBean;
                }
            }

            logger.warn("IncentiveScheme entry missing for referral code " + referralCode);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    // Delete incentive scheme details for the incentive scheme id
    public void deleteIncentiveScheme(IncentiveSchemeDBBean incentiveSchemeDBBean) {
        Session session = openSession();

        try {
            session.delete(incentiveSchemeDBBean);
        } finally {
            closeSession(session);
        }
    }

    //Add incentiveScheme having ValidCountries and CreditSchemes
    public int addIncentiveScheme(IncentiveSchemeDBBean incentiveSchemeDBBean, List<ValidCountryDBBean> validCountryDBBeanList, List<CreditSchemeDBBean> creditSchemeDBBeanList) throws ServiceException {
        Session session = openSession();
        Transaction txn = null;

        try {

            txn = session.beginTransaction();

            //Add incentiveSchemeDBBean
            session.save(incentiveSchemeDBBean);

            int incentiveSchemeId = incentiveSchemeDBBean.getId();

            //Add validCountry in validCountryDBBeanList
            for (ValidCountryDBBean validCountryDBBean : validCountryDBBeanList) {

                //Set incentiveSchemeId
                validCountryDBBean.setIncentiveSchemeId(incentiveSchemeId);

                //Save
                session.save(validCountryDBBean);
            }

            //Add creditScheme in creditSchemeDBBeanList
            for (CreditSchemeDBBean creditSchemeDBBean : creditSchemeDBBeanList) {

                //Set incentiveSchemeId
                creditSchemeDBBean.setIncentiveSchemeId(incentiveSchemeId);

                session.save(creditSchemeDBBean);
            }

            //Commit transaction
            txn.commit();

            //Set transaction to null (To rollback)
            txn = null;

            return incentiveSchemeDBBean.getId();

        } catch (Exception e) {

            if (txn != null) {
                txn.rollback();
            }

            logger.error("Failed to add incentive scheme",e);
            return 0;
        } finally {
            closeSession(session);
        }
    }

    //Update incentiveScheme having ValidCountries and creditSchemes
    public void updateIncentiveScheme(IncentiveSchemeDBBean incentiveSchemeDBBean, List<ValidCountryDBBean> validCountryDBBeanList, List<CreditSchemeDBBean> creditSchemeDBBeanList) throws DatabaseException {

        Session session = openSession();
        Transaction txn = null;

        try {

            if (incentiveSchemeDBBean == null) {
                logger.error("failed to add Incentive scheme");
                return;
            }

            txn = session.beginTransaction();

            //Update incentiveSchemeBean
            session.saveOrUpdate(incentiveSchemeDBBean);

            int incentiveSchemeId = incentiveSchemeDBBean.getId();

            //Update validCountries if it is not null
            if (validCountryDBBeanList != null) {

                //Fetch all the validCountries which are in the table
                List existingvalidCountryDBBeanList = session.createCriteria(ValidCountryDBBean.class)
                        .add(Restrictions.eq("incentiveSchemeId", incentiveSchemeId))
                        .addOrder(Order.asc("id"))
                        .list();

                updateValidCountriesForIncentiveId(session , incentiveSchemeId, validCountryDBBeanList, existingvalidCountryDBBeanList);
            }

            //Update creditSchemes if it is not null
            if (creditSchemeDBBeanList != null) {

                //Fetch all the incentiveSchemes from table
                List existingCreditSchemeDBBeans = session.createCriteria(CreditSchemeDBBean.class)
                        .add(Restrictions.eq("incentiveSchemeId", incentiveSchemeDBBean.getId()))
                        .addOrder(Order.asc("id"))
                        .list();

                 updateCreditSchemesForIncentiveId(session, incentiveSchemeId, creditSchemeDBBeanList, existingCreditSchemeDBBeans);
            }

            //Commit transaction
            txn.commit();

            //Set transaction to null (To rollback)
            txn = null;


        } catch (Exception e) {

            if (txn != null) {
                txn.rollback();
            }

            logger.error("Failed to update incentive scheme with id : " + incentiveSchemeDBBean.getId(), e);
            throw new DatabaseException(ErrorCode.REFERRAL_SCHEME_UPDATION_FAILED);
        } finally {

            closeSession(session);
        }
    }

    public List<IncentiveSchemeDBBean> getIncentiveSchemesWhichAreEnabled() {

        Session session = openSession();

        try {
            String s = "select I from IncentiveSchemeDBBean I where I.schemeEnabled =:schemeEnabled";

            List incentiveSchemeDBBeans =  session.createQuery(s)
                    .setParameter("schemeEnabled",(byte) 1)
//                    .setParameter("tenantId", tenantId)
                    .list();

            if(incentiveSchemeDBBeans != null && incentiveSchemeDBBeans.size() != 0){

                return incentiveSchemeDBBeans;
            }

            logger.warn("IncentiveScheme entry not found for schemeEnabled to true");
            return null;
        }
        finally {
            closeSession(session);
        }
    }

        public List<IncentiveSchemeDBBean> getIncentiveSchemes(String countryCode, String isEnabled, PageInfo inputPageInfo, int tenantId) {


        Session session = openSession();

        try {

            //Query to fetch incentiveSchemes
            Query query = getQueryForGetIncentiveScheme(session, countryCode, isEnabled, tenantId);

            List incentiveSchemeDBBeanList;

            //Paging enabled
            if(inputPageInfo!= null ){

                //If the startIndex and count is intentionally set to zero
                if(inputPageInfo.getCount() == 0 && inputPageInfo.getStart() == 0) return null;

                incentiveSchemeDBBeanList = query.
                        setMaxResults(inputPageInfo.getCount()).
                        setFirstResult(inputPageInfo.getStart()).
                        list();
            }
            //Paging disabled
            else {

                incentiveSchemeDBBeanList = query.list();
            }

            if(CollectionUtils.isNotEmpty(incentiveSchemeDBBeanList)){

                return incentiveSchemeDBBeanList;
            }
            //commenting this block because we are already Fetching default scheme also.
//            else {
//
//                //IncentiveSchemesList is null
//                //If country is mentioned and there is no incentiveSchemes for that country, return the default scheme
//                if(StringUtils.isNotEmpty(countryCode)){
//
//                    //isEnabled is passed as a parameter,
//                    //Check whether it is true
//                    if((StringUtils.isNotEmpty(isEnabled) && "true".equals(isEnabled))
//                            || StringUtils.isEmpty(isEnabled)){
//
//                        //If it is true, then fetch the default scheme
//                        IncentiveSchemeDBBean incentiveSchemeDBBean = getActiveSchemeForCountry(Configurations.DEFAULT_COUNTRY_CODE);
//
//                        if(incentiveSchemeDBBean != null){
//
//                            incentiveSchemeDBBeanList = new ArrayList();
//                            incentiveSchemeDBBeanList.add(incentiveSchemeDBBean);
//
//                            return incentiveSchemeDBBeanList;
//                        }
//                    }
//                }
//            }

            //Log warning
            logger.warn("Incentive schemes entry missing ");

            return null;

        } finally {
            closeSession(session);
        }

    }

    public int getCountOfIncentiveSchemes(String countryCode, String isActive, String date, int tenantId) throws ParseException {
        Session session = openSession();

        try {

            //Get the query based on the parameters
            Query query = getQueryForGetIncentiveScheme(session, countryCode, isActive, tenantId);

            //Get list of incentiveSchemeDBBean
            List incentiveSchemeDBBeans = query.list();

            if(StringUtils.isNotEmpty(date))
                incentiveSchemeDBBeans = getAllIncentiveSchemesByDate(incentiveSchemeDBBeans, date);

            if (CollectionUtils.isNotEmpty(incentiveSchemeDBBeans)) {
                return incentiveSchemeDBBeans.size();
            }
            //commenting this block because we are already calling count with default scheme also.
//            else {
//
//                //IncentiveSchemesList is null
//                //If country is mentioned and there is no incentiveSchemes for that country, return the default scheme
//                if(StringUtils.isNotEmpty(countryCode)) {
//
//                    //isEnabled is passed as a parameter,
//                    //Check whether it is true
//                    if((StringUtils.isNotEmpty(isActive) && "true".equals(isActive))
//                            || StringUtils.isEmpty(isActive)){
//
//                        //If it is true, then fetch the default scheme
//                        IncentiveSchemeDBBean incentiveSchemeDBBean = getActiveSchemeForCountry(Configurations.DEFAULT_COUNTRY_CODE);
//
//                        if (incentiveSchemeDBBean != null) {
//
//                            return 1;
//                        }
//                    }
//                }
//            }

            //Log warning
            logger.warn("Incentive schemes entry missing for countryCode " + countryCode + " isActive " + isActive);

            return 0;

        } finally {

            closeSession(session);
        }
    }

    public List<IncentiveSchemeDBBean> getAllIncentiveSchemesByDate
            (List<IncentiveSchemeDBBean> incentiveSchemesList, String date) throws ParseException {

        List<IncentiveSchemeDBBean> incentiveSchemesListOnDate = new ArrayList<IncentiveSchemeDBBean>();

        for (IncentiveSchemeDBBean incentiveSchemeDBBean : incentiveSchemesList){

            Date requiredDate = CommonUtils.convertStringToDate(date);

            //Get validity start and end dates of the existing scheme
            Date validityStartDate = incentiveSchemeDBBean.getValidityStartDate();
            Date validityEndDate = incentiveSchemeDBBean.getValidityEndDate();

            if((requiredDate.compareTo(validityStartDate)) >=0
                    && (requiredDate.compareTo(validityEndDate) <=0)){

                incentiveSchemesListOnDate.add(incentiveSchemeDBBean);
            }
        }
        return incentiveSchemesListOnDate;
    }


//    //IncentiveSchemeDao Util methods


//    //Get sql query
    private Query getQueryForGetIncentiveScheme(Session session,String countryCode, String isEnabled, int tenantId){

        //Query which is selected conditionally based on the arguments
        Query query ;

        //String query which is conditionally chosen
        String stringQuery;

        //With countryCode
        if(countryCode != null){

            List<String> countryCodes = new ArrayList<String>();
            if(!countryCode.equals("ALL")){
                countryCodes.add("ALL");
            }
            countryCodes.add(countryCode);

            //With isEnabled
            if(isEnabled != null){

                Byte isEnabledByte = "true".equals(isEnabled) ? (byte) 1 : (byte) 0;

                //Get incentiveSchemes : countryCode, isEnabled, inputPageInfo
                stringQuery = "select I from IncentiveSchemeDBBean I, ValidCountryDBBean V where " +
                        "V.country in (:countryCodes) and " +
                        "I.schemeEnabled =:isEnabledByte and " +
                        "I.id = V.incentiveSchemeId and I.tenantId =:tenantId order by I.id asc";

                query = session.createQuery(stringQuery)
                        .setParameterList("countryCodes", countryCodes)
                        .setParameter("isEnabledByte", isEnabledByte)
                        .setParameter("tenantId", tenantId);

            }
            else {

                //Without isEnabled

                //Get incentiveSchemes : countryCode, inputPageInfo
                stringQuery = "select I from IncentiveSchemeDBBean I, ValidCountryDBBean V where " +
                        "V.country in (:countryCodes) and " +
                        "I.id = V.incentiveSchemeId and I.tenantId =:tenantId order by I.id asc";

                query = session.createQuery(stringQuery)
                        .setParameterList("countryCodes", countryCodes)
                        .setParameter("tenantId", tenantId);
            }
        }
        //Without countryCode
        else{

            //With isEnabled
            if(isEnabled != null){

                Byte isEnabledByte = "true".equals(isEnabled) ? (byte) 1 : (byte) 0;

                //Get incentiveSchemes : isEnabled, inputPageInfo
                stringQuery = "select I from IncentiveSchemeDBBean I where " +
                        "I.schemeEnabled =:isEnabledByte and I.tenantId =:tenantId order by I.id asc";

                query = session.createQuery(stringQuery)
                        .setParameter("isEnabledByte", isEnabledByte)
                        .setParameter("tenantId", tenantId);
            }
            else {
                //Without isEnabled

                //Get incentiveSchemes : inputPageInfo
                stringQuery = "select I from IncentiveSchemeDBBean I where I.tenantId =:tenantId order by I.id asc";

                query = session.createQuery(stringQuery)
                        .setParameter("tenantId", tenantId);
            }
        }

        return query;
    }

//    Update validCountries for incentiveSchemeId
    private void updateValidCountriesForIncentiveId(Session session, int incentiveSchemeId, List<ValidCountryDBBean> validCountryDBBeanList, List<ValidCountryDBBean> existingvalidCountryDBBeanList){

        if(existingvalidCountryDBBeanList != null) {
            //Create map of existing validCountry
            Map<String, ValidCountryDBBean> existingValidCountryDBBeanMap = new HashMap<String, ValidCountryDBBean>();

            for (ValidCountryDBBean validCountryDBBean : existingvalidCountryDBBeanList) {

                existingValidCountryDBBeanMap.put(validCountryDBBean.getCountry(), validCountryDBBean);

            }

            //Create map of updated validCountry
            Map<String, ValidCountryDBBean> validCountryDBBeanMap = new HashMap<String, ValidCountryDBBean>();

            for (ValidCountryDBBean validCountryDBBean : validCountryDBBeanList) {

                validCountryDBBeanMap.put(validCountryDBBean.getCountry(), validCountryDBBean);

            }

            //Go through all the existingValidCountry, remove which are not be in the table and update those which is already present
            for (String country : existingValidCountryDBBeanMap.keySet()) {

                if (validCountryDBBeanMap.containsKey(country)) {

                    //Update which are there in the table
                    ValidCountryDBBean validCountryDBBean = existingValidCountryDBBeanMap.get(country);
                    session.saveOrUpdate(validCountryDBBean);

                    validCountryDBBeanMap.remove(country);
                } else {
                    //Delete which are not in the list
                    ValidCountryDBBean validCountryDBBean = existingValidCountryDBBeanMap.get(country);
                    session.delete(validCountryDBBean);
                }
            }

            //Add which are not in the table
            for (String country : validCountryDBBeanMap.keySet()) {

                    ValidCountryDBBean validCountryDBBean = validCountryDBBeanMap.get(country);

                    //Set incentiveSchemeID
                    validCountryDBBean.setIncentiveSchemeId(incentiveSchemeId);

                    session.save(validCountryDBBean);

            }
        }
    }

    //    Update validCountries for incentiveSchemeId
    private void updateCreditSchemesForIncentiveId(Session session, int incentiveSchemeId, List<CreditSchemeDBBean> creditSchemeDBBeanList, List<CreditSchemeDBBean> existingCreditSchemeDBBeans){

        //Create map of updated CreditSchemeDBBeans
        Map<String, CreditSchemeDBBean> creditSchemeDBBeanMap = new HashMap<String, CreditSchemeDBBean>();

        for (CreditSchemeDBBean creditSchemeDBBean : creditSchemeDBBeanList) {
            creditSchemeDBBeanMap.put(creditSchemeDBBean.getCurrency(), creditSchemeDBBean);
        }

        //Create map of existing CreditSchemeDBBeans
        Map<String, CreditSchemeDBBean> existingCreditSchemeDBBeanMap = new HashMap<String, CreditSchemeDBBean>();

        for (CreditSchemeDBBean creditSchemeDBBean : existingCreditSchemeDBBeans) {
            existingCreditSchemeDBBeanMap.put(creditSchemeDBBean.getCurrency(), creditSchemeDBBean);
        }

        //Update which are in the table
        for (String currency : existingCreditSchemeDBBeanMap.keySet()) {

            if (creditSchemeDBBeanMap.containsKey(currency)) {

                CreditSchemeDBBean existingCreditScheme = existingCreditSchemeDBBeanMap.get(currency);
                CreditSchemeDBBean updatedCreditScheme = creditSchemeDBBeanMap.get(currency);

                //Update
                existingCreditScheme.setNewCustomerAmount(updatedCreditScheme.getNewCustomerAmount());
                existingCreditScheme.setAdvocateAmount(updatedCreditScheme.getAdvocateAmount());
                session.saveOrUpdate(existingCreditScheme);
            }
        }
    }

    private Map<String, Object> fetchValidCountryAndCreditSchemesFromIncentiveBean(IncentiveSchemeDBBean incentiveSchemeDBBean, String preferredCurrency){

        Map<String, Object> schemeDetails;

        //Fetch validCountries for this incentiveScheme
        List<ValidCountryDBBean> validCountryDBBeans = validCountryDao.getValidCountriesForIncentiveSchemeId(incentiveSchemeDBBean.getId());

        //Fetch creditScheme for this country and currency
        List<CreditSchemeDBBean> creditSchemeDBBeans = new ArrayList<CreditSchemeDBBean>();

        if(preferredCurrency != null){

            CreditSchemeDBBean creditSchemeDBBean = creditSchemeDao.getCreditScheme(incentiveSchemeDBBean.getId(), preferredCurrency);
            creditSchemeDBBeans.add(creditSchemeDBBean);
        }
        else{

            creditSchemeDBBeans = creditSchemeDao.getCreditSchemesForIncentiveScheme(incentiveSchemeDBBean.getId());

        }

        if(validCountryDBBeans != null && creditSchemeDBBeans!= null) {

            return utilMethodGetMapOfSchemeDetails(incentiveSchemeDBBean, validCountryDBBeans, creditSchemeDBBeans);
        }
        return null;
    }

    private Map<String, Object> fetchValidCountryAndCreditSchemesFromIncentiveBean(IncentiveSchemeDBBean incentiveSchemeDBBean){

        return fetchValidCountryAndCreditSchemesFromIncentiveBean(incentiveSchemeDBBean, null);
    }

    private Map<String, Object> utilMethodGetMapOfSchemeDetails(IncentiveSchemeDBBean incentiveSchemeDBBean, List<ValidCountryDBBean> validCountryDBBeans, List<CreditSchemeDBBean> creditSchemeDBBeans){

        Map<String, Object> schemeDetails = new HashMap<String, Object>();

        schemeDetails.put(Configurations.INCENTIVE_SCHEME_BEAN, incentiveSchemeDBBean);
        schemeDetails.put(Configurations.VALIDCOUNTRY_BEAN_LIST, validCountryDBBeans);
        schemeDetails.put(Configurations.CREDIT_SCHEME_BEAN, creditSchemeDBBeans);

        return schemeDetails;

    }

    //Get Active incentiveScheme from the list of incentiveScheme based on the current system date
//    private IncentiveSchemeDBBean utilMethodGetCurrentActiveIncentiveScheme ( List<IncentiveSchemeDBBean> incentiveSchemeDBBeans){
//
//        for(IncentiveSchemeDBBean incentiveSchemeDBBean : incentiveSchemeDBBeans){
//
//            if(utilMethodDoesTimelineMatchesCurrentTimeline(incentiveSchemeDBBean.getValidityStartDate(), incentiveSchemeDBBean.getValidityEndDate())){
//
//                return incentiveSchemeDBBean;
//            }
//        }
//
//        return null;
//    }

    //Returns true if current time is within the parameter start time and end time
    private boolean utilMethodDoesTimelineMatchesCurrentTimeline(java.sql.Date startDate, java.sql.Date endDate){

        //Get currentDate
        java.sql.Date currentDate = CommonUtils.getCurrentDate();

        //Compare current date with startDate and endDate
        if(currentDate.after(startDate) && currentDate.before(endDate)){

            return true;
        }

        return false;
    }


}
