package com.gigsky.database.dao;

import com.gigsky.common.CommonUtils;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.exception.DatabaseException;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.List;

public class CustomerDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(CustomerDao.class);


    //Create customer information
    //If customer entry exists then it replaces
    public int createCustomerInfo(CustomerDBBean customer) throws ConstraintViolationException {
        Session session = openSession();
        try {
            session.save(customer);
            return customer.getId();

        } catch (ConstraintViolationException constraintException) {

            //Close session
            session.close();

            //Customer table has unique index on tenant_id and gsCustomerId
            //If the entry is already there, update the customer entry

            logger.warn("Create CustomerInfo Exception: ", constraintException);

            //Get customerId for update
            CustomerDBBean customerInfoByGsCustomerId = getCustomerInfoByGsCustomerId(customer.getGsCustomerId(), customer.getTenantId());

            if (customerInfoByGsCustomerId != null) {
                //Set customerId
                customer.setId(customerInfoByGsCustomerId.getId());

                //Update customer entry
                updateCustomerInfo(customer);

                return customer.getId();
            } else {
                return -1;
            }

        } catch (Exception e) {
            logger.error("Create customer failure ", e);
            return -1;
        } finally {
            closeSession(session);
        }
    }

    public void updateCustomerInfoForDeletion(CustomerDBBean customerDBBean) {
        Session session = openSession();
        try{
            session.saveOrUpdate(customerDBBean);
        }
        finally {
            closeSession(session);
        }
    }

    //Update customer information
    public void updateCustomerInfo(CustomerDBBean fromCustomer) {
        Session session = openSession();

        try {

            CustomerDBBean toCustomer = (CustomerDBBean) session.get(CustomerDBBean.class, fromCustomer.getId());

            if (fromCustomer.getAccountCreationTime() != null) {
                toCustomer.setAccountCreationTime(fromCustomer.getAccountCreationTime());
            }
            if (StringUtils.isNotEmpty(fromCustomer.getReferralStatus())) {
                toCustomer.setReferralStatus(fromCustomer.getReferralStatus());
            }
            if (fromCustomer.getAdvocateId() != null) {
                toCustomer.setAdvocateId(fromCustomer.getAdvocateId());
            }
            if (StringUtils.isNotEmpty(fromCustomer.getCountry())) {
                toCustomer.setCountry(fromCustomer.getCountry());
            }
            if (StringUtils.isNotEmpty(fromCustomer.getEmailId())) {
                toCustomer.setEmailId(fromCustomer.getEmailId());
            }
            if (StringUtils.isNotEmpty(fromCustomer.getFirstName())) {
                toCustomer.setFirstName(fromCustomer.getFirstName());
            }
            if (StringUtils.isNotEmpty(fromCustomer.getLanguage())) {
                toCustomer.setLanguage(fromCustomer.getLanguage());
            }
            if (StringUtils.isNotEmpty(fromCustomer.getLastName())) {
                toCustomer.setLastName(fromCustomer.getLastName());
            }
            if (fromCustomer.getLastReferralMailSent() != null) {
                toCustomer.setLastReferralMailSent(fromCustomer.getLastReferralMailSent());
            }
            if (StringUtils.isNotEmpty(fromCustomer.getStatus())) {
                toCustomer.setStatus(fromCustomer.getStatus());
            }
            if (fromCustomer.isDeleted()) {
                toCustomer.setDeleted(fromCustomer.isDeleted());
            }

            session.saveOrUpdate(toCustomer);
        } finally {
            closeSession(session);
        }
    }

    //Update customer information
    public void updateCustomerInfo(Session session, CustomerDBBean customer) {
        session.saveOrUpdate(customer);
    }

    //Get customerId from GsCustomerId
    public int getCustomerIdForBackendCustomerId(long gsCustomerId, int tenantId) {
        Session session = openSession();

        try {
            CustomerDBBean customerDBBean = (CustomerDBBean) session.createCriteria(CustomerDBBean.class)
                    .add(Restrictions.eq("gsCustomerId", gsCustomerId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .uniqueResult();
            if (customerDBBean != null) {
                return customerDBBean.getId();
            }
            logger.warn("Customer entry missing for gsCustomerId: " + gsCustomerId);
            return 0;
        } finally {
            closeSession(session);
        }

    }

    //Get customer information for the customerId
    public CustomerDBBean getCustomerInfo(int customerId, int tenantId) {
        Session session = openSession();

        try {
            CustomerDBBean customer = (CustomerDBBean) session.createCriteria(CustomerDBBean.class)
                    .add(Restrictions.eq("id", customerId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .uniqueResult();

            if (customer != null) {
                return customer;
            }

            logger.warn("Customer entry is missing for " + customerId);
            return null;
        } finally {
            closeSession(session);
        }
    }

    public CustomerDBBean getCustomerInfo(Session session, int customerId, int tenantId) {
        CustomerDBBean customer = (CustomerDBBean) session.createCriteria(CustomerDBBean.class)
                .add(Restrictions.eq("id", customerId))
                .add(Restrictions.eq("tenantId", tenantId))
                .uniqueResult();

        if (customer != null) {
            return customer;
        }

        logger.warn("Customer entry is missing for " + customerId);
        return null;
    }

    //Get customer information for the customerId
    public CustomerDBBean getCustomerInfoWithOutTenantId(int customerId) {
        Session session = openSession();

        try {
            CustomerDBBean customer = (CustomerDBBean) session.createCriteria(CustomerDBBean.class)
                    .add(Restrictions.eq("id", customerId))
                    .uniqueResult();

            if (customer != null) {
                return customer;
            }

            logger.warn("Customer entry is missing for " + customerId);
            return null;
        } finally {
            closeSession(session);
        }
    }

    //Get customer information for the gsCustomerId
    public CustomerDBBean getCustomerInfoByGsCustomerId(long gsCustomerId, int tenantId) {
        Session session = openSession();
        try {
            CustomerDBBean customer = (CustomerDBBean) session.createCriteria(CustomerDBBean.class)
                    .add(Restrictions.eq("gsCustomerId", gsCustomerId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .uniqueResult();

            if (customer != null) {
                return customer;
            }

            logger.warn("Customer entry is missing for " + gsCustomerId);
            return null;
        } finally {
            closeSession(session);
        }
    }


    //Get customer information for the gsCustomerId
    public CustomerDBBean getCustomerInfoByGsCustomerId(Session session, long gsCustomerId, int tenantId) {
        CustomerDBBean customer = (CustomerDBBean) session.createCriteria(CustomerDBBean.class)
                .add(Restrictions.eq("gsCustomerId", gsCustomerId))
                .add(Restrictions.eq("tenantId", tenantId))
                .uniqueResult();

        if (customer != null) {
            return customer;
        }

        logger.warn("Customer entry is missing for " + gsCustomerId);
        return null;
    }

    //Get customer information for referralCode
    public CustomerDBBean getCustomerInfoByReferralCode(String referralCode, int tenantId) {
        Session session = openSession();

        try {
            //Create customerDBBean
            CustomerDBBean customerDBBean = (CustomerDBBean) session.createQuery("select C from CustomerDBBean C, ReferralCodeDBBean R where R.customerId = C.id and R.referralCode =:referralCode and C.tenantId =:tenantId ")
                    .setParameter("referralCode", referralCode)
                    .setParameter("tenantId", tenantId)
                    .uniqueResult();

            //Return customerDBBean if the query fetches result
            if (customerDBBean != null) {
                return customerDBBean;
            }

            //Log warning
            logger.warn("Customer entry missing for referral code : " + referralCode);

            //Return null
            return null;
        } finally {
            closeSession(session);
        }
    }

    //Get customer information for referralCode
    public CustomerDBBean getCustomerInfoByEmail(String emailId, int tenantId) {
        Session session = openSession();

        try {
            CustomerDBBean customer = (CustomerDBBean) session.createCriteria(CustomerDBBean.class)
                    .add(Restrictions.eq("emailId", emailId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .uniqueResult();

            if (customer != null) {
                return customer;
            }

            //Log warning
            logger.warn("Customer entry missing for email : " + emailId);

            //Return null
            return null;
        } finally {
            closeSession(session);
        }
    }

    //Delete customer information
    public void deleteCustomerInfo(CustomerDBBean customer) {
        Session session = openSession();

        try {
            session.delete(customer);
        } finally {
            closeSession(session);
        }
    }

    //Delete customer information
    public void deleteCustomerInfoByCustomerId(int customerId, int tenantId) {
        Session session = openSession();
        CustomerDBBean customerDBBean = getCustomerInfo(customerId, tenantId);
        deleteCustomerInfo(customerDBBean);
    }

    public List<CustomerDBBean> getValidCustomers(int maxCount, long initialDelay, long resendDelay) {

        Session session = openSession();
        try {

            // initial delay and resend delay maintaining in Configuration key values
            long initialTimeDelay = System.currentTimeMillis() - initialDelay;
            long resendTimeDelay = System.currentTimeMillis() - resendDelay;

            // converting time delays from millis to local time
            Timestamp initialTimeStamp = CommonUtils.convertMilliSecondsToTimeStamp(initialTimeDelay);
            Timestamp resendTimeStamp = CommonUtils.convertMilliSecondsToTimeStamp(resendTimeDelay);

            // Initial time stamp means current time - initial delay time
            // resend time stamp means current date - resend delay time

            // If customers lastInviteSent null and createTime less than or equal to Initial time stamp or
            // customers lastInviteSent less than or equal to resend time stamp, we are going to send emails.

            //(!isDeleted && ((lastReferralMailSent == NULL && createTime < initialTimestamp) || lastReferralMailSent < resendTimestamp))
            List<CustomerDBBean> customers = (List<CustomerDBBean>) session.createCriteria(CustomerDBBean.class)
                    .add(Restrictions.and(Restrictions.eq("deleted", false), Restrictions.or(Restrictions.and(Restrictions.isNull("lastReferralMailSent"), Restrictions.le("createTime", initialTimeStamp)),
                            Restrictions.le("lastReferralMailSent", resendTimeStamp))))
                    .setMaxResults(maxCount)
                    .list();

            if (customers != null) {
                return customers;
            }

            logger.warn("Customers entry missing for time delays..");
            return null;
        } finally {
            closeSession(session);
        }
    }

}
