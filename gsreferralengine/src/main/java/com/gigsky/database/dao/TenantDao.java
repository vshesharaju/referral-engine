package com.gigsky.database.dao;

import com.gigsky.database.bean.TenantDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by shwethars on 25/01/19.
 */
public class TenantDao  extends AbstractDao  {
    private static final Logger logger = LoggerFactory.getLogger(TenantDao.class);


    public int createTenant(TenantDBBean tenantDBBean)
    {
        Session session = openSession();

        try {
            session.save(tenantDBBean);
            return tenantDBBean.getId();
        }
        finally {
            closeSession(session);
        }
    }

    public void deleteTenant(TenantDBBean tenantDBBean)
    {
        Session session = openSession();

        try
        {
            session.delete(tenantDBBean);
        }
        finally {
            closeSession(session);
        }
    }


    public void deleteTenant(int tenantId)
    {
        TenantDBBean tenantInfo = getTenantInfo(tenantId);
        deleteTenant(tenantInfo);
    }



    public TenantDBBean getTenantInfo(int tenantId){
        Session session = openSession();

        try {
            TenantDBBean tenantDBBean = (TenantDBBean) session.createCriteria(TenantDBBean.class)
                    .add(Restrictions.eq("id",tenantId))
                    .uniqueResult();
            if(tenantDBBean!=null) {
                return tenantDBBean;
            }
            logger.warn("Tenant entry missing for tenantId: "+tenantId);
            return null;
        }
        finally {
            closeSession(session);
        }

    }

    public List<TenantDBBean> getAllTenants(){
        Session session = openSession();

        try {
            List<TenantDBBean> tenantDBBeans = (List<TenantDBBean>) session.createCriteria(TenantDBBean.class).
                    list();
            if(tenantDBBeans!=null) {
                return tenantDBBeans;
            }
            logger.warn("Tenant entries are missing");
            return null;
        }
        finally {
            closeSession(session);
        }
    }

}
