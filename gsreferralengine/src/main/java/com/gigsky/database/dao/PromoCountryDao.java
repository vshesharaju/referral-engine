package com.gigsky.database.dao;

import com.gigsky.database.bean.PromoCountryDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by prsubbareddy on 30/11/17.
 */
public class PromoCountryDao extends AbstractDao{

    private static final Logger logger = LoggerFactory.getLogger(PromoCountryDao.class);

    //Add promo Country
    public int addPromoCountry(PromoCountryDBBean promoCountryDBBean) {
        Session session = openSession();

        try {
            session.save(promoCountryDBBean);
            return promoCountryDBBean.getId();
        } finally {
            closeSession(session);
        }
    }


    // Update Promo Country
    public void updatePromoCountry(PromoCountryDBBean promoCountryDBBean) {
        Session session = openSession();

        try {
            session.saveOrUpdate(promoCountryDBBean);
        } finally {
            closeSession(session);
        }
    }


    // Delete Promo Country
    public void deletePromoCountry(PromoCountryDBBean promoCountryDBBean) {
        Session session = openSession();

        try {
            session.delete(promoCountryDBBean);
        } finally {
            closeSession(session);
        }
    }


    // Get Promo Country by id
    public PromoCountryDBBean getPromoCountry(int promoCountryId) {
        Session session = openSession();
        try {
            PromoCountryDBBean promoCountryDBBean = (PromoCountryDBBean) session.createCriteria(PromoCountryDBBean.class).
                    add(Restrictions.eq("id", promoCountryId))
                    .uniqueResult();
            if (promoCountryDBBean != null) {
                return promoCountryDBBean;
            }
            logger.warn("PromoCountry entry missing for promoCountryId: " + promoCountryId);
            return null;
        } finally {
            closeSession(session);
        }
    }


    // Get Promo Country's by promo id
    public List<PromoCountryDBBean> getPromoCountries(int promoId) {
        Session session = openSession();
        try {
            List<PromoCountryDBBean> promoCountryDBBeanList = (List<PromoCountryDBBean>) session.createCriteria(PromoCountryDBBean.class).
                    add(Restrictions.eq("promotionId", promoId))
                    .addOrder(Order.asc("country")).list();
            if (promoCountryDBBeanList != null) {
                return promoCountryDBBeanList;
            }
            logger.warn("PromoCountries entry missing for promoId: " + promoId);
            return null;
        } finally {
            closeSession(session);
        }
    }
}
