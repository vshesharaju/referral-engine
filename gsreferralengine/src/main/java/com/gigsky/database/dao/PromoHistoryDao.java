package com.gigsky.database.dao;

import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.PromoHistoryDBBean;
import com.gigsky.rest.bean.PageInfo;
import com.gigsky.rest.bean.PromoUsageResultSet;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.text.ParseException;
import java.util.List;

/**
 * Created by prsubbareddy on 30/11/17.
 */
public class PromoHistoryDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(PromoHistoryDao.class);

    // Add Promo history
    public int addPromotionHistory(PromoHistoryDBBean promoHistoryDBBean) {
        Session session = openSession();

        try {
            session.save(promoHistoryDBBean);
            return promoHistoryDBBean.getId();
        } finally {
            closeSession(session);
        }
    }

    public int addPromotionHistory(Session session ,PromoHistoryDBBean promoHistoryDBBean) {
//        Session session = openSession();

//        try {
            session.save(promoHistoryDBBean);
            return promoHistoryDBBean.getId();
//        } finally {
//            closeSession(session);
//        }
    }


    // Update Promo history
    public void updatePromotionHistory(PromoHistoryDBBean promoHistoryDBBean) {
        Session session = openSession();

        try {
            session.saveOrUpdate(promoHistoryDBBean);
        } finally {
            closeSession(session);
        }
    }

    // Update Promo history
    public void updatePromotionHistory(Session session ,PromoHistoryDBBean promoHistoryDBBean) {
         session.saveOrUpdate(promoHistoryDBBean);
    }


    // Get promo history by iccid
//    public PromoHistoryDBBean getPromotionHistory(String  iccid) {
//        Session session = openSession();
//        try {
//            PromoHistoryDBBean promoHistoryDBBean = (PromoHistoryDBBean) session.createCriteria(PromoHistoryDBBean.class).
//                    add(Restrictions.eq("iccid", iccid))
//                    .uniqueResult();
//            if (promoHistoryDBBean != null) {
//                return promoHistoryDBBean;
//            }
//            logger.warn("PromoHistoryDBBean entry missing for iccid: " + iccid);
//            return null;
//        } finally {
//            closeSession(session);
//        }
//    }
//
//
//    public List<PromoHistoryDBBean> getPromotionHistoryListByPromoId(Long customerId, int promoId) {
//        Session session = openSession();
//        try {
//            List<PromoHistoryDBBean> promotionDBBeanList = (List<PromoHistoryDBBean>) session.createCriteria(PromoHistoryDBBean.class).
//                    add(Restrictions.eq("gsCustomerId", customerId))
//                    .add(Restrictions.eq("promotionId", promoId))
//                    .list();
//            if (promotionDBBeanList != null) {
//                return promotionDBBeanList;
//            }
//            logger.warn("PromoHistoryDBBean entry missing for gsCustomerId: " + customerId);
//            return null;
//        } finally {
//            closeSession(session);
//        }
//    }

    public List<PromoHistoryDBBean> getPromotionHistoryList(long gsCustomerId) {
        Session session = openSession();
        try {
            List<PromoHistoryDBBean> promotionDBBeanList = (List<PromoHistoryDBBean>) session.createCriteria(PromoHistoryDBBean.class)
                    .add(Restrictions.eq("gsCustomerId", gsCustomerId))
                    .list();
            if (promotionDBBeanList != null) {
                return promotionDBBeanList;
            }
            logger.warn("PromoHistoryDBBean entry missing for gsCustomerId: " + gsCustomerId);
            return null;
        } finally {
            closeSession(session);
        }
    }

    public List<PromoHistoryDBBean> getPromotionHistoryList(String iccId) {
        Session session = openSession();
        try {
            List<PromoHistoryDBBean> promotionDBBeanList = (List<PromoHistoryDBBean>) session.createCriteria(PromoHistoryDBBean.class)
                    .add(Restrictions.eq("iccid", iccId))
                    .list();
            if (promotionDBBeanList != null) {
                return promotionDBBeanList;
            }
            logger.warn("PromoHistoryDBBean entry missing for iccid: " + iccId);
            return null;
        } finally {
            closeSession(session);
        }
    }

    public List<PromoHistoryDBBean> getPromotionHistoryList(int promoId, String iccId) {
        Session session = openSession();
        try {
            List<PromoHistoryDBBean> promotionDBBeanList = (List<PromoHistoryDBBean>) session.createCriteria(PromoHistoryDBBean.class)
                    .add(Restrictions.eq("promotionId", promoId))
                    .add(Restrictions.eq("iccid", iccId))
                    .list();
            if (promotionDBBeanList != null) {
                return promotionDBBeanList;
            }
            logger.warn("PromoHistoryDBBean entry missing for iccid: " + iccId);
            return null;
        } finally {
            closeSession(session);
        }
    }

    public List<PromoHistoryDBBean> getPromotionHistoryList(Session session, int promoId, String iccId) {
            List<PromoHistoryDBBean> promotionDBBeanList = (List<PromoHistoryDBBean>) session.createCriteria(PromoHistoryDBBean.class)
                    .add(Restrictions.eq("promotionId", promoId))
                    .add(Restrictions.eq("iccid", iccId))
                    .list();
            if (promotionDBBeanList != null) {
                return promotionDBBeanList;
            }
            logger.warn("PromoHistoryDBBean entry missing for iccid: " + iccId);
            return null;
    }

    public List<PromoHistoryDBBean> getPromotionHistoryList(Session session, Long gsCustomerId, int promoId, String iccId) {
        List<PromoHistoryDBBean> promotionDBBeanList = (List<PromoHistoryDBBean>) session.createCriteria(PromoHistoryDBBean.class)
                .add(Restrictions.eq("gsCustomerId", gsCustomerId))
                .add(Restrictions.eq("promotionId", promoId))
                .add(Restrictions.eq("iccid", iccId))
                .list();
        if (promotionDBBeanList != null) {
            return promotionDBBeanList;
        }
        logger.warn("PromoHistoryDBBean entry missing for iccid: " + iccId);
        return null;
    }

    public List<PromoHistoryDBBean> getPromotionHistoryByPromoId(Long gsCustomerId, int promoId) {
        Session session = openSession();
        try {
            List<PromoHistoryDBBean> promoHistoryDBBeans = (List<PromoHistoryDBBean>) session.createCriteria(PromoHistoryDBBean.class).
                    add(Restrictions.eq("gsCustomerId", gsCustomerId))
                    .add(Restrictions.eq("promotionId", promoId))
                    .list();
            if (promoHistoryDBBeans != null) {
                return promoHistoryDBBeans;
            }
            logger.warn("PromoHistoryDBBean entry missing for gsCustomerId: " + gsCustomerId);
            return null;
        } finally {
            closeSession(session);
        }
    }

    public List<PromoHistoryDBBean> getPromotionHistoryForFailed(int maxCount) {
        Session session = openSession();
        try {
            List<PromoHistoryDBBean> promotionDBBeanList = (List<PromoHistoryDBBean>) session.createCriteria(PromoHistoryDBBean.class)
                    .add(Restrictions.eq("status", Configurations.PromoEventsTaskStatus.CREDIT_ADD_FAILED))
                    .setMaxResults(maxCount)
                    .addOrder(Order.asc("id"))
                    .list();
            if (promotionDBBeanList != null) {
                return promotionDBBeanList;
            }
            logger.warn("PromoHistoryDBBean entry missing credit failed status CREDIT_ADD_FAILED");
            return null;
        } finally {
            closeSession(session);
        }
    }

    public List<PromoUsageResultSet> getPromoUsageList(Integer promoId, String sortBy, PageInfo inputPageInfo) {


        Session session = openSession();

        try {
            //Query to fetch promotions
            Query query = getQueryForGetPromoUsageList(session,promoId,sortBy);

            List promoHistoryList = CommonUtils.getQueryResponse(inputPageInfo,query);

            if(CollectionUtils.isNotEmpty(promoHistoryList)){
                return promoHistoryList;
            }

            logger.warn("Promotions entry missing ");

            return null;

        }
        finally {

            closeSession(session);
        }

    }


    //search for the promocode
    private Query getQueryForGetPromoUsageList(Session session, Integer promoId,String sortBy){

        //Query which is selected conditionally based on the arguments
        Query query ;

        //String query which is conditionally chosen
        String stringQuery;

        //With sortBy set to ASC
        if(sortBy!=null && sortBy.toUpperCase().equals("ASC")){

            stringQuery = "select D as promoHistoryDBBean, P.currentRedeemCount as redeemCount, C.firstName as firstName, " +
                    "C.lastName as lastName, C.emailId as emailId from PromotionDBBean P, PromoHistoryDBBean D, CustomerDBBean C " +
                    "where P.id = D.promotionId and C.gsCustomerId = D.gsCustomerId and  D.promotionId =:promoId" +
                    " and D.status = 'COMPLETE' order by D.createTime asc";

            query = session.createQuery(stringQuery)
                    .setParameter("promoId",promoId);

        }
        else {
            //if sortBy is not set or set to DESC
            stringQuery = "select D as promoHistoryDBBean, P.currentRedeemCount as redeemCount, C.firstName as firstName," +
                    " C.lastName as lastName, C.emailId as emailId from PromotionDBBean P, PromoHistoryDBBean D,CustomerDBBean C " +
                    "where P.id = D.promotionId and C.gsCustomerId = D.gsCustomerId and D.promotionId =:promoId " +
                    "and D.status = 'COMPLETE' order by D.createTime desc";

            query = session.createQuery(stringQuery)
                    .setParameter("promoId",promoId);
        }
        query.setResultTransformer(Transformers.aliasToBean(PromoUsageResultSet.class));
        return query;
    }

    public int getCountOfPromoUsage(Integer promoId, String sortBy) throws ParseException {
        Session session = openSession();

        try {

            //Get the query based on the parameters
            Query query = getQueryForGetPromoUsageList(session, promoId, sortBy);

            //Get list of promotionDBBean
            List promotionDBBeans = query.list();

            if (CollectionUtils.isNotEmpty(promotionDBBeans)) {
                return promotionDBBeans.size();
            }


            return 0;

        } finally {

            closeSession(session);
        }
    }

}
