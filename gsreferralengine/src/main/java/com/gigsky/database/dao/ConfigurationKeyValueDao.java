package com.gigsky.database.dao;

import com.gigsky.database.bean.ConfigurationsKeyValueDBBean;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConfigurationKeyValueDao extends AbstractDao {

	private static final Logger logger = LoggerFactory.getLogger(ConfigurationKeyValueDao.class);

	public String getConfigurationValue(String configKey) {
		Session session = openSession();
		try {
			String configValue = (String) session.createCriteria(ConfigurationsKeyValueDBBean.class)
					.setProjection(Projections.projectionList().add(Projections.property("value")))
					.add(Restrictions.eq("ckey", configKey))
					.uniqueResult();

			if (StringUtils.isNotEmpty(configValue)) {
				return configValue;
			}
			logger.warn("Configuration entry is missing for " + configKey);
			return StringUtils.EMPTY;
		}
		finally {
			closeSession(session);
		}
	}

	//Get configurationValue by configKey and defaultvalue
	public String getConfigurationValue(String configKey, String defaultValue) {
		String configurationValue = getConfigurationValue(configKey);
		if (StringUtils.isEmpty(configurationValue)) {
			configurationValue = defaultValue;
		}
		return configurationValue;
	}

	public String setConfigurationValue(ConfigurationsKeyValueDBBean configurationsKeyValue)
	{
		Session session = openSession();

		try
		{
			session.save(configurationsKeyValue);
			return configurationsKeyValue.getCkey();
		}

		finally {
			closeSession(session);
		}
	}

	public String updateConfigurationValue(ConfigurationsKeyValueDBBean configurationsKeyValue)
	{
		Session session = openSession();

		try
		{
			session.saveOrUpdate(configurationsKeyValue);
			return configurationsKeyValue.getCkey();
		}

		finally {
			closeSession(session);
		}
	}

//	public void deleteConfigurationValue(ConfigurationsKeyValueDBBean configurationsKeyValue)
//	{
//		Session session = openSession();
//
//		try
//		{
//			session.delete(configurationsKeyValue);
//		}
//
//		finally {
//			closeSession(session);
//		}
//	}
}
