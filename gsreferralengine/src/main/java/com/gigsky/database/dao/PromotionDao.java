package com.gigsky.database.dao;

import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.rest.bean.PageInfo;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.exception.ServiceException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by prsubbareddy on 30/11/17.
 */
public class PromotionDao extends AbstractDao {


    @Autowired
    private PromoCountryDao promoCountryDao;

    @Autowired
    private PromoCreditDao promoCreditDao;

    private static final Logger logger = LoggerFactory.getLogger(PromotionDao.class);

    //Add promotion having PromoCountries and PromoCredits
    public int addPromotion(PromotionDBBean promotionDBBean, List<PromoCountryDBBean> promoCountryDBBeanList, List<PromoCreditDBBean> promoCreditDBBeanList) throws ServiceException {
        Session session = openSession();
        Transaction txn = null;

        try {

            txn = session.beginTransaction();

            //Add promotionDBBean
            session.save(promotionDBBean);

            int promotionId = promotionDBBean.getId();

            //Add promoCountry in promoCountryDBBeanList
            for (PromoCountryDBBean promoCountryDBBean : promoCountryDBBeanList) {

                //Set promotionId
                promoCountryDBBean.setPromotionId(promotionId);

                //Save
                session.save(promoCountryDBBean);
            }

            //Add promoCredit in promoCreditDBBeanList
            for (PromoCreditDBBean promoCreditDBBean : promoCreditDBBeanList) {

                //Set promotionId
                promoCreditDBBean.setPromotionId(promotionId);

                session.save(promoCreditDBBean);
            }

            //Commit transaction
            txn.commit();

            //Set transaction to null (To rollback)
            txn = null;

            return promotionDBBean.getId();

        } catch (Exception e) {

            if (txn != null) {
                txn.rollback();
            }

            logger.error("Failed to add promotion", e);
            return 0;
        } finally {
            closeSession(session);
        }
    }

    //Update customer information
    public void updatePromotion(PromotionDBBean promotionDBBean)
    {
        Session session = openSession();

        try
        {
            session.saveOrUpdate(promotionDBBean);
        }

        finally {
            closeSession(session);
        }
    }

    //Update promotion having PromoCountries and promoCredits
    public void updatePromotion(PromotionDBBean promotionDBBean, List<PromoCountryDBBean> promoCountryDBBeanList, List<PromoCreditDBBean> promoCreditDBBeanList) throws DatabaseException {

        Session session = openSession();
        Transaction txn = null;

        try {

            if (promotionDBBean == null) {
                logger.error("failed to update promotion");
                return;
            }

            txn = session.beginTransaction();

            //Update promotionBean
            session.saveOrUpdate(promotionDBBean);

            int promotionId = promotionDBBean.getId();

            //Update promoCountries if it is not null
            if (promoCountryDBBeanList != null) {

                //Fetch all the promoCountries which are in the table
                List existingPromoCountryDBBeanList = session.createCriteria(PromoCountryDBBean.class)
                        .add(Restrictions.eq("promotionId", promotionId))
                        .addOrder(Order.asc("id"))
                        .list();

                updatePromoCountries(session , promotionId, promoCountryDBBeanList, existingPromoCountryDBBeanList);
            }

            //Update promoCredits if it is not null
            if (promoCreditDBBeanList != null) {

                //Fetch all the promotions from table
                List existingPromoCreditDBBeanList = session.createCriteria(PromoCreditDBBean.class)
                        .add(Restrictions.eq("promotionId", promotionDBBean.getId()))
                        .addOrder(Order.asc("id"))
                        .list();

                updatePromoCredits(session, promoCreditDBBeanList, existingPromoCreditDBBeanList);
            }

            //Commit transaction
            txn.commit();

            //Set transaction to null (To rollback)
            txn = null;


        } catch (Exception e) {

            if (txn != null) {
                txn.rollback();
            }

            logger.error("Failed to update promotion with id : " + promotionDBBean.getId(), e);
            throw new DatabaseException(ErrorCode.PROMOTION_UPDATION_FAILED);
        } finally {

            closeSession(session);
        }
    }


    // Delete promotion
    public void deletePromotion(PromotionDBBean promotionDBBean) throws DatabaseException{
        Session session = openSession();
        Transaction txn = null;

        try {
            if (promotionDBBean == null) {
                logger.error("failed to delete promotion");
                return;
            }

            txn = session.beginTransaction();

            int promoId = promotionDBBean.getId();

            deletePromoCountries(session, promoId);

            deletePromoCredits(session, promoId);

            session.delete(promotionDBBean);

            //Commit transaction
            txn.commit();

            //Set transaction to null (To rollback)
            txn = null;


        }  catch (Exception e) {

            if (txn != null) {
                txn.rollback();
            }

            logger.error("Failed to delete promotion with id : " + promotionDBBean.getId(), e);
            throw new DatabaseException(ErrorCode.PROMOTION_UPDATION_FAILED);
        } finally {

            closeSession(session);
        }
    }

    // Get promotion details by promo code
    public Map<String, Object> getPromotionByPromoCode(String  promoCode, int tenantId) {
        Session session = openSession();
        try {
            Map<String, Object> promotionCountryCreditMap;

            PromotionDBBean promotionDBBean = (PromotionDBBean) session.createCriteria(PromotionDBBean.class).
                    add(Restrictions.and(Restrictions.eq("promoCode", promoCode).ignoreCase(),
                            Restrictions.eq("tenantId", tenantId)))
                    .uniqueResult();
            if (promotionDBBean != null) {
                promotionCountryCreditMap = fetchPromoCountryAndPromoCredit(promotionDBBean, null);

                if(promotionCountryCreditMap != null){

                    return promotionCountryCreditMap;
                }
            }
            logger.warn("PromotionDBBean entry missing for PromoCode: " + promoCode+ " and tenantId: "+ tenantId);
            return null;
        } finally {
            closeSession(session);
        }
    }


    // Get promotion by promo code
    public PromotionDBBean getPromotion(String  promoCode, int tenantId) {
        Session session = openSession();
        try {

            PromotionDBBean promotionDBBean = (PromotionDBBean) session.createCriteria(PromotionDBBean.class).
                    add(Restrictions.and(Restrictions.eq("promoCode", promoCode).ignoreCase(),
                            Restrictions.eq("tenantId", tenantId)))
                    .uniqueResult();
            if (promotionDBBean != null) {
                return promotionDBBean;
            }
            logger.warn("PromotionDBBean entry missing for PromoCode: " + promoCode+ " and tenantId: "+ tenantId);
            return null;
        } finally {
            closeSession(session);
        }
    }

    public Session getSession()
    {
        return openSession();
    }

    public void closeSession()
    {
        closeSession(openSession());
    }

    public PromotionDBBean getPromotion(Session session, int promoId, int tenantId)
    {
        try {
            final Query query = session.createQuery("select P from PromotionDBBean P where P.id=:promoId and P.tenantId=:tenantId")
                    .setParameter("promoId",promoId)
                    .setParameter("tenantId",tenantId)
                    .setLockMode("P", LockMode.PESSIMISTIC_WRITE);

            return (PromotionDBBean) query.uniqueResult();
        }
        catch (Exception e)
        {
            logger.error("Error during write lock for promotion", e);
            closeSession(session);
            return null;
        }
    }

    public PromotionDBBean getPromotion(Session session, String promoCode, int tenantId)
    {
        try {
            final Query query = session.createQuery("select P from PromotionDBBean P where upper(P.promoCode) =upper(:promoCode) and P.tenantId=:tenantId")
                    .setParameter("promoCode",promoCode)
                    .setParameter("tenantId",tenantId)
                    .setLockMode("P", LockMode.PESSIMISTIC_WRITE);

            return (PromotionDBBean) query.uniqueResult();
        }
        catch (Exception e)
        {
            logger.error("Error during write lock for promotion", e);
            closeSession(session);
            return null;
        }
    }

    public void updatePromotion(Session session, PromotionDBBean promotionDBBean)
    {
        session.update(promotionDBBean);
    }


    // Get Active promotion for enabled promotions by promo code
    public PromotionDBBean getActivePromotion(String  promoCode, int tenantId, String isActive) {

        Byte isActiveByte = "true".equals(isActive) ? (byte) 1 : (byte) 0;

        Session session = openSession();
        try {

            PromotionDBBean promotionDBBean = (PromotionDBBean) session.createCriteria(PromotionDBBean.class).
                    add(Restrictions.eq("promoCode", promoCode).ignoreCase())
                    .add(Restrictions.eq("tenantId", tenantId))
                    .add(Restrictions.eq("promoEnabled", (byte)1))
                    .add(Restrictions.eq("activePromo", isActiveByte))
                    .uniqueResult();
            if (promotionDBBean != null) {
                return promotionDBBean;
            }
            logger.warn("Active PromotionDBBean entry missing for PromoCode: " + promoCode+ " and tenantId: "+ tenantId);
            return null;
        } finally {
            closeSession(session);
        }
    }


    //Get promo credit from the active promotion for preferred currency
    public PromoCreditDBBean getPromoCreditFromActivePromotion(String  promoCode, int tenantId, String preferredCurrency){

        Session session = openSession();

        try {

            PromotionDBBean promotionDBBean = (PromotionDBBean) session.createCriteria(PromotionDBBean.class).
                    add(Restrictions.eq("promoCode", promoCode).ignoreCase())
                    .add(Restrictions.eq("tenantId", tenantId))
                    .add(Restrictions.eq("promoEnabled", (byte)1))
                    .add(Restrictions.eq("activePromo",  (byte)1))
                    .uniqueResult();

            if(promotionDBBean != null){

                PromoCreditDBBean promoCreditDBBean = promoCreditDao.getPromoCredit(promotionDBBean.getId(), preferredCurrency);

                if(promoCreditDBBean != null){

                    return promoCreditDBBean;
                }
            }

            logger.warn("PromoCreditDBBean entry is missing for promoCode" + promoCode);

            return null;

        }
        finally {
            closeSession(session);
        }
    }

    //Gets promotion, promoCountries and promoCredits for this promotion
    public Map<String, Object> getPromotionDetails(int promoId, int tenantId){

        Session session = openSession();

        try {

            //Create a map
            Map<String, Object> promotionCountryCreditMap;

            PromotionDBBean promotionDBBean = (PromotionDBBean) session.createCriteria(PromotionDBBean.class)
                    .add(Restrictions.and(Restrictions.eq("id", promoId),
                            Restrictions.eq("tenantId", tenantId)))
                    .uniqueResult();

            if(promotionDBBean != null){

                promotionCountryCreditMap = fetchPromoCountryAndPromoCredit(promotionDBBean, null);

                if(promotionCountryCreditMap != null){

                    return promotionCountryCreditMap;
                }
            }

            logger.warn("PromotionDBBean entry is missing for promoId" + promoId);

            return null;

        }
        finally {
            closeSession(session);
        }
    }

    public PromotionDBBean getPromotion(int id, int tenantId) {
        Session session = openSession();
        try {
            PromotionDBBean promotionDBBean = (PromotionDBBean) session.createCriteria(PromotionDBBean.class).
                    add(Restrictions.and(Restrictions.eq("id", id),
                            Restrictions.eq("tenantId", tenantId)))
                    .uniqueResult();
            if (promotionDBBean != null) {
                return promotionDBBean;
            }
            logger.warn("PromotionDBBean entry missing for Id: " + id);
            return null;
        } finally {
            closeSession(session);
        }
    }

    public PromotionDBBean getPromotionByPromoId(int id) {
        Session session = openSession();
        try {
            PromotionDBBean promotionDBBean = (PromotionDBBean) session.createCriteria(PromotionDBBean.class).
                    add(Restrictions.eq("id", id))
                    .uniqueResult();
            if (promotionDBBean != null) {
                return promotionDBBean;
            }
            logger.warn("PromotionDBBean entry missing for Id: " + id);
            return null;
        } finally {
            closeSession(session);
        }
    }

    public PromotionDBBean getPromotionByPromoId(Session session, int id) {

            PromotionDBBean promotionDBBean = (PromotionDBBean) session.createCriteria(PromotionDBBean.class).
                    add(Restrictions.eq("id", id))
                    .uniqueResult();
            if (promotionDBBean != null) {
                return promotionDBBean;
            }
            logger.warn("PromotionDBBean entry missing for Id: " + id);
            return null;

    }

    public List<PromotionDBBean> getEnabledPromotions()
    {
        Session session = openSession();
        try {
            List<PromotionDBBean> enabledPromotions = (List<PromotionDBBean>)session.createCriteria(PromotionDBBean.class)
                    .add(Restrictions.eq("promoEnabled", (byte) 1))
                    .list();

            if(CollectionUtils.isNotEmpty(enabledPromotions))
            {
                return enabledPromotions;
            }

            logger.warn("No Promotions found which are enabled");
            return null;
        } finally {
            closeSession(session);
        }
    }


    // Get active promotions for countries
    public List<PromotionDBBean> getEnabledPromotionForCountries(String country, int tenantId) {

        Session session = openSession();

        try {

            List promotionDBBean = session.createQuery("select P from PromotionDBBean P, PromoCountryDBBean C " +
                    " where C.country =:countryCodes" +
                    " and P.promoEnabled =:isEnabled " +
                    " and P.activePromo =:isActive " +
                    " and C.promotionId = P.id and P.tenantId =:tenantId")
                    .setParameter("countryCodes", country)
                    .setParameter("isEnabled", (byte) 1)
                    .setParameter("isActive", (byte) 1)
                    .setParameter("tenantId", tenantId)
                    .list();

            if (promotionDBBean != null) {
                return promotionDBBean;
            }
            logger.warn("getEnabledPromotionForCountries: Promotion entry is missing for " + country);
            return null;
        } finally {
            closeSession(session);
        }
    }

    // Get active promotions for countries
    public List<PromotionDBBean> getEnabledPromotionForCountryWithPromoCode(String promoCode,String country, int tenantId) {

        Session session = openSession();

        try {

            List promotionDBBean = session.createQuery("select P from PromotionDBBean P, PromoCountryDBBean C " +
                    " where C.country =:countryCodes" +
                    " and P.promoEnabled =:isEnabled " +
                    " and P.activePromo =:isActive " +
                    " and upper(P.promoCode) = upper(:promoCode)" +
                    " and C.promotionId = P.id and P.tenantId =:tenantId")
                    .setParameter("countryCodes", country)
                    .setParameter("isEnabled", (byte) 1)
                    .setParameter("isActive", (byte) 1)
                    .setParameter("tenantId", tenantId)
                    .setParameter("promoCode", promoCode)
                    .list();

            if (promotionDBBean != null) {
                return promotionDBBean;
            }
            logger.warn("getEnabledPromotionForCountries: Promotion entry is missing for " + country);
            return null;
        } finally {
            closeSession(session);
        }
    }

    public PromotionDBBean getDefaultPromotionByPromoCode(String  promoCode, int tenantId){
        Session session = openSession();

        try {
            String country = "ALL";

            PromotionDBBean promotionDBBean = (PromotionDBBean)session.createQuery("select P from PromotionDBBean P, PromoCountryDBBean C " +
                    " where C.country =:country" +
                    " and P.promoEnabled =:isEnabled " +
                    " and P.activePromo =:isActive " +
                    " and upper(P.promoCode) = upper(:promoCode) " +
                    " and C.promotionId = P.id and P.tenantId =:tenantId")
                    .setParameter("country", country)
                    .setParameter("isEnabled", (byte) 1)
                    .setParameter("isActive", (byte) 1)
                    .setParameter("promoCode", promoCode)
                    .setParameter("tenantId", tenantId)
                    .uniqueResult();

            if (promotionDBBean != null) {
                return promotionDBBean;
            }
            logger.warn("getDefaultPromotionByPromoCode: Default Promotion entry is missing for promo code: " + promoCode);
            return null;
        } finally {
            closeSession(session);
        }
    }



    public List<PromotionDBBean> getPromotions(String countryCode, String isEnabled, String search, PageInfo inputPageInfo, int tenantId) {


        Session session = openSession();

        try {

            //Query to fetch promotions
            Query query = getQueryForGetPromotions(session, countryCode, search, isEnabled, tenantId);

            List promotionDBBeanList = CommonUtils.getQueryResponse(inputPageInfo,query);

            if(CollectionUtils.isNotEmpty(promotionDBBeanList)){

                return promotionDBBeanList;
            }

            logger.warn("Promotions entry missing ");

            return null;

        } finally {
            closeSession(session);
        }

    }



    public int getCountOfPromotions(String countryCode, String search, String isActive, String date, int tenantId) throws ParseException {
        Session session = openSession();

        try {

            //Get the query based on the parameters
            Query query = getQueryForGetPromotions(session, countryCode, search, isActive, tenantId);

            //Get list of promotionDBBean
            List promotionDBBeans = query.list();

            if(StringUtils.isNotEmpty(date))
                promotionDBBeans = getAllPromotionsByDate(promotionDBBeans, date);

            if (CollectionUtils.isNotEmpty(promotionDBBeans)) {
                return promotionDBBeans.size();
            }

            logger.warn("Promotions entry missing for countryCode " + countryCode + " isActive " + isActive);

            return 0;

        } finally {

            closeSession(session);
        }
    }



    public List<PromotionDBBean> getAllPromotionsByDate
            (List<PromotionDBBean> promotionDBBeanList, String date) throws ParseException {

        List<PromotionDBBean> promoListOnDate = new ArrayList<PromotionDBBean>();

        for (PromotionDBBean promotionDBBean : promotionDBBeanList){

            Date requiredDate = CommonUtils.convertStringToDate(date);

            //Get validity start and end dates of the existing promotion
            Date startDate = promotionDBBean.getStartDate();
            Date endDate = promotionDBBean.getEndDate();

            if((requiredDate.compareTo(startDate)) >=0
                    && (requiredDate.compareTo(endDate) <=0)){

                promoListOnDate.add(promotionDBBean);
            }
        }
        return promoListOnDate;
    }



    private Query getQueryForGetPromotions(Session session,String countryCode, String isEnabled, int tenantId){

        //Query which is selected conditionally based on the arguments
        Query query ;

        //String query which is conditionally chosen
        String stringQuery;

        //With countryCode
        if(countryCode != null){

            List<String> countryCodes = new ArrayList<String>();
            if(!countryCode.equals("ALL")){
                countryCodes.add("ALL");
            }
            countryCodes.add(countryCode);

            //With isEnabled
            if(isEnabled != null){

                Byte isEnabledByte = "true".equals(isEnabled) ? (byte) 1 : (byte) 0;

                //Get promotions : countryCode, isEnabled, inputPageInfo
                stringQuery = "select P from PromotionDBBean P, PromoCountryDBBean C where " +
                        "C.country in (:countryCodes) and " +
                        "P.promoEnabled =:isEnabledByte and " +
                        "P.id = C.promotionId and P.tenantId =:tenantId order by P.id asc";

                query = session.createQuery(stringQuery)
                        .setParameterList("countryCodes", countryCodes)
                        .setParameter("isEnabledByte", isEnabledByte)
                        .setParameter("tenantId", tenantId);

            }
            else {

                //Without isEnabled

                //Get promotions : countryCode, inputPageInfo
                stringQuery = "select P from PromotionDBBean P, PromoCountryDBBean C where " +
                        "C.country in (:countryCodes) and " +
                        "P.id = C.promotionId and P.tenantId =:tenantId order by P.id asc";

                query = session.createQuery(stringQuery)
                        .setParameterList("countryCodes", countryCodes)
                        .setParameter("tenantId", tenantId);
            }
        }
        //Without countryCode
        else{

            //With isEnabled
            if(isEnabled != null){

                Byte isEnabledByte = "true".equals(isEnabled) ? (byte) 1 : (byte) 0;

                //Get promotions : isEnabled, inputPageInfo
                stringQuery = "select P from PromotionDBBean P where " +
                        "P.promoEnabled =:isEnabledByte and P.tenantId =:tenantId order by P.id asc";

                query = session.createQuery(stringQuery)
                        .setParameter("isEnabledByte", isEnabledByte)
                        .setParameter("tenantId", tenantId);
            }
            else {
                //Without isEnabled

                //Get promotions : inputPageInfo
                stringQuery = "select P from PromotionDBBean P where P.tenantId =:tenantId order by P.id asc";

                query = session.createQuery(stringQuery)
                        .setParameter("tenantId", tenantId);
            }
        }

        return query;
    }

    //search for the promocode
    private Query getQueryForGetPromotions(Session session,String countryCode, String search, String isEnabled, int tenantId){

        //Query which is selected conditionally based on the arguments
        Query query ;

        //String query which is conditionally chosen
        String stringQuery;

        //if search is null
        if(StringUtils.isNotEmpty(search)) {

            //With countryCode
            if (countryCode != null) {

                List<String> countryCodes = new ArrayList<String>();
                if (!countryCode.equals("ALL")) {
                    countryCodes.add("ALL");
                }
                countryCodes.add(countryCode);

                //With isEnabled
                if (isEnabled != null) {

                    Byte isEnabledByte = "true".equals(isEnabled) ? (byte) 1 : (byte) 0;

                    //Get promotions : countryCode, isEnabled, inputPageInfo
                    stringQuery = "select P from PromotionDBBean P, PromoCountryDBBean C where " +
                            "C.country in (:countryCodes) and " +
                            "P.promoEnabled =:isEnabledByte and " +
                            "P.id = C.promotionId and P.tenantId =:tenantId and (P.promoCode LIKE '%" + search + "%' OR P.name LIKE '%" + search + "%') order by P.id asc";

                    query = session.createQuery(stringQuery)
                            .setParameterList("countryCodes", countryCodes)
                            .setParameter("isEnabledByte", isEnabledByte)
                            .setParameter("tenantId", tenantId);
//

                } else {

                    //Without isEnabled

                    //Get promotions : countryCode, inputPageInfo
                    stringQuery = "select P from PromotionDBBean P, PromoCountryDBBean C where " +
                            "C.country in (:countryCodes) and " +
                            "P.id = C.promotionId and P.tenantId =:tenantId and (P.promoCode LIKE '%" + search + "%' OR P.name LIKE '%" + search + "%') order by P.id asc";

                    query = session.createQuery(stringQuery)
                            .setParameterList("countryCodes", countryCodes)
                            .setParameter("tenantId", tenantId);
//
                }
            }
            //Without countryCode
            else {

                //With isEnabled
                if (isEnabled != null) {

                    Byte isEnabledByte = "true".equals(isEnabled) ? (byte) 1 : (byte) 0;

                    //Get promotions : isEnabled, inputPageInfo
                    stringQuery = "select P from PromotionDBBean P where " +
                            "P.promoEnabled =:isEnabledByte and P.tenantId =:tenantId and (P.promoCode LIKE '%" + search + "%' OR P.name LIKE '%" + search + "%') order by P.id asc";

                    query = session.createQuery(stringQuery)
                            .setParameter("isEnabledByte", isEnabledByte)
                            .setParameter("tenantId", tenantId);
                } else {
                    //Without isEnabled

                    //Get promotions : inputPageInfo
                    stringQuery = "select P from PromotionDBBean P where P.tenantId =:tenantId and (P.promoCode LIKE '%" + search + "%' OR P.name LIKE '%" + search + "%') order by P.id asc";

                    query = session.createQuery(stringQuery)
                            .setParameter("tenantId", tenantId);
                }
            }
        }
        else {
            //get all promotion
            query = getQueryForGetPromotions(session,countryCode,isEnabled,tenantId);
        }

        return query;
    }

    // Update promoCountries for promoId
    private void updatePromoCountries(Session session, int promoId, List<PromoCountryDBBean> promoCountryDBBeanList, List<PromoCountryDBBean> existingPromoCountryDBBeanList){

        if(existingPromoCountryDBBeanList != null) {
            //Create map of existing promoCountry
            Map<String, PromoCountryDBBean> existingPromoCountryDBBeanMap = new HashMap<String, PromoCountryDBBean>();

            for (PromoCountryDBBean promoCountryDBBean : existingPromoCountryDBBeanList) {

                existingPromoCountryDBBeanMap.put(promoCountryDBBean.getCountry(), promoCountryDBBean);

            }

            //Create map of updated promoCountry
            Map<String, PromoCountryDBBean> promoCountryDBBeanMap = new HashMap<String, PromoCountryDBBean>();

            for (PromoCountryDBBean promoCountryDBBean : promoCountryDBBeanList) {

                promoCountryDBBeanMap.put(promoCountryDBBean.getCountry(), promoCountryDBBean);

            }

            //Go through all the existingPromoCountry, remove which are not be in the table and update those which is already present
            for (String country : existingPromoCountryDBBeanMap.keySet()) {

                if (promoCountryDBBeanMap.containsKey(country)) {

                    //Update which are there in the table
                    PromoCountryDBBean promoCountryDBBean = existingPromoCountryDBBeanMap.get(country);
                    session.saveOrUpdate(promoCountryDBBean);

                    promoCountryDBBeanMap.remove(country);
                } else {
                    //Delete which are not in the list
                    PromoCountryDBBean promoCountryDBBean = existingPromoCountryDBBeanMap.get(country);
                    session.delete(promoCountryDBBean);
                }
            }

            //Add which are not in the table
            for (String country : promoCountryDBBeanMap.keySet()) {

                PromoCountryDBBean promoCountryDBBean = promoCountryDBBeanMap.get(country);

                //Set promotionID
                promoCountryDBBean.setPromotionId(promoId);

                session.save(promoCountryDBBean);

            }
        }
    }

    //Update promoCredits
    private void updatePromoCredits(Session session, List<PromoCreditDBBean> promoCreditDBBeanList, List<PromoCreditDBBean> existingPromoCreditDBBeanList){

        //Create map of updated PromoCreditDBBeans
        Map<String, PromoCreditDBBean> promoCreditDBBeanMap = new HashMap<String, PromoCreditDBBean>();

        for (PromoCreditDBBean promoCreditDBBean : promoCreditDBBeanList) {
            promoCreditDBBeanMap.put(promoCreditDBBean.getCurrency(), promoCreditDBBean);
        }

        //Create map of existing PromoCreditDBBeans
        Map<String, PromoCreditDBBean> existingPromoCreditDBBeanMap = new HashMap<String, PromoCreditDBBean>();

        for (PromoCreditDBBean promoCreditDBBean : existingPromoCreditDBBeanList) {
            existingPromoCreditDBBeanMap.put(promoCreditDBBean.getCurrency(), promoCreditDBBean);
        }

        //Update which are in the table
        for (String currency : existingPromoCreditDBBeanMap.keySet()) {

            if (promoCreditDBBeanMap.containsKey(currency)) {

                PromoCreditDBBean existingPromoCredit = existingPromoCreditDBBeanMap.get(currency);
                PromoCreditDBBean updatedPromoCredit = promoCreditDBBeanMap.get(currency);

                //Update
                existingPromoCredit.setCreditAmount(updatedPromoCredit.getCreditAmount());
                existingPromoCredit.setMinimumCartAmount(updatedPromoCredit.getMinimumCartAmount());
                session.saveOrUpdate(existingPromoCredit);
            }
        }
    }

    private void deletePromoCountries(Session session, int promoId){

        List<PromoCountryDBBean> existingPromoCountryDBBeanList = (List<PromoCountryDBBean>)session.createCriteria(PromoCountryDBBean.class)
                .add(Restrictions.eq("promotionId", promoId))
                .addOrder(Order.asc("id"))
                .list();

        if(existingPromoCountryDBBeanList != null) {

            for (PromoCountryDBBean promoCountryDBBean : existingPromoCountryDBBeanList) {
                session.delete(promoCountryDBBean);
            }
        }
    }

    private void deletePromoCredits(Session session, int promoId){

        List<PromoCreditDBBean> existingPromoCreditDBBeanList = (List<PromoCreditDBBean>)session.createCriteria(PromoCreditDBBean.class)
                .add(Restrictions.eq("promotionId", promoId))
                .addOrder(Order.asc("id"))
                .list();


        if(existingPromoCreditDBBeanList != null) {

            for (PromoCreditDBBean promoCreditDBBean : existingPromoCreditDBBeanList) {
                session.delete(promoCreditDBBean);
            }
        }
    }


    private Map<String, Object> fetchPromoCountryAndPromoCredit(PromotionDBBean promotionDBBean, String preferredCurrency){

        //Fetch promoCountries for this promotion
        List<PromoCountryDBBean> promoCountryDBBeans = promoCountryDao.getPromoCountries(promotionDBBean.getId());

        //Fetch promoCredit for this country and currency
        List<PromoCreditDBBean> promoCreditDBBeans = new ArrayList<PromoCreditDBBean>();

        if(preferredCurrency != null){

            PromoCreditDBBean promoCreditDBBean = promoCreditDao.getPromoCredit(promotionDBBean.getId(), preferredCurrency);
            promoCreditDBBeans.add(promoCreditDBBean);
        }
        else{

            promoCreditDBBeans = promoCreditDao.getPromoCreditList(promotionDBBean.getId());

        }

        if(promoCountryDBBeans != null && promoCreditDBBeans!= null) {

            return utilMethodGetMapOfPromotionDetails(promotionDBBean, promoCountryDBBeans, promoCreditDBBeans);
        }
        return null;
    }

    private Map<String, Object> utilMethodGetMapOfPromotionDetails(PromotionDBBean promotionDBBean, List<PromoCountryDBBean> promoCountryDBBeans, List<PromoCreditDBBean> promoCreditDBBeans){

        Map<String, Object> promotionDetails = new HashMap<String, Object>();

        promotionDetails.put(Configurations.PROMOTION_BEAN, promotionDBBean);
        promotionDetails.put(Configurations.PROMO_COUNTRY_BEAN_LIST, promoCountryDBBeans);
        promotionDetails.put(Configurations.PROMO_CREDIT_BEAN, promoCreditDBBeans);

        return promotionDetails;

    }

}
