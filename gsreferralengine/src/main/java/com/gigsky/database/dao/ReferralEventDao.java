package com.gigsky.database.dao;

import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.ReferralEventDBBean;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by pradeepragav on 04/01/17.
 */
public class ReferralEventDao extends AbstractDao {
    private static final Logger logger = LoggerFactory.getLogger(ReferralEventDao.class);

    //add ReferralEvent
    public int addReferralEvent(ReferralEventDBBean referralEventDBBean) {
        Session session = openSession();

        try {
            session.save(referralEventDBBean);
            return referralEventDBBean.getId();
        } finally {
            closeSession(session);
        }
    }

    //delete ReferralEvent
    public void deleteReferralEvent(ReferralEventDBBean referralEventDBBean) {
        Session session = openSession();

        try {
            session.delete(referralEventDBBean);
        } finally {
            closeSession(session);
        }
    }

    //update ReferralEvent
    public void updateReferralEvent(ReferralEventDBBean referralEventDBBean) {
        Session session = openSession();

        try {
            session.saveOrUpdate(referralEventDBBean);
        } finally {
            closeSession(session);
        }
    }

    public void updateReferralEvent(Session session,ReferralEventDBBean referralEventDBBean) {
            session.saveOrUpdate(referralEventDBBean);
    }

    //Get referralId by id
    public ReferralEventDBBean getReferralEvent(int referralEventId) {
        Session session = openSession();
        try {
            ReferralEventDBBean referralEventDBBean = (ReferralEventDBBean) session.createCriteria(ReferralEventDBBean.class).
                    add(Restrictions.eq("id", referralEventId))
                    .uniqueResult();
            if (referralEventDBBean != null) {
                return referralEventDBBean;
            }
            logger.warn("ReferralEvent entry missing for referralEventId: " + referralEventId);
            return null;
        } finally {
            closeSession(session);
        }
    }

    //get ReferralEvents for customer
    public List<ReferralEventDBBean> getReferralEventsForCustomer(int customerId) {
        Session session = openSession();

        try {
            List<ReferralEventDBBean> referralEventDBBeans = (List<ReferralEventDBBean>) session.createCriteria(ReferralEventDBBean.class)
                    .add(Restrictions.eq("customerId", customerId))
                    .addOrder(Order.asc("id")).list();

            if (referralEventDBBeans != null) {
                return referralEventDBBeans;
            }
            logger.warn("ReferralEvent entry is missing for customerId: " + customerId);
            return null;
        } finally {
            closeSession(session);
        }
    }

    //Get signUpEvent with status New or In Progress
//    public ReferralEventDBBean getNewOrInProcessSignUpEventDataForCustomer(int customerId){
//
//        Session session = openSession();
//
//        try {
//
//            String queryString = "select R from ReferralEventDBBean R where " +
//                    "(R.customerId=:customerId) and " +
//                    "(R.status=:statusNew or R.status=:statusInProcess) and " +
//                    "R.eventType =:eventType";
//
//            ReferralEventDBBean referralEventDBBean = (ReferralEventDBBean)session.createQuery(queryString)
//                    .setParameter("statusNew",Configurations.StatusType.STATUS_NEW)
//                    .setParameter("statusInProcess",Configurations.StatusType.STATUS_IN_PROCESS)
//                    .setParameter("eventType", Configurations.ReferralEventType.SIGNUP_EVENT)
//                    .setParameter("customerId", customerId)
//                    .uniqueResult();
//
//            if(referralEventDBBean != null){
//
//                return referralEventDBBean;
//            }
//            return null;
//
//        } catch (Exception e){
//
//            logger.error("ReferralEventDao getNewSignUpEventDataForCustomer : "+ e);
//            return null;
//        }
//        finally {
//            closeSession(session);
//        }
//    }
//
//    //Get signUpEventForCustomer
//    public ReferralEventDBBean getReferralCreditAddEventForCustomer(int customerId, String eventType) {
//
//        Session session = openSession();
//
//        try {
//
//            String queryString = "select R from ReferralEventDBBean R where " +
//                    "R.customerId=:customerId and R.eventType =:eventType";
//
//            ReferralEventDBBean referralEventDBBean = (ReferralEventDBBean)session.createQuery(queryString)
//                    .setParameter("eventType",eventType)
//                    .setParameter("customerId", customerId)
//                    .uniqueResult();
//
//            if(referralEventDBBean != null){
//
//                return referralEventDBBean;
//            }
//            return null;
//
//        } catch (Exception e){
//
//            logger.error("ReferralEventDao getNewSignUpEventDataForCustomer : "+ e);
//            return null;
//        }
//        finally {
//            closeSession(session);
//        }
//    }

        // Get referral events using customerId and eventType
    public List<ReferralEventDBBean> getReferralEventsForCustomerByEventType(int customerId, String eventType, int tenantId) {

        Session session = openSession();
        try {
            List<ReferralEventDBBean> referralEventDBBeans = (List<ReferralEventDBBean>) session.createCriteria(ReferralEventDBBean.class)
                    .add(Restrictions.eq("customerId", customerId))
                    .add(Restrictions.eq("eventType", eventType))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .list();
            if (referralEventDBBeans != null) {
                return referralEventDBBeans;
            }
            logger.warn("ReferralEvent entry missing for customerId:" + customerId + " eventType: " + eventType);
            return null;
        } finally {
            closeSession(session);
        }
    }

    public List<ReferralEventDBBean> getReferralEventsByStatus(String status, int maxCount) {
        Session session = openSession();
        try {
            List<ReferralEventDBBean> referralEventDBBeans = (List<ReferralEventDBBean>) session.createCriteria(ReferralEventDBBean.class)
                    .add(Restrictions.eq("status", status))
                    .setMaxResults(maxCount)
                    .addOrder(Order.asc("id"))
                    .list();
            if (referralEventDBBeans != null) {
                return referralEventDBBeans;
            }
            logger.warn("ReferralEvent entry missing for status: " + status);
            return null;
        } finally {
            closeSession(session);
        }
    }

    public ReferralEventDBBean getSignUpReferralEvent(int customerId, int tenantId) {

        Session session = openSession();

        try {

            ReferralEventDBBean referralEventDBBean = (ReferralEventDBBean)session.createCriteria(ReferralEventDBBean.class)
                    .add(Restrictions.eq("eventType", Configurations.ReferralEventType.SIGNUP_EVENT))
                    .add(Restrictions.eq("customerId", customerId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .add(Restrictions.ne("status", Configurations.StatusType.DUPLICATE_ENTRY))
                    .uniqueResult();

            if (referralEventDBBean != null) {
                return referralEventDBBean;
            }

            logger.warn("ReferralEvent signUp event is missing for customer" + customerId);
            return null;
        } finally {
            closeSession(session);
        }
    }

    public ReferralEventDBBean getDuplicateEvent(String eventType ,CustomerDBBean customerDBBean, String stringThatHasToBeUnique) {

        Session session = openSession();

        try {

            Criteria criteria =  session.createCriteria(ReferralEventDBBean.class)
                    .add(Restrictions.eq("eventType", eventType))
                    .add(Restrictions.eq("customerId", customerDBBean.getId()))
                    .add(Restrictions.eq("tenantId", customerDBBean.getTenantId()))
                    .add(Restrictions.like("eventData", stringThatHasToBeUnique, MatchMode.ANYWHERE))
                    .add(Restrictions.ne("status", Configurations.StatusType.DUPLICATE_ENTRY));

            //Get unique
            ReferralEventDBBean referralEventDBBean = (ReferralEventDBBean) criteria.uniqueResult();

            return referralEventDBBean;

        } finally {
            closeSession(session);
        }
    }

    public int getNumberOfReferralEvent(String eventType, long startTimeInMilli, long endTimeinMilli, int tenantId)
    {
        Session session = openSession();
        try {

            Timestamp startTime = CommonUtils.convertMilliSecondsToTimeStamp(startTimeInMilli);
            Timestamp endTime = CommonUtils.convertMilliSecondsToTimeStamp(endTimeinMilli);

            int numberOfReferralEvents =session.createCriteria(ReferralEventDBBean.class)
                    .add(Restrictions.eq("eventType", eventType))
                    .add(Restrictions.ge("createTime", startTime))
                    .add(Restrictions.le("createTime", endTime))
                    .add(Restrictions.le("tenantId", tenantId))
                    .list().size();
            return numberOfReferralEvents;
        }
        finally {
            closeSession(session);
        }
    }

}