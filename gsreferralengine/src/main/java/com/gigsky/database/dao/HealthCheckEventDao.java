package com.gigsky.database.dao;

import com.gigsky.database.bean.HealthCheckEventDBBean;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by prsubbareddy on 21/12/17.
 */
public class HealthCheckEventDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(HealthCheckEventDao.class);

    // Add health check event info
    public int addHealthCheckEventInfo(HealthCheckEventDBBean healthCheckEventDBBean) {

        Session session = openSession();

        try {
            session.save(healthCheckEventDBBean);
            return healthCheckEventDBBean.getId();
        } finally {
            closeSession(session);
        }
    }

    // Update health check event info
    public void updateHealthCheckEventInfo(HealthCheckEventDBBean healthCheckEventDBBean) {

        Session session = openSession();

        try {
            session.saveOrUpdate(healthCheckEventDBBean);
        } finally {
            closeSession(session);
        }
    }

    public HealthCheckEventDBBean getHealthCheckEvent() {
        Session session = openSession();
        try {
            HealthCheckEventDBBean healthCheckEventDBBean = (HealthCheckEventDBBean) session.createQuery("select H from HealthCheckEventDBBean H")
                    .uniqueResult();
            if(healthCheckEventDBBean!=null){
                return healthCheckEventDBBean;
            }
            logger.warn("Health check event entry missing");
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    public void deleteHealthCheckEvent(HealthCheckEventDBBean healthCheckEventDBBean) {
        Session session = openSession();

        try {
            session.delete(healthCheckEventDBBean);
        } finally {
            closeSession(session);
        }
    }

}
