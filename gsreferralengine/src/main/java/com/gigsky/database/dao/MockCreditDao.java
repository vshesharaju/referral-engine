package com.gigsky.database.dao;

import com.gigsky.database.bean.MockCreditDBBean;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by vinayr on 16/01/17.
 */
public class MockCreditDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(MockCreditDao.class);

    // Add credit info
    public int addCredit(MockCreditDBBean mockCreditDBBean)
    {
        Session session = openSession();

        try
        {
            session.save(mockCreditDBBean);
            return mockCreditDBBean.getId();
        }
        finally
        {
            closeSession(session);
        }
    }

    // Update credit info
    public void updateCredit(MockCreditDBBean mockCreditDBBean)
    {
        Session session = openSession();

        try
        {
            session.saveOrUpdate(mockCreditDBBean);
        }

        finally {
            closeSession(session);
        }
    }

    // Fetch credit info for credit id
    public MockCreditDBBean getCredit(int creditId)
    {
        Session session = openSession();

        try
        {

            MockCreditDBBean mockCreditDBBean = (MockCreditDBBean) session.createCriteria(MockCreditDBBean.class)
                    .add(Restrictions.eq("id", creditId))
                    .uniqueResult();

            if (mockCreditDBBean != null)
            {
                return mockCreditDBBean;
            }

            logger.warn("Mock credit entry is missing for " + creditId);

            return null;
        }

        finally {
            closeSession(session);
        }
    }

    // Delete customer info
//    public void deleteCredit(MockCreditDBBean mockCreditDBBean)
//    {
//        Session session = openSession();
//
//        try
//        {
//            session.delete(mockCreditDBBean);
//        }
//
//        finally {
//            closeSession(session);
//        }
//    }

    //Get creditBalance of customer
    public float getCreditBalanceOfCustomer(int customerId){

        Session session = openSession();
        try {

            List mockCredits = session.createCriteria(MockCreditDBBean.class)
                    .add(Restrictions.eq("mockcustomerId", customerId))
                    .list();

            if(CollectionUtils.isNotEmpty(mockCredits)){

                List<MockCreditDBBean> mockCreditDBBeans = mockCredits;

                float creditBalance = 0;

                for (MockCreditDBBean mockCreditDBBean : mockCreditDBBeans){

                    creditBalance += mockCreditDBBean.getBalance();
                }
                return creditBalance;
            }

            logger.info("MockCredit entries missing for gsCustomerId : " + customerId);
            return -1;
        }
        finally {

            closeSession(session);
        }
    }
}
