package com.gigsky.database.dao;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.ReferralCodeDBBean;
import com.gigsky.database.bean.ReferralCodeHistoryDBBean;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by pradeepragav on 04/01/17.
 */
public class ReferralCodeDao extends AbstractDao{
    private static final Logger logger = LoggerFactory.getLogger(ReferralCodeDao.class);

    //Add Referral code
    public int addReferralCode(ReferralCodeDBBean referralCodeDBBean){
        Session session = openSession();
        try{
            session.save(referralCodeDBBean);
            return referralCodeDBBean.getId();
        }
        catch (ConstraintViolationException constraintException) {

            logger.warn("Create referralCode Exception: ",constraintException);
            session.close();

            //Get referralCode
            referralCodeDBBean =
                    getActiveReferralCodeForChannel(referralCodeDBBean.getCustomerId(), Configurations.ReferralCodeChannelType.DEFAULT);

            if(referralCodeDBBean !=null)
            {
                logger.warn("Create referralCode, found referralCode, referralCodeId : " + referralCodeDBBean.getId());
                return referralCodeDBBean.getId();
            }
            else
            {
                logger.warn("Create referralCode, referralCode not found ");
                return -1;
            }
        }
        catch (Exception e) {
            logger.warn("Create referralCode Exception: ",e);
            return -1;
        }
        finally {
            closeSession(session);
        }
    }

    //Delete Referral code
    public void deleteReferralCode(ReferralCodeDBBean referralCodeDBBean){
        Session session = openSession();
        try {
            session.delete(referralCodeDBBean);
        }
        finally {
            closeSession(session);
        }
    }

    //Update Referral code
    public void updateReferralCode(ReferralCodeDBBean referralCodeDBBean){
        Session session = openSession();
        try{
            session.saveOrUpdate(referralCodeDBBean);
        }
        finally {
            closeSession(session);
        }
    }

    //Get Referral code by referralId
    public ReferralCodeDBBean getReferralCode(int referralId){
        Session session = openSession();
        try{
            ReferralCodeDBBean referralCodeDBBean =(ReferralCodeDBBean) session.createCriteria(ReferralCodeDBBean.class)
                    .add(Restrictions.eq("id",referralId))
                    .uniqueResult();

            if(referralCodeDBBean!=null){
                return referralCodeDBBean;
            }

            logger.warn("ReferralCode entry missing for referralId :"+referralId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    //Get Referral code by referralCode
    public ReferralCodeDBBean getReferralCodeInfoByReferralCode(String referralCode, int tenantId){
        Session session = openSession();
        try{
            ReferralCodeDBBean referralCodeDBBean =(ReferralCodeDBBean) session.createCriteria(ReferralCodeDBBean.class)
                    .add(Restrictions.eq("referralCode",referralCode))
                    .add(Restrictions.eq("tenantId",tenantId))
                    .uniqueResult();

            if(referralCodeDBBean!=null){
                return referralCodeDBBean;
            }

            logger.warn("ReferralCode entry missing for referralCode :"+referralCode);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    //Get ReferralCodes for customerId
    public List<ReferralCodeDBBean> getReferralCodesByCustomerId(int customerId, int tenantId){
        Session session = openSession();
        try {
            List<ReferralCodeDBBean> referralCodeDBBeans =(List<ReferralCodeDBBean>)session.createCriteria(ReferralCodeDBBean.class)
                    .add(Restrictions.eq("customerId",customerId))
                    .add(Restrictions.eq("tenantId",tenantId))
                    .addOrder(Order.asc("id")).list();

            if(referralCodeDBBeans != null){
                return referralCodeDBBeans;
            }

            logger.warn("ReferralCodes missing for customerId :"+customerId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    //Get ReferralCodes for incentiveScheme
    public List<ReferralCodeDBBean> getReferralCodesByIncentiveSchemeId(int incentiveSchemeId){
        Session session = openSession();
        try{
            List<ReferralCodeDBBean> referralCodeDBBeans = (List<ReferralCodeDBBean>)session.createCriteria(ReferralCodeDBBean.class)
                    .add(Restrictions.eq("incentiveSchemeId",incentiveSchemeId))
                    .addOrder(Order.asc("id")).list();
            if(referralCodeDBBeans!=null){
                return referralCodeDBBeans;
            }

            logger.warn("ReferralCodes missing for incentiveSchemeId :"+incentiveSchemeId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    //Get referral code for channel
    public ReferralCodeDBBean getActiveReferralCodeForChannel(int customerId,String channel){
        Session session = openSession();
        try {
            ReferralCodeDBBean referralCodeDBBean =(ReferralCodeDBBean)session.createCriteria(ReferralCodeDBBean.class)
                    .add(Restrictions.eq("customerId",customerId))
                    .add(Restrictions.eq("channel",channel))
                    .add(Restrictions.eq("isActive",(byte)1))
                    .uniqueResult();
            if(referralCodeDBBean!=null){
                return referralCodeDBBean;
            }

            logger.warn("ReferralCode entry missing for channel: "+channel);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    //Get ReferralCodes for incentiveScheme and customer Id
//    public List<ReferralCodeDBBean> getReferralCodesByIncentiveSchemeIdForCustomer(int incentiveSchemeId, int customerId){
//        Session session = openSession();
//        try{
//            List<ReferralCodeDBBean> referralCodeDBBeans = (List<ReferralCodeDBBean>)session.createCriteria(ReferralCodeDBBean.class)
//                    .add(Restrictions.eq("incentiveSchemeId",incentiveSchemeId))
//                    .add(Restrictions.eq("customerId",customerId))
//                    .addOrder(Order.asc("id")).list();
//            if(referralCodeDBBeans!=null){
//                return referralCodeDBBeans;
//            }
//
//            logger.warn("ReferralCodes missing for incentiveSchemeId :"+incentiveSchemeId +" and customerId :" + customerId);
//            return null;
//        }
//        finally {
//            closeSession(session);
//        }
//    }
//
//    //Get active ReferralCodes for customer Id
//    public List<ReferralCodeDBBean> getActiveReferralCodesForCustomer(int customerId){
//        Session session = openSession();
//        try{
//            List<ReferralCodeDBBean> referralCodeDBBeans = (List<ReferralCodeDBBean>)session.createCriteria(ReferralCodeDBBean.class)
//                    .add(Restrictions.eq("isActive",(byte)1))
//                    .add(Restrictions.eq("customerId",customerId))
//                    .addOrder(Order.asc("id")).list();
//            if(referralCodeDBBeans!=null){
//                return referralCodeDBBeans;
//            }
//
//            logger.warn("Active ReferralCodes missing for customerId :" + customerId);
//            return null;
//        }
//        finally {
//            closeSession(session);
//        }
//    }



    //Referral Code History DAO methods
    //Add referral code history entry
    public int addReferralCodeHistory(ReferralCodeHistoryDBBean referralCodeHistoryDBBean){
        Session session = openSession();
        try{
            session.save(referralCodeHistoryDBBean);
            return referralCodeHistoryDBBean.getId();
        }
        catch (ConstraintViolationException constraintException) {

            //Close session

            session.close();
            logger.warn("Create referralCodeHistory Exception: ", constraintException);
            ReferralCodeHistoryDBBean referralCodeHistory =
                    getReferralCodeHistory(referralCodeHistoryDBBean.getId(), referralCodeHistoryDBBean.getIncentiveSchemeId());
            if(referralCodeHistory!=null)
            {
                return referralCodeHistory.getId();
            }
            return -1;
        }
        catch (Exception e) {
            logger.error("Create referralCodeHistory for referralCodeId "+ referralCodeHistoryDBBean.getReferralCodeId() +" , Exception: ", e);
            return -1;
        }
        finally {
            closeSession(session);
        }
    }

    //Delete Referral code history entry
    public void deleteReferralCodeHistory(ReferralCodeHistoryDBBean referralCodeHistoryDBBean){
        Session session = openSession();
        try {
            session.delete(referralCodeHistoryDBBean);
        }
        finally {
            closeSession(session);
        }
    }

    //Update Referral code history entry
    public void updateReferralCodeHistory(ReferralCodeHistoryDBBean referralCodeHistoryDBBean){
        Session session = openSession();
        try{
            session.saveOrUpdate(referralCodeHistoryDBBean);
        }
        finally {
            closeSession(session);
        }
    }

    //Get the Referral code history entry based on scheme id
    public  ReferralCodeHistoryDBBean getReferralCodeHistory(int referralCodeId, int schemeId)
    {
        Session session = openSession();
        try{
            ReferralCodeHistoryDBBean referralCodeHistoryDBBean =(ReferralCodeHistoryDBBean) session.createCriteria(ReferralCodeHistoryDBBean.class)
                    .add(Restrictions.eq("referralCodeId",referralCodeId))
                    .add(Restrictions.eq("incentiveSchemeId",schemeId))
                    .uniqueResult();

            if(referralCodeHistoryDBBean!=null){
                return referralCodeHistoryDBBean;
            }

            logger.warn("ReferralCodeHistory entry missing for incentiveSchemeId :"+schemeId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    //Get the Referral code history based on referralCodeId
    public List<ReferralCodeHistoryDBBean> getReferralCodeHistory(int referralCodeId)
    {
        Session session = openSession();
        try{
            List<ReferralCodeHistoryDBBean> referralCodeHistoryDBBean =(List<ReferralCodeHistoryDBBean>) session.createCriteria(ReferralCodeHistoryDBBean.class)
                    .add(Restrictions.eq("referralCodeId",referralCodeId)).list();
            if(referralCodeHistoryDBBean!=null){
                return referralCodeHistoryDBBean;
            }

            logger.warn("ReferralCodeHistory entry missing for referralCodeId :"+referralCodeId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    // Gets the latest referral code having the email sub string
    public ReferralCodeDBBean getLatestReferralCodeByEmailSubString(String emailSubString) {
        Session session = openSession();
        try{

            String sql = "SELECT * FROM ReferralCode where referralCode REGEXP '^"+emailSubString+"[0-9]+$' order by convert(right(referralCode, LENGTH(referralCode) - "+emailSubString.length()+"), unsigned integer) desc limit 1";

            final List<ReferralCodeDBBean> referralCodeDBBeans = getReferralCodeBeanList(sql);

            //List<ReferralCodeDBBean> referralCodeDBBeans = (List<ReferralCodeDBBean>)session.createSQLQuery(query)
                    //.setProjection(Projections.property("referralCode"))
                    //.add(Restrictions.and(Restrictions.gt("w_id",100), Restrictions.sqlRestriction(word REGEXP '^[A-Z][A-Za-z]*$')))
                    //.add(Restrictions.like("referralCode", emailSubString+"REGEXP '^[[:digit:]]{10}$'"))
                    //.add(Restrictions.like("referralCode", emailSubString+"%"))
                    //.addOrder(Order.desc("referralCode"))
                    //.list();

            if(CollectionUtils.isNotEmpty(referralCodeDBBeans)){
                return referralCodeDBBeans.get(0);
            }

            logger.warn("ReferralCodes missing for email substring : "+emailSubString);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    private List<ReferralCodeDBBean> getReferralCodeBeanList(String sql){

        Session session = openSession();
        final List<ReferralCodeDBBean> referralCodeBeanList = new ArrayList<ReferralCodeDBBean>();
        if(session != null){
            try {
                Query query = session.createSQLQuery(sql);
                query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);

                List list = query.list();
                Iterator iterator = list.iterator();
                while(iterator.hasNext()){
                    Map map=(Map)iterator.next();
                    ReferralCodeDBBean referralCodeDBBean = new ReferralCodeDBBean();
                    referralCodeDBBean.setReferralCode((String) map.get("referralCode"));
                    referralCodeBeanList.add(referralCodeDBBean);
                }
            }
            finally {
                closeSession(session);
            }
        }
        return referralCodeBeanList;
    }

}
