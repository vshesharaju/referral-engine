package com.gigsky.database.dao;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.PromoEventDBBean;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by prsubbareddy on 30/11/17.
 */
public class PromoEventDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(PromoEventDao.class);

    //Add Promo Event
    public int addPromoEvent(PromoEventDBBean promoEventDBBean) {
        Session session = openSession();

        try {
            session.save(promoEventDBBean);
            return promoEventDBBean.getId();
        } finally {
            closeSession(session);
        }
    }


    //Update Promo Event
    public void updatePromoEvent(PromoEventDBBean promoEventDBBean) {
        Session session = openSession();

        try {
            session.saveOrUpdate(promoEventDBBean);
        } finally {
            closeSession(session);
        }
    }

    public void updatePromoEvent(Session session, PromoEventDBBean promoEventDBBean) {
        session.saveOrUpdate(promoEventDBBean);
    }


    //Delete Promo Event
    public void deletePromoEvent(PromoEventDBBean promoEventDBBean) {
        Session session = openSession();

        try {
            session.delete(promoEventDBBean);
        } finally {
            closeSession(session);
        }
    }

    public void deleteAllPromoEvents()
    {
        String query = "DELETE FROM PromoEventDBBean";
        Session session = openSession();

        try
        {
            session.createQuery(query).executeUpdate();
        }
        finally {
            closeSession(session);
        }

    }



    //Get promo event by id
    public PromoEventDBBean getPromoEvent(int promoEventId) {
        Session session = openSession();
        try {
            PromoEventDBBean promoEventDBBean = (PromoEventDBBean) session.createCriteria(PromoEventDBBean.class).
                    add(Restrictions.eq("id", promoEventId))
                    .uniqueResult();
            if (promoEventDBBean != null) {
                return promoEventDBBean;
            }
            logger.warn("PromoEvent entry missing for promoEventId: " + promoEventId);
            return null;
        } finally {
            closeSession(session);
        }
    }


    //Get promo event by id
    public PromoEventDBBean getPromoEvent(Session session,int promoEventId) {

            PromoEventDBBean promoEventDBBean = (PromoEventDBBean) session.createCriteria(PromoEventDBBean.class).
                    add(Restrictions.eq("id", promoEventId))
                    .uniqueResult();
            if (promoEventDBBean != null) {
                return promoEventDBBean;
            }
            logger.warn("PromoEvent entry missing for promoEventId: " + promoEventId);
            return null;
    }
//
//
//    //Get promo event by type
//    public PromoEventDBBean getPromoEvent(String eventType) {
//        Session session = openSession();
//        try {
//            PromoEventDBBean promoEventDBBean = (PromoEventDBBean) session.createCriteria(PromoEventDBBean.class).
//                    add(Restrictions.eq("eventType", eventType))
//                    .uniqueResult();
//            if (promoEventDBBean != null) {
//                return promoEventDBBean;
//            }
//            logger.warn("PromoEvent entry missing for eventType: " + eventType);
//            return null;
//        } finally {
//            closeSession(session);
//        }
//    }

    //Returns an event which is not duplicate
    public PromoEventDBBean getPromoEvent(int customerId, String eventType, String keyString, int tenantId)
    {
        Session session = openSession();
        try
        {
            Criteria criteria =  session.createCriteria(PromoEventDBBean.class)
                    .add(Restrictions.eq("eventType", eventType))
                    .add(Restrictions.eq("customerId", customerId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .add(Restrictions.like("eventData", keyString, MatchMode.ANYWHERE))
                    .add(Restrictions.ne("status", Configurations.StatusType.DUPLICATE_ENTRY));

            //Get unique
            return (PromoEventDBBean) criteria.uniqueResult();
        }
        finally
        {
            closeSession(session);
        }
    }

    public List<PromoEventDBBean> getPromoEvent(int customerId, int tenantId, String eventType)
    {
        Session session = openSession();
        try
        {

            List<PromoEventDBBean> promoEventDBBeans = (List<PromoEventDBBean>)session.createCriteria(PromoEventDBBean.class)
                    .add(Restrictions.eq("customerId", customerId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .add(Restrictions.eq("eventType", eventType))
                    .list();

            if(promoEventDBBeans != null)
            {
                return promoEventDBBeans;
            }

            logger.warn("PromoEvents not found for type : ", eventType);
            return null;

        }
        finally
        {
            closeSession(session);
        }
    }

    public List<PromoEventDBBean> getPromoEvents(String status, int count)
    {
        Session session = openSession();

        try
        {
            List<PromoEventDBBean> promoEventDBBeans = (List<PromoEventDBBean>) session.createCriteria(PromoEventDBBean.class)
                    .add(Restrictions.eq("status", status))
                    .setMaxResults(count)
                    .addOrder(Order.asc("id"))
                    .list();

            if(promoEventDBBeans != null)
            {
                return promoEventDBBeans;
            }

            logger.warn("PromoEvents not found for status : ", status);
            return null;
        }
        finally
        {
            session.close();
        }
    }
}
