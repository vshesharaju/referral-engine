package com.gigsky.database.dao;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.InvitationDBBean;
import com.gigsky.database.bean.ReferralCodeDBBean;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by pradeepragav on 04/01/17.
 */
public class InvitationDao extends AbstractDao {
    private static final Logger logger = LoggerFactory.getLogger(InvitationDao.class);

    @Autowired
    CustomerDao customerDao;

    @Autowired
    ReferralCodeDao referralCodeDao;

    //Add invitation
    public int addInvitation(InvitationDBBean invitationDBBean){
        Session session = openSession();
        try{
            session.save(invitationDBBean);
            return invitationDBBean.getId();
        }
        finally {
            closeSession(session);
        }
    }

    //Delete invitation
    public void deleteInvitation(InvitationDBBean invitationDBBean){
        Session session = openSession();
        try{
            session.delete(invitationDBBean);
        }
        finally {
            closeSession(session);
        }
    }

    //Update invitation
    public void updateInvitation(InvitationDBBean invitationDBBean){
        Session session = openSession();
        try{
            session.saveOrUpdate(invitationDBBean);
        }
        finally {
            closeSession(session);
        }

    }

    //Get invitaiton by id
    public InvitationDBBean getInvitationById(int invitationId){
        Session session = openSession();
        try {
            InvitationDBBean invitationDBBean = (InvitationDBBean) session.createCriteria(InvitationDBBean.class).
                    add(Restrictions.eq("id",invitationId))
                    .uniqueResult();
            if(invitationDBBean!=null){
                return invitationDBBean;
            }
            logger.warn("Invitation entry missing for invitationId: "+invitationId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

//    public InvitationDBBean getInvitationByEmailIdForReferralCode(String referralCode, String emailId){
//        Session session = openSession();
//        try{
//            //Get referralCodeDBBean for referralCodeId
//            ReferralCodeDBBean referralCodeDBBean = referralCodeDao.getReferralCodeInfoByReferralCode(referralCode);
//
//            if(referralCodeDBBean==null){
//                //ReferrralCode entry not found
//                return null;
//            }
//
//            InvitationDBBean invitationDBBean = (InvitationDBBean) session.createCriteria(InvitationDBBean.class).
//                    add(Restrictions.eq("emailId",emailId))
//                    .add(Restrictions.eq("referralCodeId",referralCodeDBBean.getId()))
//                    .uniqueResult();
//
//            if(invitationDBBean!=null){
//                return invitationDBBean;
//            }
//
//            logger.warn("Invitation entry missing for emailId: "+emailId);
//            return null;
//        }
//        finally {
//            closeSession(session);
//        }
//    }
//
//    //Get invitation by emailId
//    public InvitationDBBean getInvitationByEmailId(String emailId){
//        Session session = openSession();
//        try{
//
//            InvitationDBBean invitationDBBean = (InvitationDBBean) session.createCriteria(InvitationDBBean.class).
//                    add(Restrictions.eq("emailId",emailId))
//                    .uniqueResult();
//
//            if(invitationDBBean!=null){
//                return invitationDBBean;
//            }
//
//            logger.warn("Invitation entry missing for emailId: "+emailId);
//            return null;
//        }
//        finally {
//            closeSession(session);
//        }
//    }

    //Get invitation by targetCustomerId
    public InvitationDBBean getInvitationByTargetCustomerId(int targetCustomerId){
        Session session = openSession();
        try{

            InvitationDBBean invitationDBBean = (InvitationDBBean) session.createCriteria(InvitationDBBean.class).
                    add(Restrictions.eq("targetCustomerId",targetCustomerId))
                    .uniqueResult();

            if(invitationDBBean!=null){
                return invitationDBBean;
            }

            logger.warn("Invitation entry missing for targetCustomerId: "+targetCustomerId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    //Get invitations for given referral code
    public List<InvitationDBBean> getInvitationsForReferralCode(int referralCodeId){
        Session session = openSession();
        try{
            List<InvitationDBBean> invitationDBBeans = (List<InvitationDBBean>) session.createCriteria(InvitationDBBean.class)
                    .add(Restrictions.eq("referralCodeId",referralCodeId))
                    .addOrder(Order.asc("id"))
                    .list();

            if(invitationDBBeans!=null){
                return invitationDBBeans;
            }
            logger.warn("Invtitations missing for referralCodeId :"+referralCodeId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    //Get invitations for given Customer
    public List<InvitationDBBean> getInvitationsForCustomer(int customerId, int startIndex, int count, int tenantId){
        Session session = openSession();
        try {
            CustomerDBBean customerDBBean = customerDao.getCustomerInfo(customerId, tenantId);
            int id = customerDBBean.getId();

            if(id>0) {

                String queryStr = "select I from ReferralCodeDBBean R, InvitationDBBean I where R.id = I.referralCodeId and R.customerId =:customerId order by I.id asc";

                List invitationDBBeans = null;

                //In case of startIndex and count are intentionally set to Zero

                //If the startIndex and count is intentionally set to zero
                if(startIndex == 0 && count == 0) return null;

                invitationDBBeans =  session.createQuery(queryStr)
                        .setParameter("customerId", id)
                        .setFirstResult(startIndex)
                        .setMaxResults(count)
                        .list();


                if(CollectionUtils.isNotEmpty(invitationDBBeans)){
                    return invitationDBBeans;
                }

                logger.warn("Invitation entries not found for customerId: "+customerId);
            }
            //Customer entry not found
            return null;
        }
        finally {
            closeSession(session);
        }
    }

    // Get the count of invitations for customer
    public int getInvitationCountForCustomer(int customerId, int tenantId){
        Session session = openSession();
        try {
            int id = customerDao.getCustomerInfo(customerId, tenantId).getId();
            if(id>0) {
                String queryStr = "select I " +
                        "from ReferralCodeDBBean R, InvitationDBBean I " +
                        "where R.id = I.referralCodeId and " +
                        "R.customerId =:customerId";

                List invitationDBBeans = session.createQuery(queryStr)
                        .setParameter("customerId", id)
                        .list();
                if(invitationDBBeans!=null){
                    return invitationDBBeans.size();
                }
                logger.warn("Invitation entries not found for customerId: "+customerId);
            }
            //Customer entry not found
            return 0;
        }
        finally {
            closeSession(session);
        }
    }

    // increment resend count of invitation by using invitationId
    public void incrementInvitationResendCount(int invitationId){
        Session session = openSession();
        try {
            InvitationDBBean invitationDBBean = getInvitationById(invitationId);
            if(invitationDBBean == null){
                logger.error("Failed to fetch invitation for id: "+invitationId);
            }
            String queryStr = "update InvitationDBBean set resendCount = resendCount+1 where id=:invitationId";
            session.createQuery(queryStr)
                    .setParameter("invitationId",invitationId)
                    .executeUpdate();
        }
        finally {
            closeSession(session);
        }
    }

    //Get invitation for given AdvocateCustomer and targetCustomerId
//    public InvitationDBBean getInvitationForAdvocateIdAndNewCustomerId(int advocateCustomerId, int targetCustomerId){
//        Session session = openSession();
//        try {
//            CustomerDBBean advocateCustomerDBBean = customerDao.getCustomerInfo(advocateCustomerId);
//            CustomerDBBean targetCustomerDBBean = customerDao.getCustomerInfo(targetCustomerId);
//
//            if(advocateCustomerDBBean.getId() > 0 && targetCustomerDBBean.getId() > 0) {
//
//                String queryStr = "select I from ReferralCodeDBBean R, InvitationDBBean I " +
//                        "where R.id = I.referralCodeId and " +
//                        "R.customerId =:advocateCustomerId and " +
//                        "I.targetCustomerId =:targetCustomerIdentity";
//
//                InvitationDBBean invitationDBBean = (InvitationDBBean) session.createQuery(queryStr)
//                        .setParameter("advocateCustomerId", advocateCustomerDBBean.getId())
//                        .setParameter("targetCustomerIdentity",targetCustomerDBBean.getId())
//                        .list();
//
//                if(invitationDBBean!=null){
//                    return invitationDBBean;
//                }
//                logger.warn("Invitation entries not found for advocateCustomerId: "
//                        +advocateCustomerDBBean.getId() + " and targetCustomerId : "+ targetCustomerDBBean.getId());
//                return null;
//            }
//            else{
//                //Customer entry not found
//                logger.warn("Invitation entries not found for advocateCustomerId: "
//                        +advocateCustomerDBBean.getId() + " and targetCustomerId : "+ targetCustomerDBBean.getId());
//                return null;
//            }
//        }
//        finally {
//            closeSession(session);
//        }
//    }

    public List<InvitationDBBean> getInvitationsForCreditFailed(String status, int maxCount) {
        Session session = openSession();
        try{

            List<InvitationDBBean> listInvitations = (List<InvitationDBBean>) session.createCriteria(InvitationDBBean.class)
                    .add(Restrictions.eq(status, Configurations.StatusType.CREDIT_ADD_FAILED))
                    .setMaxResults(maxCount)
                    .addOrder(Order.asc("id"))
                    .list();

            if(listInvitations != null){
                return listInvitations;
            }

            logger.warn("Invitations entry missing for credit failed status " + status);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

}
