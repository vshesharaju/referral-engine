package com.gigsky.database.dao;

import com.gigsky.database.bean.TenantDBBean;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Created by shwethars on 29/01/19.
 */
public class TenantMockDao extends AbstractDao{

    private static final Logger logger = LoggerFactory.getLogger(TenantMockDao.class);

    public ArrayList<TenantDBBean> addTenants(){

        ArrayList<TenantDBBean> inputTenantDBBeans = new ArrayList<TenantDBBean>();
        ArrayList<TenantDBBean> outputTenantDBBeans = new ArrayList<TenantDBBean>();

        TenantDBBean t1 = new TenantDBBean();
        t1.setId(100);
        t1.setName("Test1");
        t1.setDescription("Test1 Tenant");
        inputTenantDBBeans.add(t1);



        TenantDBBean t2 = new TenantDBBean();
        t2.setId(101);
        t2.setName("Test2");
        t2.setDescription("Test2 Tenant");
        inputTenantDBBeans.add(t2);

        Session session = openSession();
        Transaction txn = null;

        try {

            txn = session.beginTransaction();

            for (TenantDBBean tenant : inputTenantDBBeans) {

                session.save(tenant);

                outputTenantDBBeans.add(tenant);
            }

            //Commit transaction
            txn.commit();

            //Set transaction to null (To rollback)
            txn = null;

            return outputTenantDBBeans;

        } catch (Exception e) {

            if (txn != null) {
                txn.rollback();
            }

            logger.error("AbstractTenantMockData: Failed to add tenants", e);
            return null;
        } finally {
            closeSession(session);
        }
    }

    public TenantDBBean addTenant(TenantDBBean tenantDBBean){
        Session session = openSession();
        Transaction txn = null;

        try {

            txn = session.beginTransaction();

            session.save(tenantDBBean);

            //Commit transaction
            txn.commit();

            //Set transaction to null (To rollback)
            txn = null;

            return tenantDBBean;

        } catch (Exception e) {

            if (txn != null) {
                txn.rollback();
            }

            logger.error("AbstractTenantMockData: Failed to add tenant", e);
            return null;
        } finally {
            closeSession(session);
        }
    }

    public void deleteTenants(ArrayList<TenantDBBean> tenants) {
        Session session = openSession();
        Transaction txn = null;

        try {

            txn = session.beginTransaction();

            for (TenantDBBean tenant : tenants) {

                session.delete(tenant);
            }

            //Commit transaction
            txn.commit();

            //Set transaction to null (To rollback)
            txn = null;

        } catch (Exception e) {

            if (txn != null) {
                txn.rollback();
            }

            logger.error("AbstractTenantMockData: Failed to delete tenants", e);
        } finally {
            closeSession(session);
        }
    }

    public void deleteTenant(TenantDBBean tenant) {
        Session session = openSession();
        Transaction txn = null;

        try {

            txn = session.beginTransaction();

            session.delete(tenant);

            //Commit transaction
            txn.commit();

            //Set transaction to null (To rollback)
            txn = null;

        } catch (Exception e) {

            if (txn != null) {
                txn.rollback();
            }

            logger.error("AbstractTenantMockData: Failed to delete tenant", e);
        } finally {
            closeSession(session);
        }
    }
}
