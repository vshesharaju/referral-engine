package com.gigsky.database.dao;

import com.gigsky.database.bean.PromoCreditDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by prsubbareddy on 30/11/17.
 */
public class PromoCreditDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(PromoCreditDao.class);

//    // Add Promo Credit
//    public int addPromoCredit(PromoCreditDBBean promoCreditDBBean) {
//        Session session = openSession();
//
//        try {
//            session.save(promoCreditDBBean);
//            return promoCreditDBBean.getId();
//        } finally {
//            closeSession(session);
//        }
//    }
//
//
//    // Update Promo Credit
//    public void updatePromoCredit(PromoCreditDBBean promoCreditDBBean) {
//        Session session = openSession();
//
//        try {
//            session.saveOrUpdate(promoCreditDBBean);
//        } finally {
//            closeSession(session);
//        }
//    }
//
//
//    // Delete Promo Credit
//    public void deletePromoCredit(PromoCreditDBBean promoCreditDBBean) {
//        Session session = openSession();
//
//        try {
//            session.delete(promoCreditDBBean);
//        } finally {
//            closeSession(session);
//        }
//    }
//
//
//    // Get Promo Credit by id
//    public PromoCreditDBBean getPromoCredit(int promoCreditId) {
//        Session session = openSession();
//        try {
//            PromoCreditDBBean promoCreditDBBean = (PromoCreditDBBean) session.createCriteria(PromoCreditDBBean.class).
//                    add(Restrictions.eq("id", promoCreditId))
//                    .uniqueResult();
//            if (promoCreditDBBean != null) {
//                return promoCreditDBBean;
//            }
//            logger.warn("PromoCredit entry missing for promoCreditId: " + promoCreditId);
//            return null;
//        } finally {
//            closeSession(session);
//        }
//    }


    // Get Promo credit by promo id and currency
    public PromoCreditDBBean getPromoCredit(int promoId, String currency) {
        Session session = openSession();
        try {
            PromoCreditDBBean promoCreditDBBean = (PromoCreditDBBean) session.createCriteria(PromoCreditDBBean.class)
                    .add(Restrictions.eq("promotionId", promoId))
                    .add(Restrictions.eq("currency", currency))
                    .uniqueResult();
            if (promoCreditDBBean != null) {
                return promoCreditDBBean;
            }
            logger.warn("PromoCredit entry missing for promoId: " + promoId + " and for currency: " + currency);
            return null;
        } finally {
            closeSession(session);
        }
    }

    public PromoCreditDBBean getPromoCredit(Session session, int promoId, String currency) {
            PromoCreditDBBean promoCreditDBBean = (PromoCreditDBBean) session.createCriteria(PromoCreditDBBean.class)
                    .add(Restrictions.eq("promotionId", promoId))
                    .add(Restrictions.eq("currency", currency))
                    .uniqueResult();
            if (promoCreditDBBean != null) {
                return promoCreditDBBean;
            }
            logger.warn("PromoCredit entry missing for promoId: " + promoId + " and for currency: " + currency);
            return null;

    }


    // Get Promo credits by promo Id
    public List<PromoCreditDBBean> getPromoCreditList(int promoId){
        Session session = openSession();

        try {
            List<PromoCreditDBBean> promoCreditDBBeans = (List<PromoCreditDBBean>) session.createCriteria(PromoCreditDBBean.class)
                    .add(Restrictions.eq("promotionId",promoId))
                    .addOrder(Order.asc("id"))
                    .list();

            if(promoCreditDBBeans!=null){
                return promoCreditDBBeans;
            }
            logger.warn("PromoCredits entry missing for promoId: "+promoId);
            return null;
        }
        finally {
            closeSession(session);
        }
    }

}
