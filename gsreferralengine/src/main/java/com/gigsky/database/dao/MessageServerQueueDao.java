package com.gigsky.database.dao;

import com.gigsky.backend.messages.dbbeans.MessagingQueueDetails;
import com.gigsky.database.bean.MessageServerClusterDBBean;
import com.gigsky.database.bean.MessageServerClusterNodeDBBean;
import com.gigsky.database.bean.MessageServerQueueDBBean;
import com.gigsky.gscommon.util.encryption.EncryptionFactory;
import com.gigsky.gscommon.util.encryption.GigSkyEncryptor;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pradeepragav on 10/04/17.
 */
public class MessageServerQueueDao extends AbstractDao {

    public static final String DEFAULT_QUEUE_NAME = "hello.direct";
    public static final String DEFAULT_HOST_NAME = "10.0.15.52";
    public static final String DEFAULT_USER_NAME = "rabbitmq";
    public static final String DEFAULT_PASSWORD = "only4G1gsky";
    public static final String DEFAULT_ROUTINGKEY = "nmr.routingKey2";
    public static final String DEFAULT_EXCHANGENAME = "nmr.direct2";

    @Autowired
    MessageServerClusterNodeDao messageServerClusterNodeDao;

    @Autowired
    MessageServerClusterDao messageServerClusterDao;

    @Autowired
    private EncryptionFactory encryptionFactory;

//    public void addMessageQueue(MessageServerQueueDBBean messageServerQueueDBBean){
//
//
//    }
//
//    public MessageServerQueueDBBean getMessageQueue(int queueId){
//
//        Session session = openSession();
//
//        try{
//
//            MessageServerQueueDBBean messageServerQueueDBBean =  (MessageServerQueueDBBean)session.createCriteria(MessageServerQueueDBBean.class)
//                    .add(Restrictions.eq("id",queueId))
//                    .uniqueResult();
//            if(messageServerQueueDBBean != null){
//
//                return messageServerQueueDBBean;
//            }
//        }
//        finally {
//            closeSession(session);
//        }
//        return null;
//    }

    public MessageServerQueueDBBean getMessageQueue(String queueName){

        Session session = openSession();

        try{

            MessageServerQueueDBBean messageServerQueueDBBean = (MessageServerQueueDBBean)session.createCriteria(MessageServerQueueDBBean.class)
                    .add(Restrictions.eq("queueName",queueName))
                    .uniqueResult();

            if(messageServerQueueDBBean != null){
                return messageServerQueueDBBean;
            }
        }
        finally {
            closeSession(session);
        }
        return null;
    }

    public MessagingQueueDetails getMessagingQueueDetails(String queueName){

        //Get queueDetails from MessageServerQueueDao
        MessageServerQueueDBBean messageServerQueueDBBean = getMessageQueue(queueName);

        //If null
        if(messageServerQueueDBBean == null){
            return getDefaultMessagingQueueDetails();
        }

        //Create MessagingQueueDetails bean using the obtained info
        MessagingQueueDetails messagingQueueDetails = new MessagingQueueDetails();

        messagingQueueDetails.setQueueId(messageServerQueueDBBean.getId());
        messagingQueueDetails.setExchangeName(messageServerQueueDBBean.getExchangeName());
        messagingQueueDetails.setQueueName(messageServerQueueDBBean.getQueueName());
        messagingQueueDetails.setRoutingKey(messageServerQueueDBBean.getRoutingKey());

        //Get ClusterNodes from MessageServerClusterDao using clusterId from the prev step
        List<MessageServerClusterNodeDBBean> messageServerClusterNodeDBBeans =
                messageServerClusterNodeDao.getNodesForCluster(messageServerQueueDBBean.getMessageServerClusterId());

        //If null
        if(CollectionUtils.isEmpty(messageServerClusterNodeDBBeans)){
            return getDefaultMessagingQueueDetails();
        }

        messagingQueueDetails.setClusterNodes(messageServerClusterNodeDBBeans);


        //Get cluster
        MessageServerClusterDBBean messageServerClusterDBBean =
                messageServerClusterDao.getMessageCluster(messageServerQueueDBBean.getMessageServerClusterId());

        //If null
        if(messageServerClusterDBBean == null){

            return getDefaultMessagingQueueDetails();
        }

        messagingQueueDetails.setClusterId(messageServerQueueDBBean.getMessageServerClusterId());
        messagingQueueDetails.setClusterName(messageServerClusterDBBean.getClusterName());
        messagingQueueDetails.setEncVersion(messageServerClusterDBBean.getEncVersion());
        messagingQueueDetails.setVirtualHost(messageServerClusterDBBean.getVirtualHost());

        //Decrypt username and password
        GigSkyEncryptor encryptor = encryptionFactory.getEncryptor(messageServerClusterDBBean.getEncVersion());

        String username = encryptor.decryptString(messageServerClusterDBBean.getUserName());
        String password = encryptor.decryptString(messageServerClusterDBBean.getPassword());

        messagingQueueDetails.setUserName(username);
        messagingQueueDetails.setPassword(password);

        return messagingQueueDetails;
    }

    public MessagingQueueDetails getDefaultMessagingQueueDetails(){


        MessagingQueueDetails queueDetails = new MessagingQueueDetails();
        queueDetails.setQueueId(1);
        queueDetails.setExchangeName(DEFAULT_EXCHANGENAME);
        queueDetails.setQueueName(DEFAULT_QUEUE_NAME);
        queueDetails.setRoutingKey(DEFAULT_ROUTINGKEY);
        queueDetails.setUserName(DEFAULT_USER_NAME);
        queueDetails.setPassword(DEFAULT_PASSWORD);

        List<MessageServerClusterNodeDBBean> clusterNodes = new ArrayList<MessageServerClusterNodeDBBean>();
        MessageServerClusterNodeDBBean clusterNode = new MessageServerClusterNodeDBBean();
        clusterNode.setId(1);
        clusterNode.setHost("10.0.15.52");
//        serverDetails.setPort("15672");
        clusterNodes.add(clusterNode);

        queueDetails.setClusterNodes(clusterNodes);

        return queueDetails;
    }
}
