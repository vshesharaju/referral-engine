package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by shwethars on 09/01/19.
 */
@Entity
@Table(name = "TenantConfigurationsKeyValue", schema = "gigskyReferralEngine")
@IdClass(TenantConfigurationsKeyValueDBBeanPK.class)
public class TenantConfigurationsKeyValueDBBean {
    private String ckey;
    private int tenantId;
    private String value;
    private Timestamp createTime;
    private Timestamp updateTime;

    @Id
    @Column(name = "ckey", nullable = false, length = 250)
    public String getCkey() {
        return ckey;
    }

    public void setCkey(String ckey) {
        this.ckey = ckey;
    }


    @Id
    @Basic
    @Column(name = "tenant_id", nullable = false)
    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }


    @Basic
    @Column(name = "value", nullable = false, length = -1)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }


    @Basic
    @Column(name = "createTime", nullable = true, insertable = false, updatable = false)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }


    @Basic
    @Column(name = "updateTime", nullable = true, insertable = false, updatable = false)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TenantConfigurationsKeyValueDBBean that = (TenantConfigurationsKeyValueDBBean) o;

        if (ckey != null ? !ckey.equals(that.ckey) : that.ckey != null) return false;
        if (tenantId != that.tenantId) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ckey != null ? ckey.hashCode() : 0;
        result = 31 * result + tenantId;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }
}
