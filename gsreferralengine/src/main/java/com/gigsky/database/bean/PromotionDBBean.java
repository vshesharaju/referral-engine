package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by prsubbareddy on 30/11/17.
 */
@Entity
@Table(name = "Promotion", schema = "gigskyReferralEngine", catalog = "")
public class PromotionDBBean {
    private int id;
    private String name;
    private String promoCode;
    private Date startDate;
    private Date endDate;
    private Integer creditExpiryPeriod;
    private Timestamp createTime;
    private Timestamp updateTime;
    private int tenantId;
    private Byte promoEnabled;
    private Byte activePromo;
    private Integer currentRedeemCount;
    private Integer maxRedeemCount;



    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "promoCode", nullable = true, length = 45)
    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    @Basic
    @Column(name = "startDate", nullable = true)
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "endDate", nullable = true)
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "creditExpiryPeriod", nullable = true)
    public Integer getCreditExpiryPeriod() {
        return creditExpiryPeriod;
    }

    public void setCreditExpiryPeriod(Integer creditExpiryPeriod) {
        this.creditExpiryPeriod = creditExpiryPeriod;
    }

    @Basic
    @Column(name = "createTime", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "tenant_id", nullable = false)
    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    @Basic
    @Column(name = "promoEnabled", nullable = true)
    public Byte getPromoEnabled() {
        return promoEnabled;
    }

    public void setPromoEnabled(Byte promoEnabled) {
        this.promoEnabled = promoEnabled;
    }

    @Basic
    @Column(name = "activePromo", nullable = true)
    public Byte getActivePromo() {
        return activePromo;
    }

    public void setActivePromo(Byte activePromo) {
        this.activePromo = activePromo;
    }

    @Basic
    @Column(name = "maxRedeemCount", nullable = true)
    public Integer getMaxRedeemCount() {
        return maxRedeemCount;
    }


    public void setMaxRedeemCount(Integer maxRedeelCount) {
        this.maxRedeemCount = maxRedeelCount;
    }

    @Basic
    @Column(name = "currentRedeemCount", nullable = true)
    public Integer getCurrentRedeemCount() {
        return currentRedeemCount;
    }

    public void setCurrentRedeemCount(Integer currentRedeemCount) {
        this.currentRedeemCount = currentRedeemCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PromotionDBBean that = (PromotionDBBean) o;

        if (id != that.id) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (promoCode != null ? !promoCode.equals(that.promoCode) : that.promoCode != null) return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (creditExpiryPeriod != null ? !creditExpiryPeriod.equals(that.creditExpiryPeriod) : that.creditExpiryPeriod != null)
            return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
        if (tenantId != that.tenantId) return false;
        if (promoEnabled != null ? !promoEnabled.equals(that.promoEnabled) : that.promoEnabled != null) return false;
        if (activePromo != null ? !activePromo.equals(that.activePromo) : that.activePromo != null) return false;
        if (maxRedeemCount != null ? !maxRedeemCount.equals(that.maxRedeemCount) : that.maxRedeemCount != null)
            return false;
        if (currentRedeemCount != null ? !currentRedeemCount.equals(that.currentRedeemCount) : that.currentRedeemCount != null)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (promoCode != null ? promoCode.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (creditExpiryPeriod != null ? creditExpiryPeriod.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + tenantId;
        result = 31 * result + (promoEnabled != null ? promoEnabled.hashCode() : 0);
        result = 31 * result + (activePromo != null ? activePromo.hashCode() : 0);
        result = 31 * result + (maxRedeemCount != null ? maxRedeemCount.hashCode() : 0);
        result = 31 * result + (currentRedeemCount != null ? currentRedeemCount.hashCode() : 0);
        return result;
    }
}
