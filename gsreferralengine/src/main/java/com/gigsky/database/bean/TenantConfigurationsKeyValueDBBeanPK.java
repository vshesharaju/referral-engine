package com.gigsky.database.bean;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by shwethars on 25/01/19.
 */
public class TenantConfigurationsKeyValueDBBeanPK implements Serializable {
    private String ckey;
    private int tenantId;

    @Column(name = "ckey", nullable = false, length = 250)
    @Id
    public String getCkey() {
        return ckey;
    }

    public void setCkey(String ckey) {
        this.ckey = ckey;
    }

    @Column(name = "tenant_id", nullable = false)
    @Basic
    @Id
    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TenantConfigurationsKeyValueDBBeanPK that = (TenantConfigurationsKeyValueDBBeanPK) o;

        if (tenantId != that.tenantId) return false;
        if (ckey != null ? !ckey.equals(that.ckey) : that.ckey != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ckey != null ? ckey.hashCode() : 0;
        result = 31 * result + tenantId;
        return result;
    }
}
