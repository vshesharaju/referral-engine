package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by vinayr on 03/01/17.
 */
@Entity
@Table(name = "ConfigurationsKeyValue", schema = "gigskyReferralEngine")
public class ConfigurationsKeyValueDBBean {
    private String ckey;
    private String value;
    private Timestamp createTime;
    private Timestamp updateTime;

    @Id
    @Column(name = "ckey", nullable = false, insertable = true, updatable = true, length = 255)
    public String getCkey() {
        return ckey;
    }

    public void setCkey(String ckey) {
        this.ckey = ckey;
    }

    @Basic
    @Column(name = "value", nullable = false, insertable = true, updatable = true, length = 65535)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Basic
    @Column(name = "createTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConfigurationsKeyValueDBBean that = (ConfigurationsKeyValueDBBean) o;

        if (ckey != null ? !ckey.equals(that.ckey) : that.ckey != null) return false;
        if (value != null ? !value.equals(that.value) : that.value != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ckey != null ? ckey.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }
}
