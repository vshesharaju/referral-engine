package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by vinayr on 03/01/17.
 */
@Entity
@Table(name = "ReferralEvent", schema = "gigskyReferralEngine")
public class ReferralEventDBBean {
    private int id;
    private String eventType;
    private String eventData;
    private Timestamp createTime;
    private Timestamp updateTime;
    private int customerId;
    private String status;
    private String statusMessage;
    private int retryCount;
    private String eventMetaData;
    private int tenantId;

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "eventType", nullable = true, insertable = true, updatable = true, length = 45)
    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    @Basic
    @Column(name = "eventData", nullable = true, insertable = true, updatable = true, length = -1)
    public String getEventData() {
        return eventData;
    }

    public void setEventData(String eventData) {
        this.eventData = eventData;
    }

    @Basic
    @Column(name = "createTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "customer_id", nullable = false, insertable = true, updatable = true)
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Basic
    @Column(name = "tenant_id", nullable = false)
    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    @Basic
    @Column(name = "status", nullable = true, length = 45)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "statusMessage", nullable = true, length = 100)
    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Basic
    @Id
    @Column(name = "retryCount", nullable = true, insertable = true, updatable = true)
    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    @Basic
    @Column(name = "eventMetaData", nullable = true, length = -1)
    public String getEventMetaData() {
        return eventMetaData;
    }

    public void setEventMetaData(String eventMetaData) {
        this.eventMetaData = eventMetaData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReferralEventDBBean that = (ReferralEventDBBean) o;

        if (id != that.id) return false;
        if (customerId != that.customerId) return false;
        if (tenantId != that.tenantId) return false;
        if (eventType != null ? !eventType.equals(that.eventType) : that.eventType != null) return false;
        if (eventData != null ? !eventData.equals(that.eventData) : that.eventData != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
        if (retryCount != that.retryCount) return false;
        if (eventMetaData != null ? !eventMetaData.equals(that.eventMetaData) : that.eventMetaData != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (eventType != null ? eventType.hashCode() : 0);
        result = 31 * result + (eventData != null ? eventData.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + customerId;
        result = 31 * result + tenantId;
        result = 31 * result + retryCount;
        result = 31 * result + (eventMetaData != null ? eventMetaData.hashCode() : 0);
        return result;
    }


}
