package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by pradeepragav on 10/04/17.
 */
@Entity
@Table(name = "MessageServerQueue", schema = "gigskyReferralEngine")
public class MessageServerQueueDBBean {
    private int id;
    private String queueName;
    private String exchangeName;
    private String routingKey;
    private Timestamp createTime;
    private Timestamp updateTime;
    private int messageServerClusterId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "queueName", nullable = true, length = 250)
    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    @Basic
    @Column(name = "exchangeName", nullable = true, length = 250)
    public String getExchangeName() {
        return exchangeName;
    }

    public void setExchangeName(String exchangeName) {
        this.exchangeName = exchangeName;
    }

    @Basic
    @Column(name = "routingKey", nullable = true, length = 255)
    public String getRoutingKey() {
        return routingKey;
    }

    public void setRoutingKey(String routingKey) {
        this.routingKey = routingKey;
    }

    @Basic
    @Column(name = "createTime", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "MessageServerCluster_id", nullable = false)
    public int getMessageServerClusterId() {
        return messageServerClusterId;
    }

    public void setMessageServerClusterId(int messageServerClusterId) {
        this.messageServerClusterId = messageServerClusterId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MessageServerQueueDBBean that = (MessageServerQueueDBBean) o;

        if (id != that.id) return false;
        if (messageServerClusterId != that.messageServerClusterId) return false;
        if (queueName != null ? !queueName.equals(that.queueName) : that.queueName != null) return false;
        if (exchangeName != null ? !exchangeName.equals(that.exchangeName) : that.exchangeName != null) return false;
        if (routingKey != null ? !routingKey.equals(that.routingKey) : that.routingKey != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (queueName != null ? queueName.hashCode() : 0);
        result = 31 * result + (exchangeName != null ? exchangeName.hashCode() : 0);
        result = 31 * result + (routingKey != null ? routingKey.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + messageServerClusterId;
        return result;
    }
}
