package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by prsubbareddy on 17/03/17.
 */
@Entity
@Table(name = "ErrorStringLocale", schema = "gigskyReferralEngine")
public class ErrorStringLocaleDBBean {
    private int id;
    private String language;
    private String errorString;
    private Timestamp createTime;
    private Timestamp updateTime;
    private int errorStringId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "language", nullable = true, length = 45)
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Basic
    @Column(name = "errorString", nullable = true, length = -1)
    public String getErrorString() {
        return errorString;
    }

    public void setErrorString(String errorString) {
        this.errorString = errorString;
    }

    @Basic
    @Column(name = "createTime", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Id
    @Column(name = "errorString_id", nullable = false)
    public int getErrorStringId() {
        return errorStringId;
    }

    public void setErrorStringId(int errorStringId) {
        this.errorStringId = errorStringId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ErrorStringLocaleDBBean that = (ErrorStringLocaleDBBean) o;

        if (id != that.id) return false;
        if (errorStringId != that.errorStringId) return false;
        if (language != null ? !language.equals(that.language) : that.language != null) return false;
        if (errorString != null ? !errorString.equals(that.errorString) : that.errorString != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (errorString != null ? errorString.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + errorStringId;
        return result;
    }
}
