package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by vinayr on 03/01/17.
 */
@Entity
@Table(name = "Invitation", schema = "gigskyReferralEngine")
public class InvitationDBBean {
    private int id;
    private String emailId;
    private Timestamp inviteTime;
    private String status;
    private Integer targetCustomerId;
    private Integer advocateCreditId;
    private Integer referralCreditId;
    private Timestamp createTime;
    private Timestamp updateTime;
    private int referralCodeId;
    private Integer resendCount;
    private String advocateCreditStatus;
    private String referralCreditStatus;
    private Integer advocateRetryCount;
    private Integer referralRetryCount;
    private Integer advocateCreditTxnId;
    private Integer referralCreditTxnId;
    private Timestamp accountCreatedOn;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "emailId", nullable = true, insertable = true, updatable = true, length = 45)
    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @Basic
    @Column(name = "inviteTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getInviteTime() {
        return inviteTime;
    }

    public void setInviteTime(Timestamp inviteTime) {
        this.inviteTime = inviteTime;
    }

    @Basic
    @Column(name = "status", nullable = true, insertable = true, updatable = true, length = 45)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "targetCustomerId", nullable = true, insertable = true, updatable = true)
    public Integer getTargetCustomerId() {
        return targetCustomerId;
    }

    public void setTargetCustomerId(Integer targetCustomerId) {
        this.targetCustomerId = targetCustomerId;
    }

    @Basic
    @Column(name = "advocateCreditId", nullable = true, insertable = true, updatable = true)
    public Integer getAdvocateCreditId() {
        return advocateCreditId;
    }

    public void setAdvocateCreditId(Integer advocateCreditTxId) {
        this.advocateCreditId = advocateCreditTxId;
    }

    @Basic
    @Column(name = "referralCreditId", nullable = true, insertable = true, updatable = true)
    public Integer getReferralCreditId() {
        return referralCreditId;
    }

    public void setReferralCreditId(Integer referralCreditTxId) {
        this.referralCreditId = referralCreditTxId;
    }

    @Basic
    @Column(name = "createTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "referralCode_id", nullable = false, insertable = true, updatable = true)
    public int getReferralCodeId() {
        return referralCodeId;
    }

    public void setReferralCodeId(int referralCodeId) {
        this.referralCodeId = referralCodeId;
    }

    @Basic
    @Column(name = "resendCount", nullable = true, insertable = true, updatable = true)
    public Integer getResendCount() {
        return resendCount;
    }

    public void setResendCount(Integer resentCount) {
        this.resendCount = resentCount;
    }

    @Basic
    @Column(name = "advocateCreditStatus", nullable = true, length = 100)
    public String getAdvocateCreditStatus() {
        return advocateCreditStatus;
    }

    public void setAdvocateCreditStatus(String advocateCreditStatus) {
        this.advocateCreditStatus = advocateCreditStatus;
    }

    @Basic
    @Column(name = "referralCreditStatus", nullable = true, length = 100)
    public String getReferralCreditStatus() {
        return referralCreditStatus;
    }

    public void setReferralCreditStatus(String referralCreditStatus) {
        this.referralCreditStatus = referralCreditStatus;
    }

    @Basic
    @Column(name = "advocateRetryCount", nullable = true)
    public Integer getAdvocateRetryCount() {
        return advocateRetryCount;
    }

    public void setAdvocateRetryCount(Integer advocateRetryCount) {
        this.advocateRetryCount = advocateRetryCount;
    }

    @Basic
    @Column(name = "referralRetryCount", nullable = true)
    public Integer getReferralRetryCount() {
        return referralRetryCount;
    }

    public void setReferralRetryCount(Integer referralRetryCount) {
        this.referralRetryCount = referralRetryCount;
    }

    @Basic
    @Column(name = "advocateCreditTxnId", nullable = true)
    public Integer getAdvocateCreditTxnId() {
        return advocateCreditTxnId;
    }

    public void setAdvocateCreditTxnId(Integer advocateCreditTxnId) {
        this.advocateCreditTxnId = advocateCreditTxnId;
    }

    @Basic
    @Column(name = "referralCreditTxnId", nullable = true)
    public Integer getReferralCreditTxnId() {
        return referralCreditTxnId;
    }

    public void setReferralCreditTxnId(Integer referralCreditTxnId) {
        this.referralCreditTxnId = referralCreditTxnId;
    }

    @Basic
    @Column(name = "accountCreatedOn", nullable = true)
    public Timestamp getAccountCreatedOn() {
        return accountCreatedOn;
    }

    public void setAccountCreatedOn(Timestamp accountCreatedOn) {
        this.accountCreatedOn = accountCreatedOn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        InvitationDBBean that = (InvitationDBBean) o;

        if (id != that.id) return false;
        if (referralCodeId != that.referralCodeId) return false;
        if (emailId != null ? !emailId.equals(that.emailId) : that.emailId != null) return false;
        if (inviteTime != null ? !inviteTime.equals(that.inviteTime) : that.inviteTime != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (targetCustomerId != null ? !targetCustomerId.equals(that.targetCustomerId) : that.targetCustomerId != null)
            return false;
        if (advocateCreditId != null ? !advocateCreditId.equals(that.advocateCreditId) : that.advocateCreditId != null)
            return false;
        if (referralCreditId != null ? !referralCreditId.equals(that.referralCreditId) : that.referralCreditId != null)
            return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
        if (resendCount != null ? !resendCount.equals(that.resendCount) : that.resendCount != null) return false;
        if (advocateCreditStatus != null ? !advocateCreditStatus.equals(that.advocateCreditStatus) : that.advocateCreditStatus != null) return false;
        if (referralCreditStatus != null ? !referralCreditStatus.equals(that.referralCreditStatus) : that.referralCreditStatus != null) return false;
        if (advocateRetryCount != null ? !advocateRetryCount.equals(that.advocateRetryCount) : that.advocateRetryCount != null) return false;
        if (referralRetryCount != null ? !referralRetryCount.equals(that.referralRetryCount) : that.referralRetryCount != null) return false;
        if (advocateCreditTxnId != null ? !advocateCreditTxnId.equals(that.advocateCreditTxnId) : that.advocateCreditTxnId != null) return false;
        if (referralCreditTxnId != null ? !referralCreditTxnId.equals(that.referralCreditTxnId) : that.referralCreditTxnId != null) return false;
        if (accountCreatedOn != null ? !accountCreatedOn.equals(that.accountCreatedOn) : that.accountCreatedOn != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (emailId != null ? emailId.hashCode() : 0);
        result = 31 * result + (inviteTime != null ? inviteTime.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (targetCustomerId != null ? targetCustomerId.hashCode() : 0);
        result = 31 * result + (advocateCreditId != null ? advocateCreditId.hashCode() : 0);
        result = 31 * result + (referralCreditId != null ? referralCreditId.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + referralCodeId;
        result = 31 * result + (resendCount != null ? resendCount.hashCode() : 0);
        result = 31 * result + (advocateCreditStatus != null ? advocateCreditStatus.hashCode() : 0);
        result = 31 * result + (referralCreditStatus != null ? referralCreditStatus.hashCode() : 0);
        result = 31 * result + advocateRetryCount;
        result = 31 * result + referralRetryCount;
        result = 31 * result + advocateCreditTxnId;
        result = 31 * result + referralCreditTxnId;
        result = 31 * result + (accountCreatedOn != null ? accountCreatedOn.hashCode() : 0);
        return result;
    }

}
