package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by vinayr on 03/01/17.
 */
@Entity
@Table(name = "Customer", schema = "gigskyReferralEngine")
public class CustomerDBBean {
    private int id;
    private Integer advocateId;
    private String language;
    private String emailId;
    private Long gsCustomerId;
    private Timestamp createTime;
    private Timestamp updateTime;
    private String status;
    private String country;
    private String referralStatus;
    private Timestamp lastReferralMailSent;
    private Timestamp accountCreationTime;
    private String firstName;
    private String lastName;
    private boolean deleted;
    private int tenantId;


    public CustomerDBBean() {
    }

    public CustomerDBBean(Long gsCustomerId, int tenantId, String emailId,  String firstName, String lastName) {
        this.emailId = emailId;
        this.gsCustomerId = gsCustomerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.tenantId = tenantId;
    }



    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "referralStatus", nullable = true, insertable = true, updatable = true, length = 100)
    public String getReferralStatus() {
        return referralStatus;
    }

    public void setReferralStatus(String referralStatus) {
        this.referralStatus = referralStatus;
    }

    @Basic
    @Column(name = "country", nullable = true, insertable = true, updatable = true, length = 45)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "advocateId", nullable = true, insertable = true, updatable = true)
    public Integer getAdvocateId() {
        return advocateId;
    }

    public void setAdvocateId(Integer advocateId) {
        this.advocateId = advocateId;
    }

    @Basic
    @Column(name = "language", nullable = true, insertable = true, updatable = true, length = 45)
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Basic
    @Column(name = "emailId", nullable = true, insertable = true, updatable = true, length = 45)
    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @Basic
    @Column(name = "gsCustomerId", nullable = true, insertable = true, updatable = true)
    public Long getGsCustomerId() {
        return gsCustomerId;
    }

    public void setGsCustomerId(Long gsCustomerId) {
        this.gsCustomerId = gsCustomerId;
    }

    @Basic
    @Column(name = "createTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "status", nullable = true, insertable = true, updatable = true, length = 45)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "lastReferralMailSent", nullable = true)
    public Timestamp getLastReferralMailSent() {
        return lastReferralMailSent;
    }

    public void setLastReferralMailSent(Timestamp lastInviteSent) {
        this.lastReferralMailSent = lastInviteSent;
    }

    @Basic
    @Column(name = "accountCreationTime", nullable = true)
    public Timestamp getAccountCreationTime() {
        return accountCreationTime;
    }

    public void setAccountCreationTime(Timestamp accountCreationTime) {
        this.accountCreationTime = accountCreationTime;
    }

    @Basic
    @Column(name = "firstName", nullable = true, insertable = true, updatable = true, length = 255)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "lastName", nullable = true, insertable = true, updatable = true, length = 255)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "tenant_id", nullable = false)
    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomerDBBean that = (CustomerDBBean) o;

        if (id != that.id) return false;
        if (advocateId != null ? !advocateId.equals(that.advocateId) : that.advocateId != null) return false;
        if (language != null ? !language.equals(that.language) : that.language != null) return false;
        if (emailId != null ? !emailId.equals(that.emailId) : that.emailId != null) return false;
        if (gsCustomerId != null ? !gsCustomerId.equals(that.gsCustomerId) : that.gsCustomerId != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (country != null ? !country.equals(that.country) : that.country != null) return false;
        if (referralStatus != null ? !referralStatus.equals(that.referralStatus) : that.referralStatus != null) return false;
        if (lastReferralMailSent != null ? !lastReferralMailSent.equals(that.lastReferralMailSent) : that.lastReferralMailSent != null) return false;
        if (accountCreationTime != null ? !accountCreationTime.equals(that.accountCreationTime) : that.accountCreationTime != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (tenantId != that.tenantId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (advocateId != null ? advocateId.hashCode() : 0);
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (emailId != null ? emailId.hashCode() : 0);
        result = 31 * result + (gsCustomerId != null ? gsCustomerId.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (referralStatus != null ? referralStatus.hashCode() : 0);
        result = 31 * result + (lastReferralMailSent != null ? lastReferralMailSent.hashCode() : 0);
        result = 31 * result + (accountCreationTime != null ? accountCreationTime.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + tenantId;
        return result;
    }

}
