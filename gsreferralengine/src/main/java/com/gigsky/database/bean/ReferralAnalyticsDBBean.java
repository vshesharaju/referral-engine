package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by vinayr on 03/01/17.
 */
@Entity
@Table(name = "ReferralAnalytics", schema = "gigskyReferralEngine")
public class ReferralAnalyticsDBBean {
    private int id;
    private String userAgent;
    private String platform;
    private Timestamp inviteOpenTime;
    private String language;
    private Timestamp createTime;
    private Timestamp updateTime;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "userAgent", nullable = true, insertable = true, updatable = true, length = 45)
    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    @Basic
    @Column(name = "platform", nullable = true, insertable = true, updatable = true, length = 45)
    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    @Basic
    @Column(name = "inviteOpenTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getInviteOpenTime() {
        return inviteOpenTime;
    }

    public void setInviteOpenTime(Timestamp inviteOpenTime) {
        this.inviteOpenTime = inviteOpenTime;
    }

    @Basic
    @Column(name = "language", nullable = true, insertable = true, updatable = true, length = 45)
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Basic
    @Column(name = "createTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReferralAnalyticsDBBean that = (ReferralAnalyticsDBBean) o;

        if (id != that.id) return false;
        if (userAgent != null ? !userAgent.equals(that.userAgent) : that.userAgent != null) return false;
        if (platform != null ? !platform.equals(that.platform) : that.platform != null) return false;
        if (inviteOpenTime != null ? !inviteOpenTime.equals(that.inviteOpenTime) : that.inviteOpenTime != null)
            return false;
        if (language != null ? !language.equals(that.language) : that.language != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (userAgent != null ? userAgent.hashCode() : 0);
        result = 31 * result + (platform != null ? platform.hashCode() : 0);
        result = 31 * result + (inviteOpenTime != null ? inviteOpenTime.hashCode() : 0);
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }
}
