package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by prsubbareddy on 30/11/17.
 */
@Entity
@Table(name = "PromoHistory", schema = "gigskyReferralEngine", catalog = "")
public class PromoHistoryDBBean {
    private int id;
    private Long gsCustomerId;
    private int promotionId;
    private Integer promoEventId;
    private String iccid;
    private Float creditAmount;
    private String currency;
    private Integer creditId;
    private Integer creditTxnId;
    private String emailNotifStatus;
    private int retryCount;
    private String status;
    private String statusMessage;
    private Timestamp createTime;
    private Timestamp updateTime;

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "gsCustomerId", nullable = true)
    public Long getGsCustomerId() {
        return gsCustomerId;
    }

    public void setGsCustomerId(Long gsCustomerId) {
        this.gsCustomerId = gsCustomerId;
    }

    @Basic
    @Column(name = "promotion_id", nullable = false)
    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    @Basic
    @Column(name = "promoEvent_id", nullable = false)
    public Integer getPromoEventId() {
        return promoEventId;
    }

    public void setPromoEventId(Integer promoEventId) {
        this.promoEventId = promoEventId;
    }

    @Basic
    @Column(name = "iccid", nullable = true, length = 100)
    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    @Basic
    @Column(name = "creditAmount", nullable = true, precision = 0)
    public Float getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(Float creditAmount) {
        this.creditAmount = creditAmount;
    }

    @Basic
    @Column(name = "currency", nullable = true, length = 45)
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Basic
    @Column(name = "creditId", nullable = true)
    public Integer getCreditId() {
        return creditId;
    }

    public void setCreditId(Integer creditId) {
        this.creditId = creditId;
    }

    @Basic
    @Column(name = "creditTxnId", nullable = true)
    public Integer getCreditTxnId() {
        return creditTxnId;
    }

    public void setCreditTxnId(Integer creditTxnId) {
        this.creditTxnId = creditTxnId;
    }

    @Basic
    @Column(name = "emailNotifStatus", nullable = true, length = 45)
    public String getEmailNotifStatus() {
        return emailNotifStatus;
    }

    public void setEmailNotifStatus(String emailNotifStatus) {
        this.emailNotifStatus = emailNotifStatus;
    }

    @Basic
    @Column(name = "status", nullable = true, length = 45)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "retryCount", nullable = true)
    public int getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(int retryCount) {
        this.retryCount = retryCount;
    }

    @Basic
    @Column(name = "statusMessage", nullable = true, length = 100)
    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    @Basic
    @Column(name = "createTime", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PromoHistoryDBBean that = (PromoHistoryDBBean) o;

        if (id != that.id) return false;
        if (promotionId != that.promotionId) return false;
        if (promoEventId != that.promoEventId) return false;
        if (gsCustomerId != null ? !gsCustomerId.equals(that.gsCustomerId) : that.gsCustomerId != null) return false;
        if (iccid != null ? !iccid.equals(that.iccid) : that.iccid != null) return false;
        if (creditAmount != null ? !creditAmount.equals(that.creditAmount) : that.creditAmount != null) return false;
        if (currency != null ? !currency.equals(that.currency) : that.currency != null) return false;
        if (creditTxnId != null ? !creditTxnId.equals(that.creditTxnId) : that.creditTxnId != null) return false;
        if (creditId != null ? !creditId.equals(that.creditId) : that.creditId != null) return false;
        if (emailNotifStatus != null ? !emailNotifStatus.equals(that.emailNotifStatus) : that.emailNotifStatus != null)
            return false;
        if (status != null ? !status.equals(that.status) : that.status != null) return false;
        if (statusMessage != null ? !statusMessage.equals(that.statusMessage) : that.statusMessage != null)
            return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (gsCustomerId != null ? gsCustomerId.hashCode() : 0);
        result = 31 * result + promotionId;
        result = 31 * result + promoEventId;
        result = 31 * result + (iccid != null ? iccid.hashCode() : 0);
        result = 31 * result + (creditAmount != null ? creditAmount.hashCode() : 0);
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + (creditTxnId != null ? creditTxnId.hashCode() : 0);
        result = 31 * result + (creditId != null ? creditId.hashCode() : 0);
        result = 31 * result + (emailNotifStatus != null ? emailNotifStatus.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (statusMessage != null ? statusMessage.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }
}
