package com.gigsky.database.bean;

import javax.persistence.*;

/**
 * Created by pradeepragav on 28/03/17.
 */
@Entity
@Table(name = "SupportedCurrency", schema = "gigskyReferralEngine")
public class SupportedCurrencyDBBean {
    private int id;
    private String currencyCode;
    private Float usdMultiplier;
    private int tenantId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "currencyCode", nullable = true, length = 45)
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currency) {
        this.currencyCode = currency;
    }

    @Basic
    @Column(name = "usdMultiplier", nullable = true, precision = 0)
    public Float getUsdMultiplier() {
        return usdMultiplier;
    }

    public void setUsdMultiplier(Float usdMultiplier) {
        this.usdMultiplier = usdMultiplier;
    }


    @Basic
    @Column(name = "tenant_id", nullable = false)
    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SupportedCurrencyDBBean that = (SupportedCurrencyDBBean) o;

        if (id != that.id) return false;
        if (currencyCode != null ? !currencyCode.equals(that.currencyCode) : that.currencyCode != null) return false;
        if (usdMultiplier != null ? !usdMultiplier.equals(that.usdMultiplier) : that.usdMultiplier != null)
            return false;
        if (tenantId != that.tenantId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (currencyCode != null ? currencyCode.hashCode() : 0);
        result = 31 * result + (usdMultiplier != null ? usdMultiplier.hashCode() : 0);
        result = 31 * result + tenantId;
        return result;
    }
}
