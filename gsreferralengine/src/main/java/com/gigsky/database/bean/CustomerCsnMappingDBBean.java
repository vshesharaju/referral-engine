package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by vinayr on 03/01/17.
 */
@Entity
@Table(name = "CustomerCSNMapping", schema = "gigskyReferralEngine")
public class CustomerCsnMappingDBBean {
    private int id;
    private String csn;
    private Integer gsCustomerId;
    private Timestamp createTime;
    private Timestamp updateTime;
    private int tenantId;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "CSN", nullable = true, insertable = true, updatable = true, length = 100)
    public String getCsn() {
        return csn;
    }

    public void setCsn(String csn) {
        this.csn = csn;
    }

    @Basic
    @Column(name = "gsCustomerId", nullable = true, insertable = true, updatable = true)
    public Integer getGsCustomerId() {
        return gsCustomerId;
    }

    public void setGsCustomerId(Integer gsCustomerId) {
        this.gsCustomerId = gsCustomerId;
    }

    @Basic
    @Column(name = "createTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "tenant_id", nullable = false)
    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomerCsnMappingDBBean that = (CustomerCsnMappingDBBean) o;

        if (id != that.id) return false;
        if (csn != null ? !csn.equals(that.csn) : that.csn != null) return false;
        if (gsCustomerId != null ? !gsCustomerId.equals(that.gsCustomerId) : that.gsCustomerId != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
        if (tenantId != that.tenantId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (csn != null ? csn.hashCode() : 0);
        result = 31 * result + (gsCustomerId != null ? gsCustomerId.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + tenantId;
        return result;
    }
}
