package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by vinayr on 03/01/17.
 */
@Entity
@Table(name = "IncentiveScheme", schema = "gigskyReferralEngine")
public class IncentiveSchemeDBBean {
    private int id;
    private Date validityStartDate;
    private Date validityEndDate;
    private Integer advocateCreditExpiryPeriod;
    private Integer referralCreditExpiryPeriod;
    private Byte schemeEnabled;
    private Timestamp createTime;
    private Timestamp updateTime;
    private Byte activeScheme;
    private int tenantId;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "validityStartDate", nullable = true, insertable = true, updatable = true)
    public Date getValidityStartDate() {
        return validityStartDate;
    }

    public void setValidityStartDate(Date validityStartDate) {
        this.validityStartDate = validityStartDate;
    }

    @Basic
    @Column(name = "validityEndDate", nullable = true, insertable = true, updatable = true)
    public Date getValidityEndDate() {
        return validityEndDate;
    }

    public void setValidityEndDate(Date validityEndDate) {
        this.validityEndDate = validityEndDate;
    }

    @Basic
    @Column(name = "advocateCreditExpiryPeriod", nullable = true, insertable = true, updatable = true)
    public Integer getAdvocateCreditExpiryPeriod() {
        return advocateCreditExpiryPeriod;
    }

    public void setAdvocateCreditExpiryPeriod(Integer advocateCreditExpiryPeriod) {
        this.advocateCreditExpiryPeriod = advocateCreditExpiryPeriod;
    }

    @Basic
    @Column(name = "referralCreditExpiryPeriod", nullable = true, insertable = true, updatable = true)
    public Integer getReferralCreditExpiryPeriod() {
        return referralCreditExpiryPeriod;
    }

    public void setReferralCreditExpiryPeriod(Integer referralCreditExpiryPeriod) {
        this.referralCreditExpiryPeriod = referralCreditExpiryPeriod;
    }

    @Basic
    @Column(name = "schemeEnabled", nullable = true, insertable = true, updatable = true)
    public Byte getSchemeEnabled() {
        return schemeEnabled;
    }

    public void setSchemeEnabled(Byte schemeEnabled) {
        this.schemeEnabled = schemeEnabled;
    }

    @Basic
    @Column(name = "createTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "activeScheme", nullable = true)
    public Byte getActiveScheme() {
        return activeScheme;
    }

    public void setActiveScheme(Byte activeScheme) {
        this.activeScheme = activeScheme;
    }

    @Basic
    @Column(name = "tenant_id", nullable = false)
    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IncentiveSchemeDBBean that = (IncentiveSchemeDBBean) o;

        if (id != that.id) return false;
        if (validityStartDate != null ? !validityStartDate.equals(that.validityStartDate) : that.validityStartDate != null)
            return false;
        if (validityEndDate != null ? !validityEndDate.equals(that.validityEndDate) : that.validityEndDate != null)
            return false;
        if (advocateCreditExpiryPeriod != null ? !advocateCreditExpiryPeriod.equals(that.advocateCreditExpiryPeriod) : that.advocateCreditExpiryPeriod != null)
            return false;
        if (referralCreditExpiryPeriod != null ? !referralCreditExpiryPeriod.equals(that.referralCreditExpiryPeriod) : that.referralCreditExpiryPeriod != null)
            return false;
        if (schemeEnabled != null ? !schemeEnabled.equals(that.schemeEnabled) : that.schemeEnabled != null)
            return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
        if (activeScheme != null ? !activeScheme.equals(that.activeScheme) : that.activeScheme != null) return false;
        if (tenantId != that.tenantId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (validityStartDate != null ? validityStartDate.hashCode() : 0);
        result = 31 * result + (validityEndDate != null ? validityEndDate.hashCode() : 0);
        result = 31 * result + (advocateCreditExpiryPeriod != null ? advocateCreditExpiryPeriod.hashCode() : 0);
        result = 31 * result + (referralCreditExpiryPeriod != null ? referralCreditExpiryPeriod.hashCode() : 0);
        result = 31 * result + (schemeEnabled != null ? schemeEnabled.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + (activeScheme != null ? activeScheme.hashCode() : 0);
        result = 31 * result + tenantId;
        return result;
    }


}
