package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by vinayr on 03/01/17.
 */
@Entity
@Table(name = "ReferralCode", schema = "gigskyReferralEngine")
public class ReferralCodeDBBean {
    private int id;
    private String channel;
    private String referralCode;
    private Timestamp createTime;
    private Timestamp updateTime;
    private int customerId;
    private int incentiveSchemeId;
    private Byte isActive;
    private Integer referredCount;
    private Timestamp firstReferredTime;
    private Byte isPromotionalCode;
    private int tenantId;

    public void setIsActive(Byte isActive) {
        this.isActive = isActive;
    }

    @Basic
    @Column(name = "isActive", nullable = true, insertable = true, updatable = true)
    public byte getIsActive() {
        return isActive;
    }

    public void setIsActive(byte isActive) {
        this.isActive = isActive;
    }

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "channel", nullable = true, insertable = true, updatable = true, length = 45)
    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    @Basic
    @Column(name = "referralCode", nullable = true, insertable = true, updatable = true, length = -1)
    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    @Basic
    @Column(name = "createTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "customer_id", nullable = false, insertable = true, updatable = true)
    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    @Basic
    @Column(name = "incentiveScheme_id", nullable = false, insertable = true, updatable = true)
    public int getIncentiveSchemeId() {
        return incentiveSchemeId;
    }

    public void setIncentiveSchemeId(int incentiveSchemeId) {
        this.incentiveSchemeId = incentiveSchemeId;
    }

    @Basic
    @Column(name = "tenant_id", nullable = false)
    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    @Basic
    @Column(name = "referredCount", nullable = true)
    public Integer getReferredCount() {
        return referredCount;
    }

    public void setReferredCount(Integer referredCount) {
        this.referredCount = referredCount;
    }

    @Basic
    @Column(name = "firstReferredTime", nullable = true)
    public Timestamp getFirstReferredTime() {
        return firstReferredTime;
    }

    public void setFirstReferredTime(Timestamp firstReferredTime) {
        this.firstReferredTime = firstReferredTime;
    }

    @Basic
    @Column(name = "isPromotionalCode", nullable = true)
    public Byte getIsPromotionalCode() {
        return isPromotionalCode;
    }

    public void setIsPromotionalCode(Byte isPromotionalCode) {
        this.isPromotionalCode = isPromotionalCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReferralCodeDBBean that = (ReferralCodeDBBean) o;

        if (id != that.id) return false;
        if (customerId != that.customerId) return false;
        if (incentiveSchemeId != that.incentiveSchemeId) return false;
        if (tenantId != that.tenantId) return false;
        if (channel != null ? !channel.equals(that.channel) : that.channel != null) return false;
        if (referralCode != null ? !referralCode.equals(that.referralCode) : that.referralCode != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
        if (isActive != null ? !isActive.equals(that.isActive) : that.isActive != null) return false;
        if (referredCount != null ? !referredCount.equals(that.referredCount) : that.referredCount != null) return false;
        if (firstReferredTime != null ? !firstReferredTime.equals(that.firstReferredTime) : that.firstReferredTime != null) return false;
        if (isPromotionalCode != null ? !isPromotionalCode.equals(that.isPromotionalCode) : that.isPromotionalCode != null) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (channel != null ? channel.hashCode() : 0);
        result = 31 * result + (referralCode != null ? referralCode.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + customerId;
        result = 31 * result + incentiveSchemeId;
        result = 31 * result + tenantId;
        result = 31 * result + (isActive != null ? isActive.hashCode() : 0);
        result = 31 * result + (referredCount != null ? referredCount.hashCode() : 0);
        result = 31 * result + (firstReferredTime != null ? firstReferredTime.hashCode() : 0);
        result = 31 * result + (isPromotionalCode != null ? isPromotionalCode.hashCode() : 0);
        return result;
    }

}
