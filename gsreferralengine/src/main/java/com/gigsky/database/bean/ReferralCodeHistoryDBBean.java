package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by prsubbareddy on 19/01/17.
 */
@Entity
@Table(name = "ReferralCodeHistory", schema = "gigskyReferralEngine")
public class ReferralCodeHistoryDBBean {
    private int id;
    private int referralCodeId;
    private Integer incentiveSchemeId;
    private Timestamp startDate;
    private Timestamp endDate;
    private Timestamp createTime;
    private Timestamp updateTime;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "referralCode_id", nullable = false)
    public int getReferralCodeId() {
        return referralCodeId;
    }

    public void setReferralCodeId(int referralCodeId) {
        this.referralCodeId = referralCodeId;
    }

    @Basic
    @Column(name = "incentiveScheme_id", nullable = true)
    public Integer getIncentiveSchemeId() {
        return incentiveSchemeId;
    }

    public void setIncentiveSchemeId(Integer incentiveSchemeId) {
        this.incentiveSchemeId = incentiveSchemeId;
    }

    @Basic
    @Column(name = "startDate", nullable = true)
    public Timestamp getStartDate() {
        return startDate;
    }

    public void setStartDate(Timestamp startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "endDate", nullable = true)
    public Timestamp getEndDate() {
        return endDate;
    }

    public void setEndDate(Timestamp endDate) {
        this.endDate = endDate;
    }

    @Basic
    @Column(name = "createTime", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ReferralCodeHistoryDBBean that = (ReferralCodeHistoryDBBean) o;

        if (id != that.id) return false;
        if (referralCodeId != that.referralCodeId) return false;
        if (incentiveSchemeId != null ? !incentiveSchemeId.equals(that.incentiveSchemeId) : that.incentiveSchemeId != null)
            return false;
        if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + referralCodeId;
        result = 31 * result + (incentiveSchemeId != null ? incentiveSchemeId.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }
}
