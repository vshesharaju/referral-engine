package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by prsubbareddy on 30/11/17.
 */
@Entity
@Table(name = "PromoIccid", schema = "gigskyReferralEngine", catalog = "")
public class PromoIccidDBBean {
    private int id;
    private String iccidStart;
    private String iccidEnd;
    private int promotionId;
    private Byte isEmailNotifRequired;
    private Timestamp createTime;
    private Timestamp updateTime;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "iccidStart", nullable = true, length = 100)
    public String getIccidStart() {
        return iccidStart;
    }

    public void setIccidStart(String iccidStart) {
        this.iccidStart = iccidStart;
    }

    @Basic
    @Column(name = "iccidEnd", nullable = true, length = 100)
    public String getIccidEnd() {
        return iccidEnd;
    }

    public void setIccidEnd(String iccidEnd) {
        this.iccidEnd = iccidEnd;
    }

    @Basic
    @Column(name = "promotion_id", nullable = false)
    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    @Basic
    @Column(name = "isEmailNotifRequired", nullable = true)
    public Byte getIsEmailNotifRequired() {
        return isEmailNotifRequired;
    }

    public void setIsEmailNotifRequired(Byte isEmailNotifRequired) {
        this.isEmailNotifRequired = isEmailNotifRequired;
    }

    @Basic
    @Column(name = "createTime", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PromoIccidDBBean that = (PromoIccidDBBean) o;

        if (id != that.id) return false;
        if (promotionId != that.promotionId) return false;
        if (iccidStart != null ? !iccidStart.equals(that.iccidStart) : that.iccidStart != null) return false;
        if (iccidEnd != null ? !iccidEnd.equals(that.iccidEnd) : that.iccidEnd != null) return false;
        if (isEmailNotifRequired != null ? !isEmailNotifRequired.equals(that.isEmailNotifRequired) : that.isEmailNotifRequired != null)
            return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (iccidStart != null ? iccidStart.hashCode() : 0);
        result = 31 * result + (iccidEnd != null ? iccidEnd.hashCode() : 0);
        result = 31 * result + promotionId;
        result = 31 * result + (isEmailNotifRequired != null ? isEmailNotifRequired.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }
}
