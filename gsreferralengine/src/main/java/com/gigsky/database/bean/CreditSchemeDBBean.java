package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by vinayr on 03/01/17.
 */
@Entity
@Table(name = "CreditScheme", schema = "gigskyReferralEngine")
public class CreditSchemeDBBean {
    private int id;
    private String currency;
    private Float advocateAmount;
    private Float newCustomerAmount;
    private int incentiveSchemeId;
    private Timestamp createTime;
    private Timestamp updateTime;

    @Id
    @Column(name = "id", nullable = false, insertable = true, updatable = true)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "currency", nullable = true, insertable = true, updatable = true, length = 45)
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Basic
    @Column(name = "advocateAmount", nullable = true, insertable = true, updatable = true, precision = 0)
    public Float getAdvocateAmount() {
        return advocateAmount;
    }

    public void setAdvocateAmount(Float advocateAmount) {
        this.advocateAmount = advocateAmount;
    }

    @Basic
    @Column(name = "newCustomerAmount", nullable = true, insertable = true, updatable = true, precision = 0)
    public Float getNewCustomerAmount() {
        return newCustomerAmount;
    }

    public void setNewCustomerAmount(Float newCustomerAmount) {
        this.newCustomerAmount = newCustomerAmount;
    }

    @Basic
    @Column(name = "incentiveScheme_id", nullable = false, insertable = true, updatable = true)
    public int getIncentiveSchemeId() {
        return incentiveSchemeId;
    }

    public void setIncentiveSchemeId(int incentiveSchemeId) {
        this.incentiveSchemeId = incentiveSchemeId;
    }

    @Basic
    @Column(name = "createTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true, insertable = true, updatable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CreditSchemeDBBean that = (CreditSchemeDBBean) o;

        if (id != that.id) return false;
        if (incentiveSchemeId != that.incentiveSchemeId) return false;
        if (currency != null ? !currency.equals(that.currency) : that.currency != null) return false;
        if (advocateAmount != null ? !advocateAmount.equals(that.advocateAmount) : that.advocateAmount != null)
            return false;
        if (newCustomerAmount != null ? !newCustomerAmount.equals(that.newCustomerAmount) : that.newCustomerAmount != null)
            return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + (advocateAmount != null ? advocateAmount.hashCode() : 0);
        result = 31 * result + (newCustomerAmount != null ? newCustomerAmount.hashCode() : 0);
        result = 31 * result + incentiveSchemeId;
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }
}
