package com.gigsky.database.bean;

import java.sql.Timestamp;

/**
 * Created by prsubbareddy on 21/12/17.
 */
public class HealthCheckEventDBBean {
    private int id;
    private String eventType;
    private String eventMetaData;
    private Byte isQueueUp;
    private Timestamp createTime;
    private Timestamp updateTime;
    private Timestamp eventReceivedTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventMetaData() {
        return eventMetaData;
    }

    public void setEventMetaData(String eventMetaData) {
        this.eventMetaData = eventMetaData;
    }

    public Byte getIsQueueUp() {
        return isQueueUp;
    }

    public void setIsQueueUp(Byte isQueueUp) {
        this.isQueueUp = isQueueUp;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HealthCheckEventDBBean that = (HealthCheckEventDBBean) o;

        if (id != that.id) return false;
        if (eventType != null ? !eventType.equals(that.eventType) : that.eventType != null) return false;
        if (eventMetaData != null ? !eventMetaData.equals(that.eventMetaData) : that.eventMetaData != null)
            return false;
        if (isQueueUp != null ? !isQueueUp.equals(that.isQueueUp) : that.isQueueUp != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (eventType != null ? eventType.hashCode() : 0);
        result = 31 * result + (eventMetaData != null ? eventMetaData.hashCode() : 0);
        result = 31 * result + (isQueueUp != null ? isQueueUp.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }

    public Timestamp getEventReceivedTime() {
        return eventReceivedTime;
    }

    public void setEventReceivedTime(Timestamp eventReceivedTime) {
        this.eventReceivedTime = eventReceivedTime;
    }
}
