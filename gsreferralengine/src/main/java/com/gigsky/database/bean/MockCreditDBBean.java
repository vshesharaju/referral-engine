package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by vinayr on 17/01/17.
 */
@Entity
@Table(name = "MockCredit", schema = "gigskyReferralEngine")
public class MockCreditDBBean {
    private int id;
    private String creditType;
    private Integer mockcustomerId;
    private Float balance;
    private String currency;
    private Timestamp expiryTime;
    private Timestamp createTime;
    private Timestamp updateTime;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "creditType", nullable = true, length = 45)
    public String getCreditType() {
        return creditType;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    @Basic
    @Column(name = "mockcustomer_id", nullable = true)
    public Integer getMockcustomerId() {
        return mockcustomerId;
    }

    public void setMockcustomerId(Integer mockcustomerId) {
        this.mockcustomerId = mockcustomerId;
    }

    @Basic
    @Column(name = "balance", nullable = true, insertable = true, updatable = true, precision = 0)
    public Float getBalance() {
        return balance;
    }

    public void setBalance(Float balance) {
        this.balance = balance;
    }

    @Basic
    @Column(name = "currency", nullable = true, length = 45)
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Basic
    @Column(name = "expiryTime", nullable = true)
    public Timestamp getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(Timestamp expiryTime) {
        this.expiryTime = expiryTime;
    }

    @Basic
    @Column(name = "createTime", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MockCreditDBBean that = (MockCreditDBBean) o;

        if (id != that.id) return false;
        if (creditType != null ? !creditType.equals(that.creditType) : that.creditType != null) return false;
        if (mockcustomerId != null ? !mockcustomerId.equals(that.mockcustomerId) : that.mockcustomerId != null)
            return false;
        if (balance != null ? !balance.equals(that.balance) : that.balance != null) return false;
        if (currency != null ? !currency.equals(that.currency) : that.currency != null) return false;
        if (expiryTime != null ? !expiryTime.equals(that.expiryTime) : that.expiryTime != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (creditType != null ? creditType.hashCode() : 0);
        result = 31 * result + (mockcustomerId != null ? mockcustomerId.hashCode() : 0);
        result = 31 * result + (balance != null ? balance.hashCode() : 0);
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + (expiryTime != null ? expiryTime.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }
}
