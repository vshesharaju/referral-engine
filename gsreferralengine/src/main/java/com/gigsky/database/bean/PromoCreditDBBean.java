package com.gigsky.database.bean;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by prsubbareddy on 30/11/17.
 */
@Entity
@Table(name = "PromoCredit", schema = "gigskyReferralEngine", catalog = "")
public class PromoCreditDBBean {
    private int id;
    private int promotionId;
    private Float creditAmount;
    private Float minimumCartAmount;
    private String currency;
    private Timestamp createTime;
    private Timestamp updateTime;

    @Basic
    @Column(name = "minimumCartAmount", nullable = true, precision = 0)
    public Float getMinimumCartAmount() {
        return minimumCartAmount;
    }

    public void setMinimumCartAmount(Float minimumCartAmount) {
        this.minimumCartAmount = minimumCartAmount;
    }

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "promotion_id", nullable = false)
    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    @Basic
    @Column(name = "creditAmount", nullable = true, precision = 0)
    public Float getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(Float creditAmount) {
        this.creditAmount = creditAmount;
    }

    @Basic
    @Column(name = "currency", nullable = true, length = 45)
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Basic
    @Column(name = "createTime", nullable = true)
    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    @Basic
    @Column(name = "updateTime", nullable = true)
    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PromoCreditDBBean that = (PromoCreditDBBean) o;

        if (id != that.id) return false;
        if (promotionId != that.promotionId) return false;
        if (creditAmount != null ? !creditAmount.equals(that.creditAmount) : that.creditAmount != null) return false;
        if (minimumCartAmount != null ? !minimumCartAmount.equals(that.minimumCartAmount) : that.minimumCartAmount != null) return false;
        if (currency != null ? !currency.equals(that.currency) : that.currency != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + promotionId;
        result = 31 * result + (creditAmount != null ? creditAmount.hashCode() : 0);
        result = 31 * result + (minimumCartAmount != null ? minimumCartAmount.hashCode() : 0);
        result = 31 * result + (currency != null ? currency.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }
}
