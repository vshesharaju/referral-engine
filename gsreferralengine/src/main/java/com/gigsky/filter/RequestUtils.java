package com.gigsky.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

/**
 * Created by on 28/10/15.
 */
public class RequestUtils {
    private static final Logger logger = LoggerFactory.getLogger(RequestUtils.class);

    public static String getLocale() {
        try {
            RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
            String locale = (String) requestAttributes.getAttribute(ReferralEngineConstants.LOCALE_KEY, RequestAttributes.SCOPE_REQUEST);
            return locale;
        }
        catch (Exception ex) {
            logger.warn("No locale found, returning default " + ReferralEngineConstants.LOCALE_ENGLISH);
            return ReferralEngineConstants.LOCALE_ENGLISH;
        }
    }

    public static int getTenantId() {
        try {
            RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
            int tenantId = (Integer) requestAttributes.getAttribute(ReferralEngineConstants.TENANT_ID, RequestAttributes.SCOPE_REQUEST);
            return tenantId;

        } catch (Exception ex) {
            logger.warn("No Tenants found, returning default " + ReferralEngineConstants.TENANT_ID);
            return Integer.valueOf(ReferralEngineConstants.TENANT_ID);
        }
    }

    public static String getContextPath() {
        try {
            RequestAttributes requestAttributes = RequestContextHolder.currentRequestAttributes();
            String locale = (String) requestAttributes.getAttribute(ReferralEngineConstants.CONTEXT_PATH, RequestAttributes.SCOPE_REQUEST);
            return locale;
        }
        catch (Exception ex) {
            logger.warn("No context path is found");
            return null;
        }
    }
}