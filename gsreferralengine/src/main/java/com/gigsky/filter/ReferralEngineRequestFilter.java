package com.gigsky.filter;

import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSUtil;
import com.sun.jersey.spi.container.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.zip.GZIPOutputStream;

@Component
public class ReferralEngineRequestFilter implements ContainerRequestFilter, ContainerResponseFilter {
    @javax.ws.rs.core.Context
    private HttpServletRequest request;
    @javax.ws.rs.core.Context
    private ServletContext servletContext;
    @Autowired
    GSUtil gsUtil;

    @Override
    public ContainerRequest filter(ContainerRequest containerRequest) {
        try {
            getLocaleInfo(containerRequest);
        } catch (ResourceValidationException e) {
            e.printStackTrace();
        } catch (ServiceException e) {
            e.printStackTrace();
        }
        setContextPath();
        return containerRequest;
    }

    private void getLocaleInfo(ContainerRequest containerRequest) throws ResourceValidationException, ServiceException {
        List<String> requestHeader = containerRequest.getRequestHeader(HttpHeaders.ACCEPT_LANGUAGE);
        String locale = StringUtils.EMPTY;
        MultivaluedMap<String, String> headers = containerRequest.getRequestHeaders();
        if (CollectionUtils.isNotEmpty(requestHeader)) {

            locale = requestHeader.get(0);

            //if (locale.contains(ReferralEngineConstants.LOCALE_ENGLISH)) {
                //locale = ReferralEngineConstants.LOCALE_ENGLISH;
           // }
        }
        int tenantId = gsUtil.extractTenantId(headers);
//
        RequestAttributes attributes = new ServletRequestAttributes(request);
        attributes.setAttribute(ReferralEngineConstants.TENANT_ID,tenantId,RequestAttributes.SCOPE_REQUEST);
        attributes.setAttribute(ReferralEngineConstants.LOCALE_KEY, locale, RequestAttributes.SCOPE_REQUEST);
        RequestContextHolder.setRequestAttributes(attributes, false);
    }

    @Override
    public ContainerResponse filter(ContainerRequest containerRequest, ContainerResponse containerResponse) {
        containerResponse.getHttpHeaders().add(HttpHeaders.CACHE_CONTROL, "no-cache, no-store, must-revalidate, private, max-age=0");
        containerResponse.getHttpHeaders().add("Pragma", "no-cache");

        containerResponse.getHttpHeaders().add(HttpHeaders.VARY, HttpHeaders.ACCEPT_ENCODING); // add vary header

        String acceptEncoding = containerRequest.getRequestHeaders().getFirst(HttpHeaders.ACCEPT_ENCODING);

        if (acceptEncoding != null && acceptEncoding.contains("gzip")) {
            // wrap entity with gzip
            if (containerResponse.getEntity() != null) {
                containerResponse.getHttpHeaders().add(HttpHeaders.CONTENT_ENCODING, "gzip");
                containerResponse.setContainerResponseWriter(new Adapter(containerResponse.getContainerResponseWriter()));
            }
        }
        return containerResponse;
    }

    public void setContextPath() {
        RequestAttributes attributes = new ServletRequestAttributes(request);
        String realPath = servletContext.getRealPath("");
        attributes.setAttribute(ReferralEngineConstants.CONTEXT_PATH, realPath, RequestAttributes.SCOPE_REQUEST);
        RequestContextHolder.setRequestAttributes(attributes, false);
    }

    private static final class Adapter implements ContainerResponseWriter {
        private final ContainerResponseWriter crw;

        private GZIPOutputStream gos;

        Adapter(ContainerResponseWriter crw) {
            this.crw = crw;
        }

        public OutputStream writeStatusAndHeaders(long contentLength, ContainerResponse response) throws IOException {
            gos = new GZIPOutputStream(crw.writeStatusAndHeaders(-1, response));
            return gos;
        }

        public void finish() throws IOException {
            gos.finish();
            crw.finish();
        }
    }
}