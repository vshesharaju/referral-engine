package com.gigsky.filter;

public interface ReferralEngineConstants {
    String LOCALE_ENGLISH = "en";
    String LOCALE_KEY = "locale";
    String TENANT_ID = "1";
    String CONTEXT_PATH = "contextPath";
}