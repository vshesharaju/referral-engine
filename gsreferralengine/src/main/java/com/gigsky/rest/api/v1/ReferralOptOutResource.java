package com.gigsky.rest.api.v1;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.rest.bean.OptOut;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.AccountService;
import com.gigsky.service.ValidationService;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by prsubbareddy on 01/02/17.
 */
@Path("/api/v1/account/{gsCustomerId}/optOut")
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
public class ReferralOptOutResource {

    private static final Logger logger = LoggerFactory.getLogger(ReferralOptOutResource.class);

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private GSUtil gsUtil;

    @POST
    public Response referralOptOut(@PathParam("gsCustomerId") long  gsCustomerId, OptOut optOut, @Context HttpHeaders headers) throws ResourceValidationException, BackendRequestException, ServiceException {

        logger.info("+++++ referralOptOut start +++++ gsCustomerId: " + gsCustomerId);

        // validation for backend customer id
        validationService.validateId((int) gsCustomerId);
        int tenantId = gsUtil.extractTenantId(headers);

        // validate optout details
        validateOptOut(optOut);

        // fetch customer details based on GsCustomerId
        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, tenantId);
        if(customerDBBean == null || (customerDBBean != null && customerDBBean.isDeleted())) {
            logger.error("Customer doesn't exist");
            throw new ResourceValidationException(ErrorCode.CUSTOMER_DOES_NOT_EXIST);
        }

        String optValue = (optOut.getOptOut())?"true":"false";
        accountService.setOptOut(customerDBBean.getId(), optValue);
        logger.info("Setting referral optOut value: " + optValue);

        logger.info("===== referralOptOut end =======");

        return Response.status(Response.Status.OK).entity(null).build();
    }

    @GET
    public Response getReferralOptOut(@PathParam("gsCustomerId") long  gsCustomerId, @Context HttpHeaders headers) throws ResourceValidationException, ServiceException {

        logger.info("+++++ getReferralOptOut start +++++ gsCustomerId: " + gsCustomerId);

        // validation for backend customer id
        validationService.validateId((int) gsCustomerId);
        int tenantId = gsUtil.extractTenantId(headers);

        // fetch customer details based on GsCustomerId
        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, tenantId);
        if(customerDBBean == null || (customerDBBean != null && customerDBBean.isDeleted())) {
            logger.error("Customer doesn't exist");
            throw new ResourceValidationException(ErrorCode.CUSTOMER_DOES_NOT_EXIST);
        }

        OptOut optOut = accountService.getOptOut(customerDBBean.getId());

        logger.info("===== getReferralOptOut end =======");

        return Response.status(Response.Status.OK).entity(optOut).build();
    }

    private void validateOptOut(OptOut optOut) throws ResourceValidationException
    {

        if(optOut.getType() == null || !optOut.getType().equals("InviteOptOut")) {
            logger.error("Referral optout requires valid type");
            throw new ResourceValidationException(ErrorCode.REFERRAL_OPT_OUT_REQUIRES_VALID_TYPE);
        }

        if(optOut.getOptOut() == null)
        {
            logger.error("Referral optout requires valid optout");
            throw new ResourceValidationException(ErrorCode.REFERRAL_OPT_OUT_REQUIRES_VALID_OPT_OUT);
        }
    }

}
