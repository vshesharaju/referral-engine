package com.gigsky.rest.api.v1;

import com.gigsky.backend.auth.AuthManagementInterface;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.ReferralCodeDao;
import com.gigsky.rest.bean.Invitation;
import com.gigsky.rest.bean.PageInfo;
import com.gigsky.rest.bean.ReferralInvite;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.ReferralInviteService;
import com.gigsky.service.ValidationService;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSUtil;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by prsubbareddy on 17/01/17.
 */

@Path("/api/v1/account/{gsCustomerId}/invites")
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Controller
public class ReferralInviteResource {

    private static final Logger logger = LoggerFactory.getLogger(ReferralInviteResource.class);

    @Autowired
    private  ReferralInviteService referralInviteService;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private AuthManagementInterface authManagementInterface;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    ReferralCodeDao referralCodeDao;

    @Autowired
    private GSUtil gsUtil;

    @GET
    @Path("/{inviteId}")
    public Response getInvite(@PathParam("gsCustomerId") long gsCustomerId, @PathParam("inviteId") long inviteId, @Context HttpHeaders headers)
            throws ResourceValidationException, ServiceException, BackendRequestException, JSONException
    {

        logger.info("+++++ getInvite start ++++ gsCustomerId: " + gsCustomerId +" inviteId: " + inviteId);

        validationService.validateId((int) gsCustomerId);
        validationService.validateId((int) inviteId);
        String token = GSUtil.extractToken(headers, validationService);
        int tenantId = gsUtil.extractTenantId(headers);

        //Check if valid token is passed
        boolean isValidToken = authManagementInterface.validateCustomerToken(token, (int) gsCustomerId, tenantId);

        //If invalid token, throw exception
        if(!isValidToken){

            logger.error("Invalid Token");
            throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
        }

        Invitation invitation = referralInviteService.getInvite((int) gsCustomerId, (int) inviteId, tenantId);

        logger.info("===== getInvite  end ======= ");

        return Response.status(Response.Status.OK).entity(invitation).build();
    }

    @GET
    public Response getInvites(@PathParam("gsCustomerId") long gsCustomerId,
                              @DefaultValue("0") @QueryParam("startIndex") int startIndex,
                              @DefaultValue("10") @QueryParam("count") int count,
                              @Context HttpHeaders headers) throws ResourceValidationException, JSONException, BackendRequestException, ServiceException
    {

        logger.info("+++++ getInvites start ++++ gsCustomerId: " + gsCustomerId);

        validationService.validateStartIndexAndCount(startIndex, count);
        validationService.validateId((int) gsCustomerId);
        String token = GSUtil.extractToken(headers, validationService);
        int tenantId = gsUtil.extractTenantId(headers);

        //Check if valid token is passed
        boolean isValidToken = authManagementInterface.validateCustomerToken(token, (int) gsCustomerId, tenantId);

        //If invalid token, throw exception
        if(!isValidToken){

            logger.error("Invalid Token");
            throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
        }

        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, tenantId);

        PageInfo inputPageInfo = new PageInfo(startIndex, count);
        PageInfo outputPageInfo =  new PageInfo();

        validationService.validatePage(inputPageInfo);

        List<Invitation> invitationList = (customerDBBean == null) ? new ArrayList<Invitation>()
                : referralInviteService.getInvitationsForCustomer(customerDBBean.getId(), inputPageInfo, outputPageInfo, tenantId);

        ReferralInvite referralInviteList = new ReferralInvite();
        referralInviteList.setType("ReferralInviteStatus");
        referralInviteList.setStartIndex(outputPageInfo.getStart());
        referralInviteList.setCount(invitationList.size());
        referralInviteList.setTotalCount(outputPageInfo.getCount());
        referralInviteList.setList(invitationList);

        logger.info("===== getInvites  end ======= ");

        return Response.status(Response.Status.OK).entity(referralInviteList).build();
    }

}
