package com.gigsky.rest.api.v1;

import com.gigsky.backend.auth.AuthManagementInterface;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CountryDBBean;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.ReferralCodeDBBean;
import com.gigsky.database.dao.*;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.rest.bean.*;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.AccountService;
import com.gigsky.service.ReferralSchemeService;
import com.gigsky.service.ValidationService;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSTaskUtil;
import com.gigsky.util.GSTokenUtil;
import com.gigsky.util.GSUtil;
import com.gigsky.util.GSSchemeUtil;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by prsubbareddy on 19/01/17.
 */
@Path("/api/v1/")
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Controller
public class ReferralSchemeResource {

    private static final Logger logger = LoggerFactory.getLogger(ReferralSchemeResource.class);

    @Autowired
    private ValidationService validationService;

    @Autowired
    private AuthManagementInterface authManagementInterface;

    @Autowired
    private ReferralSchemeService referralSchemeService;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private ValidCountryDao validCountryDao;

    @Autowired
    private GSTokenUtil gsTokenUtil;

    @Autowired
    private SupportedCurrenciesDao supportedCurrenciesDao;

    @Autowired
    private ReferralCodeDao referralCodeDao;

    @Autowired
    private AccountService accountService;

    @Autowired
    private GSTaskUtil gsTaskUtil;

    @Autowired
    private PromotionResource promotionResource;

    @Autowired
    private GSUtil gsUtil;

    @Autowired
    private GSSchemeUtil gsSchemeUtil;

    @Path("/") // you have the method return an instance of the resource you want to delegate too
    public PromotionResource getPromotionResource() {
        return promotionResource;
    }


    @POST
    @Path("/schemes")
    public Response addIncentiveScheme(Scheme scheme, @Context HttpHeaders headers) throws ResourceValidationException, DatabaseException, ParseException, BackendRequestException, ServiceException {

        logger.info("+++++ addIncentiveScheme start +++++");

        scheme = updateCountriesToUpperCase(scheme);

        //validate tenantId
        int tenantId = gsUtil.extractTenantId(headers);

        //validation for add incentive scheme
        validateIncentiveScheme(scheme, true, tenantId);

        String token = GSUtil.extractToken(headers, validationService);

        // checking user is admin or not based on permission for that customer
        // if user is not admin it will throw exception
        boolean isAdmin = authManagementInterface.validateAdminPermission(token, tenantId);
        if(! isAdmin) {
            logger.error("Insufficient Permissions");
            throw new ResourceValidationException(ErrorCode.INSUFFICIENT_PERMISSIONS);
        }

        scheme = referralSchemeService.addIncentiveScheme(scheme, tenantId);

        logger.info("Added Incentive Scheme with ID : "+ scheme.getIncentiveSchemeId());

        logger.info("====== addIncentiveScheme end ======");

        return Response.status(Response.Status.OK).entity(scheme).build();
    }

    @PUT
    @Path("/schemes/{incentiveSchemeId}")
    public Response updateIncentiveScheme(@PathParam("incentiveSchemeId") long  incentiveSchemeId,Scheme scheme, @Context HttpHeaders headers) throws ResourceValidationException, DatabaseException, ParseException, BackendRequestException, ServiceException {

        logger.info("+++++ updateIncentiveScheme start +++++ incentiveSchemeId: " + incentiveSchemeId);

        scheme = updateCountriesToUpperCase(scheme);

        validationService.validateId((int) incentiveSchemeId);

        //validate tenantId
        int tenantId = gsUtil.extractTenantId(headers);

        //validation for update incentive scheme
        validateIncentiveScheme(scheme, false, tenantId);

        String token = GSUtil.extractToken(headers, validationService);

        // checking user is admin or not based on permission
        // if user not admin it will throw exception
        boolean isAdmin = authManagementInterface.validateAdminPermission(token, tenantId);
        if(! isAdmin) {
            logger.error("Insufficient Permissions");
            throw new ResourceValidationException(ErrorCode.INSUFFICIENT_PERMISSIONS);
        }

        scheme = referralSchemeService.updateIncentiveScheme((int)incentiveSchemeId, scheme, tenantId);

        logger.info("======  updateIncentiveScheme end ======");

        return Response.status(Response.Status.OK).entity(scheme).build();
    }

    @GET
    @Path("/schemes/{incentiveSchemeId}")
    public Response getIncentiveScheme(@PathParam("incentiveSchemeId") long  incentiveSchemeId, @Context HttpHeaders headers) throws ResourceValidationException, JSONException, BackendRequestException, ServiceException {

        logger.info("+++++ getIncentiveScheme start +++++ incentiveSchemeId: " + incentiveSchemeId);

        validationService.validateId((int) incentiveSchemeId);

        String token = GSUtil.extractToken(headers, validationService);
        int tenantId = gsUtil.extractTenantId(headers);

        // customer token valid or not will be checked
        boolean isTokenValid = authManagementInterface.validateCustomerToken(token, tenantId);
        if(!isTokenValid) {
            logger.error("Invalid Token");
            throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
        }

        Scheme scheme = referralSchemeService.getIncentiveScheme((int) incentiveSchemeId, tenantId);

        logger.info("===== getIncentiveScheme end ======");

        // if user admin, directly returning response with all fields.
        return Response.status(Response.Status.OK).entity(scheme).build();
    }

    @GET
    @Path("/schemes")
    public Response getAllIncentiveSchemes(@DefaultValue("0") @QueryParam("startIndex") int startIndex,
                                           @DefaultValue("10") @QueryParam("count") int count,
                                           @QueryParam("country_code") String countryCode,
                                           @QueryParam("isEnabled") String isEnabled,
                                           @QueryParam("date") String date,
                                           @Context HttpHeaders headers) throws ResourceValidationException, JSONException, ParseException, BackendRequestException, ServiceException {

        logger.info("++++ getAllIncentiveSchemes start ++++++ countryCode " + countryCode +" isEnabled :"+ isEnabled +" date: " + date + " startIndex:"+ startIndex +" count:" + count);

        validationService.validateStartIndexAndCount(startIndex, count);

        String token = GSUtil.extractToken(headers, validationService);
        int tenantId = gsUtil.extractTenantId(headers);

        // customer token valid or not will be checked
        boolean isTokenValid = authManagementInterface.validateCustomerToken(token, tenantId);
        if(!isTokenValid) {
            logger.error("Invalid Token");
            throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
        }

        if(countryCode != null)
            validateCountryCode(countryCode);

        if(isEnabled != null && !GSSchemeUtil.validateIsEnabled(isEnabled)){
            logger.error("Referral scheme requires valid scheme enabled");
            throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_SCHEME_ENABLED);
        }

        if(date != null)
            CommonUtils.isValidDateFormat(date);

        PageInfo inputPageInfo = new PageInfo(startIndex, count);
        PageInfo outputPageInfo =  new PageInfo();

        List<Scheme> list = referralSchemeService.getAllIncentiveScheme(countryCode, isEnabled, inputPageInfo, outputPageInfo, date, tenantId);

        SchemeList schemeList = new SchemeList();
        schemeList.setType("SchemeDetails");
        schemeList.setStartIndex(outputPageInfo.getStart());
        schemeList.setCount(list.size());
        schemeList.setTotalCount(outputPageInfo.getCount());

        schemeList.setList(list);

        logger.info("===== getAllIncentiveSchemes end ======");

        return Response.status(Response.Status.OK).entity(schemeList).build();
    }

    @DELETE
    @Path("/schemes/{incentiveSchemeId}")
    public Response deleteIncentiveScheme(@PathParam("incentiveSchemeId") long  incentiveSchemeId, @Context HttpHeaders headers) throws ResourceValidationException {

        logger.error("Method not supported");
        throw new ResourceValidationException(ErrorCode.METHOD_NOT_SUPPORTED);
    }


    @GET
    @Path("/referralCode/{referralCode}")
    public Response getReferralSchemeDetails(@PathParam("referralCode") String  referralCode, @QueryParam("currency") String currency, @Context HttpHeaders headers) throws ResourceValidationException, JSONException, BackendRequestException, ServiceException {

        logger.info("+++++ getReferralSchemeDetails start +++++  Referral code : "+ referralCode +" currency: "+ currency);

        boolean isTokenValid = false;
        int tenantId = gsUtil.extractTenantId(headers);

        if(GSUtil.checkIsTokenAvailable(headers))
        {
            String token = GSUtil.extractToken(headers, validationService);
            if(token != null) {
                // customer token valid or not will be checked
                isTokenValid = authManagementInterface.validateCustomerToken(token, tenantId);
            }
        }

        // validation for  referral code
        validateReferralCode(referralCode, tenantId);
        if(StringUtils.isNotEmpty(currency) && !gsSchemeUtil.isValidCurrency(currency, tenantId)){
            logger.error("Referral scheme requires valid currency");
            throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_CURRENCY);
        }

        Scheme scheme = referralSchemeService.getSchemeForReferralSchemeDetails(referralCode.toLowerCase(), currency, tenantId);

        // If token valid then only we will return customer info..
        CustomerDBBean customerInfo = null;
        if(isTokenValid) {
            //Pass admin token
            customerInfo = getCustomerDetails(referralCode.toUpperCase(), gsTokenUtil.getValidToken(tenantId), tenantId);
        }

        ReferralScheme referralScheme = new ReferralScheme(scheme, customerInfo);

        logger.info("===== getReferralSchemeDetails end =====");

        return Response.status(Response.Status.OK).entity(referralScheme).build();
    }

    @GET
    @Path("/activeSchemes")
    public Response getActiveScheme(@QueryParam("country_code") String  countryCode, @QueryParam("currency") String currency, @Context HttpHeaders headers) throws ResourceValidationException, JSONException, BackendRequestException, ServiceException {

        logger.info("++++ getActiveScheme start +++++ countryCode: "+ countryCode +" currency:" + currency);

        int tenantId = gsUtil.extractTenantId(headers);

        if(GSUtil.checkIsTokenAvailable(headers))
        {
            String token = GSUtil.extractToken(headers, validationService);
            // customer token valid or not will be checked
            boolean isTokenValid = authManagementInterface.validateCustomerToken(token, tenantId);
            if(!isTokenValid) {
                logger.error("Invalid Token");
                throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
            }
        }

        if(countryCode != null)
            validateCountryCode(countryCode);

        if(StringUtils.isNotEmpty(currency) && !gsSchemeUtil.isValidCurrency(currency, tenantId)){
            logger.error("Referral scheme requires valid currency");
            throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_CURRENCY);
        }

        Scheme scheme = referralSchemeService.getActiveScheme(countryCode, currency, tenantId);

        logger.info("====== getActiveScheme end ======");

        return Response.status(Response.Status.OK).entity(scheme).build();
    }

    private void validateCountryCode(String countryCode) throws ResourceValidationException
    {
        CountryDBBean countryDBBean = validCountryDao.getCountryByCountryCode(countryCode);
        if(!countryCode.equals("ALL") && countryDBBean == null)
        {
            logger.error("Referral scheme requires valid country code");
            throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_COUNTRY_CODE);
        }
    }

    private void validateIncentiveScheme(Scheme scheme, boolean isAddFlow, int tenantId) throws ResourceValidationException, ParseException
    {

        // Validate incentive scheme type
        validateType(scheme);

        // validation for start date
        if(!GSSchemeUtil.isValidStartDate(scheme.getValidityStart(), scheme.getValidityEnd(), isAddFlow)) {
            logger.error("Referral scheme requires valid validity start date");
            throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_VALIDITY_START_DATE);
        }

        //validation for end date
        if(!GSSchemeUtil.isValidEndDate(scheme.getValidityStart(), scheme.getValidityEnd(), isAddFlow))
        {
            logger.error("Referral scheme requires valid validity end date");
            throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_VALIDITY_END_DATE);
        }

        // validation for countries
        validateCountry(scheme, isAddFlow);

        // validation for advocate credit expiry period
        if(!GSSchemeUtil.isValidExpiryPeriod(scheme.getAdvocateCreditExpiryPeriod(), isAddFlow))
        {
            logger.error("Referral scheme requires valid advocate credit expiry period");
            throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_ADVOCATE_CREDIT_EXPIRY_PEROID);
        }

        // validation for referral credit expiry period
        if(!GSSchemeUtil.isValidExpiryPeriod(scheme.getReferralCreditExpiryPeriod(), isAddFlow))
        {
            logger.error("Referral scheme requires valid referral credit expiry period");
            throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_REFERRAL_CREDIT_EXPIRY_PERIOD);
        }

        // validate scheme enable in add incentive scheme flow
        if(isAddFlow && scheme.getSchemeEnabled() == null)
        {
            logger.error("Referral scheme requires valid scheme enabled");
            throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_SCHEME_ENABLED);
        }

        // validate all credit schemes
        if(! isValidCreditScheme(scheme.getCreditScheme(), isAddFlow, tenantId)){
            logger.error("Referral scheme requires valid credit scheme");
            throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_CREDIT_SCHEME);
        }

        //Validate update incentive scheme, It should have at least one input value.
        if(!isAddFlow )
            validateUpdateSchemeForAtLeastOneInput(scheme);
    }

    private void validateType(Scheme scheme) throws ResourceValidationException{

        if(scheme.getType() == null || !(scheme.getType().equals("SchemeDetail")))
        {
            logger.error("Referral scheme requires valid type");
            throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_TYPE);
        }
    }

    private void validateCountry(Scheme scheme, boolean isAddFlow) throws ResourceValidationException {

        boolean isValidCountry = true;

        if(isAddFlow && (scheme.getCountry() == null || scheme.getCountry().size() == 0 || !isValidCountryList(scheme.getCountry()))) {
                isValidCountry = false;

        } else if(scheme.getCountry() != null && (scheme.getCountry().size() == 0 || !isValidCountryList(scheme.getCountry()))) {
                isValidCountry = false;
        }

        if(! isValidCountry){
            logger.error("Referral scheme requires valid country code");
            throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_COUNTRY_CODE);
        }

    }



    private boolean isValidCountryList(List<String> countries)
    {
        if(countries.contains("ALL") && countries.size() > 1)
        {
            return false;
        }

        for(int i = 0; i < countries.size(); i++)
        {
            String countryCode = countries.get(i);
            CountryDBBean countryDBBean = validCountryDao.getCountryByCountryCode(countryCode);
            if(!countryCode.equals("ALL") && countryDBBean == null)
            {
                return false;
            }
        }
        return true;
    }


    private boolean isValidCreditScheme(List<CreditScheme> schemeList, boolean isAddFlow, int tenantId) throws ResourceValidationException
    {

        // if it is add flow scheme list size should match CREDIT_SCHEME_COUNT
        if(isAddFlow && (!referralSchemeService.isValidCreditScheme(schemeList, tenantId))) {
                return false;

        } else if(schemeList != null && schemeList.size() == 0) {
                 return false;
        }

        // update case scheme list null possible
        if(schemeList != null) {
            // validate all credit schemes
            List<String> currencies =  new ArrayList<String>();

            for(int i = 0; i < schemeList.size(); i++)
            {
                CreditScheme creditScheme =  schemeList.get(i);
                String currency = creditScheme.getCurrency();

                // this validation will check any currency repeated or not
                if(!currencies.contains(currency)) {
                    currencies.add(currency);
                } else {
                    return false;
                }

                // this will check currency valid or not
                if(!gsSchemeUtil.isValidCurrency(currency, tenantId)){
                    logger.error("Referral scheme requires valid currency");
                    throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_CURRENCY);
                }

                float advocateAmount = creditScheme.getAdvocateAmount();
                if(GSSchemeUtil.isValidCreditAmount(currency,advocateAmount)){
                    logger.error("Referral scheme requires valid advocate amount");
                    throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_ADVOCATE_AMOUNT);
                }

                float newCustomerAmount =  creditScheme.getNewCustomerAmount();
                if(GSSchemeUtil.isValidCreditAmount(currency, newCustomerAmount)){
                    logger.error("Referral scheme requires valid new customer amount");
                    throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_NEW_CUSTOMER_AMOUNT);
                }
            }
        }

        return true;
    }

    private void validateReferralCode(String referralCode, int tenantId) throws ResourceValidationException
    {
        //referral should be greater than or equal to 4 and less than or equal 9
        if(referralCode == null || referralCode.length() < Configurations.REFERRAL_CODE_MIN_LENGTH || referralCode.length() > Configurations.REFERRAL_CODE_MAX_LENGTH)
        {
            logger.error("Referral scheme requires valid referral code");
            throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_REFERRAL_CODE);
        }
        else{

            //Fetch referralCodeDBBean for key referralCode
            ReferralCodeDBBean referralCodeDBBean = referralCodeDao.getReferralCodeInfoByReferralCode(referralCode, tenantId);
            boolean isMaxReferredCountReached = gsTaskUtil.isMaxReferredCountReached(referralCodeDBBean);

            //If referralCode not in DB throw exception
            if(referralCodeDBBean == null || referralCodeDBBean.getIsActive() == 0 || isMaxReferredCountReached)
            {
                logger.error("Referral scheme requires valid referral code");
                throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_VALID_REFERRAL_CODE);
            }
        }
    }

    private CustomerDBBean getCustomerDetails(String referralCode, String token, int tenantId) throws BackendRequestException, ServiceException {

        //Get gsCustomerId
        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByReferralCode(referralCode, tenantId);
        long gsCustomerId = customerDBBean.getGsCustomerId();

        //Get customerDetails
        return accountService.getCustomerDetails(gsCustomerId, customerDBBean.getTenantId());
    }

    private Scheme updateCountriesToUpperCase(Scheme scheme)
    {
        if(scheme.getCountry() != null) {
            List<String> uppercaseCountriesList = GSSchemeUtil.updateCountryCodesToUpperCase(scheme.getCountry());
            scheme.setCountry(uppercaseCountriesList);
        }
        return scheme;
    }


    private void validateUpdateSchemeForAtLeastOneInput(Scheme scheme) throws ResourceValidationException {

        if(scheme.getValidityStart() == null && scheme.getValidityEnd() == null
                && scheme.getCountry() == null && (scheme.getAdvocateCreditExpiryPeriod() == null
                || scheme.getAdvocateCreditExpiryPeriod() == 0) && (scheme.getReferralCreditExpiryPeriod() == null
                || scheme.getReferralCreditExpiryPeriod() == 0) && scheme.getSchemeEnabled() == null
                && scheme.getCreditScheme() == null)
        {
            logger.error("Referral Scheme requires at least one input value");
            throw new ResourceValidationException(ErrorCode.REFERRAL_SCHEME_REQUIRES_AT_LEAST_ONE_INPUT_VALUE);
        }
    }

}
