package com.gigsky.rest.api.v1;

import com.gigsky.backend.auth.AuthManagementInterface;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.TenantConfigurationsKeyValueDBBean;
import com.gigsky.database.dao.TenantConfigurationsKeyValueDao;
import com.gigsky.rest.bean.Config;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.ValidationService;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by prsubbareddy on 24/01/17.
 */

@Path("/api/v1/configs")
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Controller
public class ReferralConfigResource {

    private static final Logger logger = LoggerFactory.getLogger(ReferralConfigResource.class);

    @Autowired
    private AuthManagementInterface authManagementInterface;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private TenantConfigurationsKeyValueDao tenantConfigurationsKeyValueDao;

    @Autowired
    private GSUtil gsUtil;

    @GET
    public Response getReferralConfig(@Context HttpHeaders headers) throws ResourceValidationException, ServiceException{

        logger.info("++++ getReferralConfig start ++++");
        int tenantId = gsUtil.extractTenantId(headers);

        String configValue = tenantConfigurationsKeyValueDao.getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.REFERRAL_ENABLE, "false", tenantId);
        boolean referralEnable = configValue.equals("true");

        Config config = new Config();
        config.setType("ConfigDetail");
        config.setReferralEnable(referralEnable);

        logger.info("===== getReferralConfig end =====");

        return Response.status(Response.Status.OK).entity(config).build();
    }

    @PUT
    public Response updateReferralConfig(Config config, @Context HttpHeaders headers) throws ResourceValidationException, BackendRequestException, ServiceException {

        logger.info("++++ updateReferralConfig start ++++");

        String token = GSUtil.extractToken(headers, validationService);
        int tenantId = gsUtil.extractTenantId(headers);

        // checking user is admin or not based on permission for that customer
        // if user is not admin it will throw exception
        boolean isAdmin = authManagementInterface.validateAdminPermission(token, tenantId);
        if(! isAdmin) {
            logger.error("Insufficient Permissions");
            throw new ResourceValidationException(ErrorCode.INSUFFICIENT_PERMISSIONS);
        }

        validateConfig(config);
        String configValue = (config.getReferralEnable())?"true":"false";
        logger.info("Updating referral config to "+ configValue);

        TenantConfigurationsKeyValueDBBean keyValueBean = new TenantConfigurationsKeyValueDBBean();
        keyValueBean.setCkey(Configurations.TenantConfigurationKeyValueType.REFERRAL_ENABLE);
        keyValueBean.setValue(configValue);
        keyValueBean.setTenantId(tenantId);
        tenantConfigurationsKeyValueDao.updateTenantConfigurationValue(keyValueBean);

        logger.info("===== updateReferralConfig end =====");

        return Response.status(Response.Status.OK).entity(null).build();
    }

    private void validateConfig(Config config) throws ResourceValidationException {

        if(config.getType() == null || !config.getType().equals("ConfigDetail")) {
            logger.error("Referral config requires valid type");
            throw new ResourceValidationException(ErrorCode.REFERRAL_CONFIG_REQUIRES_VALID_TYPE);
        }

        if(config.getReferralEnable() == null) {
            logger.error("Referral config requires valid referral enable");
            throw new ResourceValidationException(ErrorCode.REFERRAL_CONFIG_REQUIRES_VALID_REFERRAL_ENABLE);
        }
    }

}
