package com.gigsky.rest.api.v1;

import com.gigsky.backend.auth.AuthManagementInterface;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.InvitationDBBean;
import com.gigsky.database.bean.ReferralCodeDBBean;
import com.gigsky.database.dao.*;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.rest.bean.Account;
import com.gigsky.rest.bean.AssociateReferralCode;
import com.gigsky.rest.bean.CodeInfo;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.AccountService;
import com.gigsky.service.PromotionService;
import com.gigsky.service.ValidationService;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSSchemeUtil;
import com.gigsky.util.GSTaskUtil;
import com.gigsky.util.GSUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.text.ParseException;

/**
 * Created by prsubbareddy on 18/01/17.
 */

@Path("/api/v1/account")
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Controller
public class CustomerResource {

    private static final Logger logger = LoggerFactory.getLogger(CustomerResource.class);

    @Autowired
    private ValidationService validationService;

    @Autowired
    private AuthManagementInterface authManagementInterface;

    @Autowired
    private AccountService accountService;

    @Autowired
    private ReferralCodeDao referralCodeDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private GSTaskUtil gsTaskUtil;

    @Autowired
    private InvitationDao invitationDao;

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    private PromotionService promotionService;

    @Autowired
    private SupportedCurrenciesDao supportedCurrenciesDao;

    @Autowired
    private GSUtil gsUtil;

    @Autowired
    private GSSchemeUtil gsSchemeUtil;

    @GET
    @Path("/{gsCustomerId}")
    public Response getAccountDetails(@PathParam("gsCustomerId") long gsCustomerId, @Context HttpHeaders headers) throws BackendRequestException, ServiceException, ResourceValidationException {

        logger.info("++++ getAccountDetails start ++++ customerId:" + gsCustomerId);

        validationService.validateId((int) gsCustomerId);
        String token = GSUtil.extractToken(headers, validationService);
        int tenantId = gsUtil.extractTenantId(headers);

        //Check if valid token is passed
        boolean isValidToken = authManagementInterface.validateCustomerToken(token, (int) gsCustomerId, tenantId);

        //If invalid token, throw exception
        if(!isValidToken){

            logger.error("Invalid Token");
            throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
        }

        String language = GSUtil.getLocaleLanguage();
        //logger.info("Account details header language : "+ language);

        Account account = accountService.getAccountDetails((int) gsCustomerId, token, tenantId);

        //Set or Update language for customer if not exist or update requires.
        accountService.updateLanguageForCustomer((int) gsCustomerId, language, tenantId);

        logger.info("===== getAccountDetails end =====");

        return Response.status(javax.ws.rs.core.Response.Status.OK).entity(account).build();
    }

    @POST
    @Path("/{gsCustomerId}/associate")
    public Response associateReferralCode(@PathParam("gsCustomerId") long gsCustomerId, @Context HttpHeaders headers, AssociateReferralCode associateReferralCode) throws BackendRequestException, ServiceException, ResourceValidationException {


        logger.info("++++ associateReferralCode start ++++ gsCustomerId: " + gsCustomerId);

        validationService.validateId((int) gsCustomerId);

        validateAssociateReferralCode(associateReferralCode);

        String token = GSUtil.extractToken(headers, validationService);
        int tenantId = gsUtil.extractTenantId(headers);

        // checking user is admin or not based on permission for that customer
        // if user is not admin it will throw exception
        boolean isAdmin = authManagementInterface.validateAdminPermission(token, tenantId);
        if(! isAdmin) {
            logger.error("Insufficient Permissions");
            throw new ResourceValidationException(ErrorCode.INSUFFICIENT_PERMISSIONS);
        }

        // Fetch referral code details by referral code
        ReferralCodeDBBean referralCodeDBBean = referralCodeDao.getReferralCodeInfoByReferralCode(associateReferralCode.getReferralCode(), tenantId);
        if(referralCodeDBBean == null || referralCodeDBBean.getIsActive() == 0 || gsTaskUtil.isMaxReferredCountReached(referralCodeDBBean))
        {
            logger.error("Associate referral requires valid referral code");
            throw new ResourceValidationException(ErrorCode.ASSOCIATE_REFERRAL_REQUIRES_VALID_REFERRAL_CODE);
        }

        int referredCount = 0;
        byte isPromotionalCode = 0;

        referredCount = ((referralCodeDBBean.getReferredCount() == null) ? referredCount : referralCodeDBBean.getReferredCount());
        isPromotionalCode = ((referralCodeDBBean.getIsPromotionalCode() == null) ? isPromotionalCode : referralCodeDBBean.getIsPromotionalCode());

        if(isPromotionalCode == 1)
        {
            referralCodeDBBean.setReferredCount(++referredCount);
            referralCodeDao.updateReferralCode(referralCodeDBBean);
        } else {
            int maxReferredCount = Integer.valueOf(configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.MAX_REFERRED_COUNT));
            if(referredCount == 0 )
            {
                referralCodeDBBean.setReferredCount(++referredCount);
                referralCodeDBBean.setFirstReferredTime(CommonUtils.getCurrentTimestampInUTC());
                referralCodeDao.updateReferralCode(referralCodeDBBean);
            } else {
                if(referredCount < maxReferredCount)
                {
                    referralCodeDBBean.setReferredCount(++referredCount);
                    referralCodeDao.updateReferralCode(referralCodeDBBean);
                } else {
                    referralCodeDBBean.setReferredCount(1);
                    referralCodeDBBean.setFirstReferredTime(CommonUtils.getCurrentTimestampInUTC());
                    referralCodeDao.updateReferralCode(referralCodeDBBean);
                }
            }
        }

        //check referral engine enabled or not
        if(gsTaskUtil.checkReferralEnable(tenantId)) {

            // process referral credit for customer if status as "SIGNED_UP"
            // process both referral credit & advocate credit if status as "USING_SERVICE"
            processReferralAdvocateCredit(gsCustomerId, referralCodeDBBean, token, tenantId);

        } else {
            // throw exception to admin referral program is disabled.
            throw new ResourceValidationException(ErrorCode.REFERRAL_PROGRAM_DISABLED);
        }

        logger.info("===== associateReferralCode end =====");

        return Response.status(javax.ws.rs.core.Response.Status.OK).entity(null).build();
    }

    @POST
    @Path("/{gsCustomerId}/SIM/{simId}/applyCode")
    public Response applyPromoCode(CodeInfo codeInfo, @PathParam("gsCustomerId") String  gsCustomerId, @PathParam("simId") String simId, @Context HttpHeaders headers) throws Exception {

        logger.info("+++++ applyPromoCode start +++++ gsCustomerId: " + gsCustomerId);

        int tenantId = gsUtil.extractTenantId(headers);

        validateCodeInfoInput(codeInfo, tenantId);

        String token = GSUtil.extractToken(headers, validationService);

        boolean isTokenValid = authManagementInterface.validateCustomerToken(token, tenantId);
        if(!isTokenValid) {
            logger.error("Invalid Token");
            throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
        }

        boolean isValidSimId = authManagementInterface
                .validateCustomerSimId(token, new Integer(gsCustomerId), tenantId, simId);
        if(!isValidSimId)
        {
            logger.error("Invalid simId");
            throw new ResourceValidationException(ErrorCode.INVALID_SIM_ID);
        }

        CodeInfo outputCodeInfo;

        outputCodeInfo = promotionService.applyPromoCode(codeInfo, token, tenantId, Integer.parseInt(gsCustomerId), simId);

        logger.info("====== applyPromoCode end ======");

        return Response.status(Response.Status.OK).entity(outputCodeInfo).build();
    }

    @GET
    @Path("/{gsCustomerId}/checkStatus/{code}")
    public Response checkCodeStatus(@PathParam("gsCustomerId") String  gsCustomerId, @PathParam("code") String  code, @Context HttpHeaders headers) throws ResourceValidationException, DatabaseException, ParseException, BackendRequestException, ServiceException {

        logger.info("+++++ checkStatus start +++++ gsCustomerId: " + gsCustomerId + " and code: "+code);

        String token = GSUtil.extractToken(headers, validationService);

        int tenantId = gsUtil.extractTenantId(headers);

        boolean isTokenValid = authManagementInterface.validateCustomerToken(token, tenantId);
        if(!isTokenValid) {
            logger.error("Invalid Token");
            throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
        }

        if(StringUtils.isEmpty(code))
        {
            logger.error("Invalid code");
            throw new ResourceValidationException(ErrorCode.INVALID_CODE);
        }

        CodeInfo outputCodeInfo = promotionService.checkCodeStatus(code, token, Integer.parseInt(gsCustomerId), tenantId);

        logger.info("====== checkStatus end ======");

        return Response.status(Response.Status.OK).entity(outputCodeInfo).build();
    }

    private void processReferralAdvocateCredit(long gsCustomerId, ReferralCodeDBBean referralCodeDBBean, String token, int tenantId) throws BackendRequestException, ServiceException, ResourceValidationException {

        // Get customer details for gsCustomerId
        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, tenantId);

        if(customerDBBean == null){
            //throw exception to admin customer doesn't exist
            throw new ResourceValidationException(ErrorCode.CUSTOMER_DOES_NOT_EXIST);
        }

        if(customerDBBean.getId() == referralCodeDBBean.getCustomerId()) {
            // throw exception for fraud case
            throw new ResourceValidationException(ErrorCode.CANNOT_APPLY_REFERRAL_CODE_FROM_SELF_ACCOUNT);
        }

        Integer advocateId = customerDBBean.getAdvocateId();
        if(advocateId == null) {

            Timestamp createdOn = (customerDBBean.getAccountCreationTime() != null) ? customerDBBean.getAccountCreationTime() : accountService.getCustomerDetails(gsCustomerId, customerDBBean.getTenantId()).getAccountCreationTime();

            // create invitation entry
            InvitationDBBean invitationDBBean = gsTaskUtil.createInvitationEntry(customerDBBean, referralCodeDBBean, createdOn);

            // Getting advocate id using advocate referral code bean
            customerDBBean.setAdvocateId(referralCodeDBBean.getCustomerId());
            customerDBBean.setReferralStatus(Configurations.ReferralStatus.CREDIT_ADD_IN_PROCESS);
            customerDao.updateCustomerInfo(customerDBBean);

            // add referral credit for customer
            gsTaskUtil.processAddCreditForCustomer(invitationDBBean, customerDBBean, false, null);

            // add advocate credit for customer
            if (Configurations.CustomerStatus.STATUS_USING_SERVICE.equals(customerDBBean.getStatus())) {
                gsTaskUtil.processAddCreditForCustomer(invitationDBBean, customerDBBean, true, null);
            }

        } else {

            // Get invitation details and verify referral credit id and advocate credit id.
            InvitationDBBean invitationDBBean = invitationDao.getInvitationByTargetCustomerId(customerDBBean.getId());

            Integer referralCreditId = invitationDBBean.getReferralCreditId();
            Integer advocateCreditId = invitationDBBean.getAdvocateCreditId();

            boolean isCreditAddedAlready = true;

            if (referralCreditId == null) {

                isCreditAddedAlready = false;

                // Getting advocate id using advocate referral code bean
                customerDBBean.setAdvocateId(referralCodeDBBean.getCustomerId());
                customerDBBean.setReferralStatus(Configurations.ReferralStatus.CREDIT_ADD_IN_PROCESS);
                customerDao.updateCustomerInfo(customerDBBean);

                // add referral credit for customer
                gsTaskUtil.processAddCreditForCustomer(invitationDBBean, customerDBBean, false, null);
            }

            if(Configurations.CustomerStatus.STATUS_USING_SERVICE.equals(customerDBBean.getStatus()) && advocateCreditId == null) {

                isCreditAddedAlready = false;

                // add advocate credit for customer
                gsTaskUtil.processAddCreditForCustomer(invitationDBBean, customerDBBean, true, null);
            }

            if(isCreditAddedAlready){
                //throw exception to admin credit is already added for customer
                throw new ResourceValidationException(ErrorCode.CREDIT_ALREADY_ADDED_FOR_CUSTOMER);
            }

        }

    }

    private void validateAssociateReferralCode(AssociateReferralCode associateReferralCode) throws ResourceValidationException {

        if(associateReferralCode.getType() == null || !associateReferralCode.getType().equals("AssociateReferral")){
            logger.error("Associate referral requires valid type");
            throw new ResourceValidationException(ErrorCode.ASSOCIATE_REFERRAL_REQUIRES_VALID_TYPE);
        }

        String referralCode = associateReferralCode.getReferralCode();
        //referral should be greater than or equal to 4 and less than or equal 9
        if(referralCode == null || referralCode.length() < Configurations.REFERRAL_CODE_MIN_LENGTH || referralCode.length() > Configurations.REFERRAL_CODE_MAX_LENGTH) {
            logger.error("Associate referral requires valid referral code");
            throw new ResourceValidationException(ErrorCode.ASSOCIATE_REFERRAL_REQUIRES_VALID_REFERRAL_CODE);
        }

    }

    private void validateCodeInfoInput(CodeInfo codeInfo, int tenantId) throws ResourceValidationException, ServiceException, DatabaseException, ParseException{
        if(StringUtils.isEmpty(codeInfo.getType())|| !(codeInfo.getType().equals("ApplyCode")))
        {
            logger.error("Promo Code requires valid type");
            throw new ResourceValidationException(ErrorCode.PROMO_CODE_REQUIRES_VALID_TYPE);
        }

        if(StringUtils.isEmpty(codeInfo.getCode()) || !(promotionService.isValidCode(codeInfo.getCode(), tenantId, true)))
        {
            logger.error("Promo Code requires valid code");
            throw new ResourceValidationException(ErrorCode.INVALID_PROMO_CODE);
        }

        String currency = codeInfo.getCurrency();
        if(StringUtils.isNotEmpty(currency) && !gsSchemeUtil.isValidCurrency(currency, tenantId)){
            logger.error("Promo Code requires valid currency");
            throw new ResourceValidationException(ErrorCode.PROMO_CODE_REQUIRES_VALID_CURRENCY);
        }

        Boolean validationMode = codeInfo.getValidationMode();
        if(validationMode == null)
        {
            logger.error("Promo Code requires valid validation mode");
            throw new ResourceValidationException(ErrorCode.PROMO_CODE_REQUIRES_VALID_VALIDATION_MODE);
        }

        Float cartAmount = codeInfo.getCartAmount();
        if(cartAmount == null ||
                (cartAmount!=null && (cartAmount == 0 || cartAmount <= 0)))
        {
            logger.error("Promo Code requires valid cartAmount");
            throw new ResourceValidationException(ErrorCode.PROMO_CODE_REQUIRES_VALID_CART_AMOUNT);
        }

    }



}
