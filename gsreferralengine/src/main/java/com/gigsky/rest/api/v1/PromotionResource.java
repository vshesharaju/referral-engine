package com.gigsky.rest.api.v1;

import com.gigsky.backend.auth.AuthManagementInterface;
import com.gigsky.rest.bean.PromoUsageCsv;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CountryDBBean;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.database.dao.SupportedCurrenciesDao;
import com.gigsky.database.dao.TenantDao;
import com.gigsky.database.dao.ValidCountryDao;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.gsenterprisecommon.util.csvutils.CSVExporter;
import com.gigsky.rest.bean.*;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.PromotionService;
import com.gigsky.service.ValidationService;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSUtil;
import com.gigsky.util.GSSchemeUtil;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by shwethars on 09/01/19.
 */
@Path("/")
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Controller
public class PromotionResource {

    private static final Logger logger = LoggerFactory.getLogger(PromotionResource.class);

    @Autowired
    private ValidationService validationService;

    @Autowired
    private AuthManagementInterface authManagementInterface;

    @Autowired
    private PromotionService promotionService;
    
    @Autowired
    private SupportedCurrenciesDao supportedCurrenciesDao;

    @Autowired
    private ValidCountryDao validCountryDao;

    @Autowired
    private TenantDao tenantDao;

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    private GSUtil gsUtil;

    @Autowired
    private GSSchemeUtil gsSchemeUtil;

    @POST
    @Path("promotions")
    public Response addPromotion(Promotion promotion, @Context HttpHeaders headers) throws ResourceValidationException, DatabaseException, ParseException, BackendRequestException, ServiceException {

        logger.info("+++++ addPromotion start +++++");

        promotion = updateCountriesToUpperCase(promotion);

        //validate tenantId
        int tenantId = gsUtil.extractTenantId(headers);

        validatePromotionInput(promotion, true, tenantId);

        String token = GSUtil.extractToken(headers, validationService);

        // checking user is admin or not based on permission for that customer
        // if user is not admin it will throw exception
        boolean isAdmin = authManagementInterface.validateAdminPermission(token, tenantId);
        if(! isAdmin) {
            logger.error("Insufficient Permissions");
            throw new ResourceValidationException(ErrorCode.INSUFFICIENT_PERMISSIONS);
        }

        promotion = promotionService.addPromotion(promotion, tenantId);

        logger.info("Added Promotion with promoId : "+ promotion.getPromoId());

        logger.info("====== addPromotion end ======");

        return Response.status(Response.Status.OK).entity(promotion).build();
    }

    @PUT
    @Path("promotions/{promoId}")
    public Response updatePromotion(@PathParam("promoId") long  promoId, Promotion promotion, @Context HttpHeaders headers) throws ResourceValidationException, DatabaseException, ParseException, BackendRequestException, ServiceException {

        logger.info("+++++ updatePromotion start +++++ promoId: " + promoId);

        promotion = updateCountriesToUpperCase(promotion);

        validationService.validateId((int) promoId);

        //validate tenantId
        int tenantId = gsUtil.extractTenantId(headers);

        //validation for update promotion
        validatePromotionInput(promotion, false, tenantId);

        String token = GSUtil.extractToken(headers, validationService);

        // checking user is admin or not based on permission
        // if user not admin it will throw exception
        boolean isAdmin = authManagementInterface.validateAdminPermission(token, tenantId);
        if(! isAdmin) {
            logger.error("Insufficient Permissions");
            throw new ResourceValidationException(ErrorCode.INSUFFICIENT_PERMISSIONS);
        }

        promotion = promotionService.updatePromotion((int)promoId, promotion, tenantId);

        logger.info("======  updatePromotion end ======");

        return Response.status(Response.Status.OK).entity(promotion).build();
    }


    @DELETE
    @Path("promotions/{promoId}")
    public Response deletePromotion(@PathParam("promoId") long  promoId, @Context HttpHeaders headers) throws ResourceValidationException {

        logger.error("Method not supported");
        throw new ResourceValidationException(ErrorCode.METHOD_NOT_SUPPORTED);
    }

    @GET
    @Path("promoCode/{promoCode}")
    public Response getPromotionByPromoCode(@PathParam("promoCode") String  promoCode, @Context HttpHeaders headers) throws ResourceValidationException, DatabaseException, ParseException, BackendRequestException, ServiceException {

        logger.info("+++++ getPromotionByPromoCode start +++++ promoCode: " + promoCode);

        String token = GSUtil.extractToken(headers, validationService);

        int tenantId = gsUtil.extractTenantId(headers);

        boolean isTokenValid = authManagementInterface.validateCustomerToken(token, tenantId);
        if(!isTokenValid) {
            logger.error("Invalid Token");
            throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
        }

        boolean hasPermission = authManagementInterface.validateAdminReadPermission(token,tenantId);
        if(!hasPermission) {
            logger.error("Insufficient permission");
            throw new ResourceValidationException(ErrorCode.INSUFFICIENT_PERMISSIONS);
        }

        Promotion promotion = promotionService.getPromotionByPromoCode(promoCode, tenantId);

        logger.info("======  getPromotionByPromoCode end ======");

        return Response.status(Response.Status.OK).entity(promotion).build();
    }

    @GET
    @Path("promotions/{promoId}")
    public Response getPromotion(@PathParam("promoId") long  promoId, @Context HttpHeaders headers) throws ResourceValidationException, DatabaseException, ParseException, BackendRequestException, ServiceException {

        logger.info("+++++ getPromotion start +++++ promoId: " + promoId);

        validationService.validateId((int) promoId);

        String token = GSUtil.extractToken(headers, validationService);

        int tenantId = gsUtil.extractTenantId(headers);

        boolean isTokenValid = authManagementInterface.validateCustomerToken(token, tenantId);
        if(!isTokenValid) {
            logger.error("Invalid Token");
            throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
        }

        boolean hasPermission = authManagementInterface.validateAdminReadPermission(token,tenantId);
        if(!hasPermission) {
            logger.error("Insufficient permission");
            throw new ResourceValidationException(ErrorCode.INSUFFICIENT_PERMISSIONS);
        }

        Promotion promotion = promotionService.getPromotion((int)promoId, tenantId);

        logger.info("======  getPromotion end ======");

        return Response.status(Response.Status.OK).entity(promotion).build();
    }


    @GET
    @Path("promotions")
    public Response getAllPromotions(@DefaultValue("0") @QueryParam("startIndex") int startIndex,
                                     @DefaultValue("10") @QueryParam("count") int count,
                                     @QueryParam("country_code") String countryCode,
                                     @QueryParam("isEnabled") String isEnabled,
                                     @QueryParam("date") String date,
                                     @QueryParam("search") String search,
                                     @Context HttpHeaders headers) throws ResourceValidationException, JSONException, ParseException, BackendRequestException, ServiceException {

        logger.info("++++ getAllPromotions start ++++++ countryCode " + countryCode +" isEnabled :"+ isEnabled +"search" + search + " date: " + date + " startIndex:"+ startIndex +" count:" + count);

        validationService.validateStartIndexAndCount(startIndex, count);

        String token = GSUtil.extractToken(headers, validationService);

        int tenantId = gsUtil.extractTenantId(headers);

        // customer token valid or not will be checked
        boolean isTokenValid = authManagementInterface.validateCustomerToken(token, tenantId);
        if(!isTokenValid) {
            logger.error("Invalid Token");
            throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
        }

        boolean hasPermission = authManagementInterface.validateAdminReadPermission(token,tenantId);
        if(!hasPermission) {
            logger.error("Insufficient permission");
            throw new ResourceValidationException(ErrorCode.INSUFFICIENT_PERMISSIONS);
        }

        if(countryCode != null)
            validateCountryCode(countryCode);

        if(isEnabled != null && !GSSchemeUtil.validateIsEnabled(isEnabled)){
            logger.error("Promotion requires valid promo enabled");
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_PROMO_ENABLED);
        }

        if(date != null)
            CommonUtils.isValidDateFormat(date);

        List<Promotion> list;
        PageInfo inputPageInfo = new PageInfo(startIndex, count);
        PageInfo outputPageInfo = new PageInfo();

        if((StringUtils.isNotEmpty(search) && validateSearchString(search)) || StringUtils.isEmpty(search)) {

            list = promotionService.getAllPromotions(countryCode, isEnabled, search, inputPageInfo, outputPageInfo, date, tenantId);
        }else {
            list = Collections.emptyList();
        }

        PromotionList promoList = new PromotionList();
        promoList.setType("PromoDetails");
        promoList.setStartIndex(outputPageInfo.getStart());
        promoList.setCount(list.size());
        promoList.setTotalCount(outputPageInfo.getCount());
        promoList.setList(list);

        logger.info("===== getAllPromotions end ======");

        return Response.status(Response.Status.OK).entity(promoList).build();
    }


    @GET
    @Path("validateCode/{code}")
    public Response validateCode(@PathParam("code") String  code, @QueryParam("codeType") String codeType,@Context HttpHeaders headers) throws ResourceValidationException, DatabaseException, ParseException, BackendRequestException, ServiceException {

        logger.info("+++++ validateCode start +++++ code: " + code);
        int tenantId = gsUtil.extractTenantId(headers);

        if(GSUtil.checkIsTokenAvailable(headers))
        {
            String token = GSUtil.extractToken(headers, validationService);
            if(token != null) {
                // customer token valid or not will be checked
                if(!authManagementInterface.validateCustomerToken(token, tenantId)) {
                    logger.error("Invalid Token");
                    throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
                }
            }
        }

        if(codeType!=null && !("REFERRAL".equalsIgnoreCase(codeType) || "PROMOTION".equalsIgnoreCase(codeType)))
        {
            logger.error("Invalid code type");
            throw new ResourceValidationException(ErrorCode.INVALID_CODE_TYPE);
        }

        if(!promotionService.isValidCode(codeType, code, tenantId)){
            logger.error("Invalid code");
            throw new ServiceException(ErrorCode.INVALID_CODE);
        }

        logger.info("======  validateCode end ======");

        return Response.status(Response.Status.OK).build();
    }

    @GET
    @Path("promoUsage/{promotionId}")
    public Response getCustomersByPromotion(@PathParam("promotionId") Integer promotionId,
                                            @DefaultValue("0") @QueryParam("startIndex") int startIndex,
                                            @DefaultValue("10") @QueryParam("count") int count,
                                            @DefaultValue("DESC") @QueryParam("sortBy") String sortBy,
                                            @DefaultValue("JSON") @QueryParam("format") String format,
                                            @QueryParam("token") String token,
                                            @Context HttpHeaders headers) throws ResourceValidationException, DatabaseException, ParseException, BackendRequestException, ServiceException, IOException {

        logger.info("+++get customers subscribed this promotion : " + promotionId);

        validationService.validateStartIndexAndCount(startIndex, count);

        int tenantId = gsUtil.extractTenantId(headers);

        boolean isCSV = (StringUtils.isNotEmpty(format) && format.toUpperCase().equals(gsUtil.CSV));

        if(isCSV){
            //set the token from query parameter for CSV responses
            token = gsUtil.getTokenFromString(token);
        }
        else {
            token = GSUtil.extractToken(headers, validationService);
        }

        if(StringUtils.isNotEmpty(format)){
            validateFormatString(format);
        }

        // customer token valid or not will be checked
        boolean isTokenValid = authManagementInterface.validateCustomerToken(token, tenantId);
        if (!isTokenValid) {
            logger.error("Invalid Token");
            throw new ResourceValidationException(ErrorCode.INVALID_TOKEN);
        }

        boolean hasPermission = authManagementInterface.validateAdminReadPermission(token,tenantId);
        if(!hasPermission) {
            logger.error("Insufficient permission");
            throw new ResourceValidationException(ErrorCode.INSUFFICIENT_PERMISSIONS);
        }

        //validate sortBy entry
        if(StringUtils.isNotEmpty(sortBy)){
            validateSortByInput(sortBy);
        }

        PageInfo inputPageInfo = new PageInfo(startIndex, count);

        PromoHistoryList list = promotionService.getPromoHistoryByPromoId(promotionId, sortBy, inputPageInfo);

        if(isCSV){
            logger.info("===== getRedeemedPromotions csv download======");
            return CSVExporter.exportToCsv(list.getList(),PromoUsageCsv.class,gsUtil.PROMO_USAGE_FILE_NAME+list.getPromotionId()+".csv");
        }
        else
            logger.info("===== getRedeemedPromotions end ======");
            return Response.status(Response.Status.OK).entity(list).build();

    }


    private void validatePromotionInput(Promotion promotion, boolean isAddFlow, int tenantId) throws ResourceValidationException, ParseException{

        // Validate promotion type
        validateType(promotion.getType());

        // validate promoName in add promotion flow
        if(isAddFlow && StringUtils.isEmpty(promotion.getName()))
        {
            logger.error("Promotion requires valid name");
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_NAME);
        }

        // validate promoCode in add promotion flow
        if(isAddFlow && !isValidPromoCodeForAdd(promotion.getPromoCode()))
        {
            logger.error("Promotion requires valid promo code");
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_PROMO_CODE);
        }

        // validate promoCode in update promotion flow
        if(!isAddFlow && StringUtils.isNotEmpty(promotion.getPromoCode()) && !isValidPromoCodeForAdd(promotion.getPromoCode()))
        {
            logger.error("Promotion requires valid promo code");
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_PROMO_CODE);
        }

        // validation for start date
        if(!GSSchemeUtil.isValidStartDate(promotion.getStartDate(), promotion.getEndDate(), isAddFlow))
        {
            logger.error("Promotion requires valid start date");
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_START_DATE);
        }

        //validation for end date
        if(!GSSchemeUtil.isValidEndDate(promotion.getStartDate(), promotion.getEndDate(), isAddFlow))
        {
            logger.error("Promotion requires valid end date");
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_END_DATE);
        }

        // validation for countries
        if(!validateCountries(promotion.getCountry(), isAddFlow))
        {
            logger.error("Promotion requires valid country code");
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_COUNTRY_CODE);
        }

        // validation for advocate credit expiry period
        if(!GSSchemeUtil.isValidExpiryPeriod(promotion.getCreditExpiryPeriod(), isAddFlow))
        {
            logger.error("Promotion requires valid credit expiry period");
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_CREDIT_EXPIRY_PERIOD);
        }

        // validation for max redeem count
        if(!GSSchemeUtil.isValidMaxRedeemCount(promotion.getMaxRedeemCount(), isAddFlow))
        {
            logger.error("Promotion requires valid max redeem count");
            throw new ResourceValidationException(ErrorCode.PROMOTION_CODE_REQUIRES_VALID_MAX_REDEEM_COUNT);
        }

        // validate promotion enable in add promotion flow
        if(isAddFlow && promotion.getPromoEnabled() == null)
        {
            logger.error("Promotion requires valid promotion enabled");
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_PROMO_ENABLED);
        }

        // validate all credit promotions
        if(! isValidPromoCredit(promotion.getPromoCredit(), isAddFlow, tenantId)){
            logger.error("Promotion requires valid promo credit");
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_PROMO_CREDIT);
        }

        if(promotion.getCurrentRedeemCount() != null)
        {
            logger.error("Promotion does not require current redeem count");
            throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS);
        }

        if(StringUtils.isNotEmpty(promotion.getPromoCode()) && !promotion.getPromoCode().matches("[a-zA-Z0-9]+")){
            logger.error("PromotionCode should contain only Alphanumeric values");
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_PROMO_CODE);
        }
        if(StringUtils.isNotEmpty(promotion.getName()) && !promotion.getName().matches("[a-zA-Z0-9| ]+")){
            logger.error("Promotion Name can contain only Alphanumeric values");
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_NAME);
        }

        //Validate update promotion, It should have at least one input value.
        if(!isAddFlow )
            validatePromotionForAtLeastOneInput(promotion);
    }

    private void validateType(String type) throws ResourceValidationException{

        if(type== null || !(type.equals("PromoDetail")))
        {
            logger.error("Promotion requires valid type");
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_TYPE);
        }
    }

    private boolean isValidPromoCodeForAdd(String promoCode){

        return !((StringUtils.isEmpty(promoCode) || promoCode.length() < Configurations.REFERRAL_CODE_MIN_LENGTH || promoCode.length() > Configurations.REFERRAL_CODE_MAX_LENGTH ));
    }


    private  boolean validateCountries(List<String> countries, boolean isAddFlow) throws ResourceValidationException {

        boolean isValidCountry = true;

        if(isAddFlow && (countries == null || countries.size() == 0 || !isValidCountryList(countries))) {
            isValidCountry = false;

        } else if(countries != null && (countries.size() == 0 || !isValidCountryList(countries))) {
            isValidCountry = false;
        }

        return isValidCountry;
    }

    private  boolean isValidCountryList(List<String> countries){
        if(countries.contains("ALL") && countries.size() > 1)
        {
            return false;
        }

        for(int i = 0; i < countries.size(); i++)
        {
            String countryCode = countries.get(i);
            CountryDBBean countryDBBean = validCountryDao.getCountryByCountryCode(countryCode);
            if(!countryCode.equals("ALL") && countryDBBean == null)
            {
                return false;
            }
        }
        return true;
    }

    public boolean isValidPromoCredit(List<PromoCredit> promoCredits, boolean isAddFlow, int tenantId) throws ResourceValidationException
    {

        // if it is add flow promotion list size should match supported currencies
        if(isAddFlow && !promotionService.isValidPromoCreditDetails(promoCredits, tenantId)) {
            return false;

        } else if(promoCredits != null && promoCredits.size() == 0) {
            return false;
        }

        // update case promotion list null possible
        if(promoCredits != null) {
            // validate all promo credits
            List<String> currencies =  new ArrayList<String>();

            for(int i = 0; i < promoCredits.size(); i++)
            {
                PromoCredit promoCredit =  promoCredits.get(i);
                String currency = promoCredit.getCurrency();

                // this validation will check any currency repeated or not
                if(!currencies.contains(currency)) {
                    currencies.add(currency);
                } else {
                    return false;
                }

//                 this will check currency valid or not
                if(!gsSchemeUtil.isValidCurrency(currency, tenantId)) {
                    logger.error("Promotion requires valid currency");
                    throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_CURRENCY);
                }

                if(promoCredit.getCreditAmount() == null || GSSchemeUtil.isValidCreditAmount(currency,promoCredit.getCreditAmount())){
                    logger.error("Promotion requires valid credit amount");
                    throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_CREDIT_AMOUNT);
                }

                if(promoCredit.getMinimumCartAmount() == null ||GSSchemeUtil.isValidCreditAmount(currency,promoCredit.getMinimumCartAmount())){
                    logger.error("Promotion requires valid minimum cart amount");
                    throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_MINIMUM_CART_AMOUNT);
                }

                //Removed check for minimumCartAmount should be greater than promoCredit
//                if(promoCredit.getMinimumCartAmount() < promoCredit.getCreditAmount())
//                {
//                    logger.error("Promotion requires valid minimum cart amount");
//                    throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_MINIMUM_CART_AMOUNT);
//                }
            }
        }

        return true;
    }

    private void validatePromotionForAtLeastOneInput(Promotion promotion) throws ResourceValidationException {

        if(promotion.getName() == null && promotion.getPromoCode() == null && promotion.getStartDate() == null && promotion.getEndDate() == null
                && promotion.getCountry() == null && (promotion.getCreditExpiryPeriod() == null || promotion.getCreditExpiryPeriod() == 0) &&  promotion.getPromoEnabled() == null
                && promotion.getPromoCredit() == null && (promotion.getMaxRedeemCount() == null || promotion.getMaxRedeemCount() == 0))
        {
            logger.error("Promotion requires at least one input value");
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_AT_LEAST_ONE_INPUT_VALUE);
        }
    }

    public void validateCountryCode(String countryCode) throws ResourceValidationException
    {
        CountryDBBean countryDBBean = validCountryDao.getCountryByCountryCode(countryCode);
        if(!countryCode.equals("ALL") && countryDBBean == null)
        {
            logger.error("Promotion requires valid country code");
            throw new ResourceValidationException(ErrorCode.PROMOTION_REQUIRES_VALID_COUNTRY_CODE);
        }
    }

    public void validateSortByInput(String sortBy) throws ResourceValidationException
    {
        if(!(sortBy.equals("ASC")||sortBy.equals("DESC"))){
            logger.error("Promotion History requires valid sorting entry");
            throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS);
        }
    }

    private boolean validateSearchString(String search) {
        boolean check = search.matches("[a-zA-Z0-9| ]+");
        return check;
    }

    private void validateFormatString(String format) throws ResourceValidationException {
        String responseType = format.toUpperCase();
        if(!(responseType.equals(gsUtil.JSON)||responseType.equals(gsUtil.CSV))){
            throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS);
        }
    }

    private Promotion updateCountriesToUpperCase(Promotion promotion) {

        if(promotion.getCountry() != null) {
            List<String> uppercaseCountriesList = GSSchemeUtil.updateCountryCodesToUpperCase(promotion.getCountry());
            promotion.setCountry(uppercaseCountriesList);
        }
        return promotion;
    }
}
