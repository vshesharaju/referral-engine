package com.gigsky.rest.exception;

import com.gigsky.exception.ReferralException;

/**
 * Created by pradeepragav on 22/02/17.
 */
public class ResourceValidationException extends ReferralException {

    public ResourceValidationException(ErrorCode code)
    {
        super(code);
    }

    public ResourceValidationException(String message)
    {
        super(message);
    }

    public ResourceValidationException(String message, Exception e)
    {
        super(message, e);
    }

    public ResourceValidationException(Exception e)
    {
        super(e);
    }

}
