package com.gigsky.rest.bean;

import com.gigsky.database.bean.PromoCreditDBBean;

/**
 * Created by shwethars on 10/01/19.
 */
public class PromoCredit {

    private String currency;
    private Float creditAmount;
    private Float minimumCartAmount;

    public Float getMinimumCartAmount() {
        return minimumCartAmount;
    }

    public void setMinimumCartAmount(Float minimumCartAmount) {
        this.minimumCartAmount = minimumCartAmount;
    }

    public PromoCredit() {}

    public PromoCredit(PromoCreditDBBean promoCreditDBBean) {
        currency = promoCreditDBBean.getCurrency();
        creditAmount = promoCreditDBBean.getCreditAmount();
        minimumCartAmount = promoCreditDBBean.getMinimumCartAmount();
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Float getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(Float creditAmount) {
        this.creditAmount = creditAmount;
    }
}
