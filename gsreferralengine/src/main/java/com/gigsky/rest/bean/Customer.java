package com.gigsky.rest.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.gigsky.util.CustomerDateAndTimeDeserialize;

import java.sql.Timestamp;

/**
 * Created by vinayr on 11/01/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Customer {

    private String type;
    private String emailId;
    private String firstName;
    private String lastName;
    private long customerId;  //Backend Customer Id
    private String country;
    private String preferredCurrency;
    private String accountActivationStatus;
    private int errorInt;

    @JsonDeserialize(using= CustomerDateAndTimeDeserialize.class)
    private Timestamp createdOn;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPreferredCurrency() {
        return preferredCurrency;
    }

    public void setPreferredCurrency(String preferredCurrency) {
        this.preferredCurrency = preferredCurrency;
    }

    public Timestamp getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Timestamp createdOn) {
        this.createdOn = createdOn;
    }

    public String getAccountActivationStatus() {
        return accountActivationStatus;
    }

    public void setAccountActivationStatus(String accountActivationStatus) {
        this.accountActivationStatus = accountActivationStatus;
    }

    public int getErrorInt() {
        return errorInt;
    }

    public void setErrorInt(int errorInt) {
        this.errorInt = errorInt;
    }
}
