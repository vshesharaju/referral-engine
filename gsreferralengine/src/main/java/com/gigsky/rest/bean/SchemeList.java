package com.gigsky.rest.bean;

import java.util.List;

/**
 * Created by pradeepragav on 12/01/17.
 */
public class SchemeList {

    private String type;
    private int startIndex;
    private int count;
    private int totalCount;
    private List<Scheme> list;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<Scheme> getList() {
        return list;
    }

    public void setList(List<Scheme> list) {
        this.list = list;
    }

}
