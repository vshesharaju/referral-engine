package com.gigsky.rest.bean;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * created by karthik on 30/9/19
 */

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PromoHistory {
    private String type;
    private Long customerId;
    private String iccId;
    private Float creditAmount;
    private String currency;
    private Integer creditId;
    private Integer creditTransactionId;
    private String firstName;
    private String lastName;
    private String emailId;
    private String redemptionTime;


    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public PromoHistory(String type){
        this.type = type;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRedemptionTime() {
        return redemptionTime;
    }

    public void setRedemptionTime(String redemptionTime) {
        this.redemptionTime = redemptionTime;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long gsCustomerId) {
        this.customerId = gsCustomerId;
    }


    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public Float getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(Float creditAmount) {
        this.creditAmount = creditAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Integer getCreditId() {
        return creditId;
    }

    public void setCreditId(Integer creditId) {
        this.creditId = creditId;
    }

    public Integer getCreditTransactionId() {
        return creditTransactionId;
    }

    public void setCreditTransactionId(Integer creditTransactionId) {
        this.creditTransactionId = creditTransactionId;
    }

}
