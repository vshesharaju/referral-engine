package com.gigsky.rest.bean;


import com.fasterxml.jackson.annotation.JsonInclude;

import javax.xml.bind.annotation.XmlElement;

@JsonInclude( JsonInclude.Include.NON_NULL)
public class BuildInfo {
	private final String type = "BuildInfo";

	@XmlElement(name = "jenkins_build", defaultValue = "-1")
	@JsonInclude( JsonInclude.Include.NON_DEFAULT)
	private int jenkins_build = -1;
	private String date_time = null;

	@XmlElement(name = "svn_version", defaultValue = "-1")
	@JsonInclude( JsonInclude.Include.NON_DEFAULT)
	private int svn_version = -1;

	@XmlElement(name = "db_version", defaultValue = "-1")
	@JsonInclude( JsonInclude.Include.NON_DEFAULT)
	private int db_version = -1;
    private String db_type = null;

	public String getType() {
		return type;
	}

	public int getJenkins_build() {
		return jenkins_build;
	}

	public void setJenkins_build(int jenkins_build) {
		this.jenkins_build = jenkins_build;
	}

	public String getDate_time() {
		return date_time;
	}

	public void setDate_time(String date_time) {
		this.date_time = date_time;
	}

	public int getSvn_version() {
		return svn_version;
	}

	public void setSvn_version(int svn_version) {
		this.svn_version = svn_version;
	}

	public int getDb_version() {
		return db_version;
	}

	public void setDb_version(int db_version) {
		this.db_version = db_version;
	}

    public String getDb_type() {
        return db_type;
    }

    public void setDb_type(String db_type) {
        this.db_type = db_type;
    }
}
