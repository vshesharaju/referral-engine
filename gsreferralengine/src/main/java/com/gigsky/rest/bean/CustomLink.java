package com.gigsky.rest.bean;

/**
 * Created by vinayr on 13/01/17.
 */
public class CustomLink {

    private String channel;
    private String referralCode;
    private String referralUrl;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getReferralUrl() {
        return referralUrl;
    }

    public void setReferralUrl(String referralUrl) {
        this.referralUrl = referralUrl;
    }
}
