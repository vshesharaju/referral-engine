package com.gigsky.rest.bean;

import java.util.List;

/**
 * Created by shwethars on 24/01/19.
 */
public class PromotionList {

    private String type;
    private int startIndex;
    private int count;
    private int totalCount;
    private List<Promotion> list;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<Promotion> getList() {
        return list;
    }

    public void setList(List<Promotion> list) {
        this.list = list;
    }
}
