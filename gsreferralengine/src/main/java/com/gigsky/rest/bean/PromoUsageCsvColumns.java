package com.gigsky.rest.bean;

/**
 * created by karthik on 8/11/19
 */

public interface PromoUsageCsvColumns {

    String CUSTOMER_ID = "CustomerId";
    String FIRST_NAME= "FirstName";
    String LAST_NAME = "LastName";
    String EMAIL = "EmailId";
    String ICC_ID = "IccId";
    String CREDIT_AMOUNT = "CreditAmount";
    String CURRENCY = "Currency";
    String CREDIT_ID = "CreditId";
    String CREDIT_TRANSACTION_ID = "CreditTransactionId";
    String REDEMPTION_TIME= "RedemptionTime";

}
