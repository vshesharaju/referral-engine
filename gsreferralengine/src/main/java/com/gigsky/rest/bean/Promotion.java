package com.gigsky.rest.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by shwethars on 10/01/19.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Promotion {
    private String type;
    private String name;
    private String promoCode;
    private int promoId;
    private String startDate;
    private String endDate;
    private List<String> country;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_NULL)
    private Integer creditExpiryPeriod;
    @XmlElement(name = "promoEnabled", defaultValue = "-1")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private Boolean promoEnabled;
    private List<PromoCredit> promoCredit;
    private Integer currentRedeemCount;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_NULL)
    private Integer maxRedeemCount;

    public Promotion()
    {

    }

    public Promotion(String type, String name, String promoCode,
                     int promoId, String startDate, String endDate,
                     List<String> country, Integer creditExpiryPeriod, Boolean promoEnabled,
                     List<PromoCredit> promoCredit, Integer currentRedeemCount, Integer maxRedeemCount)
    {
        this.type = type;
        this.promoId = promoId;

        if(name != null){
            this.name = name;
        }

        if(promoCode != null){
            this.promoCode = promoCode;
        }

        if(startDate != null){
            this.startDate = startDate;
        }

        if(endDate != null){
            this.endDate = endDate;
        }

        if(country != null){
            this.country = country;
        }

        if(creditExpiryPeriod != null){
            this.creditExpiryPeriod = creditExpiryPeriod;
        }

        this.promoEnabled = promoEnabled;

        if(promoCredit != null){
            this.promoCredit = promoCredit;
        }

        if(maxRedeemCount!=null)
        {
            this.maxRedeemCount = maxRedeemCount;
        }

        if(currentRedeemCount!=null)
        {
            this.currentRedeemCount = currentRedeemCount;
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public int getPromoId() {
        return promoId;
    }

    public void setPromoId(int promoId) {
        this.promoId = promoId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<String> getCountry() {
        return country;
    }

    public void setCountry(List<String> country) {
        this.country = country;
    }

    public Integer getCreditExpiryPeriod() {
        return creditExpiryPeriod;
    }

    public void setCreditExpiryPeriod(Integer creditExpiryPeriod) {
        this.creditExpiryPeriod = creditExpiryPeriod;
    }

    public Boolean getPromoEnabled() {
        return promoEnabled;
    }

    public void setPromoEnabled(Boolean promoEnabled) {
        this.promoEnabled = promoEnabled;
    }

    public List<PromoCredit> getPromoCredit() {
        return promoCredit;
    }

    public void setPromoCredit(List<PromoCredit> promoCredit) {
        this.promoCredit = promoCredit;
    }

    public Integer getCurrentRedeemCount() {
        return currentRedeemCount;
    }

    public void setCurrentRedeemCount(Integer currentRedeemCount) {
        this.currentRedeemCount = currentRedeemCount;
    }

    public Integer getMaxRedeemCount() {
        return maxRedeemCount;
    }

    public void setMaxRedeemCount(Integer maxRedeemCount) {
        this.maxRedeemCount = maxRedeemCount;
    }
}
