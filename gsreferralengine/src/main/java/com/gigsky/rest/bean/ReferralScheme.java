package com.gigsky.rest.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.gigsky.database.bean.CustomerDBBean;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by pradeepragav on 12/01/17.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ReferralScheme {

    private String type;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_NULL)
    private String advocateEmailId;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_NULL)
    private String firstName;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_NULL)
    private String lastName;
    private Scheme incentiveScheme;

    public ReferralScheme(Scheme scheme, CustomerDBBean customer) {

        this.type = "ReferralSchemeDetail";

        if(customer != null) {
            this.advocateEmailId = customer.getEmailId();
            this.firstName = customer.getFirstName();
            this.lastName = customer.getLastName();
        }

        this.incentiveScheme = scheme;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAdvocateEmailId() {
        return advocateEmailId;
    }

    public void setAdvocateEmailId(String advocateEmailId) {
        this.advocateEmailId = advocateEmailId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Scheme getIncentiveScheme() {
        return incentiveScheme;
    }

    public void setIncentiveScheme(Scheme incentiveScheme) {
        this.incentiveScheme = incentiveScheme;
    }
}
