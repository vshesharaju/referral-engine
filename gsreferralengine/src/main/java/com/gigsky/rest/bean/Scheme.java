package com.gigsky.rest.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by pradeepragav on 12/01/17.
 */

@JsonInclude( JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Scheme {

    private String type;
    private int incentiveSchemeId;
    private String validityStart;
    private String validityEnd;
    private List<String> country;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_NULL)
    private Integer advocateCreditExpiryPeriod;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_NULL)
    private Integer referralCreditExpiryPeriod;
    @XmlElement(name = "schemeEnabled", defaultValue = "-1")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private Boolean schemeEnabled;
    private List<CreditScheme> creditScheme;

    public Scheme()
    {

    }

    public Scheme(String type, int incentiveSchemeId,String validityStart, String validityEnd, List<String> country, Integer advocateCreditExpiryPeriod, Integer referralCreditExpiryPeriod, boolean schemeEnabled, List<CreditScheme> creditScheme) {
        this.type = type;
        this.incentiveSchemeId = incentiveSchemeId;

        if(validityStart!=null){
            this.validityStart = validityStart;
        }

        if(validityEnd!=null){
            this.validityEnd = validityEnd;
        }

        if(country!=null){
            this.country = country;
        }

        if(advocateCreditExpiryPeriod != null) {
            this.advocateCreditExpiryPeriod = advocateCreditExpiryPeriod;
        }

        if(referralCreditExpiryPeriod != null) {
            this.referralCreditExpiryPeriod = referralCreditExpiryPeriod;
        }

        this.schemeEnabled = schemeEnabled;

        if(creditScheme!=null){
            this.creditScheme = creditScheme;
        }
    }

    public int getIncentiveSchemeId() { return incentiveSchemeId;}

    public void setIncentiveSchemeId(int incentiveSchemeId) { this.incentiveSchemeId = incentiveSchemeId; }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValidityStart() {
        return validityStart;
    }

    public void setValidityStart(String validityStart) {
        this.validityStart = validityStart;
    }

    public String getValidityEnd() {
        return validityEnd;
    }

    public void setValidityEnd(String validityEnd) {
        this.validityEnd = validityEnd;
    }

    public List<String> getCountry() {
        return country;
    }

    public void setCountry(List<String> country) {
        this.country = country;
    }

    public Integer getAdvocateCreditExpiryPeriod() {
        return advocateCreditExpiryPeriod;
    }

    public void setAdvocateCreditExpiryPeriod(Integer advocateCreditExpiryPeriod) {
        this.advocateCreditExpiryPeriod = advocateCreditExpiryPeriod;
    }

    public Integer getReferralCreditExpiryPeriod() {
        return referralCreditExpiryPeriod;
    }

    public void setReferralCreditExpiryPeriod(Integer referralCreditExpiryPeriod) {
        this.referralCreditExpiryPeriod = referralCreditExpiryPeriod;
    }

    public Boolean getSchemeEnabled() {
        return schemeEnabled;
    }

    public void setSchemeEnabled(Boolean schemeEnabled) {
        this.schemeEnabled = schemeEnabled;
    }

    public List<CreditScheme> getCreditScheme() {
        return creditScheme;
    }

    public void setCreditScheme(List<CreditScheme> creditScheme) {
        this.creditScheme = creditScheme;
    }
}
