package com.gigsky.rest.bean;


import com.fasterxml.jackson.annotation.JsonInclude;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by pradeepragav on 11/01/17.
 */

//Based on Send invite request body
public class Invite {
    private String type;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private List<Integer> inviteIds;
    private List<String> emailIds;
    private String userAgent;
    private String platForm;

    public List<Integer> getInviteIds() {
        return inviteIds;
    }

    public void setInviteIds(List<Integer> inviteIds) {
        this.inviteIds = inviteIds;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<String> getEmailIds() {
        return emailIds;
    }

    public void setEmailIds(List<String> emailIds) {
        this.emailIds = emailIds;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getPlatForm() {
        return platForm;
    }

    public void setPlatForm(String platForm) {
        this.platForm = platForm;
    }
}
