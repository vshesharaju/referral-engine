package com.gigsky.rest.bean;


import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude( JsonInclude.Include.NON_NULL)
public class ErrorResponse {
	private final String type = "error";
	private int errorInt;
	private String userDisplayErrorStr;
	private int userDisplayErrorCode;

	public ErrorResponse()
	{

	}

	public ErrorResponse(int code, String message)
	{
		errorInt = code;
		userDisplayErrorStr = message;
		userDisplayErrorCode = code;
	}

	public String getType() {
		return type;
	}

	public int getErrorInt() {
		return errorInt;
	}

	public void setErrorInt(int errorInt) {
		this.errorInt = errorInt;
	}

	public String getUserDisplayErrorStr() {
		return userDisplayErrorStr;
	}

	public void setUserDisplayErrorStr(String userDisplayErrorStr) {
		this.userDisplayErrorStr = userDisplayErrorStr;
	}

	public int getUserDisplayErrorCode() {
		return userDisplayErrorCode;
	}

	public void setUserDisplayErrorCode(int userDisplayErrorCode) {
		this.userDisplayErrorCode = userDisplayErrorCode;
	}
}
