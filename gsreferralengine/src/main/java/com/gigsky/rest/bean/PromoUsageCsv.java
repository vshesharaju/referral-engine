package com.gigsky.rest.bean;
import com.gigsky.gsenterprisecommon.util.csvutils.CsvExport;

/**
 * created by karthik on 6/11/19
 */


public class PromoUsageCsv {
    @CsvExport(index = 1, title = PromoUsageCsvColumns.CUSTOMER_ID)
    private Long customerId;
    @CsvExport(index = 2, title = PromoUsageCsvColumns.FIRST_NAME)
    private String firstName;
    @CsvExport(index = 3, title = PromoUsageCsvColumns.LAST_NAME)
    private String lastName;
    @CsvExport(index = 4, title = PromoUsageCsvColumns.EMAIL)
    private String emailId;
    @CsvExport(index = 5, title = PromoUsageCsvColumns.ICC_ID)
    private String iccId;
    @CsvExport(index = 6, title = PromoUsageCsvColumns.CREDIT_AMOUNT)
    private String creditAmount;
    @CsvExport(index = 7, title = PromoUsageCsvColumns.CURRENCY)
    private String currency;
    @CsvExport(index = 8, title = PromoUsageCsvColumns.CREDIT_ID)
    private String creditId;
    @CsvExport(index = 9, title = PromoUsageCsvColumns.CREDIT_TRANSACTION_ID)
    private String creditTransactionId;
    @CsvExport(index = 10, title = PromoUsageCsvColumns.REDEMPTION_TIME)
    private String redemptionTime;


    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public String getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(String creditAmount) {
        this.creditAmount = creditAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCreditId() {
        return creditId;
    }

    public void setCreditId(String creditId) {
        this.creditId = creditId;
    }

    public String getCreditTransactionId() {
        return creditTransactionId;
    }

    public void setCreditTransactionId(String creditTransactionId) {
        this.creditTransactionId = creditTransactionId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getRedemptionTime() {
        return redemptionTime;
    }

    public void setRedemptionTime(String redemptionTime) {
        this.redemptionTime = redemptionTime;
    }
}
