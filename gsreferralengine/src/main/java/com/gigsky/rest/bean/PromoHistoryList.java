package com.gigsky.rest.bean;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import java.util.List;

/**
 * Created by karthik on 30/9/19
 */


@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class PromoHistoryList {

    private String type;
    private int promotionId;
    private int currentRedeemCount;
    private int startIndex;
    private int count;
    private int totalCount;
    private List<PromoHistory> list;

    public PromoHistoryList(String type){
        this.type = type;
    }

    public PromoHistoryList(){
        this.type = "PromoHistoryList";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<PromoHistory> getList() {
        return list;
    }

    public void setList(List<PromoHistory> list) {
        this.list = list;
    }

    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promoId) {
        this.promotionId = promoId;
    }

    public int getCurrentRedeemCount() {
        return currentRedeemCount;
    }

    public void setCurrentRedeemCount(int currentRedeemCount) {
        this.currentRedeemCount = currentRedeemCount;
    }
}
