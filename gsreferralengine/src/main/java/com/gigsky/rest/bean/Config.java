package com.gigsky.rest.bean;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by vinayr on 11/01/17.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
public class Config {

    private String type;

    @XmlElement(name = "referralEnable", defaultValue = "-1")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private Boolean referralEnable;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getReferralEnable() {
        return referralEnable;
    }

    public void setReferralEnable(Boolean referralEnable) {
        this.referralEnable = referralEnable;
    }

}
