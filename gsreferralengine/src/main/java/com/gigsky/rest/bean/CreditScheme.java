package com.gigsky.rest.bean;

import com.gigsky.database.bean.CreditSchemeDBBean;

/**
 * Created by pradeepragav on 12/01/17.
 */
//Used in IncentiveScheme APIs
public class CreditScheme {
    private String currency;
    private float advocateAmount;
    private float newCustomerAmount;

    public CreditScheme(){}

    public CreditScheme(CreditSchemeDBBean creditSchemeDBBean){

        currency = creditSchemeDBBean.getCurrency();
        advocateAmount = creditSchemeDBBean.getAdvocateAmount();
        newCustomerAmount = creditSchemeDBBean.getNewCustomerAmount();
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public float getAdvocateAmount() {
        return advocateAmount;
    }

    public void setAdvocateAmount(float advocateAmount) {
        this.advocateAmount = advocateAmount;
    }

    public float getNewCustomerAmount() {
        return newCustomerAmount;
    }

    public void setNewCustomerAmount(float newCustomerAmount) {
        this.newCustomerAmount = newCustomerAmount;
    }
}
