package com.gigsky.rest.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Timestamp;

/**
 * Created by vinayr on 11/01/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Credit {

    private String type;
    private int creditId;
    private float creditBalance;
    private float addedAmount;
    private String creditType;
    private String currency;
    private int creditTxnId;
    private Timestamp expiry;
    private String description;
    private int errorInt;

    public float getAddedAmount() {
        return addedAmount;
    }

    public void setAddedAmount(float addedAmount) {
        this.addedAmount = addedAmount;
    }

    public int getErrorInt() {
        return errorInt;
    }

    public void setErrorInt(int errorInt) {
        this.errorInt = errorInt;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }


    public int getCreditId() {
        return creditId;
    }

    public void setCreditId(int creditId) {
        this.creditId = creditId;
    }

    public float getCreditBalance() {
        return creditBalance;
    }

    public void setCreditBalance(float creditBalance) {
        this.creditBalance = creditBalance;
    }

    public String getCreditType() {
        return creditType;
    }

    public void setCreditType(String creditType) {
        this.creditType = creditType;
    }

    public Timestamp getExpiry() {
        return expiry;
    }

    public void setExpiry(Timestamp expiry) {
        this.expiry = expiry;
    }

    public int getCreditTxnId() {
        return creditTxnId;
    }

    public void setCreditTxnId(int creditTxnId) {
        this.creditTxnId = creditTxnId;
    }


}
