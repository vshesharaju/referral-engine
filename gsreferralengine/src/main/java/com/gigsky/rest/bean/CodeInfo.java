package com.gigsky.rest.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by shwethars on 04/02/19.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CodeInfo {
    private String type;
    private Float amount;
    private String currency;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_NULL)
    private String status;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_NULL)
    private String code;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_NULL)
    private Float cartAmount;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_NULL)
    private Boolean validationMode;

    public Float getCartAmount() {
        return cartAmount;
    }

    public void setCartAmount(Float cartAmount) {
        this.cartAmount = cartAmount;
    }

    public Boolean getValidationMode() {
        return validationMode;
    }

    public void setValidationMode(Boolean validationMode) {
        this.validationMode = validationMode;
    }

    public CodeInfo(String type, Float amount, String currency, String status) {
        this.type = type;
        this.amount = amount;
        this.currency = currency;
        this.status = status;
    }

    public CodeInfo(String type, String currency, String code) {
        this.type = type;
        this.code = code;

        if(currency != null){
            this.currency = currency;
        }
    }

    public CodeInfo(){

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Float getAmount() {
        return amount;
    }

    public void setAmount(Float amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
