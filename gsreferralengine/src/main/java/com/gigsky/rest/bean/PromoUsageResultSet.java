package com.gigsky.rest.bean;
import com.gigsky.database.bean.PromoHistoryDBBean;


/**
 * created by karthik on 15/10/19
 */


public class PromoUsageResultSet {

    private PromoHistoryDBBean promoHistoryDBBean;

    private Integer redeemCount;
    private String firstName;
    private String lastName;
    private String emailId;

    public PromoUsageResultSet(PromoHistoryDBBean promoHistoryDBBean, Integer redeemCount,
                               String firstName, String lastName, String emailId){
        this.promoHistoryDBBean = promoHistoryDBBean;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailId = emailId;
        this.redeemCount = redeemCount;
    }

    public PromoUsageResultSet(){ }

    public PromoHistoryDBBean getPromoHistoryDBBean() {
        return promoHistoryDBBean;
    }

    public void setPromoHistoryDBBean(PromoHistoryDBBean promoHistoryDBBean) {
        this.promoHistoryDBBean = promoHistoryDBBean;
    }

    public Integer getRedeemCount() {
        return redeemCount;
    }

    public void setRedeemCount(Integer redeemCount) {
        this.redeemCount = redeemCount;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }
}
