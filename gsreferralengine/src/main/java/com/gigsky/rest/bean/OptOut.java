package com.gigsky.rest.bean;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by prsubbareddy on 01/02/17.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
public class OptOut {

    private String type;
    @XmlElement(name = "optOut", defaultValue = "-1")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private Boolean optOut;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getOptOut() {
        return optOut;
    }

    public void setOptOut(Boolean optOut) {
        this.optOut = optOut;
    }

}
