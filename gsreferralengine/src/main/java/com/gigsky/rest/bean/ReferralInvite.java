package com.gigsky.rest.bean;

import java.util.List;

/**
 * Created by pradeepragav on 10/01/17.
 */
public class ReferralInvite {

    private String type;
    private int startIndex;
    private int count;
    private int totalCount;
    private List<Invitation> list;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<Invitation> getList() {
        return list;
    }

    public void setList(List<Invitation> list) {
        this.list = list;
    }
}
