package com.gigsky.rest.bean;

import com.gigsky.common.CommonUtils;
import com.gigsky.database.bean.InvitationDBBean;

/**
 * Created by prsubbareddy on 06/01/17.
 */

//Based on Get invite detail response body
public class Invitation {

    private String type;
    private int inviteId;
    private String emailId;
    private String status;
    private String inviteTime;
    private String accountCreatedOn;
    private float advocateAmount;
    private String advocateCurrency;
    private int advocateTxnId;

    public Invitation(InvitationDBBean invitationDBBean) {

        type = "InviteDetail";
        inviteId = invitationDBBean.getId();
        emailId = invitationDBBean.getEmailId();

        status = invitationDBBean.getStatus();

        if(invitationDBBean.getInviteTime() != null)
            inviteTime = CommonUtils.convertTimestampToString(invitationDBBean.getInviteTime());

            accountCreatedOn = CommonUtils.convertTimestampToString(invitationDBBean.getAccountCreatedOn());

        if(invitationDBBean.getAdvocateCreditTxnId() != null)
            advocateTxnId = invitationDBBean.getAdvocateCreditTxnId();

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getInviteId() {
        return inviteId;
    }

    public void setInviteId(int inviteId) {
        this.inviteId = inviteId;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        status = status;
    }

    public String getInviteTime() {
        return inviteTime;
    }

    public void setInviteTime(String inviteTime) {
        this.inviteTime = inviteTime;
    }

    public String getAccountCreatedOn() {
        return accountCreatedOn;
    }

    public void setAccountCreatedOn(String accountCreatedOn) {
        this.accountCreatedOn = accountCreatedOn;
    }

    public float getAdvocateAmount() {
        return advocateAmount;
    }

    public void setAdvocateAmount(float advocateAmount) {
        this.advocateAmount = advocateAmount;
    }

    public String getAdvocateCurrency() {
        return advocateCurrency;
    }

    public void setAdvocateCurrency(String advocateCurrency) {
        this.advocateCurrency = advocateCurrency;
    }

    public int getAdvocateTxnId() {
        return advocateTxnId;
    }

    public void setAdvocateTxnId(int advocateTxnId) {
        this.advocateTxnId = advocateTxnId;
    }
}
