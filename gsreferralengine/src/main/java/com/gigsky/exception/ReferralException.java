package com.gigsky.exception;

import com.gigsky.rest.exception.ErrorCode;

/**
 * Created by pradeepragav on 10/01/17.
 */
public class ReferralException extends Exception {

    private ErrorCode errorCode;

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public ReferralException() {

    }

    public ReferralException(ErrorCode code)
    {
        errorCode = code;
    }

    protected ReferralException(String message)
    {
        super(message);
    }

    protected ReferralException(ErrorCode errorCode, String message)
    {
        super(message);
        this.errorCode = errorCode;
    }

    protected ReferralException(String message, Exception e)
    {
        super(message,e);
    }

    protected ReferralException(Exception e)
    {
        super(e);
    }
}
