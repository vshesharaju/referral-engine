package com.gigsky.executor;

import com.gigsky.common.Configurations;
import com.gigsky.common.GigskyDuration;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.service.EventService;
import com.gigsky.util.GSTaskUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.*;

/**
 * Created by anant on 27/10/15.
 *
 * Task executor will periodically check for events which are not processed and process them
 */
public class TimerEventsExecutor implements Runnable, BaseExecutorInterface {

    private static final Logger logger = LoggerFactory.getLogger(TimerEventsExecutor.class);

    @Autowired
    ConfigurationKeyValueDao configurationKeyValuesDao;

    @Autowired
    EventService eventService;

    @Autowired
    GSTaskUtil gsTaskUtil;

    private static ScheduledExecutorService executorService = null;

    private static final long DEFAULT_EVENT_EXEC_TIME_MS = 5000;
    private static final int DEFAULT_SHUTDOWN_WAIT_TIME_MS = 10000;

    @Override
    public synchronized void start() throws ExecutionException, InterruptedException {
        logger.debug("TimerEventsExecutor: start+++");

        // start init method for runnable schedule
        scheduleExecutorService();

        logger.debug("TimerEventsExecutor: start---");
    }

    @Override
    public void shutdown() {
        logger.debug("TimerEventsExecutor: shutdown+++");

        int shutdownWaitMS;
        String shutdownWaitTime = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.SHUTDOWN_WAIT_TIME_MS);

        if (StringUtils.isEmpty(shutdownWaitTime)) {
            shutdownWaitMS = DEFAULT_SHUTDOWN_WAIT_TIME_MS;
        } else {
            shutdownWaitMS = (int) new GigskyDuration(shutdownWaitTime).convertTimeInMilliSeconds();
        }

        try {
            if (executorService != null) {
                logger.debug("TimerEventsExecutor:executorService.shutdown ++");

                // Disable new tasks from being submitted
                executorService.shutdownNow();
                logger.debug("TimerEventsExecutor:executorService.shutdown --");

                logger.debug("TimerEventsExecutor:executorService is Terminated flag is false waiting for " +
                        shutdownWaitMS + " millisecs");

                // Wait a while for tasks to respond to being cancelled
                executorService.awaitTermination(shutdownWaitMS, TimeUnit.MILLISECONDS);
            }
        }
        catch (InterruptedException ex) {
            logger.error(Configurations.ErrorMsgType.SYSTEM_ERROR + " : shutdown failed with exception ", ex);
        }
        logger.debug("TimerEventsExecutor:awaitTermination--");
        executorService = null;

        logger.debug("TimerEventsExecutor: shutdown---");
    }

    @Override
    public void run() {

        try {
            runInternal();
        } catch (Exception e) {
            logger.error("Exception:" , e);
        }
    }

    @Override
    public void runInternal() {
        logger.debug("TimerEventsExecutor runInternal+++");

        // Check backend server available and NGW server availability
        // if backend servers running, we can start process
        if(gsTaskUtil.IfBackendServersRunning()) {
                eventService.startProcess();
        }

        logger.debug("TimerEventsExecutor runInternal---");
    }

    private void scheduleExecutorService() {

        long runnableScheduleTimeMS;
        String eventExecTime = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.EVENT_EXEC_TIME_MS);

        if (StringUtils.isEmpty(eventExecTime))
        {
            runnableScheduleTimeMS = DEFAULT_EVENT_EXEC_TIME_MS;
        } else {
            runnableScheduleTimeMS = new GigskyDuration(eventExecTime).convertTimeInMilliSeconds();
        }

        if(executorService == null) {
            executorService = Executors.newSingleThreadScheduledExecutor(Executors.defaultThreadFactory());
        }

        logger.debug("runnableScheduleTimeMS = " + runnableScheduleTimeMS);
        executorService.scheduleAtFixedRate(this, runnableScheduleTimeMS, runnableScheduleTimeMS, TimeUnit.MILLISECONDS);
    }

}
