package com.gigsky.executor;

import java.util.concurrent.ExecutionException;

/**
 * Created by pradeepragav on 07/02/17.
 */
public interface BaseExecutorInterface {

    void start() throws ExecutionException, InterruptedException;
    void shutdown();
    void runInternal();
}
