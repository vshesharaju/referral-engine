package com.gigsky.executor;

import com.gigsky.common.Configurations;
import com.gigsky.common.GigskyDuration;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.scheduler.PromoEventTaskScheduler;
import com.gigsky.util.GSTaskUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by pradeepragav on 01/12/17.
 * Schedules simActivationPromoTask based on the fixed rate mentioned in configuration key values
 */

//TODO : Can this be moved to TimerEventsExecutor
public class PromotionEventExecutor implements Runnable, BaseExecutorInterface
{
    private static final Logger logger = LoggerFactory.getLogger(PromotionEventExecutor.class);
    private static ScheduledExecutorService executorService = null;

    private static final long DEFAULT_PROMO_EXEC_TIME_MS = 5000;
    private static final int DEFAULT_SHUTDOWN_WAIT_TIME_MS = 10000;

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValuesDao;

    @Autowired
    private GSTaskUtil gsTaskUtil;

    @Autowired
    private PromoEventTaskScheduler promoEventTaskScheduler;

    @Override
    public void start() throws ExecutionException, InterruptedException
    {
        logger.debug("PromotionEventExecutor: start+++");

        // cal method for scheduleExecutorService
        scheduleExecutorService();

        logger.debug("PromotionEventExecutor: start---");
    }

    @Override
    public void shutdown()
    {
        logger.debug("PromotionEventExecutor: shutdown+++");

        int shutdownWaitMS;
        String shutdownWaitTime = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.SHUTDOWN_WAIT_TIME_MS);

        if (StringUtils.isEmpty(shutdownWaitTime)) {
            shutdownWaitMS = DEFAULT_SHUTDOWN_WAIT_TIME_MS;
        } else {
            shutdownWaitMS = (int) new GigskyDuration(shutdownWaitTime).convertTimeInMilliSeconds();
        }

        try {
            if (executorService != null) {
                logger.debug("PromotionEventExecutor:executorService.shutdown ++");

                // Disable new tasks from being submitted
                executorService.shutdownNow();
                logger.debug("PromotionEventExecutor:executorService.shutdown --");

                logger.debug("PromotionEventExecutor:executorService is Terminated flag is false waiting for " +
                        shutdownWaitMS + " millisecs");

                // Wait a while for tasks to respond to being cancelled
                executorService.awaitTermination(shutdownWaitMS, TimeUnit.MILLISECONDS);
            }
        }
        catch (InterruptedException ex) {
            logger.error(Configurations.ErrorMsgType.SYSTEM_ERROR + " : shutdown failed with exception ", ex);
        }

        logger.debug("PromotionEventExecutor:awaitTermination--");
        executorService = null;

        logger.debug("PromotionEventExecutor: shutdown---");
    }

    @Override
    public void run()
    {
        try
        {
            runInternal();
        }
        catch (Exception e)
        {
            logger.error("Exception:" , e);
        }
    }

    @Override
    public void runInternal()
    {
        logger.debug("PromotionEventExecutor runInternal+++");


        if(gsTaskUtil.IfBackendServersRunning())
        {
            promoEventTaskScheduler.startProcess();
        }

        logger.debug("PromotionEventExecutor runInternal---");
    }

    private void scheduleExecutorService()
    {
        long runnableScheduleTimeMS;

        String eventExecTime = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.EVENT_EXEC_TIME_MS);

        if (StringUtils.isEmpty(eventExecTime))
        {
            runnableScheduleTimeMS = DEFAULT_PROMO_EXEC_TIME_MS;
        }
        else
        {
            runnableScheduleTimeMS = new GigskyDuration(eventExecTime).convertTimeInMilliSeconds();
        }

        if(executorService == null)
        {
            executorService = Executors.newSingleThreadScheduledExecutor(Executors.defaultThreadFactory());
        }

        logger.debug("runnableScheduleTimeMS = " + runnableScheduleTimeMS);
        executorService.scheduleAtFixedRate(this, runnableScheduleTimeMS, runnableScheduleTimeMS, TimeUnit.MILLISECONDS);
    }

}
