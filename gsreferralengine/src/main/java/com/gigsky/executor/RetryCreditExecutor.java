package com.gigsky.executor;

import com.gigsky.common.Configurations;
import com.gigsky.common.GigskyDuration;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.scheduler.RetryCreditScheduler;
import com.gigsky.util.GSTaskUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.*;

/**
 * Created by prsubbareddy on 08/02/17.
 */
public class RetryCreditExecutor implements Runnable, BaseExecutorInterface {

    private static final Logger logger = LoggerFactory.getLogger(RetryCreditExecutor.class);

    private static ScheduledExecutorService executorService = null;

    private static final long DEFAULT_RETRY_CREDIT_EXEC_TIME_MS = 3600000;
    private static final int DEFAULT_SHUTDOWN_WAIT_TIME_MS = 10000;

    @Autowired
    ConfigurationKeyValueDao configurationKeyValuesDao;

    @Autowired
    RetryCreditScheduler retryCreditScheduler;

    @Autowired
    GSTaskUtil gsTaskUtil;

    @Override
    public synchronized void start() throws ExecutionException, InterruptedException {
        logger.debug("RetryCreditExecutor: start+++");

        // cal method for scheduleExecutorService
        scheduleExecutorService();

        logger.debug("RetryCreditExecutor: start---");
    }

    @Override
    public void shutdown() {
        logger.debug("RetryCreditExecutor: shutdown+++");

        int shutdownWaitMS;
        String shutdownWaitTime = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.SHUTDOWN_WAIT_TIME_MS);

        if (StringUtils.isEmpty(shutdownWaitTime)) {
            shutdownWaitMS = DEFAULT_SHUTDOWN_WAIT_TIME_MS;
        } else {
            shutdownWaitMS = (int) new GigskyDuration(shutdownWaitTime).convertTimeInMilliSeconds();
        }

        try {
            if (executorService != null) {
                logger.debug("RetryCreditExecutor:executorService.shutdown ++");

                // Disable new tasks from being submitted
                executorService.shutdownNow();
                logger.debug("RetryCreditExecutor:executorService.shutdown --");

                logger.debug("RetryCreditExecutor:executorService is Terminated flag is false waiting for " +
                        shutdownWaitMS + " millisecs");

                // Wait a while for tasks to respond to being cancelled
                executorService.awaitTermination(shutdownWaitMS, TimeUnit.MILLISECONDS);
            }
        }
        catch (InterruptedException ex) {
            logger.error(Configurations.ErrorMsgType.SYSTEM_ERROR + " : shutdown failed with exception ", ex);
        }

        logger.debug("RetryCreditExecutor:awaitTermination--");
        executorService = null;

        logger.debug("RetryCreditExecutor: shutdown---");
    }

    @Override
    public void run() {

        try {
            runInternal();
        } catch (Exception e) {
            logger.error("Exception:" , e);
        }
    }

    @Override
    public void runInternal() {
        logger.debug("RetryCreditExecutor runInternal+++");

        // Check referral engine enabled or not, backend server available and NGW server availability
        // if referral process true, we can start process
        if(gsTaskUtil.IfBackendServersRunning()) {
            retryCreditScheduler.startProcess();
        }

        logger.debug("RetryCreditExecutor runInternal---");
    }

    private void scheduleExecutorService() {

        long runnableScheduleTimeMS;
        String eventExecTime = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.RETRY_CREDIT_EXEC_TIME_MS);

        if (StringUtils.isEmpty(eventExecTime)) {
            runnableScheduleTimeMS = DEFAULT_RETRY_CREDIT_EXEC_TIME_MS;
        } else {
            runnableScheduleTimeMS = new GigskyDuration(eventExecTime).convertTimeInMilliSeconds();
        }

        if(executorService == null) {
            executorService = Executors.newSingleThreadScheduledExecutor(Executors.defaultThreadFactory());
        }

        logger.debug("runnableScheduleTimeMS = " + runnableScheduleTimeMS);
        executorService.scheduleAtFixedRate(this, runnableScheduleTimeMS, runnableScheduleTimeMS, TimeUnit.MILLISECONDS);
    }
}
