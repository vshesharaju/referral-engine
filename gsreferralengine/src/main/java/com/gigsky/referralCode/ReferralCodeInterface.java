package com.gigsky.referralCode;

import com.gigsky.rest.bean.ReferralCode;

/**
 * Created by vinayr on 12/01/17.
 */
public interface ReferralCodeInterface {

    public String generateReferralCode(ReferralCode referralCode, int count);
}
