package com.gigsky.referralCode;

import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.IncentiveSchemeDBBean;
import com.gigsky.database.bean.ReferralCodeDBBean;
import com.gigsky.database.bean.ReferralCodeHistoryDBBean;
import com.gigsky.database.dao.IncentiveSchemeDao;
import com.gigsky.database.dao.ReferralCodeDao;
import com.gigsky.rest.bean.ReferralCode;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.exception.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by vinayr on 12/01/17.
 */
public class ReferralCodeManagement {

    private static final Logger logger = LoggerFactory.getLogger(ReferralCodeManagement.class);

    @Autowired
    private DefaultReferralCodeImpl defaultReferralCodeImpl;

    @Autowired
    private IncentiveSchemeDao incentiveSchemeDao;

    @Autowired
    private ReferralCodeDao referralCodeDao;


    //Generate the referral code string for the channel specified
    public String generateReferralCode(ReferralCode referralCode, int count)
    {
        //DEFAULT channel
        if (Configurations.ReferralCodeChannelType.DEFAULT.equals(referralCode.getChannel()))
        {
            return defaultReferralCodeImpl.generateReferralCode(referralCode, count);
        }

        return null;
    }

    //Creates the referral code for the customer using the active scheme based on the country
    public ReferralCodeDBBean createReferralCodeForActiveScheme(ReferralCode referralCode, CustomerDBBean customerDBBean) throws ServiceException
    {
        //Get the active scheme for the country of the customer
        IncentiveSchemeDBBean incentiveSchemeDBBean = getActiveScheme(customerDBBean.getCountry(), customerDBBean.getTenantId());

        //Generate the referral code
        String referralString = generateUniqueReferralCode(referralCode, customerDBBean.getTenantId());

        //Create the referral code for the active scheme
        byte active = 1;
        ReferralCodeDBBean referralCodeDBBean = new ReferralCodeDBBean();
        referralCodeDBBean.setCustomerId(referralCode.getCustomerId());
        referralCodeDBBean.setChannel(referralCode.getChannel());
        referralCodeDBBean.setReferralCode(referralString);
        referralCodeDBBean.setIncentiveSchemeId(incentiveSchemeDBBean.getId());
        referralCodeDBBean.setTenantId(customerDBBean.getTenantId());
        referralCodeDBBean.setIsActive(active);

        return referralCodeDBBean;
    }

    //Check if the referral code belongs to an active scheme
    public boolean incentiveSchemeActiveOrNot(ReferralCodeDBBean referralCodeDBBean, String country) throws ServiceException {

        int tenantId = referralCodeDBBean.getTenantId();

        //Get the scheme of the referral code
        IncentiveSchemeDBBean incentiveSchemeMappedToReferralCode = incentiveSchemeDao.getIncentiveScheme(referralCodeDBBean.getIncentiveSchemeId(),tenantId);

        //Get country specific incentiveScheme
        IncentiveSchemeDBBean incentiveSchemeForCountry = getActiveScheme(country, tenantId);

        if (incentiveSchemeMappedToReferralCode != null)
        {
            //Return iff scheme mapped to referralCode is same as the incentive for country
            if(incentiveSchemeForCountry != null &&
                    incentiveSchemeForCountry.getId() == incentiveSchemeMappedToReferralCode.getId())
            {
                //Check if the scheme is enabled or not
                if (incentiveSchemeMappedToReferralCode.getActiveScheme() != 0) {
                    return true;
                }
            }
        }

        return false;
    }

    //Create unique referral code
    public String generateUniqueReferralCode(ReferralCode referralCode, int tenantId)
    {

        String emailSubString = defaultReferralCodeImpl.getEmailSubString(referralCode.getEmailId().toUpperCase());
        // Fetch latest referralCode that email substring matches
        ReferralCodeDBBean previousReferralCodeBeanWithSameEmailSubstring =
                referralCodeDao.getLatestReferralCodeByEmailSubString(emailSubString);

        int countOfReferralCodesWithSameEmailSubstring;

        //If the latest referralcode is present
        if(previousReferralCodeBeanWithSameEmailSubstring != null)
        {
            //Get referralCode from referralCodeDbBean
            String referralCodeStr = previousReferralCodeBeanWithSameEmailSubstring.getReferralCode();
            //Get the count from referralCode
            String countStr = referralCodeStr.replaceAll(emailSubString,"");
            //Convert string to integer
            countOfReferralCodesWithSameEmailSubstring = new Integer(countStr);
        }
        else
        {
            //If no referralCode has this email substring, set to zero
            countOfReferralCodesWithSameEmailSubstring = 0;
        }

        // generate referral code by passing referral code and referral code match count
        String referralString = generateReferralCode(referralCode, countOfReferralCodesWithSameEmailSubstring);

        //Check if the referral string is unique
        // This should get referral code
        ReferralCodeDBBean referralCodeDBBean = referralCodeDao.getReferralCodeInfoByReferralCode(referralString, tenantId);
        if (referralCodeDBBean != null)
        {
            //generate referral string if already exist
            referralString = generateReferralCode(referralCode, countOfReferralCodesWithSameEmailSubstring + 1);
        }
        return referralString;
    }

    //Update existing referralCode if it is not mapped to Active incentiveScheme
    public void updateExistingReferralCode(ReferralCodeDBBean referralCodeDBBean, String country) throws ServiceException {

        //Check if the referral code belongs to an active scheme
        boolean isReferralValid = incentiveSchemeActiveOrNot(referralCodeDBBean, country);

        if (!isReferralValid)
        {
            //Scheme is not active anymore.
            //Update the referral code history table based on the scheme Id
            updateReferralCodeHistory(referralCodeDBBean);

            //Remap the referral code to an active scheme
            remapReferralCodeToActiveScheme(referralCodeDBBean, country);
        }
    }


    //Remap the referral code to an active scheme
    public void remapReferralCodeToActiveScheme(ReferralCodeDBBean referralCodeDBBean, String country) throws ServiceException
    {
        //Get the active scheme
        IncentiveSchemeDBBean incentiveSchemeDBBean = getActiveScheme(country, referralCodeDBBean.getTenantId());

        //Create the referral code history entry for this scheme Id
        ReferralCodeHistoryDBBean referralCodeHistoryDBBean = createReferralCodeHistory(referralCodeDBBean.getId(), incentiveSchemeDBBean.getId());
        referralCodeDao.addReferralCodeHistory(referralCodeHistoryDBBean);

        //Update the scheme Id and remap the referral code
        referralCodeDBBean.setIncentiveSchemeId(incentiveSchemeDBBean.getId());
        referralCodeDao.updateReferralCode(referralCodeDBBean);
    }

    //Fetches the active scheme based on the country
    private IncentiveSchemeDBBean getActiveScheme(String country, int tenantId) throws ServiceException
    {
        //Get the active scheme for the country of the customer
        IncentiveSchemeDBBean incentiveSchemeDBBean = incentiveSchemeDao.getActiveSchemeForCountry(country, tenantId);

        //If the incentive scheme is not there then fetch the default incentive scheme
        if (incentiveSchemeDBBean == null)
        {
            incentiveSchemeDBBean = incentiveSchemeDao.getActiveSchemeForCountry("ALL", tenantId);

            if (incentiveSchemeDBBean == null)
            {
                logger.error("Referral scheme entry not found");
                throw new ServiceException(ErrorCode.REFERRAL_SCHEME_NOT_FOUND);
            }
        }

        return incentiveSchemeDBBean;
    }

    //Create Referral Code History
    public ReferralCodeHistoryDBBean createReferralCodeHistory(int referralCodeId, int schemeId)
    {
        //Create the referral code history bean
        ReferralCodeHistoryDBBean referralCodeHistoryDBBean = new ReferralCodeHistoryDBBean();
        referralCodeHistoryDBBean.setReferralCodeId(referralCodeId);
        referralCodeHistoryDBBean.setIncentiveSchemeId(schemeId);
        referralCodeHistoryDBBean.setStartDate(CommonUtils.getCurrentTimestampInUTC());
        return referralCodeHistoryDBBean;
    }

    //Update the end date for Referral Code history entry for the scheme Id
    private void updateReferralCodeHistory(ReferralCodeDBBean referralCodeDBBean)
    {
        //Get the referral code history entry
        ReferralCodeHistoryDBBean referralCodeHistoryDBBean =
                referralCodeDao.getReferralCodeHistory(referralCodeDBBean.getId(), referralCodeDBBean.getIncentiveSchemeId());

        //Update the end date in the history table
        referralCodeHistoryDBBean.setEndDate(CommonUtils.getCurrentTimestampInUTC());
        referralCodeDao.updateReferralCodeHistory(referralCodeHistoryDBBean);

    }
}
