package com.gigsky;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.ErrorStringDBBean;
import com.gigsky.database.dao.ErrorStringDao;
import com.gigsky.database.dao.TenantConfigurationsKeyValueDao;
import com.gigsky.exception.ReferralException;
import com.gigsky.filter.RequestUtils;
import com.gigsky.rest.bean.ErrorResponse;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.rest.exception.GigskyReferralEngineServerException;
import com.gigsky.rest.exception.ResourceValidationException;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSUtil;
import javassist.NotFoundException;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import java.text.ParseException;
import java.util.List;

@Provider
@Component
public class GigskyReferralEngineExceptionMapper implements ExceptionMapper<Throwable> {

	private static final Logger logger = LoggerFactory.getLogger(GigskyReferralEngineExceptionMapper.class);


	@Autowired
	private ErrorStringDao errorStringDao;

	@Autowired
	private TenantConfigurationsKeyValueDao tenantConfigurationsKeyValueDao;

	public static ErrorResponse createErrorResponse(ErrorStringDBBean errorStringBean) {
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorInt(errorStringBean.getErrorId());
		errorResponse.setUserDisplayErrorStr(errorStringBean.getErrorString());
		errorResponse.setUserDisplayErrorCode(errorStringBean.getErrorId());
		return errorResponse;
	}

	@Override
	public Response toResponse(Throwable ex) {

		ErrorStringDBBean errorStringBean;

		// Fetching locale from request headers
		String locale = GSUtil.getLocaleLanguage();
		int tenantId = RequestUtils.getTenantId();
		//If locale not matches with supported locales return set default language
		List<String> supportedLocaleList = Configurations.getSupportedLocale(tenantId);

		if(!supportedLocaleList.contains(locale)) {
			String defLang = Configurations.TenantConfigurationKeyValueType.TENANT_DEF_LANG;
			locale = tenantConfigurationsKeyValueDao.getTenantConfigurationValue(defLang,tenantId);
		}

		if (ex instanceof GigskyReferralEngineServerException) {

			logger.error("GigskyReferralEngineServerException occurred");
			GigskyReferralEngineServerException lGsNGWServerException = (GigskyReferralEngineServerException) ex;
			errorStringBean = errorStringDao.getErrorStringByErrorId(lGsNGWServerException.getErrorCode().statusCode);

		} else if(ex instanceof ReferralException)
		{
			//Log stack trace
			if(!(ex instanceof ServiceException || ex instanceof ResourceValidationException))
			{
				logger.error( ex.getClass().getName() +" occurred");
			}

			//If error message is passed
			ReferralException notificationException = (ReferralException) ex;
			if(StringUtils.isEmpty(notificationException.getMessage())) {
				errorStringBean = errorStringDao.getErrorStringByErrorId(notificationException.getErrorCode().statusCode, locale);
			}
			else
			{
				errorStringBean =  errorStringDao.getErrorStringByErrorId(notificationException.getErrorCode().statusCode, locale);
				errorStringBean.setErrorString(notificationException.getMessage());
			}

		} else if (ex instanceof NotFoundException) {

			errorStringBean = errorStringDao.getErrorStringByErrorId(ErrorCode.RESOURCE_NOT_FOUND.statusCode);
			logger.error("URL not found " + ex.getMessage());

		} else if (ex instanceof WebApplicationException) {

			errorStringBean = errorStringDao.getErrorStringByErrorId(ErrorCode.RESOURCE_NOT_FOUND.statusCode);

			int httpErrorCode = ((WebApplicationException) ex).getResponse().getStatus();
			if (httpErrorCode == ErrorCode.HTTP_METHOD_NOT_SUPPORTED_CODE.statusCode)
			{
				errorStringBean.getHttpErrorId().setErrorCode(ErrorCode.HTTP_RESOURCE_NOT_SUPPORTED_CODE.statusCode);
			} else {
				errorStringBean.getHttpErrorId().setErrorCode(((WebApplicationException) ex).getResponse().getStatus());
			}
			logger.error("resource not found " + ex.getMessage());

		}
		else if (ex instanceof IllegalArgumentException) {

			errorStringBean = errorStringDao.getErrorStringByErrorId(ErrorCode.INVALID_ARGUMENTS.statusCode);
			logger.error("Illegal arguments " + ex.getMessage());

		}
		else if (ex instanceof HttpClientErrorException) {

			errorStringBean = errorStringDao.getErrorStringByErrorId(ErrorCode.RESOURCE_NOT_FOUND.statusCode);
			logger.error("HttpClientErrorException due to invalid arguments " + ex.getMessage());

		}
		else if (ex instanceof JsonMappingException) {

			errorStringBean = errorStringDao.getErrorStringByErrorId(ErrorCode.INVALID_ARGUMENTS.statusCode);
			String errorMessage = ex.getMessage();
			if (errorMessage != null && (errorMessage.indexOf("(") > 0)) {
				errorMessage = errorMessage.substring(0, errorMessage.indexOf("("));
				errorStringBean.setErrorString(errorMessage);
			}
			logger.error("JsonMappingException " + errorMessage);

		}
		else if(ex instanceof JsonParseException)
		{
			errorStringBean = errorStringDao.getErrorStringByErrorId(ErrorCode.INVALID_ARGUMENTS.statusCode);
			String errorMessage = ex.getMessage();
			if (errorMessage != null && (errorMessage.indexOf("\n") > 0)) {
				errorMessage = errorMessage.substring(0, errorMessage.indexOf("\n"));
				errorStringBean.setErrorString(errorMessage);
			}
			logger.error("JsonParseException " + errorMessage);
		}
		else if (ex instanceof ParseException) {

			errorStringBean = errorStringDao.getErrorStringByErrorId(ErrorCode.INVALID_DATE.statusCode);
			String errorMessage = ex.getMessage();
			if (errorMessage != null && (errorMessage.indexOf("(") > 0)) {
				errorMessage = errorMessage.substring(0, errorMessage.indexOf("("));
				errorStringBean.setErrorString(errorMessage);
			}
			logger.error("ParseException " + errorMessage);

		}
		else {
			errorStringBean = errorStringDao.getErrorStringByErrorId(ErrorCode.UNKNOWN_ERROR.statusCode);
			logger.error("Unexpected " + ex.getClass().getName() + " occurred due to " + ex.getMessage());
		}

		// construct error response using error string bean
		if (errorStringBean != null) {
			return Response.status(errorStringBean.getHttpErrorId().getErrorCode())
					.entity(createErrorResponse(errorStringBean))
					.type(MediaType.APPLICATION_JSON).build();
		}
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
	}
}
