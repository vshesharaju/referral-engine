package com.gigsky.scheduler;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.PromoEventDBBean;
import com.gigsky.database.dao.PromoEventDao;
import com.gigsky.task.ReferralEventTaskFactory;
import com.gigsky.task.PromoEventsTask;
import com.gigsky.util.GSTaskUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by pradeepragav on 01/12/17.
 * Creates queue of tasks and assigns a thread from the thread pool
 * Can be extended to other promoTask Ex. plan purchase and sign up event promotions
 */
public class PromoEventTaskScheduler extends BaseSchedulerImpl
{

    private static final Logger logger = LoggerFactory.getLogger(PromoEventTaskScheduler.class);

    private static final int DEFAULT_PROMO_EVENTS_COUNT = 500;
    private static final int DEFAULT_PROMO_EVENTS_QUEUE_SIZE = 500;

    @Autowired
    private PromoEventDao promoEventDao;

    @Autowired
    private ReferralEventTaskFactory referralEventTaskFactory;

    @Autowired
    GSTaskUtil gsTaskUtil;

    @Override
    public void initialize() {
        logger.debug("PromoEventTaskScheduler:initialize++");
        super.init(DEFAULT_PROMO_EVENTS_QUEUE_SIZE);
    }

    @Override
    public void startProcess()
    {
        int promoEventsCount = DEFAULT_PROMO_EVENTS_COUNT;
        int pendingTasks;

        String promoEventsCountValue = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.PROMO_EVENTS_COUNT);

        if(StringUtils.isNotEmpty(promoEventsCountValue))
        {
            promoEventsCount = Integer.parseInt(promoEventsCountValue);
        }

        synchronized (this) {
            pendingTasks = threadPoolExecutor.getQueue().size();
        }

        promoEventsCount -= pendingTasks;
        if (promoEventsCount > 0)
        {
            processPromoEvents(promoEventsCount);
        }
    }

    private void processPromoEvents(int promoEventsCount)
    {
        //Fetch promoEventsCount of SIM activation events
        List<PromoEventDBBean> promoEventDBBeans = promoEventDao.getPromoEvents(Configurations.StatusType.STATUS_NEW, promoEventsCount);

        //Submit those event entries to PromoSimActivationTask
        for(PromoEventDBBean promoEventDBBean : promoEventDBBeans)
        {
            // Check promotion enabled for specific tenant else mark as complete and not processed
            if(gsTaskUtil.checkPromotionEnable(promoEventDBBean.getTenantId())){
                //Change the status message to IN_PROCESS in PromoTable entry
                promoEventDBBean.setStatus(Configurations.StatusType.STATUS_IN_PROCESS);
                promoEventDao.updatePromoEvent(promoEventDBBean);

                //Submit promoEvent to the task
                PromoEventsTask promoEventsTask =
                        (PromoEventsTask) referralEventTaskFactory
                                .createReferralEventTask("PROMO_EVENTS_TASK");

                promoEventsTask.setPromoEventDBBean(promoEventDBBean);
                threadPoolExecutor.submit(promoEventsTask);
            } else{
                promoEventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
                promoEventDBBean.setStatusMessage(Configurations.StatusType.NOT_PROCESSED);
                promoEventDao.updatePromoEvent(promoEventDBBean);
            }
        }
    }


    @Override
    public void destroy(){
        logger.debug("PromoEventTaskScheduler:destroy++");
        super.destroy();
    }

}
