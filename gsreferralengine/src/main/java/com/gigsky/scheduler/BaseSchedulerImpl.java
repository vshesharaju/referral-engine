package com.gigsky.scheduler;

import com.gigsky.common.Configurations;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by prsubbareddy on 10/02/17.
 */
public class BaseSchedulerImpl implements BaseSchedulerInterface {

    private static final Logger logger = LoggerFactory.getLogger(BaseSchedulerImpl.class);

    private final int DEFAULT_THREAD_COUNT = 4;
    private final int MAX_THREAD_COUNT = 8;
    private final int DEFAULT_THREAD_IDLE_TIME = 10;
    private int DEFAULT_QUEUE_SIZE = 500;

    protected ThreadPoolExecutor threadPoolExecutor = null;

    @Autowired
    protected ConfigurationKeyValueDao configurationKeyValueDao;

    @Override
    public void initialize()
    {

    }

    public void init() {
        logger.debug("BaseSchedulerImpl:initialize++");

        if (threadPoolExecutor == null) {
            synchronized (BaseSchedulerImpl.class) {
                if (threadPoolExecutor == null) {

                    int numOfThreads = DEFAULT_THREAD_COUNT;
                    String processingThreads = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.REFERRAL_PROCESSING_THREADS);

                    if(StringUtils.isNotEmpty(processingThreads))
                    {
                        numOfThreads = Integer.parseInt(processingThreads);
                    }
                    logger.debug("BaseSchedulerImpl:initialize--");
                    threadPoolExecutor = new ThreadPoolExecutor(numOfThreads, MAX_THREAD_COUNT, DEFAULT_THREAD_IDLE_TIME, TimeUnit.MILLISECONDS, new ArrayBlockingQueue<Runnable>(DEFAULT_QUEUE_SIZE));
                }
            }
        }
    }

    public void init(int queueCount){

        DEFAULT_QUEUE_SIZE = queueCount;
        init();
    }

    @Override
    public void startProcess() {

    }

    @Override
    public void destroy() {
        logger.debug("BaseSchedulerImpl:destroy++");
        try {
            if (threadPoolExecutor != null) {
                logger.debug("BaseSchedulerImpl:executorService.shutdown ++");
                threadPoolExecutor.shutdown();
                logger.debug("BaseSchedulerImpl:executorService.shutdown --");

                while (!threadPoolExecutor.isTerminated()) {
                    logger.debug("BaseSchedulerImpl:executorService is Terminated flag is false waiting for 1 secs");
                    threadPoolExecutor.awaitTermination(1, TimeUnit.SECONDS);
                }
            }
        }
        catch (InterruptedException ex) {
            logger.error(Configurations.ErrorMsgType.SYSTEM_ERROR + " : shutdown failed with exception ", ex);
        }
        logger.debug("BaseSchedulerImpl:awaitTermination--");
        logger.debug("BaseSchedulerImpl:destroy--");
    }
}
