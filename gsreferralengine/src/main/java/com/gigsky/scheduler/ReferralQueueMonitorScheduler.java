package com.gigsky.scheduler;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.backend.ngw.NgwManagementInterface;
import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.common.GigskyDuration;
import com.gigsky.database.bean.HealthCheckEventDBBean;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.database.dao.HealthCheckEventDao;
import com.gigsky.database.dao.TenantConfigurationsKeyValueDao;
import org.apache.commons.lang.StringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by prsubbareddy on 08/02/17.
 */
public class ReferralQueueMonitorScheduler implements Job {

    private static final Logger logger = LoggerFactory.getLogger(ReferralQueueMonitorScheduler.class);

    @Autowired
    private HealthCheckEventDao healthCheckEventDao;

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    private NgwManagementInterface ngwManagementInterface;

    @Autowired
    private TenantConfigurationsKeyValueDao tenantConfigurationsKeyValueDao;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        // Process referral queue monitor using Health Check Event
        processReferralQueueMonitor();
    }

    public void processReferralQueueMonitor() {

        HealthCheckEventDBBean healthCheckEventDBBean = healthCheckEventDao.getHealthCheckEvent();

        if(healthCheckEventDBBean != null)
        {
            try {
                // Get timestamp of HealthCheckEventDBBean event received time
                Timestamp timeStamp = healthCheckEventDBBean.getEventReceivedTime();

                // Getting time in millis for entry, base on last event received time
                long timeInMilliSeconds = timeStamp.getTime();

                // Getting time in millis for current time
                long currentTimeInMilliSeconds = CommonUtils.getCurrentTimestamp().getTime();

                boolean isQueueUp = true;
                boolean healthCheckQueueUp = true;

                // Default referral queue max delay to send emails
                long referralQueueMaxDelay = Configurations.MILLISECONDS_FOR_TEN_MINUTES;

                // Fetch referral queue max delay time in millis from configuration key values
                String referralQueueDelay = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.REFERRAL_QUEUE_MAX_DELAY_IN_MS);
                if(StringUtils.isNotEmpty(referralQueueDelay)) {
                    referralQueueMaxDelay = new GigskyDuration(referralQueueDelay).convertTimeInMilliSeconds();
                }

                // Check if time duration exceeds 10 MINUTES, queue not received any events
                if(currentTimeInMilliSeconds - timeInMilliSeconds >= referralQueueMaxDelay) {
                    isQueueUp = false;
                }

                // Getting queue status from health check event entry from data base
                if(healthCheckEventDBBean.getIsQueueUp() == null || healthCheckEventDBBean.getIsQueueUp() == 0){
                    healthCheckQueueUp = false;
                }


                if(isQueueUp) {
                    // update health check event with queue up
                    logger.info("Rabbit MQ server queue is up");
                    healthCheckEventDBBean.setIsQueueUp((byte) 1);
                } else{
                    // update health check event with queue down
                    logger.info("Rabbit MQ server queue is down");
                    healthCheckEventDBBean.setIsQueueUp((byte) 0);
                }

                healthCheckEventDao.updateHealthCheckEventInfo(healthCheckEventDBBean);

                // if both flags are not matched, we just send email
                if(isQueueUp != healthCheckQueueUp){

                    if(isQueueUp)
                    {
                        // send email with queue up message
                        sendEmail(true);
                    } else {
                        // send email with queue down message
                        sendEmail(false);
                    }
                }
            } catch (Exception e) {
                // throw error logs for exception
                logger.error("ReferralQueueUpTask exception:" + e, e);
            }
        }
    }

    protected void sendEmail(boolean isQueueUp) throws BackendRequestException
    {
        //Language
        String language = "en";

        int tenantId = Integer.parseInt(configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.DEFAULT_TENANT_ID, "1"));

        //Get adminEmailId using configurationKeyValue table
        String adminEmailId = tenantConfigurationsKeyValueDao
                .getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.GS_ADMIN_EMAIL_ID, tenantId);

        //Admin EmailId
        List adminInternalEmail = new ArrayList();
        adminInternalEmail.add(adminEmailId);

        if(isQueueUp)
        {
            logger.info("Sending Health Check Event email for rabbit mq server up ");
            ngwManagementInterface.sendEmail(Configurations.EmailType.RABBIT_MQ_SERVER_UP, adminInternalEmail, language, null, tenantId);
        } else{
            logger.info("Sending Health Check Event email for rabbit mq server down ");
            ngwManagementInterface.sendEmail(Configurations.EmailType.RABBIT_MQ_SERVER_DOWN, adminInternalEmail, language, null, tenantId);
        }

    }
}
