package com.gigsky.scheduler;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import com.gigsky.task.PromotionRetryCreditTask;
import com.gigsky.task.ReferralEventTaskFactory;
import com.gigsky.task.RetryAddCreditTask;
import com.gigsky.util.GSTaskUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by prsubbareddy on 08/02/17.
 */
public class RetryCreditScheduler extends BaseSchedulerImpl {

    private static final Logger logger = LoggerFactory.getLogger(RetryCreditScheduler.class);

    private static final int DEFAULT_RETRY_CREDIT_COUNT = 500;
    private static final int DEFAULT_RETRY_CREDIT_QUEUE_SIZE = 500;

    @Autowired
    InvitationDao invitationDao;

    @Autowired
    ReferralEventTaskFactory referralEventFactory;

    @Autowired
    PromoHistoryDao promoHistoryDao;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    GSTaskUtil gsTaskUtil;

    @Autowired
    PromoEventDao promoEventDao;

    @Autowired
    PromotionDao promotionDao;

    @Override
    public void initialize() {
        logger.debug("RetryCreditScheduler:initialize++");
        super.init(DEFAULT_RETRY_CREDIT_QUEUE_SIZE);
    }

    @Override
    public void startProcess()
    {
        int retryCreditEventsCount = DEFAULT_RETRY_CREDIT_COUNT;
        int pendingTasks;
        String retryCreditCountValue = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.RETRY_CREDIT_COUNT);

        if(StringUtils.isNotEmpty(retryCreditCountValue))
        {
            retryCreditEventsCount = Integer.parseInt(retryCreditCountValue);
        }

        synchronized (this) {
            pendingTasks = threadPoolExecutor.getQueue().size();
        }

        retryCreditEventsCount -= pendingTasks;
        if (retryCreditEventsCount > 0)
        {
            processReferralRetryCredit(false, retryCreditEventsCount);
        }

        synchronized (this) {
            pendingTasks = threadPoolExecutor.getQueue().size();
        }

        retryCreditEventsCount -= pendingTasks;
        if (retryCreditEventsCount > 0)
        {
            processReferralRetryCredit(true, retryCreditEventsCount);
        }

        retryCreditEventsCount -= pendingTasks;
        if(retryCreditEventsCount > 0)
        {
            processPromotionRetryCredit(retryCreditEventsCount);
        }

    }

    // Process referral retry credit using invitation table
    private void processReferralRetryCredit(boolean isAdvocate, int count) {

        //fetching invitations list for add credit failures
        List<InvitationDBBean> invitationDBBeanList;

        if(isAdvocate) {
            // fetch advocate credit failed invitations
            invitationDBBeanList = invitationDao.getInvitationsForCreditFailed("advocateCreditStatus", count);
        } else{
            // fetch referral credit failed invitations
            invitationDBBeanList = invitationDao.getInvitationsForCreditFailed("referralCreditStatus", count);
        }

        for(InvitationDBBean invitationBean: invitationDBBeanList) {
            processReferralRetryCreditTask(invitationBean, isAdvocate);
        }
    }

    private void processReferralRetryCreditTask(InvitationDBBean invitationBean, boolean isAdvocate) {

        CustomerDBBean customerDBBean = customerDao.getCustomerInfoWithOutTenantId(invitationBean.getTargetCustomerId());

        // Check if referral enabled, then only process retry credit else no need to change status
        if(gsTaskUtil.checkReferralEnable(customerDBBean.getTenantId()))
        {
            if(isAdvocate) {
                invitationBean.setAdvocateCreditStatus(Configurations.ReferralStatus.CREDIT_ADD_IN_PROCESS);
            } else {
                invitationBean.setReferralCreditStatus(Configurations.ReferralStatus.CREDIT_ADD_IN_PROCESS);
            }

            invitationDao.updateInvitation(invitationBean);

            RetryAddCreditTask retryAddCreditTask = (RetryAddCreditTask) referralEventFactory.createReferralEventTask("RETRY_ADD_CREDIT_TASK");
            retryAddCreditTask.setInvitationBean(invitationBean, isAdvocate);
            threadPoolExecutor.submit(retryAddCreditTask);
        }
    }

    //process promotional retry credit using promotion history table
    private void processPromotionRetryCredit(int count) {

        List<PromoHistoryDBBean> historyDBBeansList = promoHistoryDao.getPromotionHistoryForFailed(count);

        for(PromoHistoryDBBean historyDBBean: historyDBBeansList) {

            int tenantId;

            // Promotion apply code cases there won't be any promo event.
            if(historyDBBean.getPromoEventId() != null) {
                PromoEventDBBean promoEventDBBean = promoEventDao.getPromoEvent(historyDBBean.getPromoEventId());
                tenantId = promoEventDBBean.getTenantId();

            }else{
                //fetch tenant id based on the promotionId
                int promotionId = historyDBBean.getPromotionId();
                PromotionDBBean promotionDBBean = promotionDao.getPromotionByPromoId(promotionId);
                tenantId = promotionDBBean.getTenantId();
            }

            // Check if promotion enabled, then only process retry credit else no need to change status
            if(gsTaskUtil.checkPromotionEnable(tenantId))
            {
                // update history table with in process
                historyDBBean.setStatus(Configurations.StatusType.STATUS_IN_PROCESS);
                promoHistoryDao.updatePromotionHistory(historyDBBean);

                // Submit promo history bean for process retry
                PromotionRetryCreditTask promotionRetryCreditTask = (PromotionRetryCreditTask) referralEventFactory.createReferralEventTask("RETRY_PROMOTION_CREDIT_TASK");
                promotionRetryCreditTask.setPromotionHistoryBean(historyDBBean);
                threadPoolExecutor.submit(promotionRetryCreditTask);
            }
        }
    }

    @Override
    public void destroy(){
        logger.debug("RetryCreditScheduler:destroy++");
        super.destroy();
    }
}
