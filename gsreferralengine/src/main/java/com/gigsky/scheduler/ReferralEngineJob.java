package com.gigsky.scheduler;

import com.gigsky.backend.credit.SupportedCurrenciesInterface;
import com.gigsky.common.CommonUtils;
import com.gigsky.common.GigskyDuration;
import com.gigsky.database.bean.IncentiveSchemeDBBean;
import com.gigsky.database.bean.PromotionDBBean;
import com.gigsky.database.bean.TenantDBBean;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.database.dao.IncentiveSchemeDao;
import com.gigsky.database.dao.PromotionDao;
import com.gigsky.database.dao.TenantDao;
import com.gigsky.task.DeleteAccountAlertEmailTask;
import org.apache.commons.collections.CollectionUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * Created by pradeepragav on 07/02/17.
 *
 * SchemeValidityScheduler is a task scheduled using quartz scheduler
 * This task is scheduled at 12:00AM(UTC) every day
 *
 * Updates each scheme by comparing validity with current time
 *
 * Updates promotion by comparing validity with current time
 *
 * Checks the number of delete accounts on that day and sends an email if it exceeds the threshold
 *
 * Updates supported currencies
 *
 */

public class ReferralEngineJob implements Job {

    private static final Logger logger = LoggerFactory.getLogger(ReferralEngineJob.class);

    //Interval for which delete account events should be fetched
    private static final String DELETE_ACCOUNT_EVENT_INTERVAL = "000001000000000";

    @Autowired
    private IncentiveSchemeDao incentiveSchemeDao;

    @Autowired
    private SupportedCurrenciesInterface supportedCurrenciesInterface;

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    private DeleteAccountAlertEmailTask deleteAccountAlertEmailTask;

    @Autowired
    private TenantDao tenantDao;

    @Autowired
    private PromotionDao promotionDao;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        Timestamp currentTime = CommonUtils.getCurrentTimestampInUTC();
        logger.info("Running SchemeValidityScheduler triggered by quartz, time : " + currentTime);


        /*Validate schemes*/
        validateIncentiveSchemes();

        /*Validate promotions*/
        validatePromotions();

        /*Check number of deleted accounts*/
        runDeleteAccountAlertEmailTask();

        /*Update supported currencies*/
        updateSupportedCurrencies();
    }



    public void runDeleteAccountAlertEmailTask()
    {
        //Get current time in milliseconds in UTC
        Timestamp currentTimestampInUTC = CommonUtils.getCurrentTimestampInUTC();
        long currentTimeInMillis = currentTimestampInUTC.getTime();

        //Get the interval for which delete account events should be counted
        String deleteAccountEventInterval = DELETE_ACCOUNT_EVENT_INTERVAL;
        GigskyDuration gigskyDuration = new GigskyDuration(deleteAccountEventInterval);
        long deleteAccountEventIntervalInMillis = gigskyDuration.convertTimeInMilliSeconds();

        //Get start and end time
        long startTimeInMillis = currentTimeInMillis - deleteAccountEventIntervalInMillis;
        long endTimeInMillis = currentTimeInMillis;

        /*Run delete account alert email task,
         which sends email to admin
          if delete accounts in the given interval
           reaches the maximum count*/
        deleteAccountAlertEmailTask.setStartTimeInMillis(startTimeInMillis);
        deleteAccountAlertEmailTask.setEndTimeInMillis(endTimeInMillis);
        deleteAccountAlertEmailTask.run();

    }


    public void validateIncentiveSchemes(){

        //Get incentiveSchemesWhichAreEnabled
        List<IncentiveSchemeDBBean> incentiveSchemeDBBeanList=
                incentiveSchemeDao.getIncentiveSchemesWhichAreEnabled();

        if(CollectionUtils.isEmpty(incentiveSchemeDBBeanList)){

            return;
        }

        //For each incentiveScheme
        for(IncentiveSchemeDBBean incentiveSchemeDBBean : incentiveSchemeDBBeanList){

            //Check the validity of the incentive Scheme
            if(compareWithCurrentDate(incentiveSchemeDBBean.getValidityEndDate())){

                //Activate the scheme if it is not activated
                if (incentiveSchemeDBBean.getActiveScheme() == 0) {

                    //Set active scheme
                    incentiveSchemeDBBean.setActiveScheme((byte) 1);

                    //Update incentiveScheme
                    incentiveSchemeDao.updateIncentiveScheme(incentiveSchemeDBBean);
                }
            }
            else {

                //Incentive Scheme is expired

                //Set isActive and isEnabled to NO
                incentiveSchemeDBBean.setSchemeEnabled((byte) 0);
                incentiveSchemeDBBean.setActiveScheme((byte) 0);

                //Update incentiveScheme
                incentiveSchemeDao.updateIncentiveScheme(incentiveSchemeDBBean);
            }
        }
    }

    public void validatePromotions()
    {
        //Fetch promotions which are enabled
        List<PromotionDBBean> enabledPromotions = promotionDao.getEnabledPromotions();

        if(CollectionUtils.isEmpty(enabledPromotions))
        {
            logger.info("No promotions for checking validators");
            return;
        }

        //For each promotion change the active status w.r.t to current time
        for(PromotionDBBean promotionDBBean : enabledPromotions)
        {
            //If the end date is greater than the current date
            if(compareWithCurrentDate(promotionDBBean.getEndDate()))
            {
                //Valid promotion
                if(promotionDBBean.getActivePromo() == 0)
                {
                    promotionDBBean.setActivePromo((byte)1);
                    promotionDao.updatePromotion(promotionDBBean);
                }
            }
            else
            {
                //Expire promotion
                if(promotionDBBean.getActivePromo() == 1)
                {
                    promotionDBBean.setActivePromo((byte)0);
                    promotionDao.updatePromotion(promotionDBBean);
                }
            }
        }
    }


    /*Update supportedCurrencies*/
    public void updateSupportedCurrencies()
    {
        //Get list of tenants, for each tenantId get supportedCurrencies
        List<TenantDBBean> allTenants = tenantDao.getAllTenants();

        for(TenantDBBean tenantDBBean : allTenants)
        {
            supportedCurrenciesInterface.updateSupportedCurrencies(tenantDBBean.getId());
        }
    }


    private boolean compareWithCurrentDate(Date endDate){

        //Get currentDate
        Date currentDate = CommonUtils.getCurrentDate();

        //If end date is less than the current date
        if(currentDate.compareTo(endDate) > 0){

            return false;
        }

        return true;
    }


}
