package com.gigsky.scheduler;

/**
 * Created by pradeepragav on 07/02/17.
 */
public interface BaseSchedulerInterface {

    void initialize();
    void startProcess();
    void destroy();
}
