package com.gigsky.scheduler;

import com.gigsky.common.Configurations;
import com.gigsky.common.GigskyDuration;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.task.ReferralEventTaskFactory;
import com.gigsky.task.ReferralSendEmailTask;
import com.gigsky.util.GSTaskUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * Created by prsubbareddy on 08/02/17.
 */
public class ReferralProgramEmailScheduler extends BaseSchedulerImpl {

    private static final Logger logger = LoggerFactory.getLogger(ReferralProgramEmailScheduler.class);

    private static final int DEFAULT_REFERRAL_EMAIL_QUEUE_SIZE = 500;
    private static final int DEFAULT_REFERRAL_SEND_EMAIL_COUNT = 500;
    private static final long DEFAULT_REFERRAL_INITIAL_DELAY = Configurations.MILLISECONDS_IN_A_DAY;
    private static final String DEFAULT_REFERRAL_RESEND_DELAY = "000300000000000";

    @Autowired
    CustomerDao customerDao;

    @Autowired
    ReferralEventTaskFactory referralEventFactory;

    @Autowired
    GSTaskUtil gsTaskUtil;

    @Override
    public void initialize() {
        logger.debug("ReferralProgramEmailScheduler:destroy++");

        int referralEmailQueueSize = DEFAULT_REFERRAL_EMAIL_QUEUE_SIZE;

        // Setting QUEUE size as referral emails sending count
        String sendEmailCountValue = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.REFERRAL_SEND_EMAIL_COUNT);

        if(StringUtils.isNotEmpty(sendEmailCountValue)) {
            referralEmailQueueSize = Integer.parseInt(sendEmailCountValue);
        }

        super.init(referralEmailQueueSize);
    }

    @Override
    public void startProcess()
    {
        int sendEmailCount = DEFAULT_REFERRAL_SEND_EMAIL_COUNT;
        int pendingTasks;
        String sendEmailCountValue = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.REFERRAL_SEND_EMAIL_COUNT);

        if(StringUtils.isNotEmpty(sendEmailCountValue))
        {
            sendEmailCount = Integer.parseInt(sendEmailCountValue);
        }

        synchronized (this) {
            pendingTasks = threadPoolExecutor.getQueue().size();
        }

        sendEmailCount -= pendingTasks;
        if (sendEmailCount > 0)
        {
            // process emails
            processReferralProgramEmails(sendEmailCount);
        }

    }

    private void processReferralProgramEmails(int count) {

        //fetch list of customers using createTime to current time >= initial delay || last invite sent to current time >= resend delay
        long initialDelay = DEFAULT_REFERRAL_INITIAL_DELAY;

        String initialDelayTime = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.REFERRAL_INITIAL_DELAY);

        if(StringUtils.isNotEmpty(initialDelayTime)) {
            initialDelay = new GigskyDuration(initialDelayTime).convertTimeInMilliSeconds();
        }

        long resendDelay = new GigskyDuration(DEFAULT_REFERRAL_RESEND_DELAY).convertTimeInMilliSeconds();

        String resendDelayTime = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.REFERRAL_RESEND_DELAY);

        if(StringUtils.isNotEmpty(resendDelayTime)) {
            resendDelay =  new GigskyDuration(resendDelayTime).convertTimeInMilliSeconds();
        }

        List<CustomerDBBean> customers = customerDao.getValidCustomers(count, initialDelay, resendDelay);


        //Check customer specific tenant referral enable and processs
        for(CustomerDBBean customerDBBean : customers) {

            if(gsTaskUtil.checkReferralEnable(customerDBBean.getTenantId()) && gsTaskUtil.checkReferralEmailEnable(customerDBBean.getTenantId())){
                // referral send email task will send emails to customers
                ReferralSendEmailTask emailTask = (ReferralSendEmailTask) referralEventFactory.createReferralEventTask("REFERRAL_EMAIL_TASK");
                emailTask.setCustomerInfo(customerDBBean);
                threadPoolExecutor.submit(emailTask);
            }

        }

    }

    @Override
    public void destroy(){
        logger.debug("ReferralProgramEmailScheduler:destroy++");
        super.destroy();
    }
}
