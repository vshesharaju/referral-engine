package com.gigsky.task;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.backend.ngw.NgwManagementInterface;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.TenantDBBean;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.database.dao.ReferralEventDao;
import com.gigsky.database.dao.TenantConfigurationsKeyValueDao;
import com.gigsky.database.dao.TenantDao;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pradeepragav on 15/06/18.
 */
public class DeleteAccountAlertEmailTask
{
    private static final Logger logger = LoggerFactory.getLogger(DeleteAccountAlertEmailTask.class);
    private static final int DEFAULT_MAX_DELETE_ACCOUNT = 10;

    private long startTimeInMillis;
    private long endTimeInMillis;

    public long getStartTimeInMillis() {
        return startTimeInMillis;
    }

    public void setStartTimeInMillis(long startTimeInMillis) {
        this.startTimeInMillis = startTimeInMillis;
    }

    public long getEndTimeInMillis() {
        return endTimeInMillis;
    }

    public void setEndTimeInMillis(long endTimeInMillis) {
        this.endTimeInMillis = endTimeInMillis;
    }

    @Autowired
    ReferralEventDao referralEventDao;

    @Autowired
    ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    NgwManagementInterface ngwManagementInterface;

    @Autowired
    TenantConfigurationsKeyValueDao tenantConfigurationsKeyValueDao;

    @Autowired
    TenantDao tenantDao;

    public void run()
    {
        // Fetch only default tenant which is gigsky and send mail
        int tenantId = Integer.parseInt(configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.DEFAULT_TENANT_ID, "1"));

        /*Fetch number of deleted accounts for the given interval*/
        int numberOfDeleteAccountEvents = referralEventDao
                .getNumberOfReferralEvent(Configurations.ReferralEventType.ACCOUNT_DELETE_EVENT,
                        startTimeInMillis, endTimeInMillis, tenantId);

        /*Get maximum number of deleted accounts per given interval*/
        String maxAccountDeleteStr= configurationKeyValueDao
                .getConfigurationValue(Configurations.ConfigurationKeyValueType.MAX_DELETE_ACCOUNT);

        int maxAccountDelete = DEFAULT_MAX_DELETE_ACCOUNT;
        if(StringUtils.isNotEmpty(maxAccountDeleteStr))
        {
            maxAccountDelete = Integer.parseInt(maxAccountDeleteStr);
        }

        /*If number of delete events greater than ‘MAX_ACCOUNT_DELETE’
        call notification gateway API*/
        if(maxAccountDelete <= numberOfDeleteAccountEvents)
        {
            /*Call send email alert API*/
            //Language
            String language = "en";

            //Get adminEmailId using configurationKeyValue table
            String adminEmailId = tenantConfigurationsKeyValueDao
                    .getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.GS_ADMIN_EMAIL_ID, tenantId);

            //Admin EmailId
            List adminInternalEmail = new ArrayList();
            adminInternalEmail.add(adminEmailId);

            logger.info("Sending Deleted accounts check email");
            try {
                ngwManagementInterface.sendEmail(Configurations.EmailType.DELETED_ACCOUNTS_THRESHOLD,
                        adminInternalEmail, language, null, tenantId);
                logger.info("Sending Deleted accounts check email");
            } catch (BackendRequestException e) {
                logger.error("FAILED Deleted accounts check email",e);
                e.printStackTrace();
            }
        }

    }

}
