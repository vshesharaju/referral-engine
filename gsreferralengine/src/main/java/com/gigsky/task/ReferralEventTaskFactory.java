package com.gigsky.task;

/**
 * Created by prsubbareddy on 30/01/17.
 */
public interface ReferralEventTaskFactory {

    TaskInterface createReferralEventTask(String name);

}
