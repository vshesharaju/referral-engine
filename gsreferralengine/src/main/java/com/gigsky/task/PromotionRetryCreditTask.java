package com.gigsky.task;

import com.gigsky.backend.credit.CreditManagementInterface;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.backend.ngw.NgwManagementInterface;
import com.gigsky.backend.ngw.beans.Option;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import com.gigsky.rest.bean.Credit;
import com.gigsky.util.GSSchemeUtil;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prsubbareddy on 07/12/17.
 * Retry promotion credit to the customer
 */
public class PromotionRetryCreditTask implements TaskInterface {

    private static final Logger logger = LoggerFactory.getLogger(PromotionRetryCreditTask.class);

    @Autowired
    private PromoIccidDao promoIccidDao;

    @Autowired
    private PreferenceDao preferenceDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private CreditManagementInterface creditManagementInterface;

    @Autowired
    private PromoHistoryDao promoHistoryDao;

    @Autowired
    private NgwManagementInterface ngwManagementInterface;

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    private PromoEventDao promoEventDao;

    @Autowired
    private PromotionDao promotionDao;

    @Autowired
    private PromoCreditDao promoCreditDao;

    private PromoHistoryDBBean promoHistoryDBBean;

    public void setPromotionHistoryBean(PromoHistoryDBBean promoHistDBBean) {
        promoHistoryDBBean = promoHistDBBean;
    }

    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }

    @Override
    public void run() {

        int retryCount = promoHistoryDBBean.getRetryCount();

        // Getting max retry count from config key values
        int maxRetryCount = Configurations.DEFAULT_MAX_CREDIT_RETRY_COUNT;
        String maxRetry = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.MAX_CREDIT_RETRY_COUNT);
        if(StringUtils.isNotEmpty(maxRetry)) {
            maxRetryCount = Integer.valueOf(maxRetry);
        }

        // Check retry count if not exceeds DEFAULT_MAX_CREDIT_RETRY_COUNT
        if(retryCount < maxRetryCount) {

            Session session = promotionDao.getSession();
            Transaction transaction = session.beginTransaction();

            try {

                PromoEventDBBean promoEventDBBean = null;
                int tenantId;

                // Promotion apply code cases there won't be any promo event.
                if(promoHistoryDBBean.getPromoEventId() != null) {
                    promoEventDBBean = promoEventDao.getPromoEvent(session, promoHistoryDBBean.getPromoEventId());
                    tenantId = promoEventDBBean.getTenantId();
                }
                else
                {
                    //fetch tenant id based on the promotionId
                    int promotionId = promoHistoryDBBean.getPromotionId();
                    PromotionDBBean promotionDBBean = promotionDao.getPromotionByPromoId(session, promotionId);
                    tenantId = promotionDBBean.getTenantId();
                }

                //Get promotion
                PromotionDBBean promotion = promotionDao.getPromotion(session, promoHistoryDBBean.getPromotionId(), tenantId);

                //Check if promotion is valid
                if(!(promotion.getActivePromo() == 1
                        && promotion.getCurrentRedeemCount() < promotion.getMaxRedeemCount()))
                {
                    //Promotion is no more valid
                    promoHistoryDBBean.setStatus(Configurations.PromoEventsTaskStatus.REACHED_MAX_REDEEM_COUNT);
                    promoHistoryDao.updatePromotionHistory(session,promoHistoryDBBean);

                    transaction.commit();
                    transaction = null;
                    promotionDao.closeSession(session);
                    return;
                }

                //Get credit
                PromoCreditDBBean promoCreditDBBean =
                        promoCreditDao.getPromoCredit(session, promotion.getId(), promoHistoryDBBean.getCurrency());

                //Create credit bean
                Credit credit = GSSchemeUtil.prepareCreditDetails(promoHistoryDBBean.getCurrency(),
                        promoCreditDBBean,
                        promotion.getCreditExpiryPeriod());

                CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(session,
                        promoHistoryDBBean.getGsCustomerId(), tenantId);

                //Add credit API call
                Credit creditResponse = creditManagementInterface.addCredit(promoHistoryDBBean.getGsCustomerId().intValue(), credit, tenantId);

                if (creditResponse != null &&
                        (Configurations.CreditAddResponseStatus.SUCCESS.equals(creditResponse.getType())))
                {

                    logger.info("retryCreditForPromotion, add credit SUCCESS for customerId " + promoHistoryDBBean.getGsCustomerId() );

                    //Set credit transaction
                    promoHistoryDBBean.setCreditTxnId(creditResponse.getCreditTxnId());

                    //Set credit ID
                    promoHistoryDBBean.setCreditId(creditResponse.getCreditId());

                    //Set status
                    promoHistoryDBBean.setStatus(Configurations.PromoEventsTaskStatus.CREDIT_ADD_SUCCESS);

                    //Get opt out preference
                    PreferenceDBBean optOutPreference = preferenceDao.getPreferenceByKey(session, customerDBBean.getId(), Configurations.PreferrenceKeyType.OPT_OUT);

                    boolean optValue = (optOutPreference.getValue().equals("true"));

                    PromoIccidDBBean promoIccidDBBean = promoIccidDao.getPromoIccid(session, promoHistoryDBBean.getIccid());

                    if(promoEventDBBean != null && promoIccidDBBean != null && Configurations.PromoEventType.SIM_ACTIVATION_EVENT.equals(promoEventDBBean.getEventType()) && promoIccidDBBean.getIsEmailNotifRequired() == 1 && !optValue)
                    {
                        sendEmail(promoHistoryDBBean);

                        //Set email status
                        promoHistoryDBBean.setEmailNotifStatus(Configurations.EmailStatus.SENT);
                    } else if(promoEventDBBean != null && Configurations.PromoEventType.SIGNUP_EVENT.equals(promoEventDBBean.getEventType())){
                        // Customer referral status updating with Credit add failed
                        customerDBBean.setReferralStatus(Configurations.ReferralStatus.SIGNUP_WITH_PROMOTION);
                        customerDao.updateCustomerInfo(session,customerDBBean);
                    }

                    //Increment current redeem count by one
                    promotion.setCurrentRedeemCount(promotion.getCurrentRedeemCount() + 1);
                    promotionDao.updatePromotion(session, promotion);
                    promoHistoryDao.updatePromotionHistory(session,promoHistoryDBBean);
                    transaction.commit();
                }
                else
                {
                    //Credit add failed
                    logger.info("retryCreditForPromotion, add credit FAILED for customerId " + promoHistoryDBBean.getGsCustomerId());

                    promoHistoryDBBean.setRetryCount(++retryCount);
                    promoHistoryDBBean.setStatus(Configurations.PromoEventsTaskStatus.CREDIT_ADD_FAILED);
                    promoHistoryDao.updatePromotionHistory(session,promoHistoryDBBean);
                    transaction.commit();
                }
            }
            catch (Exception e){

                // throw error logs for exception
                logger.error("Retry promotion credit exception:" + e,e);
                promoHistoryDBBean.setRetryCount(++retryCount);
                promoHistoryDBBean.setStatus(Configurations.PromoEventsTaskStatus.CREDIT_ADD_FAILED);
                promoHistoryDao.updatePromotionHistory(promoHistoryDBBean);
                if(transaction!=null)
                {
                    transaction.rollback();
                }
            }
            finally {
                if(session != null)
                {
                    transaction = null;
                    promotionDao.closeSession(session);
                }
            }

        }
    }

    protected void sendEmail(PromoHistoryDBBean promoHistoryDBBean) throws BackendRequestException
    {
        PromoEventDBBean promoEventDBBean = promoEventDao.getPromoEvent(promoHistoryDBBean.getPromoEventId());

        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(promoHistoryDBBean.getGsCustomerId(), promoEventDBBean.getTenantId());
        //Currency
        Option option = new Option();
        option.setCurrency(promoHistoryDBBean.getCurrency());

        //Language
        String language = (customerDBBean.getLanguage() != null) ? customerDBBean.getLanguage() : Configurations.DEFAULT_LANGUAGE_CODE;

        //Name
        option.setCustomerName(customerDBBean.getFirstName());

        //Email id
        option.setCustomerEmail(customerDBBean.getEmailId());

        //Promo amount
        option.setPromoAmount(promoHistoryDBBean.getCreditAmount().intValue());

        //Iccid
        option.setCsn(promoHistoryDBBean.getIccid());

        //GSCustomerId
        option.setGsCustomerId(customerDBBean.getGsCustomerId().intValue());

        List<String> emailIds = new ArrayList<String>();
        emailIds.add(customerDBBean.getEmailId());

        logger.info("Sending ICCID_PROMOTION_CREDIT email with options ");
        //todo : check if language changes based on tenants
        ngwManagementInterface.sendEmail(Configurations.EmailType.ICCID_PROMOTION_CREDIT, emailIds, language, option, customerDBBean.getTenantId());
    }

}
