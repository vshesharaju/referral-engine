package com.gigsky.task;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.InvitationDBBean;
import com.gigsky.database.bean.ReferralEventDBBean;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.InvitationDao;
import com.gigsky.database.dao.ReferralCodeDao;
import com.gigsky.database.dao.ReferralEventDao;
import com.gigsky.messaging.beans.EmailIDChange;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.util.GSTaskUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by prsubbareddy on 30/01/17.
 */
public class EmailChangeEventTask implements TaskInterface {

    private static final Logger logger = LoggerFactory.getLogger(EmailChangeEventTask.class);
    private ReferralEventDBBean referralEventDBBean;

    @Autowired
    GSTaskUtil gsTaskUtil;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    ReferralEventDao referralEventDao;

    @Autowired
    InvitationDao invitationDao;

    @Autowired
    ReferralCodeDao referralCodeDao;

    public void setReferralEvent(ReferralEventDBBean referralEvent) {
        this.referralEventDBBean = referralEvent;
    }

    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }

    @Override
    public void run() {

        try {

            //Get eventData from referralEventDBBean
            EventMessageData eventMessageData = GSTaskUtil.getMessageData(referralEventDBBean.getEventData(), referralEventDBBean.getEventType());
            EmailIDChange emailIDChangeData = (EmailIDChange) eventMessageData;

            // Fetch customer details and update status
            CustomerDBBean customerDBBean = customerDao.getCustomerInfo(referralEventDBBean.getCustomerId(), referralEventDBBean.getTenantId());

            //Update the customer details with the new email id
            customerDBBean.setEmailId(emailIDChangeData.getNewEmailId());
            customerDao.updateCustomerInfo(customerDBBean);

            // If invitation is there, update invitation with new emailId.
            InvitationDBBean invitationDBBean = invitationDao.getInvitationByTargetCustomerId(customerDBBean.getId());
            if(invitationDBBean != null){
                invitationDBBean.setEmailId(emailIDChangeData.getNewEmailId());
                invitationDao.updateInvitation(invitationDBBean);
            }

            //Update referral event status to complete
            referralEventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
            referralEventDao.updateReferralEvent(referralEventDBBean);

        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            referralEventDBBean.setStatus(Configurations.StatusType.STATUS_FAILED);
            referralEventDao.updateReferralEvent(referralEventDBBean);
        }

    }
}
