package com.gigsky.task;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.ReferralEventDBBean;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.ReferralEventDao;
import com.gigsky.util.GSTaskUtil;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by prsubbareddy on 30/01/17.
 */
public class PlanExpiredEventTask implements TaskInterface {

    private ReferralEventDBBean referralEventDBBean;

    public void setReferralEvent(ReferralEventDBBean referralEvent) {
        this.referralEventDBBean = referralEvent;
    }

    @Autowired
    GSTaskUtil gsTaskUtil;

    @Autowired
    ReferralEventDao referralEventDao;

    @Autowired
    CustomerDao customerDao;

    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }

    @Override
    public void run() {

        // Fetch customer details and update status
        CustomerDBBean customerDBBean = customerDao.getCustomerInfo(referralEventDBBean.getCustomerId(), referralEventDBBean.getTenantId());
        customerDBBean.setStatus(Configurations.CustomerStatus.STATUS_USING_SERVICE);
        //Update customerDBBean using customerDao
        customerDao.updateCustomerInfo(customerDBBean);

        // commented this code to avoid making a cal for add credit in case of plan expiry
        // add credit for advocate
        //gsTaskUtil.addCredit(referralEventDBBean);

        //update referral event status to complete
        referralEventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
        referralEventDao.updateReferralEvent(referralEventDBBean);
    }
}
