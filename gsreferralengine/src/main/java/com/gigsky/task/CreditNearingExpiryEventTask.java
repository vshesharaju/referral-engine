package com.gigsky.task;

import com.gigsky.backend.ngw.NgwManagementInterface;
import com.gigsky.backend.ngw.beans.Option;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.ReferralEventDBBean;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.ReferralEventDao;
import com.gigsky.messaging.beans.CreditExpiry;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.util.GSTaskUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prsubbareddy on 30/01/17.
 */
public class CreditNearingExpiryEventTask implements TaskInterface {

    private static final Logger logger = LoggerFactory.getLogger(CreditNearingExpiryEventTask.class);
    private ReferralEventDBBean referralEventDBBean;

    @Autowired
    GSTaskUtil gsTaskUtil;

    @Autowired
    ReferralEventDao referralEventDao;

    @Autowired
    NgwManagementInterface ngwManagementInterface;

    @Autowired
    CustomerDao customerDao;

    public void setReferralEvent(ReferralEventDBBean referralEvent) {
        this.referralEventDBBean = referralEvent;
    }

    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }

    @Override
    public void run() {

        try {

            EventMessageData eventMessageData = GSTaskUtil.getMessageData(referralEventDBBean.getEventData(), referralEventDBBean.getEventType());
            CreditExpiry creditNearExpiryData = (CreditExpiry) eventMessageData;

            // Fetch customer details and update status
            CustomerDBBean customerDBBean = customerDao.getCustomerInfo(referralEventDBBean.getCustomerId(), referralEventDBBean.getTenantId());

            String emailId = customerDBBean.getEmailId();
            String language = customerDBBean.getLanguage();
            String expiry = creditNearExpiryData.getExpiry();
            String creditAmount = creditNearExpiryData.getCreditAmount();

            // prepare emails list
            List<String> emailIds = new ArrayList<String>();
            emailIds.add(emailId);

            // setting required options for credit expired email
            Option option = new Option();

            // sending email for credit nearing expiry case for customer
            ngwManagementInterface.sendEmail(Configurations.EmailType.CREDIT_EXPIRED, emailIds, language, option, referralEventDBBean.getTenantId());

            //update referral event status to complete
            referralEventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
            referralEventDao.updateReferralEvent(referralEventDBBean);

        }
        catch (Exception e) {
            logger.error(e.getMessage(),e);
            referralEventDBBean.setStatus(Configurations.StatusType.STATUS_FAILED);
            referralEventDao.updateReferralEvent(referralEventDBBean);
        }
    }
}
