package com.gigsky.task;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.PreferenceDBBean;
import com.gigsky.database.bean.ReferralEventDBBean;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.PreferenceDao;
import com.gigsky.database.dao.ReferralEventDao;
import com.gigsky.messaging.beans.CurrencyChange;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.util.GSTaskUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by prsubbareddy on 30/01/17.
 */
public class CurrencyChangeEventTask implements TaskInterface {

    private static final Logger logger = LoggerFactory.getLogger(CurrencyChangeEventTask.class);
    private ReferralEventDBBean referralEventDBBean;

    @Autowired
    GSTaskUtil gsTaskUtil;

    @Autowired
    PreferenceDao preferenceDao;

    @Autowired
    ReferralEventDao referralEventDao;

    @Autowired
    CustomerDao customerDao;

    public void setReferralEvent(ReferralEventDBBean referralEvent) {
        this.referralEventDBBean = referralEvent;
    }

    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }

    @Override
    public void run() {

        try {

            //Get eventData from referralEventDBBean
            EventMessageData eventMessageData = GSTaskUtil.getMessageData(referralEventDBBean.getEventData(), referralEventDBBean.getEventType());
            CurrencyChange currencyChangeData = (CurrencyChange) eventMessageData;

            // Fetch customer details and update status
            CustomerDBBean customerDBBean = customerDao.getCustomerInfo(referralEventDBBean.getCustomerId(), referralEventDBBean.getTenantId());

            // setting new currency and updating preferences
            PreferenceDBBean preferenceDBBean = preferenceDao.getPreferenceByKey(customerDBBean.getId(), Configurations.PreferrenceKeyType.PREFERRED_CURRENCY);
            preferenceDBBean.setValue(currencyChangeData.getNewCurrency());
            preferenceDao.updatePreference(preferenceDBBean);

            //update referral event status to complete
            referralEventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
            referralEventDao.updateReferralEvent(referralEventDBBean);

        } catch (Exception e) {
            logger.error(e.getMessage(),e);
            referralEventDBBean.setStatus(Configurations.StatusType.STATUS_FAILED);
            referralEventDao.updateReferralEvent(referralEventDBBean);
        }

    }
}
