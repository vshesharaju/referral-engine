package com.gigsky.task;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.ReferralEventDBBean;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.ReferralEventDao;
import com.gigsky.messaging.beans.AccountInformationChange;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.util.GSTaskUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by prsubbareddy on 21/11/17.
 */
public class AccountInformationChangeEventTask implements TaskInterface {

    private static final Logger logger = LoggerFactory.getLogger(AccountInformationChangeEventTask.class);
    private ReferralEventDBBean referralEventDBBean;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    ReferralEventDao referralEventDao;

    public void setReferralEvent(ReferralEventDBBean referralEvent) {
        this.referralEventDBBean = referralEvent;
    }

    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }

    @Override
    public void run() {

        try {
            // Fetching account info change message data
            EventMessageData eventMessageData = GSTaskUtil.getMessageData(referralEventDBBean.getEventData(), referralEventDBBean.getEventType());
            AccountInformationChange accountInfo = (AccountInformationChange) eventMessageData;

            // Fetch gs customer id from input message data
            int gsCustomerId = accountInfo.getCustomerId().intValue();

            // Set first name, last name and country details
            CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(gsCustomerId, referralEventDBBean.getTenantId());
            customerDBBean.setFirstName(accountInfo.getFirstName());
            customerDBBean.setLastName(accountInfo.getLastName());
            customerDBBean.setCountry(accountInfo.getCountry());
            customerDBBean.setLanguage(accountInfo.getLanguageCode());

            // Update customer info with latest data
            customerDao.updateCustomerInfo(customerDBBean);

            //Update referral event status to complete
            referralEventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
            referralEventDao.updateReferralEvent(referralEventDBBean);
        }
        catch (Exception e ){
            logger.error(e.getMessage(),e);
            referralEventDBBean.setStatus(Configurations.StatusType.STATUS_FAILED);
            referralEventDao.updateReferralEvent(referralEventDBBean);
        }

    }
}
