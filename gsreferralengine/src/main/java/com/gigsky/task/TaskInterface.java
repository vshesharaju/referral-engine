package com.gigsky.task;

/**
 * Created by prsubbareddy on 30/01/17.
 */
public interface TaskInterface extends Runnable {

    public int getTaskId();

    public String getTaskDescription();

}
