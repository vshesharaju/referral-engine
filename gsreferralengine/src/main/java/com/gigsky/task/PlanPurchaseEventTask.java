package com.gigsky.task;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.InvitationDBBean;
import com.gigsky.database.bean.ReferralEventDBBean;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.InvitationDao;
import com.gigsky.database.dao.ReferralEventDao;
import com.gigsky.util.GSTaskUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by prsubbareddy on 30/01/17.
 */
public class PlanPurchaseEventTask implements TaskInterface {

    private static final Logger logger = LoggerFactory.getLogger(PlanPurchaseEventTask.class);
    private ReferralEventDBBean referralEventDBBean;

    public void setReferralEvent(ReferralEventDBBean referralEvent) {
        this.referralEventDBBean = referralEvent;
    }

    @Autowired
    GSTaskUtil gsTaskUtil;

    @Autowired
    ReferralEventDao referralEventDao;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    InvitationDao invitationDao;

    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }

    @Override
    public void run() {

        try
        {
            //If signUp event is not processed then don't take up this event
            if(!gsTaskUtil.checkIfSignUpEventIsProcessed(referralEventDBBean)){

                return;
            }

            //add credit for advocate if referral credit status is not CREDIT_DEDUCTED
            InvitationDBBean invitationDBBean = invitationDao.getInvitationByTargetCustomerId(referralEventDBBean.getCustomerId());

            String referralCreditStatus = invitationDBBean.getReferralCreditStatus();
            if(!Configurations.StatusType.CREDIT_DEDUCTED.equals(referralCreditStatus))
            {
                // Fetch customer details and update status
                CustomerDBBean customerDBBean = customerDao.getCustomerInfo(referralEventDBBean.getCustomerId(), referralEventDBBean.getTenantId());
                customerDBBean.setStatus(Configurations.CustomerStatus.STATUS_USING_SERVICE);
                //Update customerDBBean using customerDao
                customerDao.updateCustomerInfo(customerDBBean);

                // add credit for advocate
                gsTaskUtil.addCredit(referralEventDBBean);
            } else {

                //update referral status message for fraud case
                referralEventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
                referralEventDBBean.setStatusMessage(Configurations.StatusType.CREDIT_NOT_ADDED_FRAUD_DETECTED);
                referralEventDao.updateReferralEvent(referralEventDBBean);

                //update invitation advocate credit status as "CREDIT_NOT_ADDED_FRAUD_DETECTED"
                invitationDBBean.setAdvocateCreditStatus(Configurations.StatusType.CREDIT_NOT_ADDED_FRAUD_DETECTED);
                invitationDao.updateInvitation(invitationDBBean);

            }

        } catch(Exception e) {
            logger.error("plan purchase event task error:" + e.getMessage(),e);
            referralEventDBBean.setStatus(Configurations.StatusType.STATUS_FAILED);
            referralEventDao.updateReferralEvent(referralEventDBBean);
        }

    }
}
