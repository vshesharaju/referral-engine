package com.gigsky.task;

import com.gigsky.backend.customer.CustomerManagementInterface;
import com.gigsky.backend.ngw.NgwManagementInterface;
import com.gigsky.backend.ngw.beans.Option;
import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CreditSchemeDBBean;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.PreferenceDBBean;
import com.gigsky.database.bean.ReferralCodeDBBean;
import com.gigsky.database.dao.*;
import com.gigsky.service.AccountService;
import com.gigsky.util.GSTokenUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by prsubbareddy on 10/02/17.
 */
public class ReferralSendEmailTask implements TaskInterface {

    private static final Logger logger = LoggerFactory.getLogger(ReferralSendEmailTask.class);

    private CustomerDBBean customerDBBean;

    @Autowired
    NgwManagementInterface ngwManagementInterface;

    @Autowired
    ReferralCodeDao referralCodeDao;

    @Autowired
    IncentiveSchemeDao incentiveSchemeDao;

    @Autowired
    PreferenceDao preferenceDao;

    @Autowired
    CustomerManagementInterface customerManagementInterface;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    GSTokenUtil gsTokenUtil;

    @Autowired
    TenantConfigurationsKeyValueDao tenantConfigurationsKeyValueDao;

    @Autowired
    private AccountService accountService;

    public void setCustomerInfo(CustomerDBBean customerInfo) {
        this.customerDBBean = customerInfo;
    }

    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }

    @Override
    public void run() {

        try {

            List emailIds = new ArrayList();
            emailIds.add(customerDBBean.getEmailId());

            // fetching language from customer, if null set to default language code
            String language = (StringUtils.isNotEmpty(customerDBBean.getLanguage())) ? customerDBBean.getLanguage() : tenantConfigurationsKeyValueDao
                    .getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.TENANT_DEF_LANG,customerDBBean.getTenantId());

            // Getting referral code using customer id and channel as DEFAULT
            ReferralCodeDBBean referralCodeDBBean = referralCodeDao.getActiveReferralCodeForChannel(customerDBBean.getId(), Configurations.ReferralCodeChannelType.DEFAULT);

            //Add admin token to fetch customer details
            long gsCustomerId = customerDBBean.getGsCustomerId();
            CustomerDBBean updatedCustomerDBBean = accountService.getCustomerDetails(gsCustomerId, customerDBBean.getTenantId());

            // Setting options for email
            Option referralEmailOption = new Option();
            if(updatedCustomerDBBean.getFirstName() != null){
                referralEmailOption.setCustomerName(updatedCustomerDBBean.getFirstName());
            }
            referralEmailOption.setCustomerEmail(customerDBBean.getEmailId());
            referralEmailOption.setReferralCode(referralCodeDBBean.getReferralCode());
            referralEmailOption.setGsCustomerId((int) gsCustomerId);

            PreferenceDBBean preferredCurrency = preferenceDao.getPreferenceByKey(customerDBBean.getId(), Configurations.PreferrenceKeyType.PREFERRED_CURRENCY);

            Map<String, Object> incentiveScheme = incentiveSchemeDao.getActiveSchemeForReferralCode(referralCodeDBBean.getReferralCode(), preferredCurrency.getValue(), customerDBBean.getTenantId());

            List<CreditSchemeDBBean> creditSchemeDBBeans = (List<CreditSchemeDBBean>) incentiveScheme.get(Configurations.CREDIT_SCHEME_BEAN);

            // Credit scheme bean for preferred currency
            CreditSchemeDBBean creditSchemeDBBean = creditSchemeDBBeans.get(0);

            float advocateAmount = creditSchemeDBBean.getAdvocateAmount();
            float referralAmount = creditSchemeDBBean.getNewCustomerAmount();

            referralEmailOption.setAdvocateAmount((int) advocateAmount);
            referralEmailOption.setReferralAmount((int) referralAmount);
            referralEmailOption.setCurrency(preferredCurrency.getValue());

            // sending email with options
            ngwManagementInterface.sendEmail(Configurations.EmailType.REFERRAL_INVITE, emailIds, language, referralEmailOption, customerDBBean.getTenantId());

            //update customer with last referral mail sent with current time
            customerDBBean.setLastReferralMailSent(CommonUtils.getCurrentTimestamp());
            customerDao.updateCustomerInfo(customerDBBean);

        } catch (Exception e) {
            logger.error("Referral initial sending email exception: " + e,e);
        }

    }

}
