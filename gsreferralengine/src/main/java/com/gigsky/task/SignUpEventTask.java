package com.gigsky.task;

import com.gigsky.backend.customer.CustomerManagementInterface;
import com.gigsky.backend.ngw.NgwManagementInterface;
import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.common.GigskyDuration;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.ReferralCodeDBBean;
import com.gigsky.database.bean.ReferralEventDBBean;
import com.gigsky.database.dao.*;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.messaging.beans.UserSignup;
import com.gigsky.referralCode.ReferralCodeManagement;
import com.gigsky.rest.bean.Customer;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSTaskUtil;
import com.gigsky.util.GSTokenUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.text.ParseException;

/**
 * Created by prsubbareddy on 30/01/17.
 */
public class SignUpEventTask implements TaskInterface {

    private static final Logger logger = LoggerFactory.getLogger(SignUpEventTask.class);

    private ReferralEventDBBean referralEventDBBean;

    @Autowired
    CustomerManagementInterface customerManagementInterface;

    @Autowired
    ReferralCodeManagement referralCodeManagement;

    @Autowired
    CustomerDao customerDao;

    @Autowired
    ReferralCodeDao referralCodeDao;

    @Autowired
    GSTaskUtil gsTaskUtil;

    @Autowired
    GSTokenUtil gsTokenUtil;

    @Autowired
    ReferralEventDao referralEventDao;

    @Autowired
    ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    NgwManagementInterface ngwManagementInterface;

    @Autowired
    PreferenceDao preferenceDao;

    @Autowired
    IncentiveSchemeDao incentiveSchemeDao;

    @Autowired
    PromotionDao promotionDao;

    public void setReferralEvent(ReferralEventDBBean referralEvent) {
        this.referralEventDBBean = referralEvent;
    }

    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }

    @Override
    public void run() {

        try
        {
            // It will process event using referral code
            processEventData();
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(),e);
        }

    }

    private void processEventData() throws ParseException
    {
        try {

            int tenantId = referralEventDBBean.getTenantId();

            // Fetch customer details and update status
            CustomerDBBean customerDBBean = customerDao.getCustomerInfo(referralEventDBBean.getCustomerId(), tenantId);
            long gsNewCustomerId = customerDBBean.getGsCustomerId();

            //Add admin token to fetch customer details
            Customer customer = customerManagementInterface.getCustomerDetails((int) gsNewCustomerId, gsTokenUtil.getValidToken(tenantId), tenantId);

            //Update customer country using customer info
            customerDBBean.setCountry(customer.getCountry());
            customerDBBean.setAccountCreationTime(customer.getCreatedOn());
            customerDBBean.setFirstName(customer.getFirstName());
            customerDBBean.setLastName(customer.getLastName());
            customerDBBean.setStatus(Configurations.CustomerStatus.STATUS_SIGNED_UP);
            customerDao.updateCustomerInfo(customerDBBean);

            //Add Preferred Currency to Customer Preferences
            gsTaskUtil.createPreference(customerDBBean.getId(), Configurations.PreferrenceKeyType.PREFERRED_CURRENCY, customer.getPreferredCurrency());

            //Add default optout for customer as "false", if entry is not there
            gsTaskUtil.createPreference(customerDBBean.getId(), Configurations.PreferrenceKeyType.OPT_OUT, "false");

            // Get message data for UserSignup
            EventMessageData eventMessageData = GSTaskUtil.getMessageData(referralEventDBBean.getEventData(), referralEventDBBean.getEventType());
            UserSignup signUpEventData = (UserSignup) eventMessageData;

            // Account created time
            Timestamp createdAccountTime = customer.getCreatedOn();

            String referralCode = signUpEventData.getReferralCode();
            // Referral code validation for existing or new customers
            if(referralCode == null || referralCode.length() == 0) {

                // Generate referral code and update referral status SIGNUP_WITHOUT_REFERRAL
                updateStatusWIthOutReferral(customerDBBean, Configurations.ReferralStatus.SIGNUP_WITHOUT_REFERRAL);
            } else {

                // change referral code to lower case
                referralCode = referralCode.toUpperCase();

                // check if input code is valid and promotional code
                //add promotion enable check
                if(gsTaskUtil.checkPromotionEnable(customerDBBean.getTenantId()) && gsTaskUtil.isValidPromoCode(referralCode, customerDBBean.getTenantId()))
                {
                    // Generate referral code and update referral status CREDIT_ADD_IN_PROCESS
                    updateStatusWIthOutReferral(customerDBBean, Configurations.ReferralStatus.CREDIT_ADD_IN_PROCESS);
                }
                // check if referral code is valid and referral code
                // add referral enable check
                 else if(gsTaskUtil.checkReferralEnable(customerDBBean.getTenantId()) && gsTaskUtil.isValidReferralCode(referralCode, customerDBBean.getTenantId())) {


                    // validate created account should be with in 7 days
                    if(CommonUtils.getCurrentTimestampInUTC().getTime() - createdAccountTime.getTime() <
                            Integer.parseInt(configurationKeyValueDao.getConfigurationValue(Configurations.CREATED_ON_MAX_DURATION)))
                    {
                        // Getting advocate referral code bean using referral code
                        ReferralCodeDBBean advocateReferralCodeDBBean = referralCodeDao.getReferralCodeInfoByReferralCode(referralCode, customerDBBean.getTenantId());

                        // Getting advocate id using advocate referral code bean
                        customerDBBean.setAdvocateId(advocateReferralCodeDBBean.getCustomerId());
                        customerDBBean.setReferralStatus(Configurations.ReferralStatus.CREDIT_ADD_IN_PROCESS);
                        customerDao.updateCustomerInfo(customerDBBean);

                        // creating referral code for new customer
                        gsTaskUtil.createReferralCodeForCustomer(customerDBBean);

                        int referredCount = 0;
                        byte isPromotionalCode = 0;

                        referredCount = ((advocateReferralCodeDBBean.getReferredCount() == null) ? referredCount : advocateReferralCodeDBBean.getReferredCount());
                        isPromotionalCode = ((advocateReferralCodeDBBean.getIsPromotionalCode() == null) ? isPromotionalCode : advocateReferralCodeDBBean.getIsPromotionalCode());

                        // Handling promotional referral code flow
                        if(isPromotionalCode == 1)
                        {
                    /*
                        Check promotional max count from configs with DB referred count
                        if not reaches max count, update referral bean and add credit
                        else update count and return
                     */
                            int maxPromotedCount = Integer.valueOf(configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.MAX_PROMOTED_COUNT));
                            if(referredCount < maxPromotedCount)
                            {
                                advocateReferralCodeDBBean.setReferredCount(++referredCount);
                                referralCodeDao.updateReferralCode(advocateReferralCodeDBBean);
                            } else {

                                //Update referralEvent
                                referralEventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
                                referralEventDao.updateReferralEvent(referralEventDBBean);

                                //Update customer referral status
                                customerDBBean.setReferralStatus(Configurations.StatusType.CREDIT_ADD_FAILED);
                                customerDao.updateCustomerInfo(customerDBBean);

                                //Increment referred count
                                advocateReferralCodeDBBean.setReferredCount(++referredCount);
                                referralCodeDao.updateReferralCode(advocateReferralCodeDBBean);
                                return;
                            }
                        }
                        else
                        {
                            // Handling general referral code flow

                    /*
                        Check referred max count from configs with DB referred count
                        if count == 0 update referral bean with count 1 and first referred time and add credit
                        if count != 0 and less than max referred count update referral bean with count and add credit
                        if count reaches max referred count,
                            if first referred time to current time is less than reset referred time just update counter and return
                            if first referred time to current time  reaches reset referred time update count with 1 and first referred time with current time and add credit
                     */

                            int maxReferredCount = Integer.valueOf(configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.MAX_REFERRED_COUNT));
                            if(referredCount == 0)
                            {
                                advocateReferralCodeDBBean.setReferredCount(1);
                                advocateReferralCodeDBBean.setFirstReferredTime(CommonUtils.getCurrentTimestampInUTC());
                                referralCodeDao.updateReferralCode(advocateReferralCodeDBBean);
                            } else {
                                if(advocateReferralCodeDBBean.getFirstReferredTime() != null)
                                {
                                    // Getting time from config key values and converting to millis
                                    long resetReferredTimeInMS;
                                    String resetReferredTime = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.RESET_TIME_REFERRED_COUNT);
                                    if(StringUtils.isEmpty(resetReferredTime))
                                    {
                                        resetReferredTimeInMS = Configurations.DEFAULT_RESET_TIME_REFERRED_COUNT;
                                    }else{
                                        resetReferredTimeInMS = new GigskyDuration(resetReferredTime).convertTimeInMilliSeconds();
                                    }

                                    // difference time between current time to first referred time
                                    long diffTime = CommonUtils.getCurrentTimestampInUTC().getTime() - advocateReferralCodeDBBean.getFirstReferredTime().getTime();

                                    if(diffTime < resetReferredTimeInMS)
                                    {
                                        advocateReferralCodeDBBean.setReferredCount(referredCount+1);
                                        referralCodeDao.updateReferralCode(advocateReferralCodeDBBean);
                                        // If referred count reaches max referred count, we are not adding referral credit
                                        if(referredCount >= maxReferredCount) {
                                            // update referral status SIGNUP_WITH_INVALID_REFERRAL
                                            updateStatusWIthOutReferral(customerDBBean, Configurations.ReferralStatus.SIGNUP_WITH_INVALID_REFERRAL);
                                            return;
                                        }
                                    } else
                                    {
                                        advocateReferralCodeDBBean.setReferredCount(1);
                                        advocateReferralCodeDBBean.setFirstReferredTime(CommonUtils.getCurrentTimestampInUTC());
                                        referralCodeDao.updateReferralCode(advocateReferralCodeDBBean);
                                    }
                                } else {
                                    // update referral status SIGNUP_WITH_INVALID_REFERRAL
                                    updateStatusWIthOutReferral(customerDBBean, Configurations.ReferralStatus.SIGNUP_WITH_INVALID_REFERRAL);
                                    return;
                                }
                            }
                        }

                        // create invitation for new customer using advocate referral code
                        gsTaskUtil.createInvitationEntry(customerDBBean, advocateReferralCodeDBBean, createdAccountTime);

                        // add referral credit for new customer
                        gsTaskUtil.addCredit(referralEventDBBean);

                    } else
                    {
                        // Generate referral code and update referral status SIGNUP_WITHOUT_REFERRAL
                        updateStatusWIthOutReferral(customerDBBean, Configurations.ReferralStatus.SIGNUP_WITHOUT_REFERRAL);
                    }
                } else {

                    // Generate referral code and update referral status SIGNUP_WITH_INVALID_REFERRAL
                    updateStatusWIthOutReferral(customerDBBean, Configurations.ReferralStatus.SIGNUP_WITH_INVALID_REFERRAL);
                }
            }
        }
        catch(Exception e) {
            logger.error("Sign up Event Exception "+ e.toString(),e);

            referralEventDBBean.setStatus(Configurations.StatusType.STATUS_FAILED);
            referralEventDao.updateReferralEvent(referralEventDBBean);
        }
    }

    private void updateStatusWIthOutReferral(CustomerDBBean customerDBBean, String statusMessage) throws ServiceException {
        customerDBBean.setReferralStatus(statusMessage);
        customerDao.updateCustomerInfo(customerDBBean);

        // creating referral code for new customer
        gsTaskUtil.createReferralCodeForCustomer(customerDBBean);

        // update referral event with status as COMPLETE
        referralEventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
        referralEventDao.updateReferralEvent(referralEventDBBean);
    }

}
