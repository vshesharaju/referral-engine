package com.gigsky.task;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.PromoEventDBBean;
import com.gigsky.database.dao.PromoEventDao;
import com.gigsky.messaging.EventMessage;
import com.gigsky.messaging.EventMessageComposer;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.messaging.beans.SimActivation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by pradeepragav on 01/12/17.
 * Roles of PromoEventsTask
 * Add credit based on the promotion type and promotion details
 */
public class PromoEventsTask extends PromotionAddCreditTask implements TaskInterface
{
    private static final Logger logger = LoggerFactory.getLogger(PromoEventsTask.class);

    private PromoEventDBBean promoEventDBBean;

    public void setPromoEventDBBean(PromoEventDBBean promoEventDBBean) {
        this.promoEventDBBean = promoEventDBBean;
    }

    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }

    @Override
    public void run()
    {
        try
        {
            //Validate promotion and add credit
            addCreditForPromotion(promoEventDBBean);
        }
        catch (Exception e)
        {
            logger.error("PromoEventsTask failed with stackTrace  - " + e.getStackTrace(),e);
            e.printStackTrace();
        }
    }
}
