package com.gigsky.task;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigsky.backend.credit.CreditManagementInterface;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.backend.ngw.NgwManagementInterface;
import com.gigsky.backend.ngw.beans.Option;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import com.gigsky.messaging.EventMessage;
import com.gigsky.messaging.EventMessageComposer;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.messaging.beans.SimActivation;
import com.gigsky.messaging.beans.UserSignup;
import com.gigsky.rest.bean.Credit;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.AccountService;
import com.gigsky.service.exception.ServiceException;
import com.gigsky.util.GSSchemeUtil;
import com.gigsky.util.GSTaskUtil;
import com.gigsky.util.GSTokenUtil;
import com.gigsky.util.GSUtil;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pradeepragav on 05/12/17.
 * Adds credit to the customer
 */
public class PromotionAddCreditTask
{

    private static final Logger logger = LoggerFactory.getLogger(PromotionAddCreditTask.class);

    @Autowired
    private PromoIccidDao promoIccidDao;

    @Autowired
    private PromotionDao promotionDao;

    @Autowired
    private PreferenceDao preferenceDao;

    @Autowired
    private PromoCreditDao promoCreditDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private CreditManagementInterface creditManagementInterface;

    @Autowired
    private PromoHistoryDao promoHistoryDao;

    @Autowired
    private PromoEventDao promoEventDao;

    @Autowired
    private NgwManagementInterface ngwManagementInterface;

    @Autowired
    private GSTaskUtil gsTaskUtil;

    @Autowired
    private AccountService accountService;

    @Autowired
    private GSTokenUtil gsTokenUtil;

    @Autowired
    private GSUtil gsUtil;

    public void addCreditForPromotion(PromoEventDBBean promoEventDBBean) throws BackendRequestException
    {
        Session session = null;
        Transaction transaction = null;

        try {
            //Get event data
            EventMessage eventMessage = new ObjectMapper().readValue(promoEventDBBean.getEventMetaData(), EventMessage.class);

            //Get messageData using EventMessageComposer
            EventMessageData messageData = EventMessageComposer.getMessageData(eventMessage);

            //Valid promotion
            int customerId = promoEventDBBean.getCustomerId();

            //Get currency of customer
            PreferenceDBBean currencyPreference = preferenceDao
                    .getPreferenceByKey(customerId, Configurations.PreferrenceKeyType.PREFERRED_CURRENCY);

            String currency = null;
            if(currencyPreference != null)
            {
                currency = currencyPreference.getValue();
            }


            String eventType = null;
            PromotionDBBean promotion = null;
            String promoIccId = null;
            byte isEmailNotificationRequired = 0;

            int tenantId = gsUtil.extractTenantIdFromMessageData(messageData);

            if(messageData instanceof UserSignup)
            {
                eventType = Configurations.PromoEventType.SIGNUP_EVENT;

                UserSignup userSignup= (UserSignup) messageData;

                String promoCode = userSignup.getReferralCode();

                //Check if currency is set to customer
                if(StringUtils.isEmpty(currency))
                {
                    //Get token
                    String token = gsTokenUtil.getValidToken(tenantId);

                    accountService.getAccountDetails(userSignup.getCustomerId().intValue(), token, tenantId);

                    currencyPreference = preferenceDao
                            .getPreferenceByKey(customerId, Configurations.PreferrenceKeyType.PREFERRED_CURRENCY);

                    currency = currencyPreference.getValue();
                }

                CustomerDBBean customerDBBean = customerDao.getCustomerInfo(customerId, tenantId);

                String countryOfRes = customerDBBean.getCountry();
                List<PromotionDBBean> promotionDBBeans = promotionDao.getEnabledPromotionForCountryWithPromoCode(promoCode ,countryOfRes, tenantId);

                if(CollectionUtils.isEmpty(promotionDBBeans)){
                    promotion = promotionDao.getDefaultPromotionByPromoCode(promoCode, tenantId);
                }else{

                    for(PromotionDBBean promoDBBean: promotionDBBeans){
                        if(StringUtils.equalsIgnoreCase(promoDBBean.getPromoCode(), promoCode)){
                            promotion = promoDBBean;
                        }
                    }
                }

                if(promotion == null){
                    promoEventDBBean.setStatus(Configurations.PromoEventsTaskStatus.NOT_PROCESSED);
                    promoEventDao.updatePromoEvent(promoEventDBBean);
                    return;
                }
            }
            else if(messageData instanceof SimActivation)
            {

                eventType = Configurations.PromoEventType.SIM_ACTIVATION_EVENT;

                //Get SIMActivation object
                SimActivation simActivation = (SimActivation) messageData;
                promoIccId = simActivation.getIccID();

                logger.info("Valid promotion for iccid - " + promoIccId);

                if(gsTaskUtil.isValidIccIdPromoCode(promoIccId, tenantId)){

                    //Get credit for the currency which is defined for the promotion
                    PromoIccidDBBean promoIccidDBBean = promoIccidDao.getPromoIccid(promoIccId);
                    isEmailNotificationRequired = promoIccidDBBean.getIsEmailNotifRequired();
                    promotion = promotionDao.getPromotion(promoIccidDBBean.getPromotionId(), tenantId);
                } else {
                    //Invalid promotion
                    PromoIccidDBBean promoIccidDBBean = promoIccidDao.getPromoIccid(simActivation.getIccID());

                    if(promoIccidDBBean != null) {
                        promotion = promotionDao.getPromotion(promoIccidDBBean.getPromotionId(), tenantId);
                        if(promotion != null)
                        {
                            promoEventDBBean.setStatus(Configurations.PromoEventsTaskStatus.EXPIRED_PROMOTION);
                        } else{
                            promoEventDBBean.setStatus(Configurations.PromoEventsTaskStatus.NOT_PROCESSED);
                        }
                    } else {
                        promoEventDBBean.setStatus(Configurations.PromoEventsTaskStatus.NOT_PROCESSED);
                    }
                    promoEventDao.updatePromoEvent(promoEventDBBean);
                    return;
                }
            }

            //Create session and transaction
            session = promotionDao.getSession();
            transaction = session.beginTransaction();

            logger.info("Transaction started for promotion entry with promotionId: " + promotion.getId());

            promotion = promotionDao.getPromotion(session, promotion.getId(), tenantId);

            //Check currentRedeemCount
            if(promotion.getCurrentRedeemCount() >= promotion.getMaxRedeemCount())
            {
                transaction = null;
                promotionDao.closeSession(session);

                //Have different status for currentRedeemCount
                promoEventDBBean.setStatus(Configurations.PromoEventsTaskStatus.REACHED_MAX_REDEEM_COUNT);
                promoEventDao.updatePromoEvent(promoEventDBBean);
                return;
            }

            PromoCreditDBBean promoCreditDBBean = promoCreditDao.getPromoCredit(session, promotion.getId(), currency);

            //Create credit bean
            Credit credit = GSSchemeUtil.prepareCreditDetails(currency, promoCreditDBBean, promotion.getCreditExpiryPeriod());

            // Fetch customer details
            CustomerDBBean customerDBBean = customerDao.getCustomerInfo(session, customerId, promoEventDBBean.getTenantId());

            //Add credit API call
            Credit creditResponse = creditManagementInterface.addCredit(customerDBBean.getGsCustomerId().intValue(), credit, customerDBBean.getTenantId());

            PromoHistoryDBBean promoHistoryDBBean =
                    GSSchemeUtil.getPromoHistoryBean(
                            customerDBBean.getGsCustomerId().intValue(), currency,
                            promotion.getId(), promoEventDBBean.getId(),
                            promoIccId, promoCreditDBBean.getCreditAmount(),
                            0, Configurations.EmailStatus.NOT_SENT,
                            0, Configurations.PromoEventsTaskStatus.COMPLETE
                    );

            if (creditResponse != null &&
                    (Configurations.CreditAddResponseStatus.SUCCESS.equals(creditResponse.getType())))
            {

                logger.info("addCreditForPromotion, add credit SUCCESS for GS CustomerId " + customerDBBean.getGsCustomerId());

                //Set credit transaction
                promoHistoryDBBean.setCreditTxnId(creditResponse.getCreditTxnId());
                //Set credit ID
                promoHistoryDBBean.setCreditId(creditResponse.getCreditId());
                //Set status
                promoHistoryDBBean.setStatus(Configurations.PromoEventsTaskStatus.CREDIT_ADD_SUCCESS);

                //Get opt out preference
                PreferenceDBBean optOutPreference = preferenceDao.getPreferenceByKey(session, customerId, Configurations.PreferrenceKeyType.OPT_OUT);

                boolean optValue = (optOutPreference.getValue().equals("true"));

                if(Configurations.PromoEventType.SIM_ACTIVATION_EVENT.equals(eventType) && isEmailNotificationRequired == 1 && !optValue)
                {
                    sendEmail(customerDBBean, promoCreditDBBean, promoIccId);

                    //Set email status
                    promoHistoryDBBean.setEmailNotifStatus(Configurations.EmailStatus.SENT);
                } else if(Configurations.PromoEventType.SIGNUP_EVENT.equals(eventType)){
                    // Customer referral status updating with SIGNUP_WITH_PROMOTION
                    customerDBBean.setReferralStatus(Configurations.ReferralStatus.SIGNUP_WITH_PROMOTION);
                    customerDao.updateCustomerInfo(session,customerDBBean);

                    //Update currentRedeemCount
                    promotion.setCurrentRedeemCount(promotion.getCurrentRedeemCount() + 1);
                    promotionDao.updatePromotion(session,promotion);
                }
            }
            else
            {
                //Credit add failed
                logger.info("addCreditForPromotion, add credit FAILED for customerId " + customerId );
                promoHistoryDBBean.setStatus(Configurations.PromoEventsTaskStatus.CREDIT_ADD_FAILED);

                // Customer referral status updating with Credit add failed
                customerDBBean.setReferralStatus(Configurations.StatusType.CREDIT_ADD_FAILED);
                customerDao.updateCustomerInfo(session,customerDBBean);
            }

            promoHistoryDao.addPromotionHistory(session,promoHistoryDBBean);

            promoEventDBBean.setStatus(Configurations.PromoEventsTaskStatus.COMPLETE);
            promoEventDao.updatePromoEvent(session,promoEventDBBean);

            transaction.commit();
            transaction = null;
            promotionDao.closeSession(session);

            logger.info("Transaction ended for promotion entry with promotionId: " + promotion.getId());
        }
        catch ( Exception e)
        {
            if(transaction!=null)
            {
                transaction.rollback();
            }
            if(session != null)
            {
                promotionDao.closeSession(session);
            }

            //Failed due to exception
            promoEventDBBean.setStatus(Configurations.PromoEventsTaskStatus.FAILED);
            promoEventDao.updatePromoEvent(promoEventDBBean);

            logger.error("PromotionAddCreditTask failed with stackTrace  - " + e.getStackTrace(),e);
            e.printStackTrace();
        }
    }

    protected void sendEmail(CustomerDBBean customerDBBean, PromoCreditDBBean promoCreditDBBean, String iccid) throws BackendRequestException
    {
        //Currency
        Option option = new Option();
        option.setCurrency(promoCreditDBBean.getCurrency());

        //Language
        String language = (customerDBBean.getLanguage() != null) ? customerDBBean.getLanguage() : "en";

        //Name
        option.setCustomerName(customerDBBean.getFirstName());

        //Email id
        option.setCustomerEmail(customerDBBean.getEmailId());

        //Promo amount
        option.setPromoAmount(promoCreditDBBean.getCreditAmount().intValue());

        //Iccid
        option.setCsn(iccid);

        //GSCustomerId
        option.setGsCustomerId(customerDBBean.getGsCustomerId().intValue());

        List emailIds = new ArrayList();
        emailIds.add(customerDBBean.getEmailId());

        logger.info("Sending ICCID_PROMOTION_CREDIT email with options");

        ngwManagementInterface.sendEmail(Configurations.EmailType.ICCID_PROMOTION_CREDIT, emailIds, language, option, customerDBBean.getTenantId());
    }




}
