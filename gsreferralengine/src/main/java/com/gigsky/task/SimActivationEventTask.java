package com.gigsky.task;

import com.gigsky.backend.credit.CreditManagementInterface;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.backend.ngw.NgwManagementInterface;
import com.gigsky.backend.ngw.beans.Option;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import com.gigsky.messaging.EventMessageComposer;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.messaging.beans.SimActivation;
import com.gigsky.messaging.beans.UserSignup;
import com.gigsky.util.GSTaskUtil;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prsubbareddy on 30/01/17.
 *
 * - Change the status of customer to Sim activated
 * - Check if it is Apple SIM and update the customer / CSN table.
 * - Also check for fraud case before updating the table.
 * - If it is a fraud case then deduct gigsky credit from the customer
 * - Update the CSN table with the latest customer mapping
 */

public class SimActivationEventTask implements TaskInterface {

    private static final Logger logger = LoggerFactory.getLogger(SimActivationEventTask.class);


    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    @Autowired
    private CustomerCSNMappingDao customerCSNMappingDao;

    @Autowired
    private InvitationDao invitationDao;

    @Autowired
    private CreditManagementInterface creditManagementInterface;

    @Autowired
    private ReferralEventDao referralEventDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    NgwManagementInterface ngwManagementInterface;

    @Autowired
    TenantConfigurationsKeyValueDao tenantConfigurationsKeyValueDao;

    @Autowired
    PromoHistoryDao promoHistoryDao;

    @Autowired
    PromotionDao promotionDao;

    @Autowired
    PromoEventDao promoEventDao;

    private ReferralEventDBBean referralEventDBBean;


    public void setReferralEvent(ReferralEventDBBean referralEventDBBean) {
        this.referralEventDBBean = referralEventDBBean;
    }


    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }

    @Override
    public void run() {

        try {
            processSimActivationEvent();
        } catch (BackendRequestException e) {
            logger.error("Exception : ", e);
            e.printStackTrace();
        }
    }

    private void processSimActivationEvent() throws BackendRequestException {

        try {

            //Get eventData from referralEventDBBean
            EventMessageData eventMessageData = GSTaskUtil.getMessageData(referralEventDBBean.getEventData(), referralEventDBBean.getEventType());
            SimActivation simActivation = (SimActivation) eventMessageData;

            //Please check "checkIfSimActivationEventNeedsToBeProcessed" method declaration for this check
            if(!checkIfSimActivationEventNeedsToBeProcessed(simActivation))
            {
                logger.info("Processing simActivation for gsCustomerId : " + simActivation.getCustomerId()
                        + ", storing simActivation event with status new as the signUpEvent"
                        + " is still in processing for Promotion credit add");

                //Wait for PromotionEventProcessor to complete
                updateReferralEvent(Configurations.StatusType.STATUS_NEW,null);
                return;
            }

            logger.info("Processing simActivation for gsCustomerId : " + simActivation.getCustomerId());

            // Fetch customer details and update status
            CustomerDBBean customerDBBean = customerDao.getCustomerInfo(referralEventDBBean.getCustomerId(), referralEventDBBean.getTenantId());
            customerDBBean.setStatus(Configurations.CustomerStatus.STATUS_SIM_ACTIVATED);
            customerDao.updateCustomerInfo(customerDBBean);

            // fetching gs customer id from customer
            long gsCustomerId = customerDBBean.getGsCustomerId();

            updatePromoHistoryWithIccid(simActivation.getCustomerId(), simActivation.getIccID());

                //Check If SIM type is "ACME_SIM" || "ACME_GSMA_SIM" || MSFT_GSMA_ESIM || ACME_GSMA_ESIM_V2
                if(Configurations.SIMType.ACME_SIM.equals(simActivation.getSimType())
                        || Configurations.SIMType.ACME_GSMA_SIM.equals(simActivation.getSimType())
                        || Configurations.SIMType.MSFT_GSMA_ESIM.equals(simActivation.getSimType())
                        || Configurations.SIMType.ACME_GSMA_ESIM_V2.equals(simActivation.getSimType())
                        || Configurations.SIMType.ACME_GSMA_ESIM_V2_KDDI.equals(simActivation.getSimType())
                        || Configurations.SIMType.ANDROID_GSMA_ESIM.equals(simActivation.getSimType()))
                {

                    //Get customerCsnMapping by using Iccid(Csn) and tenant id
                    CustomerCsnMappingDBBean customerCsnMappingDBBean = customerCSNMappingDao.getCustomerCsnMappingByCsn(simActivation.getIccID(), referralEventDBBean.getTenantId());

                    if(customerCsnMappingDBBean == null){

                        logger.info("Processing simActivation for gsCustomerId : " + simActivation.getCustomerId() + ", CSNMapping entry not found");

                        //Not a fraud
                        //Create customerCSNMappingDBBean
                        customerCsnMappingDBBean = new CustomerCsnMappingDBBean();
                         //Set parameters to customerCsnMappingDBBean
                        customerCsnMappingDBBean.setCsn(simActivation.getIccID());
                        customerCsnMappingDBBean.setGsCustomerId((int)gsCustomerId);
                        //add tenantId
                        customerCsnMappingDBBean.setTenantId(customerDBBean.getTenantId());
                         //Add an entry into customerCsnMapping table
                        customerCSNMappingDao.addCustomerCSNMapping(customerCsnMappingDBBean);

                        // Commenting code to avoid making a cal for addCredit in SIM Activation flow
                        //gsTaskUtil.addCredit(referralEventDBBean);

                    }
                    else {

                        //Compare the gsCustomerId
                        if(simActivation.getCustomerId().longValue() != customerCsnMappingDBBean.getGsCustomerId()){

                            logger.info("CSN Mapping entry found for gsCustomerId : "+ gsCustomerId);

                            //Fraud detected, as there is another gsCustomerId mapped to the same CSN
                            //Deduct credit
                            deductReferralCredit(customerDBBean, simActivation.getIccID());

                            //TODO: If the customer referal status is in_process, we fail to catch
                            //Send email only to those customers who are part of referral status

                            //Check if referralStatus of the customer is SIGNUP_WITH_REFERRAL
                            if(StringUtils.isNotEmpty(customerDBBean.getReferralStatus()) &&
                                    Configurations.ReferralStatus.SIGNUP_WITH_REFERRAL.equals(customerDBBean.getReferralStatus()))
                            {

                                //Get adminEmailId using configurationKeyValue table
                                String adminEmailId =tenantConfigurationsKeyValueDao
                                        .getTenantConfigurationValue(Configurations.TenantConfigurationKeyValueType.GS_ADMIN_EMAIL_ID, customerDBBean.getTenantId());

                                Option emailOption = new Option();
                                emailOption.setCustomerEmail(customerDBBean.getEmailId());
                                emailOption.setCsn(customerCsnMappingDBBean.getCsn());

                                List emailIds = new ArrayList();
                                emailIds.add(adminEmailId);

                                //sending email to admin to deduct credit for fraud SIM case
                                ngwManagementInterface.sendEmail(Configurations.EmailType.INTERNAL_CREDIT_DEDUCT, emailIds, "en", emailOption, customerDBBean.getTenantId());
                            }

                            //Update csn mapping with new gscustomerId
                            customerCsnMappingDBBean.setGsCustomerId((int)gsCustomerId);
                            customerCSNMappingDao.updateCustomerCSNMapping(customerCsnMappingDBBean);
                        }
                    }
                }

                //update referral event status "COMPLETE"
                updateReferralEvent(Configurations.StatusType.STATUS_COMPLETE,null);
        }
        catch (Exception e) {
            logger.error("Exception : ",e);
            updateReferralEvent(Configurations.StatusType.STATUS_FAILED, null);
            e.printStackTrace();
        }
    }

    //Deduct promotion using promotion history table
    private void deductReferralCredit(CustomerDBBean customerDBBean, String iccId) throws Exception {

        int tenantId = customerDBBean.getTenantId();
        logger.info("Checking credit transactions for: "+ customerDBBean.getGsCustomerId()
                + ", iccId : "+iccId+", tenanId :"+tenantId);

        //Get invitationDBBean using the referralCode
        InvitationDBBean invitationDBBean = invitationDao.getInvitationByTargetCustomerId(customerDBBean.getId());

        if(invitationDBBean != null){

            //Check for referralCreditId
            Integer referralCreditId = invitationDBBean.getReferralCreditId();

            if(referralCreditId != null){

                long gsCustomerId = customerDBBean.getGsCustomerId();

                //Deduct referralCredit
                creditManagementInterface.deductCredit((int)gsCustomerId, referralCreditId, customerDBBean.getTenantId());

                logger.info("Deduct referral credit success for gsCustomerId : " + gsCustomerId);

                updateReferralEvent(Configurations.StatusType.STATUS_COMPLETE,Configurations.StatusType.CREDIT_DEDUCTED);

                // update invitation referral credit status as "CREDIT_DEDUCTED"
                invitationDBBean.setReferralCreditStatus(Configurations.StatusType.CREDIT_DEDUCTED);
                invitationDao.updateInvitation(invitationDBBean);
            }
        }
        else
        {
            logger.info("Checking promotionHistory table with iccId : "+ iccId);

            //Get SignUp event for customer
            ReferralEventDBBean signUpReferralEvent = referralEventDao.getSignUpReferralEvent(customerDBBean.getId(), tenantId);

            if(signUpReferralEvent != null)
            {
                String eventData = signUpReferralEvent.getEventData();
                EventMessageData signUpMessageData =
                        GSTaskUtil.getMessageData(eventData, Configurations.ReferralEventType.SIGNUP_EVENT);
                UserSignup userSignup = (UserSignup) signUpMessageData;

                //Check if promoCode is part of signUpEvent
                if(StringUtils.isEmpty(userSignup.getReferralCode()))
                {
                    return;
                }

                Session session = promotionDao.getSession();
                Transaction transaction = session.beginTransaction();
                logger.info("Transaction started for promotion entry with promoCode: " + userSignup.getReferralCode());

                try
                {
                    //Get promotion code used during sign up
                    PromotionDBBean promotion = promotionDao.getPromotion(session, userSignup.getReferralCode(), tenantId);

                    if(promotion != null)
                    {
                        //Get promotion history entry for this iccid
                        List<PromoHistoryDBBean> promotionHistoryList = promoHistoryDao.getPromotionHistoryList(session, customerDBBean.getGsCustomerId(), promotion.getId(),iccId);

                        if(promotionHistoryList != null && promotionHistoryList.size() != 0)
                        {
                            PromoHistoryDBBean promoHistoryDBBean = promotionHistoryList.get(0);
                            Integer creditTxnId = promoHistoryDBBean.getCreditTxnId();

                            //Check if credit
                            if(creditTxnId != null)
                            {
                                //Deduct referralCredit
                                creditManagementInterface.deductCredit(customerDBBean.getGsCustomerId().intValue(), promoHistoryDBBean.getCreditId(), customerDBBean.getTenantId());

                                logger.info("Deduct promotion credit success for gsCustomerId : " + customerDBBean.getGsCustomerId());

                                //Reduce current redeem count by 1
                                promotion.setCurrentRedeemCount(promotion.getCurrentRedeemCount() - 1);
                                promotionDao.updatePromotion(session, promotion);

                                //Update promo history dbbean
                                promoHistoryDBBean.setStatus(Configurations.StatusType.CREDIT_DEDUCTED);
                                promoHistoryDao.updatePromotionHistory(session, promoHistoryDBBean);

                                referralEventDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
                                referralEventDBBean.setStatusMessage(Configurations.StatusType.CREDIT_DEDUCTED);
                                referralEventDao.updateReferralEvent(session,referralEventDBBean);
                            }
                            transaction.commit();
                            transaction = null;
                            logger.info("Transaction ended for promotion entry with promoCode: " + userSignup.getReferralCode());
                        }
                        else
                        {
                            //This case is handle in "checkIfSimActivationEventNeedsToBeProcessed" method below
                            logger.error("PromotionHistory entry not found for promotionId : "
                                    +promotion.getId() + "with iccId :"+userSignup.getReferralCode());
                        }
                    }
                }
                catch (Exception exception)
                {
                    logger.error("Exception occured While deducting promotion credit for fraud with gsCustomerId :"
                            + customerDBBean.getGsCustomerId(),exception);
                    if(transaction != null)
                    {
                        transaction.rollback();
                    }
                }
                finally {
                    promotionDao.closeSession(session);
                }
            }
        }
    }

    private void updatePromoHistoryWithIccid(long gsCustomerId, String iccId)
    {
        logger.info("Updating promoHistory entry for gsCustomerId : "+ gsCustomerId + ", iccId : " + iccId);
        List<PromoHistoryDBBean> promotionHistoryList = promoHistoryDao.getPromotionHistoryList(gsCustomerId);
        if(promotionHistoryList != null && promotionHistoryList.size() != 0)
        {
            logger.info("PromoHistory entry found for: "+ gsCustomerId + ", iccId : " + iccId);
            PromoHistoryDBBean promoHistoryDBBean = promotionHistoryList.get(0);
            if(promoHistoryDBBean.getIccid() == null)
            {
                promoHistoryDBBean.setIccid(iccId);
                promoHistoryDao.updatePromotionHistory(promoHistoryDBBean);
            }
        }
        else
        {
            logger.info("PromoHistory entry Not found for: "+ gsCustomerId + ", iccId : " + iccId);
        }
    }


    private void updateReferralEvent(String status, String statusMessage){

        referralEventDBBean.setStatus(status);
        if(statusMessage != null)
            referralEventDBBean.setStatusMessage(statusMessage);

        referralEventDao.updateReferralEvent(referralEventDBBean);
    }

    /*In cases where the time interval between UserSignup and SimActivation events are less,
    if UserSignup is being processed
    then deduction of credit won't be possible
    if promotionHistory entry is not there*/
    private boolean checkIfSimActivationEventNeedsToBeProcessed(SimActivation simActivation)
            throws Exception
    {

        String iccId = simActivation.getIccID();
        int tenantId = simActivation.getTenantsId();

        //Get customerId
        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(simActivation.getCustomerId(), tenantId);
        if(customerDBBean != null)
        {
            int customerId = customerDBBean.getId();

            //Get SignUp event for customer
            ReferralEventDBBean signUpReferralEvent =
                    referralEventDao.getSignUpReferralEvent(customerId, tenantId);

            if(signUpReferralEvent!=null)
            {
                EventMessageData signUpMessageData = GSTaskUtil.getMessageData(signUpReferralEvent.getEventData(), signUpReferralEvent.getEventType());

                UserSignup userSignup = (UserSignup) signUpMessageData;

                if(StringUtils.isEmpty(userSignup.getReferralCode()))
                {
                    logger.info("checkIfSimActivationEventNeedsToBeProcessed return true");
                    return true;
                }

                //Get promotion code used during sign up
                PromotionDBBean promotion = promotionDao.getPromotion(userSignup.getReferralCode(), tenantId);

                if(promotion != null) {

                    //If promotion is valid
                    if (promotion.getActivePromo() == 1
                            && promotion.getCurrentRedeemCount() < promotion.getMaxRedeemCount())
                    {

                        //Check sign up event in promoevent table and if it is IN_PROCESS || NEW
                        List<PromoEventDBBean> promoEvent = promoEventDao.getPromoEvent(customerDBBean.getId(), customerDBBean.getTenantId(), Configurations.ReferralEventType.SIGNUP_EVENT);
                        if(promoEvent != null && promoEvent.size() != 0)
                        {
                            PromoEventDBBean promoEventDBBean = promoEvent.get(0);
                            if(Configurations.StatusType.STATUS_NEW.equals(promoEventDBBean.getStatus())
                                    || Configurations.StatusType.STATUS_IN_PROCESS.equals(promoEventDBBean.getStatus()))
                            {
                                logger.info("Signup event in promo event table for customerId : "
                                        + customerDBBean.getId() + "is NEW || IN_PROCESS");
                                return false;
                            }
                        }
                        else
                        {
                            logger.info("No signup event in promo event table for customerId : " + customerDBBean.getId());
                        }
                    }
                    else
                    {
                        logger.info("checkIfSimActivationEventNeedsToBeProcessed :" +
                                " Promo code invalid in signup event for gsCustomerId: "
                                +simActivation.getCustomerId() + "with promoCode : "+ userSignup.getReferralCode());
                    }
                }
            }
            else
            {
                logger.info("checkIfSimActivationEventNeedsToBeProcessed :SignUp event not found for gsCustomerId: "
                        +simActivation.getCustomerId());
            }
        }
        else
        {
            logger.info("checkIfSimActivationEventNeedsToBeProcessed :Customer Entry not yet created for gsCustomerId: "
                    +simActivation.getCustomerId());
        }
        logger.info("checkIfSimActivationEventNeedsToBeProcessed return true");
        return true;
    }


}
