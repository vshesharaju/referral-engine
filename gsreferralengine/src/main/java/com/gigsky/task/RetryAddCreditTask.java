package com.gigsky.task;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.InvitationDBBean;
import com.gigsky.database.dao.ConfigurationKeyValueDao;
import com.gigsky.database.dao.InvitationDao;
import com.gigsky.util.GSTaskUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by prsubbareddy on 08/02/17.
 */
public class RetryAddCreditTask implements TaskInterface {

    private static final Logger logger = LoggerFactory.getLogger(RetryAddCreditTask.class);

    private InvitationDBBean invitationDBBean;
    private boolean isAdvocate = false;

    @Autowired
    private InvitationDao invitationDao;

    @Autowired
    private GSTaskUtil gsTaskUtil;

    @Autowired
    private ConfigurationKeyValueDao configurationKeyValueDao;

    public void setInvitationBean(InvitationDBBean invitationDBBean, boolean isAdvocate) {
        this.invitationDBBean = invitationDBBean;
        this.isAdvocate = isAdvocate;
    }

    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }

    @Override
    public void run() {

        try {

            Integer retryCount = isAdvocate ? invitationDBBean.getAdvocateRetryCount(): invitationDBBean.getReferralRetryCount();
            // Getting max retry count from config key values
            int maxRetryCount = Configurations.DEFAULT_MAX_CREDIT_RETRY_COUNT;
            String maxRetry = configurationKeyValueDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.MAX_CREDIT_RETRY_COUNT);
            if(StringUtils.isNotEmpty(maxRetry)) {
                maxRetryCount = Integer.valueOf(maxRetry);
            }

            // if retry count greater or equal to DEFAULT_MAX_CREDIT_RETRY_COUNT
            // set status to CREDIT_ADD_FAILED_AFTER_RETRIES
            if(retryCount != null && retryCount >= maxRetryCount) {

                if(isAdvocate) {
                    invitationDBBean.setAdvocateCreditStatus(Configurations.StatusType.CREDIT_ADD_FAILED_AFTER_RETRIES);
                } else {
                    invitationDBBean.setReferralCreditStatus(Configurations.StatusType.CREDIT_ADD_FAILED_AFTER_RETRIES);
                }
                invitationDao.updateInvitation(invitationDBBean);

            } else {
                // calling retry credit for customer by passing invitation bean and retry count
                if(retryCount == null) {
                    retryCount = 0;
                }

                gsTaskUtil.retryCredit(invitationDBBean, isAdvocate, retryCount);
            }

        } catch(Exception e) {
            logger.error("retry add credit failed "+ e.getMessage(),e);
        }
    }
}
