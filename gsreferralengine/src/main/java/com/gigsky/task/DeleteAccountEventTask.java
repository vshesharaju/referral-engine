package com.gigsky.task;

import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.backend.ngw.NgwManagementInterface;
import com.gigsky.backend.ngw.beans.CustomerDetail;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import com.gigsky.messaging.beans.DeleteAccountInfo;
import com.gigsky.messaging.beans.EventMessageData;
import com.gigsky.util.GSTaskUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * Created by pradeepragav on 17/05/18.
 */
public class DeleteAccountEventTask implements TaskInterface {

    private static final Logger logger = LoggerFactory.getLogger(DeleteAccountEventTask.class);

    private ReferralEventDBBean referralEvent;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private CustomerCSNMappingDao customerCSNMappingDao;

    @Autowired
    private ReferralEventDao referralEventDao;

    @Autowired
    private InvitationDao invitationDao;

    @Autowired
    private ReferralCodeDao referralCodeDao;

    @Autowired
    private NgwManagementInterface ngwManagementInterface;

    @Autowired
    private PreferenceDao preferenceDao;

    public void setReferralEvent(ReferralEventDBBean referralEventDBBean) {
        this.referralEvent = referralEventDBBean;
    }

    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }

    @Override
    public void run()
    {
        try {
            //Get eventData from referralEventDBBean
            EventMessageData eventMessageData = GSTaskUtil.getMessageData(referralEvent.getEventData(), referralEvent.getEventType());
            DeleteAccountInfo deleteAccount = (DeleteAccountInfo) eventMessageData;

            //Make changes to DB

            /*Make changes to Customer table*/
            //In customer table, set firstName, lastName, emailId
            // with the event's anonymised emailId and country to NULL
            CustomerDBBean customerDBBean = customerDao.getCustomerInfoByGsCustomerId(eventMessageData.getCustomerId(), referralEvent.getTenantId());
            customerDBBean.setDeleted(true);
            customerDBBean.setCountry(null);
            customerDBBean.setFirstName(null);
            customerDBBean.setLastName(null);
            customerDBBean.setEmailId(deleteAccount.getNewEmailId());
            customerDao.updateCustomerInfoForDeletion(customerDBBean);

            /*Delete customer preferences*/
            preferenceDao.deletePreferences(customerDBBean.getId());

            /*Delete customerCSNMapping entries for customer*/
            customerCSNMappingDao.deleteCustomerCSNMappingByGsCustomerId(customerDBBean.getGsCustomerId().intValue());

            /*Delete ReferralEvent entries - AccountInformationChange, EmailIDChange, UserSignup*/
            //Get accountInformation change events
            List<ReferralEventDBBean> accountInformationChangeEvents
                    = referralEventDao.getReferralEventsForCustomerByEventType(customerDBBean.getId(),
                    Configurations.ReferralEventType.ACCOUNT_INFORMATION_CHANGE_EVENT, referralEvent.getTenantId());

            //Get emailId change events
            List<ReferralEventDBBean> emailIdChangeEvents = referralEventDao.getReferralEventsForCustomerByEventType(customerDBBean.getId(),
                    Configurations.ReferralEventType.EMAIL_ID_CHANGE_EVENT, referralEvent.getTenantId());

            //Get Signup events
            List<ReferralEventDBBean> signUpEvents = referralEventDao.getReferralEventsForCustomerByEventType(customerDBBean.getId(), Configurations.ReferralEventType.SIGNUP_EVENT, referralEvent.getTenantId());
            List<ReferralEventDBBean> referralEventDBBeans = accountInformationChangeEvents;
            referralEventDBBeans.addAll(emailIdChangeEvents);
            referralEventDBBeans.addAll(signUpEvents);

            //Delete the fetched events
            for(ReferralEventDBBean referralEventDBBean : referralEventDBBeans)
            {
                referralEventDao.deleteReferralEvent(referralEventDBBean);
            }


            /*Delete invitation entry*/
            List<ReferralCodeDBBean> referralCodesByCustomerId = referralCodeDao.getReferralCodesByCustomerId(customerDBBean.getId(), referralEvent.getTenantId());

            for(ReferralCodeDBBean referralCodeDBBean : referralCodesByCustomerId) {
                List<InvitationDBBean> invitationsForReferralCode = invitationDao.getInvitationsForReferralCode(referralCodesByCustomerId.get(0).getId());
                for (InvitationDBBean invitationDBBean : invitationsForReferralCode) {
                    invitationDao.deleteInvitation(invitationDBBean);
                }
                /*Delete referralCodeHistory entry*/
                List<ReferralCodeHistoryDBBean> referralCodeHistoryDBBeans = referralCodeDao.getReferralCodeHistory(referralCodeDBBean.getId());
                for (ReferralCodeHistoryDBBean referralCodeHistoryDBBean : referralCodeHistoryDBBeans) {
                    referralCodeDao.deleteReferralCodeHistory(referralCodeHistoryDBBean);
                }

                /*Disable referralCode*/
                referralCodeDBBean.setIsActive((byte)0);
                referralCodeDao.updateReferralCode(referralCodeDBBean);
            }

            /*Delete invitation's emailId with targetCustomerId*/
            InvitationDBBean invitationByTargetCustomerId = invitationDao.getInvitationByTargetCustomerId(customerDBBean.getId());
            if(invitationByTargetCustomerId != null)
            {
                invitationByTargetCustomerId.setEmailId(deleteAccount.getNewEmailId());
                invitationDao.updateInvitation(invitationByTargetCustomerId);
            }

            //Inform notification gateway
            CustomerDetail customerDetail =
                    new CustomerDetail(deleteAccount.getOldEmailId(), deleteAccount.getNewEmailId());

            String statusMessage = "";
            try
            {
                ngwManagementInterface.deleteCustomer(customerDetail, referralEvent.getTenantId());
            }
            catch (BackendRequestException e)
            {
                //Gateway delete customer failed
                statusMessage = Configurations.StatusType.FAILED_API;
            }
            finally {

                if(StringUtils.isEmpty(statusMessage))
                {
                    //Set event data to null, as it contains emailId information
                    referralEvent.setEventData(null);
                    referralEvent.setEventMetaData(null);
                }

                //Set status as complete
                referralEvent.setStatus(Configurations.StatusType.STATUS_COMPLETE);
                //Set status message
                referralEvent.setStatusMessage(statusMessage);
                //Update referralEvent using referralEventDao
                referralEventDao.updateReferralEvent(referralEvent);
            }
        } catch (Exception e)
        {
            e.printStackTrace();

            //Set status as failed
            referralEvent.setStatus(Configurations.StatusType.STATUS_FAILED);
            //Update referralEvent using referralEventDao
            referralEventDao.updateReferralEvent(referralEvent);
        }
    }
}
