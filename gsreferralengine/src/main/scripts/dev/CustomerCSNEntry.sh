#!/bin/bash

# pass input paramter for file path
input="$1";
password="$2";

# removing first line if we have field keys
#tail -n +2 "$input" > "$input.tmp" && mv "$input.tmp" "$input"

# Set "," as the field separator using $IFS
# and read line by line using while read combo
while IFS=',' read f1 f2 f3 f4 f5
do

# printing final values
echo "customer id is $f1 and csn is $f5";

# todo cal insert query for customer CSN entry
echo "INSERT INTO CustomerCSNMapping (csn,gsCustomerId) VALUES ('$f5',$f1);" | mysql -uroot gigskyReferralEngine

# To read last line of csv file
done < <(grep "" $input)
