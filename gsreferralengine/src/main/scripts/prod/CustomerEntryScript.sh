#!/bin/bash

# pass input paramter for file path
input="$1";

# token which will use for getting account details
# in case of live we need to provide admin credentials

export PYTHONIOENCODING=utf8

output=$(curl -X POST -H "Content-Type: application/json" \
--data '{ "loginType":"NORMAL", "emailId":"referral.admin@gigsky.com", "password":"liveG1gsky12-s4" }' \
https://services.gigsky.com/api/v4/account/login)

# fetching token from login curl response using python
token=$(echo "$output" | python -c "import sys, json; print json.load(sys.stdin)['token']");
echo "token is: $token";

# it will help for removing first line if we have field keys
#tail -n +2 "$input" > "$input.tmp" && mv "$input.tmp" "$input"

# Set "," as the field separator using $IFS
# and read line by line using while read combo
while IFS=',' read f1 f2 f3 f4 f5
do

# removing double quotes for string
customerId=${f1//\"/};

# printing final values
echo "customer id is $customerId";

# make a cal for getAccountDetails
curl \
--header "Authorization: Basic $token" \
--request GET \
https://services.gigsky.com/gsre/api/v1/account/$customerId

# To read last line of csv file
done < <(grep "" $input)

