-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

ALTER TABLE `gigskyReferralEngine`.`Customer` 
ADD UNIQUE INDEX `unique_customer_per_tenant` (`tenant_id` ASC, `gsCustomerId` ASC);
;

ALTER TABLE `gigskyReferralEngine`.`ReferralCode` 
ADD UNIQUE INDEX `unique_per_customer_id` (`customer_id` ASC);
;

ALTER TABLE `gigskyReferralEngine`.`Preference` 
ADD UNIQUE INDEX `unique_per_customer_id_per_pkey` (`customer_id` ASC, `pkey` ASC);
;

ALTER TABLE `gigskyReferralEngine`.`ReferralCodeHistory` 
ADD UNIQUE INDEX `unique_per_referralCode_id_per_incentiveScheme_id` (`referralCode_id` ASC, `incentiveScheme_id` ASC);
;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
