-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `gigskyReferralEngine`.`PromoHistory` 
DROP FOREIGN KEY `fk_PromoHistory_PromoEvent1`;

CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`Tenant` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(250) NOT NULL,
  `description` LONGTEXT NOT NULL,
  `createTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

ALTER TABLE `gigskyReferralEngine`.`Customer` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `gsCustomerId`,
ADD INDEX `tenant_id_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyReferralEngine`.`IncentiveScheme` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `schemeEnabled`,
ADD INDEX `fk_ IncentiveScheme_Tenant1_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyReferralEngine`.`ReferralCode` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `incentiveScheme_id`,
ADD INDEX `kk_ReferralCode_Tenant1_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyReferralEngine`.`ReferralEvent` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `customer_id`,
ADD INDEX `fk_ReferralEvent_Tenant1_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyReferralEngine`.`CustomerCSNMapping` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `updateTime`,
ADD INDEX `fk_ CustomerCSNMapping_Tenant1_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyReferralEngine`.`MockCustomer` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `country`,
ADD INDEX `fk_ MockCustomer_Tenant1_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyReferralEngine`.`SupportedCurrency` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `usdMultiplier`,
ADD INDEX `fk_SupportedCurrency_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyReferralEngine`.`Promotion` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `promoCode`,
ADD COLUMN `promoEnabled` TINYINT(1) NULL DEFAULT NULL AFTER `updateTime`,
ADD COLUMN `activePromo` TINYINT(1) NULL DEFAULT NULL AFTER `promoEnabled`,
ADD INDEX `fk_Promotion_Tenant1_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyReferralEngine`.`PromoEvent` 
ADD COLUMN `tenant_id` INT(11) NOT NULL AFTER `customer_id`,
ADD INDEX `fk_PromoEvent_Tenant1_idx` (`tenant_id` ASC);

ALTER TABLE `gigskyReferralEngine`.`PromoHistory` 
CHANGE COLUMN `promoEvent_id` `promoEvent_id` INT(11) NULL DEFAULT NULL ;

CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`TenantConfigurationsKeyValue` (
  `ckey` VARCHAR(250) NOT NULL,
  `tenant_id` INT(11) NOT NULL,
  `value` TEXT NOT NULL,
  `createTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL DEFAULT NULL,
  INDEX `tenant_id_idx` (`tenant_id` ASC),
  PRIMARY KEY (`ckey`, `tenant_id`),
  CONSTRAINT `fk_ TenantConfigurationsKeyValue_Tenant1`
    FOREIGN KEY (`tenant_id`)
    REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

ALTER TABLE `gigskyReferralEngine`.`Customer` 
ADD CONSTRAINT `fk_Customer_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyReferralEngine`.`IncentiveScheme` 
ADD CONSTRAINT `fk_ IncentiveScheme_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyReferralEngine`.`ReferralCode` 
ADD CONSTRAINT `fk_ReferralCode_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyReferralEngine`.`ReferralEvent` 
ADD CONSTRAINT `fk_ReferralEvent_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyReferralEngine`.`CustomerCSNMapping` 
ADD CONSTRAINT `fk_ CustomerCSNMapping_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyReferralEngine`.`MockCustomer` 
ADD CONSTRAINT `fk_ MockCustomer_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyReferralEngine`.`SupportedCurrency` 
ADD CONSTRAINT `fk_SupportedCurrency`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyReferralEngine`.`Promotion` 
ADD CONSTRAINT `fk_Promotion_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyReferralEngine`.`PromoEvent` 
ADD CONSTRAINT `fk_PromoEvent_Tenant1`
  FOREIGN KEY (`tenant_id`)
  REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

ALTER TABLE `gigskyReferralEngine`.`PromoHistory` 
ADD CONSTRAINT `fk_PromoHistory_PromoEvent1`
  FOREIGN KEY (`promoEvent_id`)
  REFERENCES `gigskyReferralEngine`.`PromoEvent` (`id`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;


DELIMITER $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Tenant_BEFORE_UPDATE` BEFORE UPDATE ON `Tenant` FOR EACH ROW
BEGIN
set new.updateTime = now();
END$$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`TenantConfigurationsKeyValue_BEFORE_UPDATE` BEFORE UPDATE ON `TenantConfigurationsKeyValue` FOR EACH ROW
BEGIN
set new.updateTime = now();
END$$


DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
