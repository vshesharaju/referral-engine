-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema gigskyReferralEngine
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `gigskyReferralEngine` ;

-- -----------------------------------------------------
-- Schema gigskyReferralEngine
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `gigskyReferralEngine` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
SHOW WARNINGS;
USE `gigskyReferralEngine` ;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`Customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`Customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`Customer` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `advocateId` INT NULL COMMENT '',
  `language` VARCHAR(45) NULL COMMENT '',
  `emailId` VARCHAR(45) NULL COMMENT '',
  `gsCustomerId` BIGINT(20) NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  `status` VARCHAR(45) NULL COMMENT '',
  `country` VARCHAR(45) NULL COMMENT '',
  `referralStatus` VARCHAR(100) NULL COMMENT '',
  `lastReferralMailSent` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`IncentiveScheme`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`IncentiveScheme` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`IncentiveScheme` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `validityStartDate` DATE NULL COMMENT '',
  `validityEndDate` DATE NULL COMMENT '',
  `advocateCreditExpiryPeriod` INT NULL COMMENT '',
  `referralCreditExpiryPeriod` INT NULL COMMENT '',
  `schemeEnabled` TINYINT(1) NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  `activeScheme` TINYINT(1) NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ReferralCode`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ReferralCode` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ReferralCode` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `channel` VARCHAR(45) NULL COMMENT '',
  `referralCode` LONGTEXT NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  `customer_id` INT NOT NULL COMMENT '',
  `incentiveScheme_id` INT NOT NULL COMMENT '',
  `isActive` TINYINT(1) NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_ReferralCode_Customer1_idx` (`customer_id` ASC)  COMMENT '',
  INDEX `fk_ReferralCode_IncentiveScheme1_idx` (`incentiveScheme_id` ASC)  COMMENT '',
  CONSTRAINT `fk_ReferralCode_Customer1`
  FOREIGN KEY (`customer_id`)
  REFERENCES `gigskyReferralEngine`.`Customer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ReferralCode_IncentiveScheme1`
  FOREIGN KEY (`incentiveScheme_id`)
  REFERENCES `gigskyReferralEngine`.`IncentiveScheme` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`Invitation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`Invitation` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`Invitation` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `emailId` VARCHAR(45) NULL COMMENT '',
  `inviteTime` TIMESTAMP NULL COMMENT '',
  `accountCreatedOn` TIMESTAMP NULL COMMENT '',
  `status` VARCHAR(45) NULL COMMENT '',
  `targetCustomerId` INT NULL COMMENT '',
  `advocateCreditId` INT NULL COMMENT '',
  `advocateCreditTxnId` INT NULL COMMENT '',
  `advocateCreditStatus` VARCHAR(100) NULL COMMENT '',
  `advocateRetryCount` INT NULL COMMENT '',
  `referralCreditId` INT NULL COMMENT '',
  `referralCreditTxnId` INT NULL COMMENT '',
  `referralCreditStatus` VARCHAR(100) NULL COMMENT '',
  `referralRetryCount` INT NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  `referralCode_id` INT NOT NULL COMMENT '',
  `resendCount` INT NULL DEFAULT 0 COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_Invitation_ReferralCode1_idx` (`referralCode_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Invitation_ReferralCode1`
  FOREIGN KEY (`referralCode_id`)
  REFERENCES `gigskyReferralEngine`.`ReferralCode` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`CreditScheme`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`CreditScheme` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`CreditScheme` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `currency` VARCHAR(45) NULL COMMENT '',
  `advocateAmount` FLOAT NULL COMMENT '',
  `newCustomerAmount` FLOAT NULL COMMENT '',
  `incentiveScheme_id` INT NOT NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_CreditScheme_IncentiveScheme1_idx` (`incentiveScheme_id` ASC)  COMMENT '',
  CONSTRAINT `fk_CreditScheme_IncentiveScheme1`
  FOREIGN KEY (`incentiveScheme_id`)
  REFERENCES `gigskyReferralEngine`.`IncentiveScheme` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ReferralEvent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ReferralEvent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ReferralEvent` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `eventType` VARCHAR(45) NULL COMMENT '',
  `eventData` LONGTEXT NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  `customer_id` INT NOT NULL COMMENT '',
  `status` VARCHAR(45) NULL COMMENT '',
  `statusMessage` VARCHAR(100) NULL COMMENT '',
  `retryCount` INT NULL DEFAULT 0 COMMENT '',
  `eventMetaData` LONGTEXT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_ReferralEvent_Customer1_idx` (`customer_id` ASC)  COMMENT '',
  CONSTRAINT `fk_ReferralEvent_Customer1`
  FOREIGN KEY (`customer_id`)
  REFERENCES `gigskyReferralEngine`.`Customer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ConfigurationsKeyValue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ConfigurationsKeyValue` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ConfigurationsKeyValue` (
  `ckey` VARCHAR(255) NOT NULL COMMENT '',
  `value` TEXT NOT NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`ckey`)  COMMENT '')
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`HttpError`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`HttpError` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`HttpError` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `errorCode` INT NULL COMMENT '',
  `errorString` VARCHAR(500) NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ErrorString`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ErrorString` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ErrorString` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `errorId` INT NULL COMMENT '',
  `errorString` TEXT NULL COMMENT '',
  `httpError_id` INT NOT NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_ErrorString_HttpError1_idx` (`httpError_id` ASC)  COMMENT '',
  CONSTRAINT `fk_ErrorString_HttpError1`
  FOREIGN KEY (`httpError_id`)
  REFERENCES `gigskyReferralEngine`.`HttpError` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`UrlDetail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`UrlDetail` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`UrlDetail` (
  `id` INT NOT NULL COMMENT '',
  `apiBaseUrl` VARCHAR(45) NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`VersionInformation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`VersionInformation` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`VersionInformation` (
  `major` INT NOT NULL COMMENT '',
  `minor` INT NULL COMMENT '',
  `build` INT NULL COMMENT '',
  `buildType` ENUM('dev','test','live','stage') NOT NULL COMMENT '',
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ReferralAnalytics`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ReferralAnalytics` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ReferralAnalytics` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `userAgent` VARCHAR(255) NULL COMMENT '',
  `platform` VARCHAR(45) NULL COMMENT '',
  `inviteOpenTime` TIMESTAMP NULL COMMENT '',
  `language` VARCHAR(45) NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ValidCountry`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ValidCountry` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ValidCountry` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `country` VARCHAR(45) NULL COMMENT '',
  `IncentiveScheme_id` INT NOT NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_ValidCountry_IncentiveScheme1_idx` (`IncentiveScheme_id` ASC)  COMMENT '',
  CONSTRAINT `fk_ValidCountry_IncentiveScheme1`
  FOREIGN KEY (`IncentiveScheme_id`)
  REFERENCES `gigskyReferralEngine`.`IncentiveScheme` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`Preference`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`Preference` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`Preference` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `customer_id` INT NOT NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  `pkey` VARCHAR(255) NULL COMMENT '',
  `value` TEXT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_Preferences_Customer1_idx` (`customer_id` ASC)  COMMENT '',
  CONSTRAINT `fk_Preferences_Customer1`
  FOREIGN KEY (`customer_id`)
  REFERENCES `gigskyReferralEngine`.`Customer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`CustomerCSNMapping`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`CustomerCSNMapping` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`CustomerCSNMapping` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `csn` VARCHAR(100) NULL COMMENT '',
  `gsCustomerId` BIGINT(20) NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`CountryToCurrencyMap`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`CountryToCurrencyMap` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`CountryToCurrencyMap` (
  `countryCode` VARCHAR(45) NOT NULL COMMENT '',
  `currencyCode` VARCHAR(45) NULL COMMENT '',
  `createTime` TIMESTAMP NULL COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`countryCode`)  COMMENT '')
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`MockCustomer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`MockCustomer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`MockCustomer` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `firstName` VARCHAR(45) NULL COMMENT '',
  `lastName` VARCHAR(45) NULL COMMENT '',
  `token` VARCHAR(45) NULL COMMENT '',
  `email` VARCHAR(45) NULL COMMENT '',
  `country` VARCHAR(45) NULL COMMENT '',
  `preferredCurrency` VARCHAR(45) NULL COMMENT '',
  `permission` VARCHAR(45) NULL COMMENT '',
  `status` VARCHAR(45) NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  `createdOn` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`MockCredit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`MockCredit` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`MockCredit` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `creditType` VARCHAR(45) NULL COMMENT '',
  `mockcustomer_id` INT NULL COMMENT '',
  `balance` FLOAT NULL COMMENT '',
  `currency` VARCHAR(45) NULL COMMENT '',
  `expiryTime` TIMESTAMP NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `_idx` (`mockcustomer_id` ASC)  COMMENT '',
  CONSTRAINT `fk_MockCustomer`
  FOREIGN KEY (`mockcustomer_id`)
  REFERENCES `gigskyReferralEngine`.`MockCustomer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ReferralCodeHistory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ReferralCodeHistory` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ReferralCodeHistory` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `referralCode_id` INT NOT NULL COMMENT '',
  `incentiveScheme_id` INT NULL COMMENT '',
  `startDate` TIMESTAMP NULL COMMENT '',
  `endDate` TIMESTAMP NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_ReferralCodeHistory_ReferralCode1_idx` (`referralCode_id` ASC)  COMMENT '',
  CONSTRAINT `fk_ReferralCodeHistory_ReferralCode1`
  FOREIGN KEY (`referralCode_id`)
  REFERENCES `gigskyReferralEngine`.`ReferralCode` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`Country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`Country` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`Country` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `countryCode` VARCHAR(45) NULL COMMENT '',
  `countryName` LONGTEXT NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ErrorStringLocale`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ErrorStringLocale` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ErrorStringLocale` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `language` VARCHAR(45) NULL COMMENT '',
  `errorString_id` INT NOT NULL COMMENT '',
  `errorString` TEXT NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`, `errorString_id`)  COMMENT '',
  INDEX `fk_ErrorStringLocale_ErrorString1_idx` (`errorString_id` ASC)  COMMENT '',
  CONSTRAINT `fk_ErrorStringLocale_ErrorString1`
  FOREIGN KEY (`errorString_id`)
  REFERENCES `gigskyReferralEngine`.`ErrorString` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`SupportedCurrency`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`SupportedCurrency` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`SupportedCurrency` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `currencyCode` VARCHAR(45) NULL COMMENT '',
  `usdMultiplier` FLOAT NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`MessageServerCluster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`MessageServerCluster` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`MessageServerCluster` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `clusterName` VARCHAR(255) NULL COMMENT '',
  `userName` VARCHAR(200) NULL COMMENT '',
  `password` VARCHAR(250) NULL COMMENT '',
  `virtualHost` VARCHAR(200) NULL COMMENT '',
  `encVersion` VARCHAR(10) NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`MessageServerClusterNode`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`MessageServerClusterNode` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`MessageServerClusterNode` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `host` VARCHAR(255) NULL COMMENT '',
  `port` INT NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  `MessageServerCluster_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_MessageServerClusterNode_MessageServerCluster1_idx` (`MessageServerCluster_id` ASC)  COMMENT '',
  CONSTRAINT `fk_MessageServerClusterNode_MessageServerCluster1`
  FOREIGN KEY (`MessageServerCluster_id`)
  REFERENCES `gigskyReferralEngine`.`MessageServerCluster` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`MessageServerQueue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`MessageServerQueue` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`MessageServerQueue` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `queueName` VARCHAR(250) NULL COMMENT '',
  `exchangeName` VARCHAR(250) NULL COMMENT '',
  `routingKey` VARCHAR(255) NULL COMMENT '',
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP COMMENT '',
  `updateTime` TIMESTAMP NULL COMMENT '',
  `MessageServerCluster_id` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '',
  INDEX `fk_MessageServerQueue_MessageServerCluster1_idx` (`MessageServerCluster_id` ASC)  COMMENT '',
  CONSTRAINT `fk_MessageServerQueue_MessageServerCluster1`
  FOREIGN KEY (`MessageServerCluster_id`)
  REFERENCES `gigskyReferralEngine`.`MessageServerCluster` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
USE `gigskyReferralEngine`;

DELIMITER $$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`Customer_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Customer_BEFORE_UPDATE` BEFORE UPDATE ON `Customer` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`IncentiveScheme_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`IncentiveScheme_BEFORE_UPDATE` BEFORE UPDATE ON `IncentiveScheme` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ReferralCode_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ReferralCode_BEFORE_UPDATE` BEFORE UPDATE ON `ReferralCode` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`Invitation_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Invitation_BEFORE_UPDATE` BEFORE UPDATE ON `Invitation` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`CreditScheme_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`CreditScheme_BEFORE_UPDATE` BEFORE UPDATE ON `CreditScheme` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ReferralEvent_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ReferralEvent_BEFORE_UPDATE` BEFORE UPDATE ON `ReferralEvent` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ConfigurationsKeyValue_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ConfigurationsKeyValue_BEFORE_UPDATE` BEFORE UPDATE ON `ConfigurationsKeyValue` FOR EACH ROW
  begin
    set new.updateTime = now();
  end
$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`HttpError_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`HttpError_BEFORE_UPDATE` BEFORE UPDATE ON `HttpError` FOR EACH ROW
  begin
    set new.updateTime = now();
  end
$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ErrorString_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ErrorString_BEFORE_UPDATE` BEFORE UPDATE ON `ErrorString` FOR EACH ROW
  begin
    set new.updateTime = now();
  end
$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`UrlDetail_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`UrlDetail_BEFORE_UPDATE` BEFORE UPDATE ON `UrlDetail` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`VersionInformation_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`VersionInformation_BEFORE_UPDATE` BEFORE UPDATE ON `VersionInformation` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ReferralAnalytics_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ReferralAnalytics_BEFORE_UPDATE` BEFORE UPDATE ON `ReferralAnalytics` FOR EACH ROW
  begin
    set new.updateTime = now();
  end   $$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ValidCountry_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ValidCountry_BEFORE_UPDATE` BEFORE UPDATE ON `ValidCountry` FOR EACH ROW
  begin
    set new.updateTime = now();
  end $$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`Preference_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Preference_BEFORE_UPDATE` BEFORE UPDATE ON `Preference` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`CustomerCSNMapping_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`CustomerCSNMapping_BEFORE_UPDATE` BEFORE UPDATE ON `CustomerCSNMapping` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`CountryToCurrencyMap_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`CountryToCurrencyMap_BEFORE_UPDATE` BEFORE UPDATE ON `CountryToCurrencyMap` FOR EACH ROW
  begin
    set new.updateTime = now();
  END
$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`MockCustomer_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`MockCustomer_BEFORE_UPDATE` BEFORE UPDATE ON `MockCustomer` FOR EACH ROW
  BEGIN
    set new.updateTime = now();
  END
$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`MockCredit_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`MockCredit_BEFORE_UPDATE` BEFORE UPDATE ON `MockCredit` FOR EACH ROW
  BEGIN
    set new.updateTime = now();
  END
$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ReferralCodeHistory_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ReferralCodeHistory_BEFORE_UPDATE` BEFORE UPDATE ON `ReferralCodeHistory` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`Country_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Country_BEFORE_UPDATE` BEFORE UPDATE ON `Country` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ErrorStringLocale_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ErrorStringLocale_BEFORE_UPDATE` BEFORE UPDATE ON `ErrorStringLocale` FOR EACH ROW
  begin
    set new.updateTime = now();
  end
$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`SupportedCurrency_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`SupportedCurrency_BEFORE_UPDATE` BEFORE UPDATE ON `SupportedCurrency` FOR EACH ROW
  begin
    set new.updateTime = now();
  end
$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`MessageServerCluster_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`MessageServerCluster_BEFORE_UPDATE` BEFORE UPDATE ON `MessageServerCluster` FOR EACH ROW
  begin
    set new.updateTime = now();
  end	$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`MessageServerClusterNode_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`MessageServerClusterNode_BEFORE_UPDATE` BEFORE UPDATE ON `MessageServerClusterNode` FOR EACH ROW
  begin
    set new.updateTime = now();
  end
$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`MessageServerQueue_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`MessageServerQueue_BEFORE_UPDATE` BEFORE UPDATE ON `MessageServerQueue` FOR EACH ROW
  begin
    set new.updateTime = now();
  end
$$

SHOW WARNINGS$$

DELIMITER ;
