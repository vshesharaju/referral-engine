SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`Promotion` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL DEFAULT NULL,
  `promoCode` VARCHAR(45) NULL DEFAULT NULL,
  `startDate` DATE NULL DEFAULT NULL,
  `endDate` DATE NULL DEFAULT NULL,
  `creditExpiryPeriod` INT(11) NULL DEFAULT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`PromoIccid` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `iccidStart` VARCHAR(100) NULL DEFAULT NULL,
  `iccidEnd` VARCHAR(100) NULL DEFAULT NULL,
  `promotion_id` INT(11) NOT NULL,
  `isEmailNotifRequired` TINYINT(1) NULL DEFAULT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ICCID_Promotion2_idx` (`promotion_id` ASC),
  CONSTRAINT `fk_ICCID_Promotion2`
    FOREIGN KEY (`promotion_id`)
    REFERENCES `gigskyReferralEngine`.`Promotion` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`PromoCountry` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `promotion_id` INT(11) NOT NULL,
  `country` VARCHAR(45) NULL DEFAULT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_PromoCountry_Promotion1_idx` (`promotion_id` ASC),
  CONSTRAINT `fk_PromoCountry_Promotion1`
    FOREIGN KEY (`promotion_id`)
    REFERENCES `gigskyReferralEngine`.`Promotion` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`PromoCredit` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `promotion_id` INT(11) NOT NULL,
  `creditAmount` FLOAT(11) NULL DEFAULT NULL,
  `currency` VARCHAR(45) NULL DEFAULT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_PromoCredit_Promotion1_idx` (`promotion_id` ASC),
  CONSTRAINT `fk_PromoCredit_Promotion1`
    FOREIGN KEY (`promotion_id`)
    REFERENCES `gigskyReferralEngine`.`Promotion` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`PromoEvent` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `eventType` VARCHAR(45) NULL DEFAULT NULL,
  `eventData` LONGTEXT NULL DEFAULT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL DEFAULT NULL,
  `customer_id` INT(11) NOT NULL,
  `status` VARCHAR(45) NULL DEFAULT NULL,
  `statusMessage` VARCHAR(100) NULL DEFAULT NULL,
  `retryCount` INT(11) NULL DEFAULT 0,
  `eventMetaData` LONGTEXT NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ReferralEvent_Customer1_idx` (`customer_id` ASC),
  CONSTRAINT `fk_ReferralEvent_Customer10`
    FOREIGN KEY (`customer_id`)
    REFERENCES `gigskyReferralEngine`.`Customer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`PromoHistory` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `gsCustomerId` BIGINT(20) NULL DEFAULT NULL,
  `promotion_id` INT(11) NOT NULL,
  `promoEvent_id` INT(11) NOT NULL,
  `iccid` VARCHAR(100) NULL DEFAULT NULL,
  `creditAmount` FLOAT(11) NULL DEFAULT NULL,
  `currency` VARCHAR(45) NULL DEFAULT NULL,
  `creditTxnId` INT(11) NULL DEFAULT NULL,
  `emailNotifStatus` VARCHAR(45) NULL DEFAULT NULL,
  `retryCount` INT(11) NULL DEFAULT NULL,
  `status` VARCHAR(45) NULL DEFAULT NULL,
  `statusMessage` VARCHAR(100) NULL DEFAULT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_PromoHistory_Promotion1_idx` (`promotion_id` ASC),
  INDEX `fk_PromoHistory_PromoEvent1_idx` (`promoEvent_id` ASC),
  CONSTRAINT `fk_PromoHistory_Promotion1`
    FOREIGN KEY (`promotion_id`)
    REFERENCES `gigskyReferralEngine`.`Promotion` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PromoHistory_PromoEvent1`
    FOREIGN KEY (`promoEvent_id`)
    REFERENCES `gigskyReferralEngine`.`PromoEvent` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


DELIMITER $$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`Customer_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Customer_BEFORE_UPDATE` BEFORE UPDATE ON `Customer` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`IncentiveScheme_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`IncentiveScheme_BEFORE_UPDATE` BEFORE UPDATE ON `IncentiveScheme` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ReferralCode_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ReferralCode_BEFORE_UPDATE` BEFORE UPDATE ON `ReferralCode` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`Invitation_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Invitation_BEFORE_UPDATE` BEFORE UPDATE ON `Invitation` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`CreditScheme_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`CreditScheme_BEFORE_UPDATE` BEFORE UPDATE ON `CreditScheme` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ReferralEvent_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ReferralEvent_BEFORE_UPDATE` BEFORE UPDATE ON `ReferralEvent` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ConfigurationsKeyValue_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ConfigurationsKeyValue_BEFORE_UPDATE` BEFORE UPDATE ON `ConfigurationsKeyValue` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`HttpError_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`HttpError_BEFORE_UPDATE` BEFORE UPDATE ON `HttpError` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ErrorString_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ErrorString_BEFORE_UPDATE` BEFORE UPDATE ON `ErrorString` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`UrlDetail_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`UrlDetail_BEFORE_UPDATE` BEFORE UPDATE ON `UrlDetail` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`VersionInformation_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`VersionInformation_BEFORE_UPDATE` BEFORE UPDATE ON `VersionInformation` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ReferralAnalytics_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ReferralAnalytics_BEFORE_UPDATE` BEFORE UPDATE ON `ReferralAnalytics` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ValidCountry_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ValidCountry_BEFORE_UPDATE` BEFORE UPDATE ON `ValidCountry` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`Preference_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Preference_BEFORE_UPDATE` BEFORE UPDATE ON `Preference` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`CustomerCSNMapping_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`CustomerCSNMapping_BEFORE_UPDATE` BEFORE UPDATE ON `CustomerCSNMapping` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`CountryToCurrencyMap_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`CountryToCurrencyMap_BEFORE_UPDATE` BEFORE UPDATE ON `CountryToCurrencyMap` FOR EACH ROW
  begin
    set new.updateTime = now();
  END$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`MockCustomer_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`MockCustomer_BEFORE_UPDATE` BEFORE UPDATE ON `MockCustomer` FOR EACH ROW
  BEGIN
    set new.updateTime = now();
  END$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`MockCredit_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`MockCredit_BEFORE_UPDATE` BEFORE UPDATE ON `MockCredit` FOR EACH ROW
  BEGIN
    set new.updateTime = now();
  END$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ReferralCodeHistory_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ReferralCodeHistory_BEFORE_UPDATE` BEFORE UPDATE ON `ReferralCodeHistory` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`Country_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Country_BEFORE_UPDATE` BEFORE UPDATE ON `Country` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ErrorStringLocale_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ErrorStringLocale_BEFORE_UPDATE` BEFORE UPDATE ON `ErrorStringLocale` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`SupportedCurrency_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`SupportedCurrency_BEFORE_UPDATE` BEFORE UPDATE ON `SupportedCurrency` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`MessageServerCluster_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`MessageServerCluster_BEFORE_UPDATE` BEFORE UPDATE ON `MessageServerCluster` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`MessageServerClusterNode_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`MessageServerClusterNode_BEFORE_UPDATE` BEFORE UPDATE ON `MessageServerClusterNode` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`MessageServerQueue_BEFORE_UPDATE` $$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`MessageServerQueue_BEFORE_UPDATE` BEFORE UPDATE ON `MessageServerQueue` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Promotion_BEFORE_UPDATE` BEFORE UPDATE ON `Promotion` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`PromoIccid_BEFORE_UPDATE` BEFORE UPDATE ON `PromoIccid` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`PromoCountry_BEFORE_UPDATE` BEFORE UPDATE ON `PromoCountry` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`PromoCredit_BEFORE_UPDATE` BEFORE UPDATE ON `PromoCredit` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`PromoEvent_BEFORE_UPDATE` BEFORE UPDATE ON `PromoEvent` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$

USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`PromoHistory_BEFORE_UPDATE` BEFORE UPDATE ON `PromoHistory` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$


DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
