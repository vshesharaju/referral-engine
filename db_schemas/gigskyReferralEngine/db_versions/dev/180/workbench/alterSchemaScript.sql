-- MySQL Workbench Synchronization

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `gigskyReferralEngine`.`Promotion` 
ADD COLUMN `currentRedeemCount` INT(11) NULL DEFAULT 0 AFTER `activePromo`,
ADD COLUMN `maxRedeemCount` INT(11) NULL DEFAULT 0 AFTER `currentRedeemCount`;

ALTER TABLE `gigskyReferralEngine`.`PromoCredit` 
ADD COLUMN `minimumCartAmount` FLOAT(11) NULL DEFAULT 0 AFTER `updateTime`;

ALTER TABLE `gigskyReferralEngine`.`PromoHistory` 
ADD COLUMN `creditId` INT(11) NULL DEFAULT 0 AFTER `currency`;

CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`MockSim` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `MockCustomer_id` INT(11) NOT NULL,
  `simId` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_MockSim_MockCustomer1_idx` (`MockCustomer_id` ASC),
  CONSTRAINT `fk_MockSim_MockCustomer1`
    FOREIGN KEY (`MockCustomer_id`)
    REFERENCES `gigskyReferralEngine`.`MockCustomer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
