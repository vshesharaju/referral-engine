-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema gigskyReferralEngine
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `gigskyReferralEngine` ;

-- -----------------------------------------------------
-- Schema gigskyReferralEngine
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `gigskyReferralEngine` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
SHOW WARNINGS;
USE `gigskyReferralEngine` ;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`Tenant`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`Tenant` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`Tenant` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(250) NOT NULL,
  `description` LONGTEXT NOT NULL,
  `createTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`Customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`Customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`Customer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(255) NULL,
  `lastName` VARCHAR(255) NULL,
  `advocateId` INT NULL,
  `language` VARCHAR(45) NULL,
  `emailId` VARCHAR(45) NULL,
  `gsCustomerId` BIGINT(20) NULL,
  `tenant_id` INT(11) NOT NULL,
  `accountCreationTime` TIMESTAMP NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `status` VARCHAR(45) NULL,
  `country` VARCHAR(45) NULL,
  `referralStatus` VARCHAR(100) NULL,
  `lastReferralMailSent` TIMESTAMP NULL,
  `deleted` TINYINT(1) NULL DEFAULT false,
  PRIMARY KEY (`id`),
  INDEX `tenant_id_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_Customer_Tenant1`
    FOREIGN KEY (`tenant_id`)
    REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`IncentiveScheme`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`IncentiveScheme` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`IncentiveScheme` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `validityStartDate` DATE NULL,
  `validityEndDate` DATE NULL,
  `advocateCreditExpiryPeriod` INT NULL,
  `referralCreditExpiryPeriod` INT NULL,
  `schemeEnabled` TINYINT(1) NULL,
  `tenant_id` INT(11) NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `activeScheme` TINYINT(1) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ IncentiveScheme_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_ IncentiveScheme_Tenant1`
    FOREIGN KEY (`tenant_id`)
    REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ReferralCode`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ReferralCode` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ReferralCode` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `channel` VARCHAR(45) NULL,
  `referralCode` LONGTEXT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `customer_id` INT NOT NULL,
  `incentiveScheme_id` INT NOT NULL,
  `tenant_id` INT(11) NOT NULL,
  `isActive` TINYINT(1) NULL,
  `referredCount` INT NULL,
  `firstReferredTime` TIMESTAMP NULL,
  `isPromotionalCode` TINYINT(1) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ReferralCode_Customer1_idx` (`customer_id` ASC),
  INDEX `fk_ReferralCode_IncentiveScheme1_idx` (`incentiveScheme_id` ASC),
  INDEX `kk_ReferralCode_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_ReferralCode_Customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `gigskyReferralEngine`.`Customer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ReferralCode_IncentiveScheme1`
    FOREIGN KEY (`incentiveScheme_id`)
    REFERENCES `gigskyReferralEngine`.`IncentiveScheme` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ReferralCode_Tenant1`
    FOREIGN KEY (`tenant_id`)
    REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`Invitation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`Invitation` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`Invitation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `emailId` VARCHAR(45) NULL,
  `inviteTime` TIMESTAMP NULL,
  `accountCreatedOn` TIMESTAMP NULL,
  `status` VARCHAR(45) NULL,
  `targetCustomerId` INT NULL,
  `advocateCreditId` INT NULL,
  `advocateCreditTxnId` INT NULL,
  `advocateCreditStatus` VARCHAR(100) NULL,
  `advocateRetryCount` INT NULL,
  `referralCreditId` INT NULL,
  `referralCreditTxnId` INT NULL,
  `referralCreditStatus` VARCHAR(100) NULL,
  `referralRetryCount` INT NULL,
  `createTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `referralCode_id` INT NOT NULL,
  `resendCount` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_Invitation_ReferralCode1_idx` (`referralCode_id` ASC),
  CONSTRAINT `fk_Invitation_ReferralCode1`
    FOREIGN KEY (`referralCode_id`)
    REFERENCES `gigskyReferralEngine`.`ReferralCode` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`CreditScheme`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`CreditScheme` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`CreditScheme` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `currency` VARCHAR(45) NULL,
  `advocateAmount` FLOAT NULL,
  `newCustomerAmount` FLOAT NULL,
  `incentiveScheme_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_CreditScheme_IncentiveScheme1_idx` (`incentiveScheme_id` ASC),
  CONSTRAINT `fk_CreditScheme_IncentiveScheme1`
    FOREIGN KEY (`incentiveScheme_id`)
    REFERENCES `gigskyReferralEngine`.`IncentiveScheme` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ReferralEvent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ReferralEvent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ReferralEvent` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `eventType` VARCHAR(45) NULL,
  `eventData` LONGTEXT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `customer_id` INT NOT NULL,
  `tenant_id` INT(11) NOT NULL,
  `status` VARCHAR(45) NULL,
  `statusMessage` VARCHAR(100) NULL,
  `retryCount` INT NULL DEFAULT 0,
  `eventMetaData` LONGTEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ReferralEvent_Customer1_idx` (`customer_id` ASC),
  INDEX `fk_ReferralEvent_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_ReferralEvent_Customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `gigskyReferralEngine`.`Customer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ReferralEvent_Tenant1`
    FOREIGN KEY (`tenant_id`)
    REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ConfigurationsKeyValue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ConfigurationsKeyValue` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ConfigurationsKeyValue` (
  `ckey` VARCHAR(255) NOT NULL,
  `value` TEXT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`ckey`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`HttpError`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`HttpError` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`HttpError` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `errorCode` INT NULL,
  `errorString` VARCHAR(500) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ErrorString`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ErrorString` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ErrorString` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `errorId` INT NULL,
  `errorString` TEXT NULL,
  `httpError_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ErrorString_HttpError1_idx` (`httpError_id` ASC),
  CONSTRAINT `fk_ErrorString_HttpError1`
    FOREIGN KEY (`httpError_id`)
    REFERENCES `gigskyReferralEngine`.`HttpError` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`UrlDetail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`UrlDetail` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`UrlDetail` (
  `id` INT NOT NULL,
  `apiBaseUrl` VARCHAR(45) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`VersionInformation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`VersionInformation` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`VersionInformation` (
  `major` INT NOT NULL,
  `minor` INT NULL,
  `build` INT NULL,
  `buildType` ENUM('dev','test','live','stage') NOT NULL,
  `id` INT NOT NULL AUTO_INCREMENT,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ReferralAnalytics`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ReferralAnalytics` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ReferralAnalytics` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `userAgent` VARCHAR(255) NULL,
  `platform` VARCHAR(45) NULL,
  `inviteOpenTime` TIMESTAMP NULL,
  `language` VARCHAR(45) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ValidCountry`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ValidCountry` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ValidCountry` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `country` VARCHAR(45) NULL,
  `IncentiveScheme_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ValidCountry_IncentiveScheme1_idx` (`IncentiveScheme_id` ASC),
  CONSTRAINT `fk_ValidCountry_IncentiveScheme1`
    FOREIGN KEY (`IncentiveScheme_id`)
    REFERENCES `gigskyReferralEngine`.`IncentiveScheme` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`Preference`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`Preference` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`Preference` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `customer_id` INT NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `pkey` VARCHAR(255) NULL,
  `value` TEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Preferences_Customer1_idx` (`customer_id` ASC),
  CONSTRAINT `fk_Preferences_Customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `gigskyReferralEngine`.`Customer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`CustomerCSNMapping`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`CustomerCSNMapping` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`CustomerCSNMapping` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `csn` VARCHAR(100) NULL,
  `gsCustomerId` BIGINT(20) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `tenant_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ CustomerCSNMapping_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_ CustomerCSNMapping_Tenant1`
    FOREIGN KEY (`tenant_id`)
    REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`CountryToCurrencyMap`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`CountryToCurrencyMap` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`CountryToCurrencyMap` (
  `countryCode` VARCHAR(45) NOT NULL,
  `currencyCode` VARCHAR(45) NULL,
  `createTime` TIMESTAMP NULL,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`countryCode`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`MockCustomer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`MockCustomer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`MockCustomer` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstName` VARCHAR(45) NULL,
  `lastName` VARCHAR(45) NULL,
  `token` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `country` VARCHAR(45) NULL,
  `tenant_id` INT(11) NOT NULL,
  `preferredCurrency` VARCHAR(45) NULL,
  `permission` VARCHAR(45) NULL,
  `status` VARCHAR(45) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `createdOn` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ MockCustomer_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_ MockCustomer_Tenant1`
    FOREIGN KEY (`tenant_id`)
    REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`MockCredit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`MockCredit` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`MockCredit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `creditType` VARCHAR(45) NULL,
  `mockcustomer_id` INT NULL,
  `balance` FLOAT NULL,
  `currency` VARCHAR(45) NULL,
  `expiryTime` TIMESTAMP NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `_idx` (`mockcustomer_id` ASC),
  CONSTRAINT `fk_MockCustomer`
    FOREIGN KEY (`mockcustomer_id`)
    REFERENCES `gigskyReferralEngine`.`MockCustomer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ReferralCodeHistory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ReferralCodeHistory` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ReferralCodeHistory` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `referralCode_id` INT NOT NULL,
  `incentiveScheme_id` INT NULL,
  `startDate` TIMESTAMP NULL,
  `endDate` TIMESTAMP NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ReferralCodeHistory_ReferralCode1_idx` (`referralCode_id` ASC),
  CONSTRAINT `fk_ReferralCodeHistory_ReferralCode1`
    FOREIGN KEY (`referralCode_id`)
    REFERENCES `gigskyReferralEngine`.`ReferralCode` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`Country`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`Country` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`Country` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `countryCode` VARCHAR(45) NULL,
  `countryName` LONGTEXT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`ErrorStringLocale`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`ErrorStringLocale` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`ErrorStringLocale` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `language` VARCHAR(45) NULL,
  `errorString_id` INT NOT NULL,
  `errorString` TEXT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`, `errorString_id`),
  INDEX `fk_ErrorStringLocale_ErrorString1_idx` (`errorString_id` ASC),
  CONSTRAINT `fk_ErrorStringLocale_ErrorString1`
    FOREIGN KEY (`errorString_id`)
    REFERENCES `gigskyReferralEngine`.`ErrorString` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`SupportedCurrency`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`SupportedCurrency` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`SupportedCurrency` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `currencyCode` VARCHAR(45) NULL,
  `usdMultiplier` FLOAT NULL,
  `tenant_id` INT(11) NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_SupportedCurrency_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_SupportedCurrency`
    FOREIGN KEY (`tenant_id`)
    REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`MessageServerCluster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`MessageServerCluster` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`MessageServerCluster` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `clusterName` VARCHAR(255) NULL,
  `userName` VARCHAR(200) NULL,
  `password` VARCHAR(250) NULL,
  `virtualHost` VARCHAR(200) NULL,
  `encVersion` VARCHAR(10) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`MessageServerClusterNode`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`MessageServerClusterNode` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`MessageServerClusterNode` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `host` VARCHAR(255) NULL,
  `port` INT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `MessageServerCluster_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_MessageServerClusterNode_MessageServerCluster1_idx` (`MessageServerCluster_id` ASC),
  CONSTRAINT `fk_MessageServerClusterNode_MessageServerCluster1`
    FOREIGN KEY (`MessageServerCluster_id`)
    REFERENCES `gigskyReferralEngine`.`MessageServerCluster` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`MessageServerQueue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`MessageServerQueue` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`MessageServerQueue` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `queueName` VARCHAR(250) NULL,
  `exchangeName` VARCHAR(250) NULL,
  `routingKey` VARCHAR(255) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `MessageServerCluster_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_MessageServerQueue_MessageServerCluster1_idx` (`MessageServerCluster_id` ASC),
  CONSTRAINT `fk_MessageServerQueue_MessageServerCluster1`
    FOREIGN KEY (`MessageServerCluster_id`)
    REFERENCES `gigskyReferralEngine`.`MessageServerCluster` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`Promotion`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`Promotion` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`Promotion` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NULL,
  `promoCode` VARCHAR(45) NULL,
  `tenant_id` INT(11) NOT NULL,
  `startDate` DATE NULL,
  `endDate` DATE NULL,
  `creditExpiryPeriod` INT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `promoEnabled` TINYINT(1) NULL,
  `activePromo` TINYINT(1) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_Promotion_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_Promotion_Tenant1`
    FOREIGN KEY (`tenant_id`)
    REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`PromoIccid`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`PromoIccid` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`PromoIccid` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `iccidStart` VARCHAR(100) NULL,
  `iccidEnd` VARCHAR(100) NULL,
  `promotion_id` INT NOT NULL,
  `isEmailNotifRequired` TINYINT(1) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ICCID_Promotion2_idx` (`promotion_id` ASC),
  CONSTRAINT `fk_ICCID_Promotion2`
    FOREIGN KEY (`promotion_id`)
    REFERENCES `gigskyReferralEngine`.`Promotion` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`PromoCountry`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`PromoCountry` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`PromoCountry` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `promotion_id` INT NOT NULL,
  `country` VARCHAR(45) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_PromoCountry_Promotion1_idx` (`promotion_id` ASC),
  CONSTRAINT `fk_PromoCountry_Promotion1`
    FOREIGN KEY (`promotion_id`)
    REFERENCES `gigskyReferralEngine`.`Promotion` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`PromoCredit`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`PromoCredit` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`PromoCredit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `promotion_id` INT NOT NULL,
  `creditAmount` FLOAT NULL,
  `currency` VARCHAR(45) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_PromoCredit_Promotion1_idx` (`promotion_id` ASC),
  CONSTRAINT `fk_PromoCredit_Promotion1`
    FOREIGN KEY (`promotion_id`)
    REFERENCES `gigskyReferralEngine`.`Promotion` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`PromoEvent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`PromoEvent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`PromoEvent` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `eventType` VARCHAR(45) NULL,
  `eventData` LONGTEXT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  `customer_id` INT NOT NULL,
  `tenant_id` INT(11) NOT NULL,
  `status` VARCHAR(45) NULL,
  `statusMessage` VARCHAR(100) NULL,
  `retryCount` INT NULL DEFAULT 0,
  `eventMetaData` LONGTEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ReferralEvent_Customer1_idx` (`customer_id` ASC),
  INDEX `fk_PromoEvent_Tenant1_idx` (`tenant_id` ASC),
  CONSTRAINT `fk_ReferralEvent_Customer10`
    FOREIGN KEY (`customer_id`)
    REFERENCES `gigskyReferralEngine`.`Customer` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PromoEvent_Tenant1`
    FOREIGN KEY (`tenant_id`)
    REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`PromoHistory`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`PromoHistory` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`PromoHistory` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `gsCustomerId` BIGINT(20) NULL,
  `promotion_id` INT NOT NULL,
  `promoEvent_id` INT NULL,
  `iccid` VARCHAR(100) NULL,
  `creditAmount` FLOAT NULL,
  `currency` VARCHAR(45) NULL,
  `creditTxnId` INT NULL,
  `emailNotifStatus` VARCHAR(45) NULL,
  `retryCount` INT NULL,
  `status` VARCHAR(45) NULL,
  `statusMessage` VARCHAR(100) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_PromoHistory_Promotion1_idx` (`promotion_id` ASC),
  INDEX `fk_PromoHistory_PromoEvent1_idx` (`promoEvent_id` ASC),
  CONSTRAINT `fk_PromoHistory_Promotion1`
    FOREIGN KEY (`promotion_id`)
    REFERENCES `gigskyReferralEngine`.`Promotion` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_PromoHistory_PromoEvent1`
    FOREIGN KEY (`promoEvent_id`)
    REFERENCES `gigskyReferralEngine`.`PromoEvent` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`HealthCheckEvent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`HealthCheckEvent` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`HealthCheckEvent` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `eventType` VARCHAR(45) NULL,
  `eventMetaData` LONGTEXT NULL,
  `isQueueUp` TINYINT(1) NULL,
  `eventReceivedTime` TIMESTAMP NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gigskyReferralEngine`.`TenantConfigurationsKeyValue`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gigskyReferralEngine`.`TenantConfigurationsKeyValue` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gigskyReferralEngine`.`TenantConfigurationsKeyValue` (
  `ckey` VARCHAR(250) NOT NULL,
  `tenant_id` INT(11) NOT NULL,
  `value` TEXT NOT NULL,
  `createTime` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL,
  INDEX `tenant_id_idx` (`tenant_id` ASC),
  PRIMARY KEY (`ckey`, `tenant_id`),
  CONSTRAINT `fk_ TenantConfigurationsKeyValue_Tenant1`
    FOREIGN KEY (`tenant_id`)
    REFERENCES `gigskyReferralEngine`.`Tenant` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;
USE `gigskyReferralEngine`;

DELIMITER $$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`Tenant_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Tenant_BEFORE_UPDATE` BEFORE UPDATE ON `Tenant` FOR EACH ROW
BEGIN
set new.updateTime = now();
END$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`Customer_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Customer_BEFORE_UPDATE` BEFORE UPDATE ON `Customer` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`IncentiveScheme_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`IncentiveScheme_BEFORE_UPDATE` BEFORE UPDATE ON `IncentiveScheme` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ReferralCode_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ReferralCode_BEFORE_UPDATE` BEFORE UPDATE ON `ReferralCode` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`Invitation_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Invitation_BEFORE_UPDATE` BEFORE UPDATE ON `Invitation` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`CreditScheme_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`CreditScheme_BEFORE_UPDATE` BEFORE UPDATE ON `CreditScheme` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ReferralEvent_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ReferralEvent_BEFORE_UPDATE` BEFORE UPDATE ON `ReferralEvent` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ConfigurationsKeyValue_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ConfigurationsKeyValue_BEFORE_UPDATE` BEFORE UPDATE ON `ConfigurationsKeyValue` FOR EACH ROW
begin
    set new.updateTime = now();
end
    $$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`HttpError_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`HttpError_BEFORE_UPDATE` BEFORE UPDATE ON `HttpError` FOR EACH ROW
begin
    set new.updateTime = now();
end
    $$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ErrorString_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ErrorString_BEFORE_UPDATE` BEFORE UPDATE ON `ErrorString` FOR EACH ROW
begin
    set new.updateTime = now();
end
    $$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`UrlDetail_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`UrlDetail_BEFORE_UPDATE` BEFORE UPDATE ON `UrlDetail` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`VersionInformation_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`VersionInformation_BEFORE_UPDATE` BEFORE UPDATE ON `VersionInformation` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ReferralAnalytics_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ReferralAnalytics_BEFORE_UPDATE` BEFORE UPDATE ON `ReferralAnalytics` FOR EACH ROW
 begin
    set new.updateTime = now();
end   $$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ValidCountry_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ValidCountry_BEFORE_UPDATE` BEFORE UPDATE ON `ValidCountry` FOR EACH ROW
       begin
    set new.updateTime = now();
end $$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`Preference_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Preference_BEFORE_UPDATE` BEFORE UPDATE ON `Preference` FOR EACH ROW
        begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`CustomerCSNMapping_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`CustomerCSNMapping_BEFORE_UPDATE` BEFORE UPDATE ON `CustomerCSNMapping` FOR EACH ROW
        begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`CountryToCurrencyMap_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`CountryToCurrencyMap_BEFORE_UPDATE` BEFORE UPDATE ON `CountryToCurrencyMap` FOR EACH ROW
begin
    set new.updateTime = now();
END
$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`MockCustomer_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`MockCustomer_BEFORE_UPDATE` BEFORE UPDATE ON `MockCustomer` FOR EACH ROW
BEGIN
	set new.updateTime = now();
END
$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`MockCredit_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`MockCredit_BEFORE_UPDATE` BEFORE UPDATE ON `MockCredit` FOR EACH ROW
BEGIN
	set new.updateTime = now();
END
$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ReferralCodeHistory_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ReferralCodeHistory_BEFORE_UPDATE` BEFORE UPDATE ON `ReferralCodeHistory` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`Country_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Country_BEFORE_UPDATE` BEFORE UPDATE ON `Country` FOR EACH ROW
        begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`ErrorStringLocale_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`ErrorStringLocale_BEFORE_UPDATE` BEFORE UPDATE ON `ErrorStringLocale` FOR EACH ROW
begin
    set new.updateTime = now();
end
    $$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`SupportedCurrency_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`SupportedCurrency_BEFORE_UPDATE` BEFORE UPDATE ON `SupportedCurrency` FOR EACH ROW
		begin
	set new.updateTime = now();
end
$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`MessageServerCluster_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`MessageServerCluster_BEFORE_UPDATE` BEFORE UPDATE ON `MessageServerCluster` FOR EACH ROW
begin
	set new.updateTime = now();
end	$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`MessageServerClusterNode_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`MessageServerClusterNode_BEFORE_UPDATE` BEFORE UPDATE ON `MessageServerClusterNode` FOR EACH ROW
begin
	set new.updateTime = now();
end
$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`MessageServerQueue_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`MessageServerQueue_BEFORE_UPDATE` BEFORE UPDATE ON `MessageServerQueue` FOR EACH ROW
begin
	set new.updateTime = now();
end
$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`Promotion_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`Promotion_BEFORE_UPDATE` BEFORE UPDATE ON `Promotion` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`PromoIccid_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`PromoIccid_BEFORE_UPDATE` BEFORE UPDATE ON `PromoIccid` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`PromoCountry_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`PromoCountry_BEFORE_UPDATE` BEFORE UPDATE ON `PromoCountry` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`PromoCredit_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`PromoCredit_BEFORE_UPDATE` BEFORE UPDATE ON `PromoCredit` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`PromoEvent_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`PromoEvent_BEFORE_UPDATE` BEFORE UPDATE ON `PromoEvent` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`PromoHistory_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`PromoHistory_BEFORE_UPDATE` BEFORE UPDATE ON `PromoHistory` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`HealthCheckEvent_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`HealthCheckEvent_BEFORE_UPDATE` BEFORE UPDATE ON `HealthCheckEvent` FOR EACH ROW
    begin
    set new.updateTime = now();
end$$

SHOW WARNINGS$$

USE `gigskyReferralEngine`$$
DROP TRIGGER IF EXISTS `gigskyReferralEngine`.`TenantConfigurationsKeyValue_BEFORE_UPDATE` $$
SHOW WARNINGS$$
USE `gigskyReferralEngine`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyReferralEngine`.`TenantConfigurationsKeyValue_BEFORE_UPDATE` BEFORE UPDATE ON `TenantConfigurationsKeyValue` FOR EACH ROW
BEGIN
set new.updateTime = now();
END$$

SHOW WARNINGS$$

DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
