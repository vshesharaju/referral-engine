START TRANSACTION;

UPDATE `gigskyReferralEngine`.`SupportedCurrency` SET `tenant_id`='1';
UPDATE `gigskyReferralEngine`.`CustomerCSNMapping` SET `tenant_id`='1';
UPDATE `gigskyReferralEngine`.`IncentiveScheme` SET `tenant_id`='1';
UPDATE `gigskyReferralEngine`.`ReferralCode` SET `tenant_id`='1';
UPDATE `gigskyReferralEngine`.`Customer` SET `tenant_id`='1';
UPDATE `gigskyReferralEngine`.`ReferralEvent` SET `tenant_id`='1';
UPDATE `gigskyReferralEngine`.`PromoEvent` SET `tenant_id`='1';
UPDATE `gigskyReferralEngine`.`Promotion` SET `tenant_id`='1';

DELETE FROM `gigskyReferralEngine`.`ConfigurationsKeyValue` WHERE `ckey`='ADMIN_EMAIL';
DELETE FROM `gigskyReferralEngine`.`ConfigurationsKeyValue` WHERE `ckey`='ADMIN_PASSWORD';
DELETE FROM `gigskyReferralEngine`.`ConfigurationsKeyValue` WHERE `ckey`='GS_ADMIN_EMAIL_ID';
DELETE FROM `gigskyReferralEngine`.`ConfigurationsKeyValue` WHERE `ckey`='PROMOTION_ENABLE';
DELETE FROM `gigskyReferralEngine`.`ConfigurationsKeyValue` WHERE `ckey`='REFERRAL_ENABLE';

COMMIT;